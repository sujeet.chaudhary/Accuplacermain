package com.pearson.itautomation.accuplacer.users.userprofiles;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.pearson.itautomation.accuplacer.core.action.SaveAndAssert;
import com.pearson.itautomation.bca.Browser;
import com.pearson.itautomation.bca.BrowserAction;
import com.pearson.itautomation.bca.BrowserElement;
import com.pearson.itautomation.bca.interaction.ClearTextElementAction;
import com.pearson.itautomation.bca.interaction.SendTextToElementAction;

/**
 * This Class will Edit the Description information of the student on the Edit
 * Institution screen details.<br>
 * <b>checkPrestate</b> - N/A <br>
 * <b>checkPostate</b> - N/A <br>
 * <b>perform</b> - This method will return true if changes have been
 * successfully saved
 * 
 * @author Deepak Radhakrishnan
 * 
 */
public class EditSiteLevelUser extends BrowserAction {

    private static final Logger LOGGER = LoggerFactory
            .getLogger(EditSiteLevelUser.class);

    private static final String OR_AREA = "OR_User";
    private static final String OR_DESCRIPTION = "UserDescription";

    private final BrowserElement txtDescription;

    private static final String strDescription = "EditDesc";

    /**
     * This Constructor will initialize the field variables
     * 
     * @param browser
     * 
     * @author Deepak Radhakrishnan
     */
    public EditSiteLevelUser(final Browser browser) {
        super(browser);

        this.txtDescription = browser
                .getBrowserElement(OR_AREA, OR_DESCRIPTION);

    }

    @Override
    protected boolean perform() {
        boolean result = true;
        if (!new ClearTextElementAction(getBrowser(), this.txtDescription)
                .performWithStates()) {
            LOGGER.error("Not able to clear text on the Description field");
            result = false;
        }
        if (!new SendTextToElementAction(getBrowser(), this.txtDescription,
                strDescription).performWithStates()) {
            LOGGER.error("Not able to send text to Description field");
            result = false;
        }

        if (!new SaveAndAssert(this.getBrowser(),
                "User Information updated successfully").performWithStates()) {
            LOGGER.error("Saving user changes failed");
            result = false;
        }

        return result;
    }
}
