package com.pearson.itautomation.accuplacer.users.userprofiles;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.pearson.itautomation.accuplacer.core.Buttons;
import com.pearson.itautomation.accuplacer.core.action.NavigateToMenu;
import com.pearson.itautomation.bca.Browser;
import com.pearson.itautomation.bca.BrowserAction;
import com.pearson.itautomation.bca.BrowserElement;
import com.pearson.itautomation.bca.interaction.LeftClickElementAction;
import com.pearson.itautomation.bca.validation.ElementExistsValidation;

/**
 * This Class will navigate the user to The add user page for any type of user <br>
 * <b>checkPrestate</b> - N/A <br>
 * <b>checkPostate</b> - This method will return true if the validation element
 * on the subsequent page is found <br>
 * <b>perform</b> - This method will return true if the user is navigated to the
 * User Profile Page
 * 
 * @author Deepak Radhakrishnan
 */
public class NavigateToAddUserPage extends BrowserAction {
    private static final Logger LOGGER = LoggerFactory
            .getLogger(NavigateToAddUserPage.class);
    private static final String TAB_MENU = "Users";
    private static final String SUB_MENU = "Manage Profiles";
    private static BrowserElement btnAddUser = Buttons
            .getButtonType5("Add User");
    private final BrowserElement elementValidatePageLoad;
    private final BrowserElement elementValidate;

    /**
     * This constructor will initialize the browser elements and the constant
     * variables
     * 
     * @param browser
     * @param strTabName
     *            Left Panel Link Text value
     * @param strSubMenuName
     *            Link Text Value
     * @param btnAddUsers
     *            Checkpoint Element
     * 
     * @author Deepak Radhakrishnan
     */
    public NavigateToAddUserPage(Browser browser) {
        super(browser);
        this.elementValidate = browser.getBrowserElement("OR_User",
                "addUserVariable");
        this.elementValidatePageLoad = browser.getBrowserElement("OR_User",
                "drpdownValueToValidatePageLoad");
    }

    @Override
    protected boolean perform() {
        boolean result = true;
        if (getBrowser().getPageSource()
                .contains("User Profile Search Results")) {
            if (!new LeftClickElementAction(this.getBrowser(), btnAddUser)
                    .performWithStates()) {
                LOGGER.error("Could not click on add users page");
                result = false;
            }
            return result;
        }

        if (!new NavigateToMenu("User Profiles", this.getBrowser(), TAB_MENU,
                SUB_MENU, btnAddUser).performWithStates()) {
            LOGGER.error("Could NOT navigate to user profiles");
            result = false;
        }
        if (!new ElementExistsValidation(getBrowser(), elementValidatePageLoad)
                .performWithStates()) {
            LOGGER.error("Coud No Find the Dropdown value for Proctor Reporter");
        }
        try {
            Thread.sleep(3000);
        } catch (InterruptedException e) {
            LOGGER.error("Error while waiting for modal window transition", e);
        }
        if (!new LeftClickElementAction(this.getBrowser(), btnAddUser)
                .performWithStates()) {
            LOGGER.error("Could not click on add users page");
            result = false;
        }
        try {
            Thread.sleep(500);
        } catch (InterruptedException e) {
            LOGGER.error("Error while waiting for modal window transition", e);
        }
        return result;
    }

    @Override
    protected boolean checkPostState() {
        boolean result = true;
        if (!new ElementExistsValidation(this.getBrowser(), elementValidate)
                .performWithStates()) {
            LOGGER.error("Could NOT find the Checkpoint element");
            result = false;
        }
        return result;

    }

}
