package com.pearson.itautomation.accuplacer.users.userprofiles;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.pearson.itautomation.bca.Browser;
import com.pearson.itautomation.bca.BrowserAction;
import com.pearson.itautomation.bca.BrowserElement;
import com.pearson.itautomation.bca.SelectorType;
import com.pearson.itautomation.bca.interaction.DropdownSelectionAction;

/**
 * This Class will Select the Group Name Form Group Name Dropdown<br>
 * <b>checkPrestate</b> - N/A <br>
 * <b>checkPostate</b> - N/A <br>
 * <b>perform</b> - This method will return true if Dropdown data selected
 * successfully.
 * 
 * @author Sujeet Chaudhary
 */
public class SelectGroupName extends BrowserAction {

    private static final Logger LOGGER = LoggerFactory
            .getLogger(SelectGroupName.class);

    private static final String OR_AREA = "OR_User";
    private static final String OR_GROUP_NAME = "UserGroupName";

    private final BrowserElement drpGroupName;

    private final String strGroupName;

    /**
     * This constructor will initialize the mandatory fields and the
     * BrowserElements from this class
     * 
     * @param browser
     * @param strGroupName
     *            The Value of 'Group Name' to be selected
     */
    public SelectGroupName(final Browser browser, String strGroupName) {
        super(browser);

        this.strGroupName = strGroupName;
        this.drpGroupName = browser.getBrowserElement(OR_AREA, OR_GROUP_NAME);
    }

    @Override
    protected boolean perform() {
        boolean result = true;

        if (!new DropdownSelectionAction(getBrowser(), this.drpGroupName,
                SelectorType.VISIBLE_TEXT, this.strGroupName)
                .performWithStates()) {
            LOGGER.error("Not able to select Group Name");
            result = false;
        }

        return result;
    }

}
