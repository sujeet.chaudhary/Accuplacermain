package com.pearson.itautomation.accuplacer.users.userprofiles;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.pearson.itautomation.bca.Browser;
import com.pearson.itautomation.bca.BrowserAction;
import com.pearson.itautomation.bca.BrowserElement;
import com.pearson.itautomation.bca.interaction.LeftClickElementAction;
import com.pearson.itautomation.bca.validation.ElementExistsValidation;

/**
 * This Class will Delete the User<br>
 * <b>checkPrestate</b> - N/A <br>
 * <b>checkPostate</b> - N/A <br>
 * <b>perform</b> - This method will return true if User Deleted Successfully.
 * 
 * @author Sujeet Chaudhary
 * 
 */
public class DeleteUser extends BrowserAction {

    private static final Logger LOGGER = LoggerFactory
            .getLogger(DeleteUser.class);

    private static final String OR_AREA = "OR_User";
    private static final String OR_DELETE_USER = "DeleteUser";
    private static final String OR_DELETE_USER_YES = "DeleteUserYes";
    private static final String OR_DELETE_USER_MESSAGE_VALIDATION = "DeleteUserMessageValidation";

    private final BrowserElement btnDeleteUser;
    private final BrowserElement btnYes;
    private final BrowserElement txtDeleteUserMessage;

    /**
     * This constructor will initialize the mandatory fields and the
     * BrowserElements from this class
     * 
     * @param browser
     * 
     */
    public DeleteUser(final Browser browser) {
        super(browser);

        this.txtDeleteUserMessage = browser.getBrowserElement(OR_AREA,
                OR_DELETE_USER_MESSAGE_VALIDATION);
        this.btnDeleteUser = browser.getBrowserElement(OR_AREA, OR_DELETE_USER);
        this.btnYes = browser.getBrowserElement(OR_AREA, OR_DELETE_USER_YES);

    }

    @Override
    protected boolean perform() {
        boolean result = true;

        if (!new LeftClickElementAction(getBrowser(), this.btnDeleteUser)
                .performWithStates()) {
            LOGGER.error("Not able to click on Delete button");
            result = false;
        }

        if (!new LeftClickElementAction(getBrowser(), this.btnYes)
                .performWithStates()) {
            LOGGER.error("Not able to click on yes button");
            result = false;
        }

        return result;
    }

    @Override
    protected boolean checkPostState() {

        boolean result = true;

        if (!new ElementExistsValidation(getBrowser(),
                this.txtDeleteUserMessage).performWithStates()) {
            LOGGER.error("Not able to validate message");
            result = false;
        }

        return result;
    }

}
