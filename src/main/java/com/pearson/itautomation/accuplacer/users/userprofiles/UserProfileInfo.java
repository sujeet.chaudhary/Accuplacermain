package com.pearson.itautomation.accuplacer.users.userprofiles;

/**
 * This Class will Provide the data to AddUserProfile. <br>
 * 
 * 
 * @author Sujeet Chaudhary
 */
public class UserProfileInfo {

    private static final String FIRST_NAME = "FirstNameUserProfile";
    private static final String LAST_NAME = "LastName";
    private static final String ADDRESS1 = "Address1";
    private static final String COUNTRY = "Algeria";
    private static final String IF_OTHER_SPECIFY = "Acdghfd";
    private static final String CITY = "City";
    private static final String ZIP_CODE = "12634438274";
    private static final String EMAIL_ADDRESS = "abc@mail";
    private static final String HOME_NUMBER = "1234567893";

    private UserProfileInfo() {
        // private constructor for the utility class
    }

    public static String getStrfirstname() {
        return FIRST_NAME;
    }

    public static String getStrlastname() {
        return LAST_NAME;
    }

    public static String getStraddress1() {
        return ADDRESS1;
    }

    public static String getStrcountry() {
        return COUNTRY;
    }

    public static String getStrifotherspecify() {
        return IF_OTHER_SPECIFY;
    }

    public static String getStrcity() {
        return CITY;
    }

    public static String getStrzippostalcode() {
        return ZIP_CODE;
    }

    public static String getStremailaddress() {
        return EMAIL_ADDRESS;
    }

    public static String getStrhomephoneno() {
        return HOME_NUMBER;
    }

}
