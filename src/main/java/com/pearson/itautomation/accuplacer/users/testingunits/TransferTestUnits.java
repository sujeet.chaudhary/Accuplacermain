package com.pearson.itautomation.accuplacer.users.testingunits;

import java.util.ArrayList;
import java.util.List;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.Select;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.pearson.itautomation.accuplacer.core.AccuplacerBrowser;
import com.pearson.itautomation.accuplacer.core.action.LeftClickOnModalPopupButton;
import com.pearson.itautomation.accuplacer.interaction.ScrollToPageTop;
import com.pearson.itautomation.bca.Browser;
import com.pearson.itautomation.bca.BrowserAction;
import com.pearson.itautomation.bca.BrowserElement;
import com.pearson.itautomation.bca.SelectorType;
import com.pearson.itautomation.bca.interaction.DropdownSelectionAction;
import com.pearson.itautomation.bca.interaction.LeftClickElementAction;
import com.pearson.itautomation.bca.interaction.SendTextToElementAction;
import com.pearson.itautomation.bca.validation.ElementExistsValidation;

/**
 * The Transfer Test Units class will be resetting the Transfer Units Form and
 * then fills all the mandatory details in the Form.<br>
 * <b>checkPrestate</b> - Checks to see if enough units are there to transfer.
 * <b>checkPostate</b> - Checks to see the assert unit counts match only if the
 * PreState was run <br>
 * <b>perform</b> - This method will return true if all the mandatory fields
 * have been filled in and transfer button clicked successfully to transfer test
 * units.
 * 
 * @author Solomon Lingala
 * 
 */

public class TransferTestUnits extends BrowserAction {

    private static final Logger LOGGER = LoggerFactory
            .getLogger(TransferTestUnits.class);
    private static final String OR_AREA = "OR_User";

    private static final String OR_NOOFUNITSTOTRANSFER = "NoOfUnitsToTransfer";
    private static final String OR_TRANSFERUNITSFROM = "TransferUnitsFrom";
    private static final String OR_TRANSFERUNITSTO = "TransferUnitsTo";
    private static final String OR_TRANSFER_BUTTON = "TransferButton";
    private static final String OR_RESET_BUTTON = "ResetButton";
    private static final String OR_TRANSFERTESTUNITSDROPDOWNVALOADED = "drpdownValueToValidateTransferUnits";
    private static final String OR_TRANSFERTESTUNITS = "TTUYesButton";
    private static final String OR_VALIDATE_TRANSFER = "TTUValidateTranser";
    private static final String REG_EX_DECIMAL = " \\(\\d*\\.*\\d*\\)";

    private String strTransferUnitsFrom;
    private String strTransferUnitsTo;
    private final String strNoOfUnits;

    private final BrowserElement txtNoOfUnitsToTransfer;
    private final BrowserElement drpTransferUnitsFrom;
    private final BrowserElement drpTransferUnitsTo;
    private final BrowserElement btnTransfer;
    private final BrowserElement btnReset;
    private final BrowserElement validateTranser;

    private final BrowserElement drpTransferUnitCheck;
    private final BrowserElement btnYes;

    private double expectedUnitsFrom;
    private double expectedUnitsTo;
    private boolean preStateRan = false;

    /**
     * This constructor will initialize the mandatory fields and the
     * BrowserElements from this class
     * 
     * @param browser
     * @param strTransferUnitsFrom
     *            Value to pick from the Transfer Units from drop down.
     * @param strTransferUnitsTo
     *            Value to pick from the Transfer Units to drop down.
     * @param strNoOfUnitsToTransfer
     *            Value to pick from the Transfer Units to drop down.
     */

    public TransferTestUnits(Browser browser, String strTransferUnitsFrom,
            String strTransferUnitsTo, String strNoOfUnitsToTransfer) {
        super(browser);
        this.strTransferUnitsFrom = strTransferUnitsFrom + REG_EX_DECIMAL;
        this.strTransferUnitsTo = strTransferUnitsTo + REG_EX_DECIMAL;
        this.strNoOfUnits = strNoOfUnitsToTransfer;
        this.drpTransferUnitsFrom = browser.getBrowserElement(OR_AREA,
                OR_TRANSFERUNITSFROM);
        this.drpTransferUnitsTo = browser.getBrowserElement(OR_AREA,
                OR_TRANSFERUNITSTO);
        this.drpTransferUnitCheck = browser.getBrowserElement(OR_AREA,
                OR_TRANSFERTESTUNITSDROPDOWNVALOADED);
        this.txtNoOfUnitsToTransfer = browser.getBrowserElement(OR_AREA,
                OR_NOOFUNITSTOTRANSFER);
        this.btnTransfer = browser.getBrowserElement(OR_AREA,
                OR_TRANSFER_BUTTON);
        this.btnReset = browser.getBrowserElement(OR_AREA, OR_RESET_BUTTON);
        this.btnYes = browser.getBrowserElement(OR_AREA, OR_TRANSFERTESTUNITS);
        this.validateTranser = browser.getBrowserElement(OR_AREA,
                OR_VALIDATE_TRANSFER);
    }

    @Override
    public boolean checkPreState() {
        boolean result = true;

        WebElement checkElement = getBrowser().getElementWithWait(
                this.drpTransferUnitCheck);

        if (checkElement != null) {
            double transferUnit = Double.parseDouble(strNoOfUnits);
            drpTransferUnitCheck.setTimeout(10000);
            new ElementExistsValidation(getBrowser(), drpTransferUnitCheck)
                    .performWithStates();

            this.expectedUnitsFrom = getNumericFromString(getTextFromOption(
                    drpTransferUnitsFrom, strTransferUnitsFrom)) - transferUnit;
            this.expectedUnitsTo = getNumericFromString(getTextFromOption(
                    drpTransferUnitsTo, strTransferUnitsTo)) + transferUnit;
            if (expectedUnitsFrom < 0) {
                result = false;
                LOGGER.warn("There are not enough units to transfer");
            }
            preStateRan = true;
        } else {
            result = false;
            LOGGER.warn("Could not find the Transfer Units From drop down");
        }

        return result;
    }

    @Override
    protected boolean perform() {
        boolean result = true;

        if (result
                && !new LeftClickElementAction(this.getBrowser(), btnReset)
                        .performWithStates()) {
            LOGGER.error("Failed to click on Reset Button");
            result = false;
        }

        // Check to see if the Transfer Units From drop down value is loaded.
        WebElement checkElement = getBrowser().getElementWithWait(
                this.drpTransferUnitCheck);

        if (checkElement != null) {

            if (!new DropdownSelectionAction(this.getBrowser(),
                    this.drpTransferUnitsFrom, SelectorType.VISIBLE_TEXT_REGEX,
                    this.strTransferUnitsFrom).performWithStates()) {
                LOGGER.error("Failed to select Transfer Units From");
                result = false;
            }

            if (!new DropdownSelectionAction(this.getBrowser(),
                    this.drpTransferUnitsTo, SelectorType.VISIBLE_TEXT_REGEX,
                    this.strTransferUnitsTo).performWithStates()) {
                LOGGER.error("Failed to select Transfer Units To");
                result = false;
            }
        } else {
            result = false;
            LOGGER.error("The Transfer Units drop down values did not load within the timeout period.");
        }

        if (!new SendTextToElementAction(this.getBrowser(),
                this.txtNoOfUnitsToTransfer, this.strNoOfUnits)
                .performWithStates()) {
            LOGGER.error("Failed to type # Of Units To Transfer text");
            result = false;
        }

        if (!new ScrollToPageTop(getBrowser()).performWithStates()) {
            LOGGER.error("Not able to Scroll to Page Top");
            result = false;
        }

        if (result
                && !new LeftClickElementAction(this.getBrowser(), btnTransfer)
                        .performWithStates()) {
            LOGGER.error("Failed to click on Transfer Button");
            result = false;
        }

        if (result
                && !new LeftClickOnModalPopupButton(this.getBrowser(), btnYes)
                        .performWithStates()) {
            LOGGER.error("Failed to click on Yes Button");
            result = false;
        }

        if (!new ScrollToPageTop(getBrowser()).performWithStates()) {
            LOGGER.error("Not able to Scroll to Page Top");
            result = false;
        }

        return result;

    }

    @Override
    public boolean checkPostState() {
        boolean result = true;
        if (!new ElementExistsValidation(this.getBrowser(), validateTranser)
                .performWithStates()) {
            LOGGER.error("Failed to Validate Transfer units");
            result = false;
        }
        // To do: Search for the "Units have been Transferred" element to appear
        // before you do your validation.

        if (preStateRan) {
            ((AccuplacerBrowser) getBrowser()).performWaits();
            double actualUnitsFrom = getNumericFromString(getTextFromOption(
                    drpTransferUnitsFrom, strTransferUnitsFrom));
            if (expectedUnitsFrom != actualUnitsFrom) {
                LOGGER.error("The actual Transfer Units From ("
                        + actualUnitsFrom
                        + ") does not match expected transfer Units From ("
                        + expectedUnitsFrom + ")");
                result = false;
            }

            double actualUnitsTo = getNumericFromString(getTextFromOption(
                    drpTransferUnitsTo, strTransferUnitsTo));

            if (expectedUnitsTo != actualUnitsTo) {
                LOGGER.error("The actual Transfer Units To (" + actualUnitsTo
                        + ")does not match expected transfer Units To ("
                        + expectedUnitsTo + ")");
                result = false;
            }
        } else {
            LOGGER.info("Prestate was not run so Post State is skipped");
        }

        return result;
    }

    private final String getTextFromOption(final BrowserElement browserElement,
            final String regEx) {
        String result = "";
        List<WebElement> list = new ArrayList<>();

        while (list.size() <= 1) {

            list = new Select(this.getBrowser().getElementWithWait(
                    browserElement)).getOptions();
        }

        for (WebElement element : list) {
            String txt = element.getText();
            if (txt.matches(regEx)) {
                result = txt;
                break;
            }
        }
        return result;
    }

    private final double getNumericFromString(final String string) {
        String result = string.substring(string.indexOf("(") + 1,
                string.indexOf(")"));
        return Double.parseDouble(result);

    }
}
