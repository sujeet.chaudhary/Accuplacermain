package com.pearson.itautomation.accuplacer.users.userprofiles;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.pearson.itautomation.bca.Browser;
import com.pearson.itautomation.bca.BrowserAction;
import com.pearson.itautomation.bca.BrowserElement;
import com.pearson.itautomation.bca.interaction.ClearTextElementAction;
import com.pearson.itautomation.bca.interaction.LeftClickElementAction;
import com.pearson.itautomation.bca.interaction.SendTextToElementAction;
import com.pearson.itautomation.bca.validation.ElementExistsValidation;

/**
 * This Class will Search the Approved User<br>
 * <b>checkPrestate</b> - N/A <br>
 * <b>checkPostate</b> - N/A <br>
 * <b>perform</b> - This method will return true if the User appears in search
 * result successfully.
 * 
 * @author Sujeet Chaudhary
 */
public class SearchApprovedUser extends BrowserAction {

    private static final Logger LOGGER = LoggerFactory
            .getLogger(SearchApprovedUser.class);

    private static final String OR_AREA = "OR_User";
    private static final String OR_SEARCH_USERNAME = "SearchUserNameField";
    private static final String OR_USER_SEARCH = "SearchButton";

    private final BrowserElement txtUserName;
    private final BrowserElement btnSearch;

    private final String strUserName;

    /**
     * This constructor will initialize the mandatory fields and the
     * BrowserElements from this class
     * 
     * @param browser
     * @param strUserName
     *            Value of 'User Name'
     * 
     */
    public SearchApprovedUser(final Browser browser, String strUserName) {
        super(browser);

        this.strUserName = strUserName;
        this.btnSearch = browser.getBrowserElement(OR_AREA, OR_USER_SEARCH);
        this.txtUserName = browser.getBrowserElement(OR_AREA,
                OR_SEARCH_USERNAME);
    }

    @Override
    protected boolean perform() {
        boolean result = true;

        if (!new ElementExistsValidation(getBrowser(), this.txtUserName)
                .performWithStates()) {
            LOGGER.error("Unable to locate User Name field");
            result = false;
        }

        if (!new ClearTextElementAction(getBrowser(), this.txtUserName)
                .performWithStates()) {
            LOGGER.error("Deleting the text from User Name field failed");
            result = false;
        }

        if (!new SendTextToElementAction(getBrowser(), this.txtUserName,
                this.strUserName).performWithStates()) {
            LOGGER.error("Not able to send text to User Name field");
            result = false;
        }

        if (!new LeftClickElementAction(getBrowser(), this.btnSearch)
                .performWithStates()) {
            LOGGER.error("Not able to click on Search button");
            result = false;
        }

        return result;
    }
}
