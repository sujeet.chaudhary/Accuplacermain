package com.pearson.itautomation.accuplacer.users.userprofiles;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.pearson.itautomation.bca.Browser;
import com.pearson.itautomation.bca.BrowserAction;
import com.pearson.itautomation.bca.BrowserElement;
import com.pearson.itautomation.bca.interaction.LeftClickElementAction;
import com.pearson.itautomation.bca.interaction.SelectCheckboxAction;

/**
 * This Class will Search the created User<br>
 * <b>checkPrestate</b> - N/A <br>
 * <b>checkPostate</b> - N/A <br>
 * <b>perform</b> - This method will return true if User Searched successfully.
 * 
 * @author Sujeet Chaudhary
 * 
 */
public class ApproveRejectUser extends BrowserAction {

    private static final Logger LOGGER = LoggerFactory
            .getLogger(ApproveRejectUser.class);

    private static final String OR_AREA = "OR_User";
    private static final String OR_REJECT_APPROVE = "RejectApproveUser";
    private static final String OR_USER_SEARCH = "SearchButton";

    private final BrowserElement chkbxRejectApproveUser;
    private final BrowserElement btnSearch;

    /**
     * This constructor will initialize the mandatory fields and the
     * BrowserElements from this class
     * 
     * @param browser
     * 
     * 
     */
    public ApproveRejectUser(final Browser browser) {
        super(browser);
        this.btnSearch = browser.getBrowserElement(OR_AREA, OR_USER_SEARCH);
        this.chkbxRejectApproveUser = browser.getBrowserElement(OR_AREA,
                OR_REJECT_APPROVE);

    }

    @Override
    protected boolean perform() {
        boolean result = true;

        if (!new SelectCheckboxAction(getBrowser(), this.chkbxRejectApproveUser)
                .performWithStates()) {
            LOGGER.error("Not able to click on RejectApproveUser Check box");
            result = false;
        }

        if (!new LeftClickElementAction(getBrowser(), this.btnSearch)
                .performWithStates()) {
            LOGGER.error("Not able to click on Search button");
            result = false;
        }

        return result;
    }
}
