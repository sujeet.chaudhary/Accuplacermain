package com.pearson.itautomation.accuplacer.users.userprofiles;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.pearson.itautomation.accuplacer.core.CommonUtilities;
import com.pearson.itautomation.bca.Browser;
import com.pearson.itautomation.bca.BrowserAction;
import com.pearson.itautomation.bca.BrowserElement;
import com.pearson.itautomation.bca.SelectorType;
import com.pearson.itautomation.bca.interaction.ClearTextElementAction;
import com.pearson.itautomation.bca.interaction.DropdownSelectionAction;
import com.pearson.itautomation.bca.interaction.SendTextToElementAction;
import com.pearson.itautomation.bca.validation.ElementExistsValidation;

/**
 * This Class will fill out the Add User Profile Form with all the mandatory
 * details.<br>
 * <b>perform</b> - This method will return true if all the First Name is filled
 * in successfully.
 * 
 * @author Deepak Radhakrishnan
 */

public class ReFillUserProfile extends BrowserAction {

    private static final Logger LOGGER = LoggerFactory
            .getLogger(ReFillUserProfile.class);

    private static final String OR_AREA = "OR_User";
    private static final String OR_FIRST_NAME = "UserFirstName";
    private static final String OR_LAST_NAME = "UserLastName";
    private static final String OR_USER_NAME = "UserUserName";
    private static final String OR_ADDRESS1 = "UserAddress1";
    private static final String OR_COUNTRY = "UserCountry";
    private static final String OR_IF_OTHER_SPECIFY = "UserIfOtherSpecify";
    private static final String OR_CITY = "UserCity";
    private static final String OR_ZIP_POSTAL_CODE = "UserZipPostalCode";
    private static final String OR_EMAIL_ADDRESS = "UserEmailAddress";
    private static final String OR_HOME_PHONE_NO = "UserHomePhoneNo";
    private static final String OR_ACTIVE_ACCOUNT_FROM = "UserActiveAccountFromDate";
    private static final String OR_ACTIVE_ACCOUNT_TO = "UserActiveAccountToDate";
    private static final String OR_DISABLED_SAVE = "disabledSaveButton";

    private final BrowserElement txtFirstName;
    private final BrowserElement txtLastName;
    private final BrowserElement txtUserName;
    private final BrowserElement txtAddress1;
    private final BrowserElement drpCountry;
    private final BrowserElement txtIfOtherSpecify;
    private final BrowserElement txtCity;
    private final BrowserElement txtZipPostalCode;
    private final BrowserElement txtEmailAddress;
    private final BrowserElement txtHomePhoneNo;
    private final BrowserElement txtFromDate;
    private final BrowserElement txtToDate;
    private final BrowserElement btnDisabledSave;
    private static final String FIRST_NAME = UserProfileInfo.getStrfirstname();
    private static final String LAST_NAME = UserProfileInfo.getStrlastname();
    private final String strUserName;
    private static final String ADDRESS1 = UserProfileInfo.getStraddress1();
    private static final String COUNTRY = UserProfileInfo.getStrcountry();
    private static final String IF_OTHER_SPECIFY = UserProfileInfo
            .getStrifotherspecify();
    private static final String CITY = UserProfileInfo.getStrcity();
    private static final String ZIP_POSTAL_CODE = UserProfileInfo
            .getStrzippostalcode();
    private static final String EMAIL_ADDRESS = UserProfileInfo
            .getStremailaddress();
    private static final String HOME_PHONE_NO = UserProfileInfo
            .getStrhomephoneno();
    private static final String FROM_DATE = CommonUtilities.getDayFromToday(0);
    private static final String TO_DATE = CommonUtilities.getDayFromToday(5);

    private static final String DISABLED_STRING = "Save Button is DISABLED";

    /**
     * This constructor will initialize the first name fields and the
     * BrowserElements from this class
     * 
     * @param browser
     */
    public ReFillUserProfile(final Browser browser, String strUserName) {
        super(browser);

        this.strUserName = strUserName;
        this.txtFirstName = browser.getBrowserElement(OR_AREA, OR_FIRST_NAME);
        this.txtLastName = browser.getBrowserElement(OR_AREA, OR_LAST_NAME);
        this.txtUserName = browser.getBrowserElement(OR_AREA, OR_USER_NAME);
        this.txtAddress1 = browser.getBrowserElement(OR_AREA, OR_ADDRESS1);
        this.drpCountry = browser.getBrowserElement(OR_AREA, OR_COUNTRY);
        this.txtIfOtherSpecify = browser.getBrowserElement(OR_AREA,
                OR_IF_OTHER_SPECIFY);
        this.txtCity = browser.getBrowserElement(OR_AREA, OR_CITY);
        this.txtZipPostalCode = browser.getBrowserElement(OR_AREA,
                OR_ZIP_POSTAL_CODE);
        this.txtEmailAddress = browser.getBrowserElement(OR_AREA,
                OR_EMAIL_ADDRESS);
        this.txtHomePhoneNo = browser.getBrowserElement(OR_AREA,
                OR_HOME_PHONE_NO);
        this.txtFromDate = browser.getBrowserElement(OR_AREA,
                OR_ACTIVE_ACCOUNT_FROM);
        this.txtToDate = browser.getBrowserElement(OR_AREA,
                OR_ACTIVE_ACCOUNT_TO);
        this.btnDisabledSave = browser.getBrowserElement(OR_AREA,
                OR_DISABLED_SAVE);

    }

    @Override
    protected boolean perform() {
        boolean result = true;
        if (!checkSaveButtonDisabledStatus()) {
            LOGGER.info("Save Button is Enabled");
            return true;
        }
        if (checkSaveButtonDisabledStatus()) {
            LOGGER.info(DISABLED_STRING);
            if (!new ClearTextElementAction(getBrowser(), this.txtFirstName)
                    .performWithStates()) {
                LOGGER.error("Not able to Clear First Name field");
                result = false;
            }
            if (!new SendTextToElementAction(getBrowser(), this.txtFirstName,
                    FIRST_NAME).performWithStates()) {
                LOGGER.error("Not able to send text to First Name field");
                result = false;

            }
        } else {
            return true;
        }
        if (checkSaveButtonDisabledStatus()) {
            LOGGER.info(DISABLED_STRING);
            if (!new DropdownSelectionAction(getBrowser(), this.drpCountry,
                    SelectorType.VISIBLE_TEXT, COUNTRY).performWithStates()) {
                LOGGER.error("Not able to select Country");
                result = false;
            }
            if (!new ClearTextElementAction(getBrowser(),
                    this.txtIfOtherSpecify).performWithStates()) {
                LOGGER.error("Not able to Clear if other field");
                result = false;
            }
            if (!new SendTextToElementAction(getBrowser(),
                    this.txtIfOtherSpecify, IF_OTHER_SPECIFY)
                    .performWithStates()) {
                LOGGER.error("Not able to send text to if other field");
                result = false;
            }
        } else {
            return true;
        }

        if (checkSaveButtonDisabledStatus()) {
            LOGGER.info(DISABLED_STRING);
            if (!new ClearTextElementAction(getBrowser(), this.txtLastName)
                    .performWithStates()) {
                LOGGER.error("Not able to Clear Last Name field");
                result = false;
            }
            if (!new SendTextToElementAction(getBrowser(), this.txtLastName,
                    LAST_NAME).performWithStates()) {
                LOGGER.error("Not able to send text to Last Name field");
                result = false;
            }
        } else {
            return true;
        }

        if (checkSaveButtonDisabledStatus()) {
            LOGGER.info(DISABLED_STRING);
            if (!new ClearTextElementAction(getBrowser(), this.txtUserName)
                    .performWithStates()) {
                LOGGER.error("Not able to Clear User Name field");
                result = false;
            }
            if (!new SendTextToElementAction(getBrowser(), this.txtUserName,
                    this.strUserName).performWithStates()) {
                LOGGER.error("Not able to send text to User Name field");
                result = false;
            }
        } else {
            return true;
        }

        if (checkSaveButtonDisabledStatus()) {
            LOGGER.info(DISABLED_STRING);
            if (!new ClearTextElementAction(getBrowser(), this.txtAddress1)
                    .performWithStates()) {
                LOGGER.error("Not able to Clear Address1field");
                result = false;
            }
            if (!new SendTextToElementAction(getBrowser(), this.txtAddress1,
                    ADDRESS1).performWithStates()) {
                LOGGER.error("Not able to send text to Address1 field");
                result = false;
            }
        } else {
            return true;
        }

        if (checkSaveButtonDisabledStatus()) {
            LOGGER.info(DISABLED_STRING);
            if (!new ClearTextElementAction(getBrowser(), this.txtCity)
                    .performWithStates()) {
                LOGGER.error("Not able to Clear City field");
                result = false;
            }
            if (!new SendTextToElementAction(getBrowser(), this.txtCity, CITY)
                    .performWithStates()) {
                LOGGER.error("Not able to send text to City field");
                result = false;
            }
        } else {
            return true;
        }

        if (checkSaveButtonDisabledStatus()) {
            LOGGER.info(DISABLED_STRING);
            if (!new ClearTextElementAction(getBrowser(), this.txtZipPostalCode)
                    .performWithStates()) {
                LOGGER.error("Not able to Clear zip/postal code field");
                result = false;
            }
            if (!new SendTextToElementAction(getBrowser(),
                    this.txtZipPostalCode, ZIP_POSTAL_CODE).performWithStates()) {
                LOGGER.error("Not able to send text to zip/postal code field");
                result = false;
            }
        } else {
            return true;
        }

        if (checkSaveButtonDisabledStatus()) {
            LOGGER.info(DISABLED_STRING);
            if (!new ClearTextElementAction(getBrowser(), this.txtEmailAddress)
                    .performWithStates()) {
                LOGGER.error("Not able to Clear EmailAddress  field");
                result = false;
            }
            if (!new SendTextToElementAction(getBrowser(),
                    this.txtEmailAddress, EMAIL_ADDRESS).performWithStates()) {
                LOGGER.error("Not able to send text to EmailAddress field");
                result = false;
            }
        } else {
            return true;
        }

        if (checkSaveButtonDisabledStatus()) {
            LOGGER.info(DISABLED_STRING);
            if (!new ClearTextElementAction(getBrowser(), this.txtHomePhoneNo)
                    .performWithStates()) {
                LOGGER.error("Not able to Clear Home Phone No field");
                result = false;
            }
            if (!new SendTextToElementAction(getBrowser(), this.txtHomePhoneNo,
                    HOME_PHONE_NO).performWithStates()) {
                LOGGER.error("Not able to send text to Home Phone No field");
                result = false;
            }
        } else {
            return true;
        }

        if (checkSaveButtonDisabledStatus()) {
            LOGGER.info(DISABLED_STRING);
            if (!new ClearTextElementAction(getBrowser(), this.txtFromDate)
                    .performWithStates()) {
                LOGGER.error("Not able to Clear From Date field");
                result = false;
            }
            if (!new SendTextToElementAction(getBrowser(), this.txtFromDate,
                    FROM_DATE).performWithStates()) {
                LOGGER.error("Not able to send text to From Date field");
                result = false;
            }
        } else {
            return true;
        }

        if (checkSaveButtonDisabledStatus()) {
            LOGGER.info(DISABLED_STRING);
            if (!new ClearTextElementAction(getBrowser(), this.txtToDate)
                    .performWithStates()) {
                LOGGER.error("Not able to Clear To Date field");
                result = false;
            }
            if (!new SendTextToElementAction(getBrowser(), this.txtToDate,
                    TO_DATE).performWithStates()) {
                LOGGER.error("Not able to send text to To Date field");
                result = false;
            }
        } else {
            return true;
        }

        if (checkSaveButtonDisabledStatus()) {
            LOGGER.error(DISABLED_STRING);
            result = false;
        }

        return result;
    }

    /**
     * This Method will return true if save button is disabled
     * 
     * @return
     */
    public boolean checkSaveButtonDisabledStatus() {
        return new ElementExistsValidation(getBrowser(), this.btnDisabledSave)
                .performWithStates();

    }
}
