package com.pearson.itautomation.accuplacer.users.userprofiles;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.pearson.itautomation.bca.Browser;
import com.pearson.itautomation.bca.BrowserAction;
import com.pearson.itautomation.bca.BrowserElement;
import com.pearson.itautomation.bca.SelectorType;
import com.pearson.itautomation.bca.interaction.DropdownSelectionAction;

/**
 * This Class will Select the UserType Form UserType Dropdown<br>
 * <b>checkPrestate</b> - N/A <br>
 * <b>checkPostate</b> - N/A <br>
 * <b>perform</b> - This method will return true if Dropdown data selected
 * successfully.
 * 
 * @author Sujeet Chaudhary
 */
public class SelectUserType extends BrowserAction {

    private static final Logger LOGGER = LoggerFactory
            .getLogger(SelectUserType.class);

    private static final String OR_AREA = "OR_User";
    private static final String OR_USER_TYPE = "UserUserType";

    private final BrowserElement drpUserType;

    private final String strUserType;

    /**
     * This constructor will initialize the mandatory fields and the
     * BrowserElements from this class
     * 
     * @param browser
     * @param strUserType
     *            The Value of 'UserType' to be selected
     */
    public SelectUserType(final Browser browser, String strUserType) {
        super(browser);

        this.strUserType = strUserType;
        this.drpUserType = browser.getBrowserElement(OR_AREA, OR_USER_TYPE);
    }

    @Override
    protected boolean perform() {
        boolean result = true;

        if (!new DropdownSelectionAction(getBrowser(), this.drpUserType,
                SelectorType.VISIBLE_TEXT, this.strUserType)
                .performWithStates()) {
            LOGGER.error("Not able to select User Type");
            result = false;
        }

        return result;
    }

}
