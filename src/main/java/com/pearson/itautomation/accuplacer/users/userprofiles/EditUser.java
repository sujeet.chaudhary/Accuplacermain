package com.pearson.itautomation.accuplacer.users.userprofiles;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.pearson.itautomation.bca.Browser;
import com.pearson.itautomation.bca.BrowserAction;
import com.pearson.itautomation.bca.BrowserElement;
import com.pearson.itautomation.bca.interaction.ClearTextElementAction;
import com.pearson.itautomation.bca.interaction.LeftClickElementAction;
import com.pearson.itautomation.bca.interaction.SendTextToElementAction;

/**
 * This Class will Edit the created User<br>
 * <b>checkPrestate</b> - N/A <br>
 * <b>checkPostate</b> - N/A <br>
 * <b>perform</b> - This method will return true if User Edited Successfully.
 * 
 * @author Sujeet Chaudhary
 * 
 */
public class EditUser extends BrowserAction {

    private static final Logger LOGGER = LoggerFactory
            .getLogger(EditUser.class);

    private static final String OR_AREA = "OR_User";
    private static final String OR_DESCRIPTION = "UserDescription";
    private static final String OR_EDIT_USER = "EditUser";

    private final BrowserElement btnEditUser;
    private final BrowserElement txtDescription;

    private static final String strDescription = "description";

    /**
     * This constructor will initialize the mandatory fields and the
     * BrowserElements from this class
     * 
     * @param browser
     * 
     */
    public EditUser(final Browser browser) {
        super(browser);

        this.btnEditUser = browser.getBrowserElement(OR_AREA, OR_EDIT_USER);

        this.txtDescription = browser
                .getBrowserElement(OR_AREA, OR_DESCRIPTION);

    }

    @Override
    protected boolean perform() {
        boolean result = true;

        if (!new LeftClickElementAction(getBrowser(), this.btnEditUser)
                .performWithStates()) {
            LOGGER.error("Not able to click on Edit button");
            result = false;
        }

        if (!new ClearTextElementAction(getBrowser(), this.txtDescription)
                .performWithStates()) {
            LOGGER.error("Not able to clear Description field");
            result = false;
        }

        if (!new SendTextToElementAction(getBrowser(), this.txtDescription,
                strDescription).performWithStates()) {
            LOGGER.error("Not able to send text to Description field");
            result = false;
        }

        return result;
    }

}
