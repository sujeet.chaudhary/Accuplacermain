package com.pearson.itautomation.accuplacer.users.testingunits;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Random;

import org.openqa.selenium.WebElement;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.pearson.itautomation.accuplacer.core.action.LeftClickOnModalPopupButton;
import com.pearson.itautomation.bca.Browser;
import com.pearson.itautomation.bca.BrowserAction;
import com.pearson.itautomation.bca.BrowserElement;
import com.pearson.itautomation.bca.SelectorType;
import com.pearson.itautomation.bca.interaction.DropdownSelectionAction;
import com.pearson.itautomation.bca.interaction.LeftClickElementAction;
import com.pearson.itautomation.bca.interaction.SendTextToElementAction;

/**
 * The Add Test Units class will be adding the Test Units in the institute or
 * site and then fills all the mandatory details in the Form.<br>
 * <b>checkPrestate</b> - <b>checkPostate</b> - Checks to see the assert unit
 * counts match only if the PreState was run <br>
 * <b>perform</b> - This method will return true if all the mandatory fields
 * have been filled in and add units button clicked successfully to add test
 * units.
 * 
 * @author Solomon Lingala
 * 
 */

public class AddTestingUnits extends BrowserAction {

    private static final Logger LOGGER = LoggerFactory
            .getLogger(AddTestingUnits.class);
    private static final String OR_AREA = "OR_User";
    private static final String OR_ADDUNITS_BUTTON = "AddUnitsButton";
    private static final String OR_SAVE_BUTTON = "SaveAddUnitsButton";

    private static final String OR_TYPEOFUNITS = "TypeOfUnits";
    private static final String OR_NOOFUNITSTOADD = "NoOfUnitsToAdd";
    private static final String OR_BASEPRICE = "basePrice";
    private static final String OR_PO = "purchaseOrder";
    private static final String OR_ORDERNO = "orderNo";
    private static final String OR_TRANSFERTESTUNITS = "TTUYesButton";

    private final String strOrgID;
    private final String strNoOfUnitsToAdd;
    private final String strTypeOfUnits;
    private final String strBasePrice;
    private final String strPO;
    private final String strIOCOrder;

    private BrowserElement txtNoOfUnitsToAdd;

    private BrowserElement btnAddUnits;
    private BrowserElement btnYes;
    private BrowserElement drpTypeOfUnits;
    private BrowserElement txtBasePrice;
    private BrowserElement txtPO;
    private BrowserElement txtOrderNo;
    private BrowserElement btnSave;
    private Connection connection;
    private int actualUnits;

    /**
     * This constructor will initialize the mandatory fields and the
     * BrowserElements from this class
     * 
     * @param browser
     * @param strOrgID
     *            Display ID of the organization to which units will be added
     * @param strTypeOfUnits
     *            Type of Units to be added
     * @param strNoOfUnits
     *            No of Units to be added
     * @param strBasePrice
     *            Base Price of Each Unit
     * @param strPO
     *            Purchase Order
     * @param strIOCOrder
     *            IOC Order No
     * @param connection
     *            database connection object.
     */

    public AddTestingUnits(Browser browser, String strOrgID,
            String strTypeOfUnits, String strNoOfUnits, String strBasePrice,
            String strPO, String strIOCOrder, final Connection connection) {
        super(browser);
        this.strOrgID = strOrgID;
        this.strNoOfUnitsToAdd = strNoOfUnits;
        this.strTypeOfUnits = strTypeOfUnits;
        this.strBasePrice = strBasePrice;
        this.strPO = strPO;
        this.strIOCOrder = strIOCOrder;
        this.btnYes = browser.getBrowserElement(OR_AREA, OR_TRANSFERTESTUNITS);
        this.connection = connection;
    }

    @Override
    public boolean checkPreState() {
        boolean result = true;

        actualUnits = getAvailableTestUnitsFromDB()
                + Integer.parseInt(this.strNoOfUnitsToAdd);

        return result;
    }

    @Override
    protected boolean perform() {
        boolean result = true;
        this.btnAddUnits = getBrowser().getBrowserElement(OR_AREA,
                OR_ADDUNITS_BUTTON).formatWithParms(strOrgID);
        this.txtNoOfUnitsToAdd = this.getBrowser().getBrowserElement(OR_AREA,
                OR_NOOFUNITSTOADD);
        this.drpTypeOfUnits = this.getBrowser().getBrowserElement(OR_AREA,
                OR_TYPEOFUNITS);
        this.txtBasePrice = this.getBrowser().getBrowserElement(OR_AREA,
                OR_BASEPRICE);
        this.txtPO = this.getBrowser().getBrowserElement(OR_AREA, OR_PO);
        this.txtOrderNo = this.getBrowser().getBrowserElement(OR_AREA,
                OR_ORDERNO);
        this.btnSave = this.getBrowser().getBrowserElement(OR_AREA,
                OR_SAVE_BUTTON);

        WebElement checkbtnAddUnits = getBrowser().getElementWithWait(
                this.btnAddUnits);

        if (checkbtnAddUnits != null) {
            if (result
                    && !new LeftClickElementAction(this.getBrowser(),
                            btnAddUnits).performWithStates()) {
                LOGGER.error("Failed to click on Add Units Button for the Organization");
                result = false;
            }
        } else {
            result = false;
            LOGGER.warn("Could not Click on the Add Units button");
        }

        if (!new SendTextToElementAction(this.getBrowser(),
                this.txtNoOfUnitsToAdd, this.strNoOfUnitsToAdd)
                .performWithStates()) {
            LOGGER.error("Failed to type # Of Units To Number of Units text field");
            result = false;
        }

        WebElement checkElement = getBrowser().getElementWithWait(
                this.drpTypeOfUnits);

        if (checkElement != null) {
            if (!new DropdownSelectionAction(this.getBrowser(), drpTypeOfUnits,
                    SelectorType.INDEX, strTypeOfUnits).performWithStates()) {
                LOGGER.error("Failed to select from Type of Units drop down selection");
                result = false;
            }
        } else {
            result = false;
            LOGGER.warn("Could not find the Type of Units From drop down");
        }

        if (!new SendTextToElementAction(this.getBrowser(), this.txtBasePrice,
                strBasePrice).performWithStates()) {
            LOGGER.error("Failed to enter the base price");
            result = false;
        }

        if (!new SendTextToElementAction(this.getBrowser(), this.txtPO,
                this.strPO).performWithStates()) {
            LOGGER.error("Failed to enter PO/Ref No.");
            result = false;
        }

        Random rand = new Random();
        int randNo = rand.nextInt(1000);
        int temp = randNo + Integer.parseInt(strIOCOrder);
        if (!new SendTextToElementAction(this.getBrowser(), this.txtOrderNo,
                String.valueOf(temp)).performWithStates()) {
            LOGGER.error("Failed to enter IOC Order No");
            result = false;
        }

        if (result
                && !new LeftClickElementAction(this.getBrowser(), btnSave)
                        .performWithStates()) {
            LOGGER.error("Failed to click on Add Units Button");
            result = false;
        }

        if (result
                && !new LeftClickOnModalPopupButton(this.getBrowser(), btnYes)
                        .performWithStates()) {
            LOGGER.error("Failed to click on Yes Button");
            result = false;
        }

        return result;

    }

    @Override
    public boolean checkPostState() {

        int limit = 5000;
        int increment = 500;
        int c = 0;
        int availableUnitsInDB = 0;
        while (c < limit) {
            c = c + increment;
            availableUnitsInDB = getAvailableTestUnitsFromDB();
            if (actualUnits == availableUnitsInDB) {
                return true;
            }
            try {
                Thread.sleep(increment);
            } catch (InterruptedException e) {
                LOGGER.error("Exception occured while executing the thread",
                        e.getCause());
            }

        }

        return false;
    }

    private int getAvailableTestUnitsFromDB() {
        String resultString = "";

        String strQuery = "select available_test_units from institution WHERE institution_id_display = '"
                + strOrgID + "' and rownum <=1";

        try (Statement stmt = connection.createStatement();
                ResultSet rs = stmt.executeQuery(strQuery);) {

            while (rs.next()) {
                resultString = rs.getString("available_test_units");
            }
        } catch (SQLException e) {
            LOGGER.error("Exception occured while executing the query",
                    e.getCause());
        }
        int expectedUnits = 0;

        if (!resultString.isEmpty()) {
            expectedUnits = Integer.parseInt(resultString);
        }
        return expectedUnits;
    }
}
