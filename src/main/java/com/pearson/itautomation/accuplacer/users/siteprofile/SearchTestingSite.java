package com.pearson.itautomation.accuplacer.users.siteprofile;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.pearson.itautomation.bca.Browser;
import com.pearson.itautomation.bca.BrowserAction;
import com.pearson.itautomation.bca.BrowserElement;
import com.pearson.itautomation.bca.SelectorType;
import com.pearson.itautomation.bca.interaction.DropdownSelectionAction;
import com.pearson.itautomation.bca.interaction.LeftClickElementAction;

public class SearchTestingSite extends BrowserAction {

    private static final Logger LOGGER = LoggerFactory
            .getLogger(SearchTestingSite.class);

    private static final String OR_AREA = "OR_AddTestingSites";
    private static final String OR_SEARCH_INSTITUTIONNAME = "OR_SearchInstitutionName";
    private static final String OR_SEARCH_SITENAME = "OR_SearchSiteName";
    private static final String OR_SEARCH = "OR_Search";
    private static final String OR_BACK = "OR_Back";

    private final BrowserElement drpInstitutionName;
    private final BrowserElement drpSiteName;
    private final BrowserElement btnSearch;
    private final BrowserElement lnkBack;

    private final String institutionName;
    private final String siteName;

    public SearchTestingSite(final Browser browser, String institutionName,
            String siteName) {
        super(browser);

        this.institutionName = institutionName;
        this.siteName = siteName;

        this.drpInstitutionName = browser.getBrowserElement(OR_AREA,
                OR_SEARCH_INSTITUTIONNAME);
        this.drpSiteName = browser.getBrowserElement(OR_AREA,
                OR_SEARCH_SITENAME);
        this.btnSearch = browser.getBrowserElement(OR_AREA, OR_SEARCH);
        this.lnkBack = browser.getBrowserElement(OR_AREA, OR_BACK);
    }

    @Override
    protected boolean perform() {
        boolean result = true;

        if (!new LeftClickElementAction(getBrowser(), this.lnkBack)
                .performWithStates()) {
            LOGGER.error("Not able to click on Back button");
            result = false;
        }

        if (!new DropdownSelectionAction(getBrowser(), this.drpInstitutionName,
                SelectorType.VISIBLE_TEXT, this.institutionName)
                .performWithStates()) {
            LOGGER.error("Not able to select text to Institution Name field");
            result = false;
        }

        if (!new DropdownSelectionAction(getBrowser(), this.drpSiteName,
                SelectorType.VISIBLE_TEXT, this.siteName).performWithStates()) {
            LOGGER.error("Not able to select text to Site Name field");
            result = false;
        }

        if (!new LeftClickElementAction(getBrowser(), this.btnSearch)
                .performWithStates()) {
            LOGGER.error("Not able to click on Search button");
            result = false;
        }

        return result;
    }

}
