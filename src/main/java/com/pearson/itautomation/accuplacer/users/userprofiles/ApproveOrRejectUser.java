package com.pearson.itautomation.accuplacer.users.userprofiles;

import org.openqa.selenium.By;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.pearson.itautomation.bca.Browser;
import com.pearson.itautomation.bca.BrowserAction;
import com.pearson.itautomation.bca.BrowserElement;
import com.pearson.itautomation.bca.interaction.LeftClickElementAction;
import com.pearson.itautomation.bca.validation.ElementExistsValidation;

/**
 * This Class will Approve the created User<br>
 * <b>checkPrestate</b> - N/A <br>
 * <b>checkPostate</b> - N/A <br>
 * <b>perform</b> - This method will return true if User Approved successfully.
 * 
 * @author Sujeet Chaudhary
 */
public class ApproveOrRejectUser extends BrowserAction {

    private static final Logger LOGGER = LoggerFactory
            .getLogger(ApproveOrRejectUser.class);

    private static final String OR_AREA = "OR_User";
    private static final String OR_APPROVE_REJECT_YES = "ApproveRejectYes";
    private static final String OR_USER_APPROVE_REJECT = "RejectApproveUser";
    private static final String OR_APPROVE_REQUEST = "ApproveRequest";

    private final BrowserElement btnYes;
    private final BrowserElement xpathForApproveReject;
    private final BrowserElement cbkRejectApproveUser;
    private final BrowserElement txtApproveRequest;

    /**
     * This constructor will initialize the mandatory fields and the
     * BrowserElements from this class
     * 
     * @param browser
     * @param xpath
     *            'XPATH' of 'Approve' link for the User to be Approved
     */
    public ApproveOrRejectUser(final Browser browser, String xpath) {
        super(browser);

        this.cbkRejectApproveUser = browser.getBrowserElement(OR_AREA,
                OR_USER_APPROVE_REJECT);
        this.xpathForApproveReject = new BrowserElement("xpath",
                By.xpath(xpath));
        this.btnYes = browser.getBrowserElement(OR_AREA, OR_APPROVE_REJECT_YES);
        this.txtApproveRequest = browser.getBrowserElement(OR_AREA,
                OR_APPROVE_REQUEST);
    }

    @Override
    protected boolean perform() {
        boolean result = true;

        if (!new LeftClickElementAction(getBrowser(),
                this.xpathForApproveReject).performWithStates()) {
            LOGGER.error("Not able to click on xpathForApproveReject button");
            return false;
        }

        if (!new LeftClickElementAction(getBrowser(), this.btnYes)
                .performWithStates()) {
            LOGGER.error("Not able to click on yes button");
            return false;
        }

        if (!new ElementExistsValidation(getBrowser(), this.txtApproveRequest)
                .performWithStates()) {
            LOGGER.error("Not able to validate the approval request");
            return false;
        }

        if (!new LeftClickElementAction(getBrowser(), this.cbkRejectApproveUser)
                .performWithStates()) {
            LOGGER.error("Not able to click on RejectApproveCheckBox");
            result = false;
        }

        return result;
    }

}
