package com.pearson.itautomation.accuplacer.users.userprofiles;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.pearson.itautomation.accuplacer.core.action.LeftClickOnModalPopupButton;
import com.pearson.itautomation.bca.Browser;
import com.pearson.itautomation.bca.BrowserAction;
import com.pearson.itautomation.bca.BrowserElement;
import com.pearson.itautomation.bca.interaction.LeftClickElementAction;
import com.pearson.itautomation.bca.validation.ElementExistsValidation;

/**
 * This Class will check the Reject/Approve CBA User checkbox and click on
 * search<br>
 * <b>checkPrestate</b> - N/A <br>
 * <b>checkPostate</b> - N/A <br>
 * <b>perform</b> - This method will return true if Searched result appears.
 * 
 * @author Sujeet Chaudhary
 * 
 */
public class SearchUserForApproveReject extends BrowserAction {

    private static final Logger LOGGER = LoggerFactory
            .getLogger(SearchUserForApproveReject.class);

    private static final String OR_AREA = "OR_User";
    private static final String OR_REJECT_APPROVE = "RejectApproveUser";
    private static final String OR_USER_SEARCH = "SearchButton";

    private final BrowserElement chkbxRejectApproveUser;
    private final BrowserElement btnSearch;

    /**
     * This constructor will initialize the mandatory fields and the
     * BrowserElements from this class
     * 
     * @param browser
     * @param strUserName
     *            'User Name' Value
     * 
     */
    public SearchUserForApproveReject(final Browser browser) {
        super(browser);
        this.btnSearch = browser.getBrowserElement(OR_AREA, OR_USER_SEARCH);
        this.chkbxRejectApproveUser = browser.getBrowserElement(OR_AREA,
                OR_REJECT_APPROVE);

    }

    @Override
    protected boolean perform() {
        boolean result = true;
        try {
            Thread.sleep(3000);
        } catch (InterruptedException e) {
            LOGGER.info("Exception", e);
        }

        if (!new ElementExistsValidation(getBrowser(),
                this.chkbxRejectApproveUser).performWithStates()) {
            LOGGER.error("Not able to click on RejectApproveUser button");
            result = false;
        }

        if (!new LeftClickElementAction(getBrowser(),
                this.chkbxRejectApproveUser).performWithStates()) {
            LOGGER.error("Not able to click on RejectApproveUser button");
            result = false;
        }

        try {
            Thread.sleep(3000);
        } catch (InterruptedException e) {
            LOGGER.info("Exception", e);
        }

        if (!new ElementExistsValidation(getBrowser(), this.btnSearch)
                .performWithStates()) {
            LOGGER.error("Not able to click on Search button");
            result = false;
        }

        if (!new LeftClickOnModalPopupButton(getBrowser(), this.btnSearch)
                .performWithStates()) {
            LOGGER.error("Not able to click on Search button");
            result = false;
        }

        return result;
    }
}
