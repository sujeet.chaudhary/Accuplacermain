package com.pearson.itautomation.accuplacer.testsetup.branchingprofiles;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.pearson.itautomation.bca.Browser;
import com.pearson.itautomation.bca.BrowserAction;
import com.pearson.itautomation.bca.BrowserElement;
import com.pearson.itautomation.bca.SelectorType;
import com.pearson.itautomation.bca.interaction.DropdownSelectionAction;
import com.pearson.itautomation.bca.interaction.LeftClickElementAction;
import com.pearson.itautomation.bca.validation.ElementExistsValidation;

/**
 * This Class will Edit the Created Branching Profile. <br>
 * <b>checkPrestate</b> - N/A <br>
 * <b>checkPostate</b> - N/A <br>
 * <b>perform</b> - This method will return true if Branching Profile will be
 * edited successfully.
 * 
 * @author Sujeet Chaudhary
 */
public class EditBranchingProfile extends BrowserAction {

    private static final Logger LOGGER = LoggerFactory
            .getLogger(EditBranchingProfile.class);

    private static final String OR_AREA = "OR_TestSetup";
    private static final String OR_ADD_ANOTHER_RULE1 = "AddAnotherRuleForAddingRule1";
    private static final String OR_BELOW_THIS_RULE_RULE1 = "BelowThisRuleForAddingRule1";
    private static final String OR_NEW_RULETYPE_RULE = "NewRuleTypeRule";
    private static final String OR_NEW_RULETYPE_ADD = "NewRuleTypeAdd";
    private static final String OR_EDIT_VALIDATION_MSG = "EditValidationMsg";
    private static final String OR_ADD_BRANCHINGPROFILE = "AddBranchingProfile";
    private static final String OR_EDIT_BRANCHING_XPATH = "BranchingProfileEditXpath";
    private static final String OR_SAVE = "Save";

    private final BrowserElement btnEditBranchingProfile;
    private final BrowserElement lnkBelowThisRuleForAddingRule1;
    private final BrowserElement drpNewRuleTypeRule;
    private final BrowserElement btnNewRuleTypeAdd;
    private final BrowserElement btnAddAnotherRuleForAddingRule1;
    private final BrowserElement txtEditValidationMsg;
    private final BrowserElement btnAddBranchingProfile;
    private final BrowserElement btnSave;

    private final String strNewRuleTypeRuleRule1;

    /**
     * This constructor will initialize the mandatory fields and the
     * BrowserElements from this class
     * 
     * @param browser
     * @param strBranchingProfileTitle
     *            The BranchingProfile title which need to be Edit
     * @param strNewRuleTypeRuleRule1
     *            The Name of the 'New Rule Type' what we want to set while
     *            Editing.
     * 
     * 
     */
    public EditBranchingProfile(final Browser browser,
            String strBranchingProfileTitle, String strNewRuleTypeRuleRule1) {
        super(browser);
        this.strNewRuleTypeRuleRule1 = strNewRuleTypeRuleRule1;

        this.btnEditBranchingProfile = browser.getBrowserElement(OR_AREA,
                OR_EDIT_BRANCHING_XPATH).formatWithParms(
                strBranchingProfileTitle, strBranchingProfileTitle);

        this.btnAddBranchingProfile = browser.getBrowserElement(OR_AREA,
                OR_ADD_BRANCHINGPROFILE);

        this.txtEditValidationMsg = browser.getBrowserElement(OR_AREA,
                OR_EDIT_VALIDATION_MSG);

        this.lnkBelowThisRuleForAddingRule1 = browser.getBrowserElement(
                OR_AREA, OR_BELOW_THIS_RULE_RULE1);

        this.drpNewRuleTypeRule = browser.getBrowserElement(OR_AREA,
                OR_NEW_RULETYPE_RULE);
        this.btnNewRuleTypeAdd = browser.getBrowserElement(OR_AREA,
                OR_NEW_RULETYPE_ADD);
        this.btnAddAnotherRuleForAddingRule1 = browser.getBrowserElement(
                OR_AREA, OR_ADD_ANOTHER_RULE1);

        this.btnSave = browser.getBrowserElement(OR_AREA, OR_SAVE);

    }

    @Override
    protected boolean checkPostState() {
        boolean result = true;

        if (!new ElementExistsValidation(getBrowser(),
                this.txtEditValidationMsg).performWithStates()) {
            LOGGER.error("Not able to validate the Message ");
            result = false;
        }

        if (!new ElementExistsValidation(getBrowser(),
                this.btnAddBranchingProfile).performWithStates()) {
            LOGGER.error("Not able to validate add Button ");
            result = false;
        }

        return result;

    }

    @Override
    protected boolean perform() {
        boolean result = true;
        if (!new LeftClickElementAction(getBrowser(),
                this.btnEditBranchingProfile).performWithStates()) {
            LOGGER.error("Not able to click on Edit Button");
            result = false;
        }

        if (!new LeftClickElementAction(getBrowser(),
                this.btnAddAnotherRuleForAddingRule1).performWithStates()) {
            LOGGER.error("Not able to click on Add another Rule for Rule1 Button");
            result = false;
        }

        if (!new LeftClickElementAction(getBrowser(),
                this.lnkBelowThisRuleForAddingRule1).performWithStates()) {
            LOGGER.error("Not able to click on Below this Rule for Rule1 Link");
            result = false;
        }

        if (!new DropdownSelectionAction(getBrowser(), this.drpNewRuleTypeRule,
                SelectorType.VISIBLE_TEXT, this.strNewRuleTypeRuleRule1)
                .performWithStates()) {
            LOGGER.error("Not able to select New Rule Type for Rule1");
            result = false;
        }

        if (!new LeftClickElementAction(getBrowser(), this.btnNewRuleTypeAdd)
                .performWithStates()) {
            LOGGER.error("Not able to Add New Rule for Rule1");
            result = false;
        }

        try {
            Thread.sleep(500);
        } catch (InterruptedException e) {
            LOGGER.warn("Failed to wait for Save to be visible");
        }

        if (!new LeftClickElementAction(getBrowser(), this.btnSave)
                .performWithStates()) {
            LOGGER.error("Not able to click on Save Button");
            result = false;
        }

        return result;

    }
}
