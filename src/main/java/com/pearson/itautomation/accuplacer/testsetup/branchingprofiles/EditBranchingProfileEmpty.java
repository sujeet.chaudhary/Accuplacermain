package com.pearson.itautomation.accuplacer.testsetup.branchingprofiles;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.pearson.itautomation.bca.Browser;
import com.pearson.itautomation.bca.BrowserAction;
import com.pearson.itautomation.bca.BrowserElement;
import com.pearson.itautomation.bca.interaction.LeftClickElementAction;
import com.pearson.itautomation.bca.validation.ElementExistsValidation;

/**
 * This Class will Edit the Created Branching Profile. <br>
 * <b>checkPrestate</b> - N/A <br>
 * <b>checkPostate</b> - N/A <br>
 * <b>perform</b> - This method will return true if Branching Profile will be
 * edited successfully.
 * 
 * @author Deepak Radhakrishnan
 */
public class EditBranchingProfileEmpty extends BrowserAction {

    private static final Logger LOGGER = LoggerFactory
            .getLogger(EditBranchingProfileEmpty.class);

    private static final String OR_AREA = "OR_TestSetup";
    private static final String OR_EDIT_VALIDATION_MSG = "EditValidationMsg";
    private static final String OR_ADD_BRANCHINGPROFILE = "AddBranchingProfile";
    private static final String OR_EDIT_BRANCHING_XPATH = "BranchingProfileEditXpath";
    private static final String OR_SAVE = "Save";
    private static final String OR_ADD_ANOTHER_RULE1 = "AddAnotherRuleForAddingRule1";

    private final BrowserElement btnEditBranchingProfile;
    private final BrowserElement btnAddAnotherRuleForAddingRule1;

    private final BrowserElement txtEditValidationMsg;
    private final BrowserElement btnAddBranchingProfile;
    private final BrowserElement btnSave;

    /**
     * This constructor will initialize the mandatory fields and the
     * BrowserElements from this class
     * 
     * @param browser
     * @param strBranchingProfileTitle
     *            The BranchingProfile title which need to be Edit
     * @param strNewRuleTypeRuleRule1
     *            The Name of the 'New Rule Type' what we want to set while
     *            Editing.
     * 
     * 
     */
    public EditBranchingProfileEmpty(final Browser browser,
            String strBranchingProfileTitle) {
        super(browser);

        this.btnEditBranchingProfile = browser.getBrowserElement(OR_AREA,
                OR_EDIT_BRANCHING_XPATH).formatWithParms(
                strBranchingProfileTitle, strBranchingProfileTitle);
        this.btnAddBranchingProfile = browser.getBrowserElement(OR_AREA,
                OR_ADD_BRANCHINGPROFILE);
        this.btnAddAnotherRuleForAddingRule1 = browser.getBrowserElement(
                OR_AREA, OR_ADD_ANOTHER_RULE1);
        this.txtEditValidationMsg = browser.getBrowserElement(OR_AREA,
                OR_EDIT_VALIDATION_MSG);
        this.btnSave = browser.getBrowserElement(OR_AREA, OR_SAVE);

    }

    @Override
    protected boolean checkPostState() {
        boolean result = true;

        if (!new ElementExistsValidation(getBrowser(),
                this.txtEditValidationMsg).performWithStates()) {
            LOGGER.error("Not able to validate the Message ");
            result = false;
        }

        if (!new ElementExistsValidation(getBrowser(),
                this.btnAddBranchingProfile).performWithStates()) {
            LOGGER.error("Not able to validate add Button ");
            result = false;
        }

        return result;

    }

    @Override
    protected boolean perform() {
        boolean result = true;
        if (!new LeftClickElementAction(getBrowser(),
                this.btnEditBranchingProfile).performWithStates()) {
            LOGGER.error("Not able to click on Edit Button");
            result = false;
        }

        if (!new ElementExistsValidation(getBrowser(),
                this.btnAddAnotherRuleForAddingRule1).performWithStates()) {
            LOGGER.error("Not able to click on Add another Rule for Rule1 Button");
            result = false;
        }
        if (!new LeftClickElementAction(getBrowser(), this.btnSave)
                .performWithStates()) {
            LOGGER.error("Not able to click on Save Button");
            result = false;
        }

        return result;

    }
}
