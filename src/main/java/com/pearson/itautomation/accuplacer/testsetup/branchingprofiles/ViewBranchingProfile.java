package com.pearson.itautomation.accuplacer.testsetup.branchingprofiles;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.pearson.itautomation.bca.Browser;
import com.pearson.itautomation.bca.BrowserAction;
import com.pearson.itautomation.bca.BrowserElement;
import com.pearson.itautomation.bca.interaction.LeftClickElementAction;
import com.pearson.itautomation.bca.validation.ElementExistsValidation;

/**
 * This Class will Verify the View Functionality of Created Branching Profile. <<br>
 * <b>checkPrestate</b> -N/A <br>
 * <b>checkPostate</b> - This method will verify whether we are on correct page
 * or not. It will return true if we are on correct page otherwise false. <br>
 * <b>perform</b> - This method will return true if View Functionality will work
 * as expected.
 * 
 * @author Sujeet Chaudhary
 */
public class ViewBranchingProfile extends BrowserAction {

    private static final Logger LOGGER = LoggerFactory
            .getLogger(ViewBranchingProfile.class);

    private static final String OR_AREA = "OR_TestSetup";
    private static final String OR_BACK_BUTTON = "BranchingProfileBackButton";
    private static final String OR_VIEW_BRANCHINGPROFILE_TITLE = "ViewBranchingProfileTitle";
    private static final String OR_ADD_BRANCHINGPROFILE = "AddBranchingProfile";
    private static final String OR_VIEW_BRANCHINGPROFILE_XPATH = "BranchingProfileViewXpath";

    private final BrowserElement btnViewBranchingProfile;
    private final BrowserElement btnBack;
    private final BrowserElement txtViewBranchingProfileTitle;
    private final BrowserElement btnAddBranchingProfile;

    /**
     * This constructor will initialize the mandatory fields and the
     * BrowserElements from this class
     * 
     * @param browser
     * @param strBranchingProfileTitle
     *            The BranchingProfile title which need to be View
     * 
     * 
     */

    public ViewBranchingProfile(final Browser browser,
            String strBranchingProfileTitle) {
        super(browser);

        this.btnAddBranchingProfile = browser.getBrowserElement(OR_AREA,
                OR_ADD_BRANCHINGPROFILE);

        this.btnViewBranchingProfile = browser.getBrowserElement(OR_AREA,
                OR_VIEW_BRANCHINGPROFILE_XPATH).formatWithParms(
                strBranchingProfileTitle, strBranchingProfileTitle);

        this.btnBack = browser.getBrowserElement(OR_AREA, OR_BACK_BUTTON);

        this.txtViewBranchingProfileTitle = browser.getBrowserElement(OR_AREA,
                OR_VIEW_BRANCHINGPROFILE_TITLE);
    }

    @Override
    protected boolean checkPostState() {
        boolean result = true;
        if (!new ElementExistsValidation(getBrowser(),
                this.btnAddBranchingProfile).performWithStates()) {
            LOGGER.error("Not able to validate add Button ");
            result = false;
        }

        return result;
    }

    @Override
    protected boolean perform() {
        boolean result = true;

        if (!new LeftClickElementAction(getBrowser(),
                this.btnViewBranchingProfile).performWithStates()) {
            LOGGER.error("Not able to click on View Button");
            result = false;
        }

        if (!new ElementExistsValidation(getBrowser(),
                this.txtViewBranchingProfileTitle).performWithStates()) {
            LOGGER.error("Not able to View Branching profile");
            result = false;
        }

        if (!new LeftClickElementAction(getBrowser(), this.btnBack)
                .performWithStates()) {
            LOGGER.error("Not able to click on Back Button");
            result = false;
        }
        return result;
    }

}
