package com.pearson.itautomation.accuplacer.testsetup.branchingprofiles;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.pearson.itautomation.accuplacer.core.action.LeftClickOnModalPopupButton;
import com.pearson.itautomation.accuplacer.interaction.ScrollToPageTop;
import com.pearson.itautomation.bca.Browser;
import com.pearson.itautomation.bca.BrowserAction;
import com.pearson.itautomation.bca.BrowserElement;

/**
 * This Class will Delete a Branching Profile Rule.<br>
 * <b>checkPrestate</b> - N/A <br>
 * <b>checkPostate</b> - This method will return true if Validation messages
 * appears successfully <br>
 * <b>perform</b> - This method will return true if all the mandatory fields
 * have been filled in and saved successfully.
 * 
 * @author Deepak Radhakrishnan
 */

public class DeleteBPRule extends BrowserAction {

    private static final Logger LOGGER = LoggerFactory
            .getLogger(DeleteBPRule.class);

    private static final String OR_AREA = "OR_TestSetup";
    private static final String OR_DELETE_BRANCHING_PROFILE_RULE = "DeleteBPRule";
    private static final String OR_DELETE_YES = "DeleteBPRuleYesBtn";
    private final BrowserElement btnYes;
    private final BrowserElement lnkDeleteBranchingProfileRule;

    private static int ruleNumber;

    /**
     * This constructor will initialize the mandatory fields and the
     * BrowserElements from this class
     * 
     * @param browser
     * @param strBranchingProfileTitle
     *            The BranchingProfile title with timestamp
     * @param strNewRuleTypeRuleRule1
     *            'New Rule Type Value' for Add Another Rule for Rule1
     * @param strNewRuleTypeRuleRule2
     *            'New Rule Type Value' for Add Another Rule for Rule2
     * @param strAdministerBackgroundQuestionGroup
     *            Value for 'Administer the Background Question Group'
     * 
     */

    public DeleteBPRule(final Browser browser, int ruleNumber) {
        super(browser);
        this.btnYes = browser.getBrowserElement(OR_AREA, OR_DELETE_YES);
        setRuleNumber(ruleNumber);
        this.lnkDeleteBranchingProfileRule = browser.getBrowserElement(OR_AREA,
                OR_DELETE_BRANCHING_PROFILE_RULE).formatWithParms(
                getRuleNumber());
    }

    @Override
    protected boolean perform() {
        boolean result = true;
        if (!new ScrollToPageTop(getBrowser()).performWithStates()) {
            LOGGER.error("Not able to scroll to page top");

        }

        if (!new LeftClickOnModalPopupButton(getBrowser(),
                this.lnkDeleteBranchingProfileRule).performWithStates()) {
            LOGGER.error("Not able to click on Delete Rule : "
                    + getRuleNumber());
            result = false;
        }
        if (!new LeftClickOnModalPopupButton(getBrowser(), this.btnYes)
                .performWithStates()) {
            LOGGER.error("Not able to click on Yes Button");
            result = false;
        }
        try {
            Thread.sleep(500);
        } catch (InterruptedException e) {
            LOGGER.error("Error while waiting");
        }
        return result;
    }

    private static int getRuleNumber() {
        return ruleNumber;
    }

    private static void setRuleNumber(int ruleNumber) {
        DeleteBPRule.ruleNumber = ruleNumber;
    }

}
