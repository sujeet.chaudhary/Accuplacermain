package com.pearson.itautomation.accuplacer.testsetup.branchingprofiles;

import org.openqa.selenium.support.ui.WebDriverWait;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.pearson.itautomation.accuplacer.core.WaitForPageLoadPredicate;
import com.pearson.itautomation.accuplacer.core.action.LeftClickOnModalPopupButton;
import com.pearson.itautomation.accuplacer.interaction.ScrollToPageTop;
import com.pearson.itautomation.bca.Browser;
import com.pearson.itautomation.bca.BrowserAction;
import com.pearson.itautomation.bca.BrowserElement;
import com.pearson.itautomation.bca.SelectorType;
import com.pearson.itautomation.bca.interaction.DropdownSelectionAction;
import com.pearson.itautomation.bca.interaction.LeftClickElementAction;
import com.pearson.itautomation.bca.interaction.SendTextToElementAction;
import com.pearson.itautomation.bca.validation.ElementExistsValidation;

/**
 * This Class will fill out the Add Branching Profile Form with all the
 * mandatory details.<br>
 * <b>checkPrestate</b> - N/A <br>
 * <b>checkPostate</b> - This method will return true if Validation messages
 * appears successfully <br>
 * <b>perform</b> - This method will return true if all the mandatory fields
 * have been filled in and saved successfully.
 * 
 * @author Sujeet Chaudhary
 */

public class AddBranchingProfile extends BrowserAction {

    private static final Logger LOGGER = LoggerFactory
            .getLogger(AddBranchingProfile.class);

    private static final String OR_AREA = "OR_TestSetup";
    private static final String OR_BRANCHINGPROFILE_TITLE = "OR_BracchingProfileTitle";
    private static final String OR_ADD_ANOTHER_RULE1 = "AddAnotherRuleForAddingRule1";
    private static final String OR_BELOW_THIS_RULE_RULE1 = "BelowThisRuleForAddingRule1";
    private static final String OR_NEW_RULETYPE_RULE = "NewRuleTypeRule";
    private static final String OR_NEW_RULETYPE_ADD = "NewRuleTypeAdd";
    private static final String OR_ADD_ANOTHER_RULE2 = "AddAnotherRule2";
    private static final String OR_BELOW_THIS_RULE_RULE2 = "BelowThisRuleRule2";
    private static final String OR_SAVE = "Save";
    private static final String OR_ADD_BRANCHINGPROFILE = "AddBranchingProfile";
    private static final String OR_VALIDATION_MSG1 = "Validation_Msg1";
    private static final String OR_VALIDATION_TEST_SETTING_DROP_DOWN = "ValidatetestSettingInDropDown";

    private final BrowserElement btnAddBranchingProfile;
    private final BrowserElement txtBranchingProfilTitle;
    private final BrowserElement btnAddAnotherRuleForAddingRule1;
    private final BrowserElement lnkBelowThisRuleForAddingRule1;
    private final BrowserElement drpNewRuleTypeRule;
    private final BrowserElement btnNewRuleTypeAdd;
    private final BrowserElement btnAddAnotherRule2;
    private final BrowserElement lnkBelowThisRule2;
    private final BrowserElement btnSave;
    private final BrowserElement txtMsg1;
    private final BrowserElement optionValidatetestSettingInDropDown;

    private final String strBranchingProfileTitle;
    private final String strNewRuleTypeRuleRule1;
    private final String strNewRuleTypeRuleRule2;

    /**
     * This constructor will initialize the mandatory fields and the
     * BrowserElements from this class
     * 
     * @param browser
     * @param strBranchingProfileTitle
     *            The BranchingProfile title with timestamp
     * @param strNewRuleTypeRuleRule1
     *            'New Rule Type Value' for Add Another Rule for Rule1
     * @param strNewRuleTypeRuleRule2
     *            'New Rule Type Value' for Add Another Rule for Rule2
     */

    public AddBranchingProfile(final Browser browser,
            String strBranchingProfileTitle, String strNewRuleTypeRuleRule1,
            String strNewRuleTypeRuleRule2) {
        super(browser);

        this.strBranchingProfileTitle = strBranchingProfileTitle;
        this.strNewRuleTypeRuleRule1 = strNewRuleTypeRuleRule1;
        this.strNewRuleTypeRuleRule2 = strNewRuleTypeRuleRule2;

        this.btnAddBranchingProfile = browser.getBrowserElement(OR_AREA,
                OR_ADD_BRANCHINGPROFILE);
        this.txtBranchingProfilTitle = browser.getBrowserElement(OR_AREA,
                OR_BRANCHINGPROFILE_TITLE);
        this.btnSave = browser.getBrowserElement(OR_AREA, OR_SAVE);
        this.txtMsg1 = browser.getBrowserElement(OR_AREA, OR_VALIDATION_MSG1);
        this.btnAddAnotherRuleForAddingRule1 = browser.getBrowserElement(
                OR_AREA, OR_ADD_ANOTHER_RULE1);
        this.btnAddAnotherRule2 = browser.getBrowserElement(OR_AREA,
                OR_ADD_ANOTHER_RULE2);
        this.lnkBelowThisRuleForAddingRule1 = browser.getBrowserElement(
                OR_AREA, OR_BELOW_THIS_RULE_RULE1);
        this.lnkBelowThisRule2 = browser.getBrowserElement(OR_AREA,
                OR_BELOW_THIS_RULE_RULE2);
        this.drpNewRuleTypeRule = browser.getBrowserElement(OR_AREA,
                OR_NEW_RULETYPE_RULE);
        this.btnNewRuleTypeAdd = browser.getBrowserElement(OR_AREA,
                OR_NEW_RULETYPE_ADD);
        this.optionValidatetestSettingInDropDown = browser.getBrowserElement(
                OR_AREA, OR_VALIDATION_TEST_SETTING_DROP_DOWN);
    }

    @Override
    protected boolean perform() {
        boolean result = true;

        if (!new LeftClickOnModalPopupButton(getBrowser(),
                this.btnAddBranchingProfile).performWithStates()) {
            LOGGER.error("Not able to click on Add Button");
            return false;
        }

        if (!new ElementExistsValidation(getBrowser(),
                this.btnAddAnotherRuleForAddingRule1).performWithStates()) {
            LOGGER.error("Not able to load Validation Add Another Rule Button ");
            result = false;
        }

        if (!new SendTextToElementAction(getBrowser(),
                this.txtBranchingProfilTitle, this.strBranchingProfileTitle)
                .performWithStates()) {
            LOGGER.error("Not able to send text to Branching Profile field");
            result = false;
        }

        if (!new LeftClickElementAction(getBrowser(),
                this.btnAddAnotherRuleForAddingRule1).performWithStates()) {
            LOGGER.error("Not able to click on Add another Rule for Rule1 Button");
            return false;
        }

        if (!new LeftClickOnModalPopupButton(getBrowser(),
                this.lnkBelowThisRuleForAddingRule1).performWithStates()) {
            LOGGER.error("Not able to click on Below this Rule for Rule1 Link");
            return false;
        }

        if (!new ElementExistsValidation(getBrowser(),
                this.optionValidatetestSettingInDropDown).performWithStates()) {
            LOGGER.error("Not able to Validate Drop Down data");
            return false;
        }

        if (!new DropdownSelectionAction(getBrowser(), this.drpNewRuleTypeRule,
                SelectorType.VISIBLE_TEXT, this.strNewRuleTypeRuleRule1)
                .performWithStates(false, true)) {
            LOGGER.error("Not able to select New Rule Type for Rule1");
            return false;
        }

        if (!new LeftClickOnModalPopupButton(getBrowser(),
                this.btnNewRuleTypeAdd).performWithStates()) {
            LOGGER.error("Not able to Add New Rule for Rule1");
            return false;
        }

        try {
            Thread.sleep(500);
        } catch (InterruptedException e) {
            LOGGER.warn("Failed to wait for Rule 2 to be visible");
        }

        if (!new LeftClickElementAction(getBrowser(), this.btnAddAnotherRule2)
                .performWithStates()) {
            LOGGER.error("Not able to click on Add another Rule for Rule2 Button");
            return false;
        }

        if (!new LeftClickElementAction(getBrowser(), this.lnkBelowThisRule2)
                .performWithStates()) {
            LOGGER.error("Not able to click on Below this Rule for Rule2 Link");
            return false;
        }

        if (!new ElementExistsValidation(getBrowser(),
                this.optionValidatetestSettingInDropDown).performWithStates()) {
            LOGGER.error("Not able to Validate Drop Down data");
            return false;
        }

        if (!new DropdownSelectionAction(getBrowser(), this.drpNewRuleTypeRule,
                SelectorType.VISIBLE_TEXT, this.strNewRuleTypeRuleRule2)
                .performWithStates()) {
            LOGGER.error("Not able to select New Rule Type for Rule2");
            return false;
        }

        if (!new LeftClickOnModalPopupButton(getBrowser(),
                this.btnNewRuleTypeAdd).performWithStates()) {
            LOGGER.error("Not able to Add New Rule for Rule2");
            return false;
        }

        try {
            Thread.sleep(500);
        } catch (InterruptedException e) {
            LOGGER.warn("Failed to wait for Save to be visible");
        }

        if (!new ScrollToPageTop(getBrowser()).performWithStates()) {
            LOGGER.error("Not able go top of the page");
            return false;

        }

        if (!new LeftClickElementAction(getBrowser(), this.btnSave)
                .performWithStates()) {
            LOGGER.error("Not able to click on Save Button");
            return false;

        }
        WebDriverWait waitForPageLoad = new WebDriverWait(getBrowser()
                .getWebDriver(), 60);
        waitForPageLoad.until(new WaitForPageLoadPredicate());
        return result;
    }

    @Override
    protected boolean checkPostState() {
        boolean result = true;

        if (!new ElementExistsValidation(getBrowser(), this.txtMsg1)
                .performWithStates()) {
            LOGGER.error("Not able to load Validation Msg ");
            result = false;
        }

        return result;
    }

}
