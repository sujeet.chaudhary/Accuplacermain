package com.pearson.itautomation.accuplacer.testsetup.branchingprofiles;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.pearson.itautomation.accuplacer.interaction.ScrollToPageEnd;
import com.pearson.itautomation.bca.Browser;
import com.pearson.itautomation.bca.BrowserAction;
import com.pearson.itautomation.bca.BrowserElement;
import com.pearson.itautomation.bca.SelectorType;
import com.pearson.itautomation.bca.interaction.DropdownSelectionAction;
import com.pearson.itautomation.bca.validation.ElementExistsValidation;

/**
 * This Class Add Apply Test Settings Branching profile Rule<br>
 * <b>checkPrestate</b> - N/A <br>
 * <b>checkPostate<</b> - N/A <br>
 * <b>perform</b> - This method will return true if the Rule is added
 * successfully with the Test
 * 
 * @author Deepak Radhakrishnan
 */

public class AddTestSettingRule extends BrowserAction {

    private static final Logger LOGGER = LoggerFactory
            .getLogger(AddTestSettingRule.class);

    private static final String OR_AREA = "OR_TestSetup";
    private static final String OR_VALIDATE_ADD_RULE = "elementValidateAddRule";
    private static final String OR_SELECT_TEST = "selectTest";
    private static final String TEST_SETTING_RULE = "Apply Test Settings";
    private String testSettingName;
    private int ruleNumber;

    private final BrowserElement drpTestSettingNameSelect;
    private final BrowserElement elementValidateAddRule;

    /**
     * This constructor will initialize the mandatory fields and the
     * BrowserElements from this class
     * 
     * @param browser
     * @param ruleNumber
     *            The Rule Position
     * @param testSettingName
     *            name of the Test Setting
     */
    public AddTestSettingRule(final Browser browser, int ruleNumber,
            String testSettingName) {

        super(browser);
        setTestSettingName(testSettingName);
        setRuleNumber(ruleNumber);
        this.elementValidateAddRule = browser.getBrowserElement(OR_AREA,
                OR_VALIDATE_ADD_RULE).formatWithParms(getRuleNumber());
        this.drpTestSettingNameSelect = browser.getBrowserElement(OR_AREA,
                OR_SELECT_TEST).formatWithParms(getRuleNumber(),
                "Apply the testing options");
    }

    @Override
    protected boolean perform() {
        boolean result = true;

        if (!new AddBranchingProfileRule(this.getBrowser(), getRuleNumber(),
                TEST_SETTING_RULE).performWithStates()) {
            LOGGER.error("Not able Add Test Setting  Rule");
            result = false;
        }
        if (!new ElementExistsValidation(this.getBrowser(),
                this.elementValidateAddRule).performWithStates()) {
            LOGGER.error("Not able validate the added Rule");
            return false;
        }
        if (!new ScrollToPageEnd(getBrowser()).performWithStates()) {
            LOGGER.error("Not able to Scroll to Page End");
            result = false;
        }
        if (!new DropdownSelectionAction(getBrowser(),
                this.drpTestSettingNameSelect, SelectorType.VISIBLE_TEXT,
                getTestSettingName()).performWithStates(false, true)) {
            LOGGER.error("Not able to select THe Test" + getTestSettingName());
            result = false;
        }
        return result;
    }

    private String getTestSettingName() {
        return testSettingName;
    }

    private void setTestSettingName(String testName) {
        this.testSettingName = testName;
    }

    public int getRuleNumber() {
        return ruleNumber;
    }

    public void setRuleNumber(int ruleNumber) {
        this.ruleNumber = ruleNumber;
    }

}
