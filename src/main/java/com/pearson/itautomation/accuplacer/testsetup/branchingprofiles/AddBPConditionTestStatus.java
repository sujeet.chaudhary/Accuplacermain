package com.pearson.itautomation.accuplacer.testsetup.branchingprofiles;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.pearson.itautomation.accuplacer.core.action.DropDownSelectByNormalizedRegEx;
import com.pearson.itautomation.accuplacer.interaction.ScrollToPageEnd;
import com.pearson.itautomation.bca.Browser;
import com.pearson.itautomation.bca.BrowserAction;
import com.pearson.itautomation.bca.BrowserElement;

/**
 * This Class Add ad condition <i><u>Test Was Completed</i></u> <b>OR</b>
 * <i><u>Test Not Taken</i></u> depending on the param passed to the constructor<br>
 * <b>checkPrestate</b> - N/A <br>
 * <b>checkPostate<</b> - N/A <br>
 * <b>perform</b> - This method will return true if the Condition is added
 * successfully with the Test
 * 
 * @author Deepak Radhakrishnan
 */

public class AddBPConditionTestStatus extends BrowserAction {

    private static final Logger LOGGER = LoggerFactory
            .getLogger(AddBPConditionTestStatus.class);

    private static final String OR_AREA = "OR_TestSetup";
    private static final String OR_SELECT_CONDITION = "selectCondition";
    private String testName;
    private int ruleNumber;
    private String conditionType;
    private String conditionForDropdown;

    private final BrowserElement drpTestNameSelect;

    /**
     * This constructor will initialize the mandatory fields and the
     * BrowserElements from this class
     * 
     * @param browser
     * @param ruleNumber
     * @param testName
     * @param conditionType
     *            Type of the condition either<i><u>Test Was Completed</i></u>
     *            <b>OR</b> <i><u>Test Not Taken</i></u>
     */
    public AddBPConditionTestStatus(final Browser browser, int ruleNumber,
            String testName, String conditionType) {

        super(browser);
        setTestName(testName);
        setRuleNumber(ruleNumber);
        setConditionType(conditionType);
        if (getConditionType().equalsIgnoreCase("Test Was Completed")) {
            conditionForDropdown = "Test was completed";
        } else {
            conditionForDropdown = "Test not taken";
        }
        this.drpTestNameSelect = browser.getBrowserElement(OR_AREA,
                OR_SELECT_CONDITION).formatWithParms(getRuleNumber(),
                conditionForDropdown);

    }

    @Override
    protected boolean perform() {
        boolean result = true;

        if (!(conditionType.equalsIgnoreCase("Test Was Completed") || conditionType
                .equalsIgnoreCase("Test Not Taken"))) {
            LOGGER.error("Condition Type Should be either 'Test Was Completed' or 'Test Not Taken' . User has provided : "
                    + conditionType);
            return false;
        }
        if (!new AddBranchingProfileCondition(this.getBrowser(),
                getRuleNumber(), getConditionType()).performWithStates()) {
            LOGGER.error("Not able Add condition : " + getConditionType());
            result = false;
        }

        if (!new ScrollToPageEnd(getBrowser()).performWithStates()) {
            LOGGER.error("Not able to Scroll to Page End");
            result = false;
        }

        if (!new DropDownSelectByNormalizedRegEx(getBrowser(),
                this.drpTestNameSelect, getTestName()).performWithStates()) {
            LOGGER.error("Not able to select THe Test" + getTestName());
            result = false;
        }

        return result;
    }

    private String getTestName() {
        return testName;
    }

    private void setTestName(String testName) {
        this.testName = testName;
    }

    public int getRuleNumber() {
        return ruleNumber;
    }

    public void setRuleNumber(int ruleNumber) {
        this.ruleNumber = ruleNumber;
    }

    private String getConditionType() {
        return conditionType;
    }

    private void setConditionType(String conditionType) {
        this.conditionType = conditionType;
    }

}
