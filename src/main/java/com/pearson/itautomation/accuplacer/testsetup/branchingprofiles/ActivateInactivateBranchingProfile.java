package com.pearson.itautomation.accuplacer.testsetup.branchingprofiles;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.pearson.itautomation.bca.Browser;
import com.pearson.itautomation.bca.BrowserAction;
import com.pearson.itautomation.bca.BrowserElement;
import com.pearson.itautomation.bca.interaction.LeftClickElementAction;
import com.pearson.itautomation.bca.validation.ElementExistsValidation;

/**
 * This Class will Activate/Inactivate Created Branching Profile. <<br>
 * <b>checkPrestate</b> -N/A <br>
 * <b>checkPostate</b> - This method will verify whether we are on correct page
 * or not. It will return true if we are on correct page otherwise false. <br>
 * <b>perform</b> - This method will return true if Branching Profile will be
 * Activated/Inactivated.
 * 
 * @author Sujeet Chaudhary
 */
public class ActivateInactivateBranchingProfile extends BrowserAction {

    private static final Logger LOGGER = LoggerFactory
            .getLogger(ActivateInactivateBranchingProfile.class);

    private static final String OR_AREA = "OR_TestSetup";
    private static final String OR_ACTIVATE_INACTIVATE_BRANCHINGPROFILE_XPATH = "BranchingProfileActivateInactivateXpath";
    private static final String OR_VALIDATION_MESSAGE_XPATH = "BranchingProfileActiveInactiveValidatinMessage";

    private final BrowserElement btnActiveInactiveBranchingProfile;
    private final BrowserElement txtActiveInactiveValidationMsg;

    /**
     * This constructor will initialize the mandatory fields and the
     * BrowserElements from this class
     * 
     * @param browser
     * @param strBranchingProfileTitle
     *            The BranchingProfile title which need to be View
     * @param actionRequred
     *            Action requred either Activate or Inactivate
     * @param index
     *            Indexes required to select correct XPath
     */

    public ActivateInactivateBranchingProfile(final Browser browser,
            String strBranchingProfileTitle, int index) {
        super(browser);

        this.btnActiveInactiveBranchingProfile = browser.getBrowserElement(
                OR_AREA, OR_ACTIVATE_INACTIVATE_BRANCHINGPROFILE_XPATH)
                .formatWithParms(strBranchingProfileTitle,
                        strBranchingProfileTitle, index);

        this.txtActiveInactiveValidationMsg = browser.getBrowserElement(
                OR_AREA, OR_VALIDATION_MESSAGE_XPATH);

    }

    @Override
    protected boolean checkPreState() {
        boolean result = true;

        if (!new ElementExistsValidation(getBrowser(),
                this.btnActiveInactiveBranchingProfile).performWithStates()) {
            LOGGER.error("Not able to Validate Activate/Inactivate Btn");
            result = false;
        }
        return result;
    }

    @Override
    protected boolean checkPostState() {
        boolean result = true;
        if (!new ElementExistsValidation(getBrowser(),
                this.txtActiveInactiveValidationMsg).performWithStates()) {
            LOGGER.error("Not able to Validate msg");
            result = false;
        }

        return result;
    }

    @Override
    protected boolean perform() {
        boolean result = true;
        if (!new LeftClickElementAction(getBrowser(),
                this.btnActiveInactiveBranchingProfile).performWithStates()) {
            LOGGER.error("Not able to click on Activate/Inactivate Button");
            result = false;
        }
        return result;
    }
}
