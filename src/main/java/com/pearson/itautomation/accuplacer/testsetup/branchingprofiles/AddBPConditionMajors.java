package com.pearson.itautomation.accuplacer.testsetup.branchingprofiles;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.pearson.itautomation.accuplacer.core.action.LeftClickOnModalPopupButton;
import com.pearson.itautomation.accuplacer.interaction.ScrollToPageEnd;
import com.pearson.itautomation.bca.Browser;
import com.pearson.itautomation.bca.BrowserAction;
import com.pearson.itautomation.bca.BrowserElement;
import com.pearson.itautomation.bca.interaction.LeftClickElementAction;

/**
 * This Class Add a condition <i><u>Majors</i></u> <br>
 * <b>checkPrestate</b> - N/A <br>
 * <b>checkPostate<</b> - N/A <br>
 * <b>perform</b> - This method will return true if the Condition is added
 * successfully with the Test
 * 
 * @author Deepak Radhakrishnan
 */

public class AddBPConditionMajors extends BrowserAction {

    private static final Logger LOGGER = LoggerFactory
            .getLogger(AddBPConditionMajors.class);

    private static final String OR_AREA = "OR_TestSetup";
    private static final String OR_MAJOR_LIST_BUTTON = "majorListButton";
    private static final String OR_MAJOR_LIST_ADD = "majorListADD";
    private static final String OR_OPTION_VALUE = "optionValueSelectMajor";

    private int ruleNumber;
    private String conditionType;
    private final BrowserElement btnAddMajorList;
    private final BrowserElement btnAddMajor;
    private final BrowserElement optionList;

    private final String[] tokenizedMajorList;

    private String[] majorName;

    /**
     * This constructor will initialize the mandatory fields and the
     * BrowserElements from this class
     * 
     * @param browser
     * @param ruleNumber
     *            The Rule Number to be added
     * @param listMajors
     *            The comma seperated Majors
     */
    public AddBPConditionMajors(final Browser browser, int ruleNumber,
            String listMajors) {
        super(browser);
        tokenizedMajorList = listMajors.split(",");
        if (tokenizedMajorList[0].contains("*** ")) {
            LOGGER.info("Reassining the demarcation prefix");
            for (int i = 1; i < tokenizedMajorList.length; i++) {
                tokenizedMajorList[i] = "*** " + tokenizedMajorList[i];
            }
        }

        setMajorName(tokenizedMajorList);
        setRuleNumber(ruleNumber);
        setConditionType("Major");
        this.btnAddMajorList = browser.getBrowserElement(OR_AREA,
                OR_MAJOR_LIST_BUTTON).formatWithParms(getRuleNumber());
        this.btnAddMajor = browser
                .getBrowserElement(OR_AREA, OR_MAJOR_LIST_ADD);

        this.optionList = browser.getBrowserElement(OR_AREA, OR_OPTION_VALUE);

    }

    @Override
    protected boolean perform() {
        boolean result = true;
        if (!new AddBranchingProfileCondition(this.getBrowser(),
                getRuleNumber(), getConditionType()).performWithStates()) {
            LOGGER.error("Not able Add condition : " + getConditionType());
            return false;
        }

        if (!new ScrollToPageEnd(getBrowser()).performWithStates()) {
            LOGGER.error("Not able to Scroll to Page End");
            result = false;
        }

        if (!new LeftClickOnModalPopupButton(getBrowser(), btnAddMajorList)
                .performWithStates()) {
            LOGGER.error("Not able to click on Add Major List Button");
            result = false;
        }
        try {
            Thread.sleep(500);
        } catch (InterruptedException e) {
            LOGGER.error("Failed to Wait for the modal window transition");

        }
        for (int k = 0; k < majorName.length; k++) {
            if (!new LeftClickElementAction(getBrowser(),
                    this.optionList.formatWithParms(majorName[k]))
                    .performWithStates()) {
                LOGGER.error("Not able to Click on the Major : " + majorName[k]);
                result = false;
            }
        }

        if (!new LeftClickOnModalPopupButton(getBrowser(), btnAddMajor)
                .performWithStates()) {
            LOGGER.error("Not able to click on Add Majors");
            result = false;
        }
        return result;
    }

    public int getRuleNumber() {
        return ruleNumber;
    }

    public void setRuleNumber(int ruleNumber) {
        this.ruleNumber = ruleNumber;
    }

    private String getConditionType() {
        return conditionType;
    }

    private void setConditionType(String conditionType) {
        this.conditionType = conditionType;
    }

    public String[] getMajorNameName() {
        return majorName;
    }

    public void setMajorName(String majorName[]) {
        this.majorName = majorName;
    }

}
