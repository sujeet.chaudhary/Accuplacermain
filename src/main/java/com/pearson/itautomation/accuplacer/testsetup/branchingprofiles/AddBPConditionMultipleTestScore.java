package com.pearson.itautomation.accuplacer.testsetup.branchingprofiles;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.pearson.itautomation.accuplacer.core.action.DropDownSelectByNormalizedRegEx;
import com.pearson.itautomation.accuplacer.core.action.LeftClickOnModalPopupButton;
import com.pearson.itautomation.accuplacer.interaction.ScrollToPageEnd;
import com.pearson.itautomation.bca.Browser;
import com.pearson.itautomation.bca.BrowserAction;
import com.pearson.itautomation.bca.BrowserElement;
import com.pearson.itautomation.bca.SelectorType;
import com.pearson.itautomation.bca.interaction.ClearTextElementAction;
import com.pearson.itautomation.bca.interaction.DropdownSelectionAction;
import com.pearson.itautomation.bca.interaction.SendTextToElementAction;

/**
 * This Class Add a condition <i><u>Single Test Score</i></u> <br>
 * <b>checkPrestate</b> - N/A <br>
 * <b>checkPostate<</b> - N/A <br>
 * <b>perform</b> - This method will return true if the Condition is added
 * successfully with the Test
 * 
 * @author Deepak Radhakrishnan
 */

public class AddBPConditionMultipleTestScore extends BrowserAction {

    private static final Logger LOGGER = LoggerFactory
            .getLogger(AddBPConditionMultipleTestScore.class);

    private static final String OR_AREA = "OR_TestSetup";
    private static final String OR_SELECT_TESTS = "multipleTestScoreTests";
    private static final String OR_MULTIPLE_SCORE_COMPARE_VALUE = "multipleScoreConditionScoreCompare";
    private static final String OR_MULTIPLE_SCORE_OPERATOR_VALUE = "multipleScoreOperatorValue";
    private static final String OR_MULTIPLE_SUBCONDITION_SINGLE_TEST_SCORE = "multipleScoreSubConditionSingleTest";
    private static final String OR_MULTIPLE_SUBCONDITION_SINGLE_TEST_LINK = "multipleScoreSubConditionLinkSingleTest";
    private static final String OR_NEW_CONDITION_TYPE = "NewRuleTypeCondition";
    private static final String OR_ADD_CONDITION_BUTTON = "conditionAddButton";

    private int ruleNumber;
    private String conditionType;
    private final BrowserElement drpTestNameSelect;
    private final BrowserElement drpOperatorValue;
    private final BrowserElement txtScore;
    private final BrowserElement btnAddAnotherSingleTestBPCondition;
    private final BrowserElement lnkAddAnotherSingleTestBPCondition;
    private final BrowserElement drpNewRuleTypeCondition;
    private final BrowserElement btnNewConditionTypeAdd;

    private final String[] multipleTestScoreConditionAndOperator;
    private final String[] multipleTestScoreTestValues;
    private final String scoreValue;

    private String[] TestNames;
    private String strOperator;

    /**
     * This constructor will initialize the mandatory fields and the
     * BrowserElements from this class
     * 
     * @param browser
     * @param ruleNumber
     *            The Rule Number to be added
     * @param singleTestCondition
     *            THe Name of the Test in the condition
     */
    public AddBPConditionMultipleTestScore(final Browser browser,
            int ruleNumber, String singleTestCondition) {
        super(browser);
        multipleTestScoreConditionAndOperator = singleTestCondition.split(",");
        multipleTestScoreTestValues = multipleTestScoreConditionAndOperator[0]
                .split("/");
        if (multipleTestScoreTestValues[0].contains("*** ")) {
            LOGGER.info("Reassining the demarcation prefix");
            for (int i = 1; i < multipleTestScoreTestValues.length; i++) {
                multipleTestScoreTestValues[i] = "*** "
                        + multipleTestScoreTestValues[i];
            }
        }
        setTestName(multipleTestScoreTestValues);
        strOperator = (multipleTestScoreConditionAndOperator[1]);
        this.scoreValue = multipleTestScoreConditionAndOperator[2];

        setRuleNumber(ruleNumber);
        setConditionType("Multiple Test Scores");
        this.drpTestNameSelect = browser.getBrowserElement(OR_AREA,
                OR_SELECT_TESTS);
        this.drpOperatorValue = browser.getBrowserElement(OR_AREA,
                OR_MULTIPLE_SCORE_OPERATOR_VALUE).formatWithParms(
                getRuleNumber());
        this.btnAddAnotherSingleTestBPCondition = browser.getBrowserElement(
                OR_AREA, OR_MULTIPLE_SUBCONDITION_SINGLE_TEST_SCORE);
        lnkAddAnotherSingleTestBPCondition = browser.getBrowserElement(OR_AREA,
                OR_MULTIPLE_SUBCONDITION_SINGLE_TEST_LINK);
        this.btnNewConditionTypeAdd = browser.getBrowserElement(OR_AREA,
                OR_ADD_CONDITION_BUTTON);
        txtScore = browser.getBrowserElement(OR_AREA,
                OR_MULTIPLE_SCORE_COMPARE_VALUE).formatWithParms(
                getRuleNumber());
        this.drpNewRuleTypeCondition = browser.getBrowserElement(OR_AREA,
                OR_NEW_CONDITION_TYPE);

    }

    @Override
    protected boolean perform() {
        boolean result = true;
        if (multipleTestScoreConditionAndOperator.length < 3) {
            LOGGER.error("Condition +"
                    + getRuleNumber()
                    + " value should have 3 params seperated by comma {Test Name ,equal to OR not equal to , Numeric score Value}");
            return false;
        }
        if (!new AddBranchingProfileCondition(this.getBrowser(),
                getRuleNumber(), getConditionType()).performWithStates()) {
            LOGGER.error("Not able Add condition : " + getConditionType());
            result = false;
        }

        if (!new ScrollToPageEnd(getBrowser()).performWithStates()) {
            LOGGER.error("Not able to Scroll to Page End");
            result = false;
        }
        if (!new ClearTextElementAction(getBrowser(),
                this.txtScore.formatWithParms(getRuleNumber()))
                .performWithStates()) {
            LOGGER.error("Not able to Clear Text Response");
            result = false;
        }

        if (!new SendTextToElementAction(getBrowser(),
                this.txtScore.formatWithParms(getRuleNumber()), scoreValue)
                .performWithStates()) {
            LOGGER.error("Not able to type the score value");
            result = false;
        }

        if (strOperator.equalsIgnoreCase("equal to")) {
            LOGGER.info("Operator selected to 'equal to'");
        }

        else if (!new DropDownSelectByNormalizedRegEx(getBrowser(),

        this.drpOperatorValue.formatWithParms(getRuleNumber()), strOperator)
                .performWithStates()) {
            LOGGER.error("Not able to select the Operator");
            result = false;
        }
        if (!new DropDownSelectByNormalizedRegEx(getBrowser(),
                this.drpTestNameSelect.formatWithParms(getRuleNumber(), 1),
                TestNames[0]).performWithStates()) {

            LOGGER.error("Not able to select the Test");
            result = false;

        }
        for (int i = 2; i <= TestNames.length; i++) {
            if (!new LeftClickOnModalPopupButton(getBrowser(),
                    this.btnAddAnotherSingleTestBPCondition.formatWithParms(
                            getRuleNumber(), i - 1)).performWithStates()) {
                LOGGER.error("Not able to Click on Add another Sub condition Rule");
                result = false;
            }
            if (!new LeftClickOnModalPopupButton(getBrowser(),
                    this.lnkAddAnotherSingleTestBPCondition.formatWithParms(
                            getRuleNumber(), i - 1)).performWithStates()) {
                LOGGER.error("Not able to Click on the link below the Add another Sub condition Rule");
                result = false;
            }

            if (!new DropdownSelectionAction(getBrowser(),
                    this.drpNewRuleTypeCondition, SelectorType.VISIBLE_TEXT,
                    "Single Test Score").performWithStates(false, true)) {
                LOGGER.error("Not able to select New Condition Type for RULE "
                        + getRuleNumber());
                result = false;
            }

            if (!new LeftClickOnModalPopupButton(getBrowser(),
                    this.btnNewConditionTypeAdd).performWithStates()) {
                LOGGER.error("Not able to click on Add popup button");
                result = false;
            }
            if (!new DropDownSelectByNormalizedRegEx(getBrowser(),
                    this.drpTestNameSelect.formatWithParms(getRuleNumber(), i),
                    TestNames[i - 1]).performWithStates()) {

                LOGGER.error("Not able to select the Test");
                result = false;

            }
        }
        return result;

    }

    public int getRuleNumber() {
        return ruleNumber;
    }

    public void setRuleNumber(int ruleNumber) {
        this.ruleNumber = ruleNumber;
    }

    private String getConditionType() {
        return conditionType;
    }

    private void setConditionType(String conditionType) {
        this.conditionType = conditionType;
    }

    public String[] getTestName() {
        return TestNames;
    }

    public void setTestName(String[] multipleTestScoreTestValues2) {
        this.TestNames = multipleTestScoreTestValues2;
    }

}
