package com.pearson.itautomation.accuplacer.testsetup.branchingprofiles;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.pearson.itautomation.accuplacer.core.action.DropDownSelectByNormalizedRegEx;
import com.pearson.itautomation.accuplacer.interaction.ScrollToPageEnd;
import com.pearson.itautomation.bca.Browser;
import com.pearson.itautomation.bca.BrowserAction;
import com.pearson.itautomation.bca.BrowserElement;
import com.pearson.itautomation.bca.interaction.ClearTextElementAction;
import com.pearson.itautomation.bca.interaction.SendTextToElementAction;

/**
 * This Class Add a condition <i><u>Single Test Score</i></u> <br>
 * <b>checkPrestate</b> - N/A <br>
 * <b>checkPostate<</b> - N/A <br>
 * <b>perform</b> - This method will return true if the Condition is added
 * successfully with the Test
 * 
 * @author Deepak Radhakrishnan
 */

public class AddBPConditionSingleTestScore extends BrowserAction {

    private static final Logger LOGGER = LoggerFactory
            .getLogger(AddBPConditionSingleTestScore.class);

    private static final String OR_AREA = "OR_TestSetup";
    private static final String OR_SELECT_CONDITION = "singleTestCondition";
    private static final String OR_SCORE_VALUE = "conditionScoreCompare";

    private int ruleNumber;
    private String conditionType;
    private final BrowserElement drpTestNameSelect;
    private final BrowserElement txtScore;

    private final String[] singleTestScoreConditionAndOperator;

    private final String scoreValue;
    private String testName;
    private String strOperator;

    /**
     * This constructor will initialize the mandatory fields and the
     * BrowserElements from this class
     * 
     * @param browser
     * @param ruleNumber
     *            The Rule Number to be added
     * @param singleTestCondition
     *            THe Name of the Test in the condition
     */
    public AddBPConditionSingleTestScore(final Browser browser, int ruleNumber,
            String singleTestCondition) {
        super(browser);
        singleTestScoreConditionAndOperator = singleTestCondition.split(",");

        setTestName(singleTestScoreConditionAndOperator[0]);
        strOperator = (singleTestScoreConditionAndOperator[1]);
        this.scoreValue = singleTestScoreConditionAndOperator[2];
        setRuleNumber(ruleNumber);
        setConditionType("Single Test Score");
        this.drpTestNameSelect = browser.getBrowserElement(OR_AREA,
                OR_SELECT_CONDITION);
        txtScore = browser.getBrowserElement(OR_AREA, OR_SCORE_VALUE);

    }

    @Override
    protected boolean perform() {
        boolean result = true;
        if (singleTestScoreConditionAndOperator.length < 3) {
            LOGGER.error("Condition +"
                    + getRuleNumber()
                    + " value should have 3 params seperated by comma {Test Name ,equal to OR not equal to , Numeric score Value}");
            return false;
        }
        if (!new AddBranchingProfileCondition(this.getBrowser(),
                getRuleNumber(), getConditionType()).performWithStates()) {
            LOGGER.error("Not able Add condition : " + getConditionType());
            result = false;
        }

        if (!new ScrollToPageEnd(getBrowser()).performWithStates()) {
            LOGGER.error("Not able to Scroll to Page End");
            result = false;
        }
        if (!new DropDownSelectByNormalizedRegEx(getBrowser(),
                this.drpTestNameSelect.formatWithParms(getRuleNumber(), 1),
                getTestName()).performWithStates()) {

            LOGGER.error("Not able to select the Background Response");
            result = false;
        }
        if (!new ClearTextElementAction(getBrowser(),
                this.txtScore.formatWithParms(getRuleNumber()))
                .performWithStates()) {
            LOGGER.error("Not able to Clear Text Response");
            result = false;
        }

        if (!new SendTextToElementAction(getBrowser(),
                this.txtScore.formatWithParms(getRuleNumber()), scoreValue)
                .performWithStates()) {
            LOGGER.error("Not able to type the score value");
            result = false;
        }

        if (strOperator.equalsIgnoreCase("equal to")) {
            LOGGER.info("Operator selected to 'equal to'");
        }

        else if (!new DropDownSelectByNormalizedRegEx(getBrowser(),

        this.drpTestNameSelect.formatWithParms(getRuleNumber(), 2), strOperator)
                .performWithStates()) {
            LOGGER.error("Not able to select the Operator");
            result = false;
        }

        return result;

    }

    public int getRuleNumber() {
        return ruleNumber;
    }

    public void setRuleNumber(int ruleNumber) {
        this.ruleNumber = ruleNumber;
    }

    private String getConditionType() {
        return conditionType;
    }

    private void setConditionType(String conditionType) {
        this.conditionType = conditionType;
    }

    public String getTestName() {
        return testName;
    }

    public void setTestName(String bQName) {
        this.testName = bQName;
    }

}
