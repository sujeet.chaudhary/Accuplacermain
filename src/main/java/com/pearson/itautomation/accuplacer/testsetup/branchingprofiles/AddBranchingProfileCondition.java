package com.pearson.itautomation.accuplacer.testsetup.branchingprofiles;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.pearson.itautomation.accuplacer.core.action.LeftClickOnModalPopupButton;
import com.pearson.itautomation.bca.Browser;
import com.pearson.itautomation.bca.BrowserAction;
import com.pearson.itautomation.bca.BrowserElement;
import com.pearson.itautomation.bca.SelectorType;
import com.pearson.itautomation.bca.interaction.DropdownSelectionAction;
import com.pearson.itautomation.bca.validation.ElementExistsValidation;

/**
 * This Class will Add a Branching profile condition<br>
 * <b>checkPrestate</b> - N/A <br>
 * <b>checkPostate</b> - This method will return true if Rule is appeared on the
 * screen<br>
 * <b>perform</b> - This method will return true THe Branching profile condition
 * has been created
 * 
 * @author Deepak Radhakrishnan
 */

public class AddBranchingProfileCondition extends BrowserAction {

    private static final Logger LOGGER = LoggerFactory
            .getLogger(AddBranchingProfileCondition.class);

    private static final String OR_AREA = "OR_TestSetup";
    private static final String OR_ADD_ANOTHER_CONDITION = "addCondition";
    private static final String OR_NEW_CONDITION_TYPE = "NewRuleTypeCondition";
    private static final String OR_VALIDATE_ADD_CONDITION = "elementValidateAddCondition";
    private static final String OR_ADD_CONDITION_BUTTON = "conditionAddButton";
    private static final String OR_DPR_ADDBP_OPTION_VALIDATION = "optnValidationNewRuleTypeCondition";
    private final BrowserElement btnAddAnotherCondition;
    private final BrowserElement drpNewRuleTypeCondition;
    private final BrowserElement btnNewConditionTypeAdd;
    private final BrowserElement elementValidateAddCondition;
    private final BrowserElement optnValidationDrpNewRuleTypeCondition;

    private String conditionType;
    private int ruleNumber;

    /**
     * This constructor will initialize the mandatory fields and the
     * BrowserElements from this class
     * 
     * @param browser
     * @param strBranchingProfileTitle
     *            The BranchingProfile title with timestamp
     * @param strNewRuleTypeRuleRule1
     *            'New Rule Type Value' for Add Another Rule for Rule1
     * @param strNewRuleTypeRuleRule2
     *            'New Rule Type Value' for Add Another Rule for Rule2
     * @param strAdministerBackgroundQuestionGroup
     *            Value for 'Administer the Background Question Group'
     * 
     */

    public AddBranchingProfileCondition(final Browser browser, int ruleNumber,
            String conditionType) {
        super(browser);
        setRuleNumber(ruleNumber);
        setConditionType(conditionType);
        this.btnAddAnotherCondition = browser.getBrowserElement(OR_AREA,
                OR_ADD_ANOTHER_CONDITION).formatWithParms(getRuleNumber());
        this.drpNewRuleTypeCondition = browser.getBrowserElement(OR_AREA,
                OR_NEW_CONDITION_TYPE);
        this.optnValidationDrpNewRuleTypeCondition = browser.getBrowserElement(
                OR_AREA, OR_DPR_ADDBP_OPTION_VALIDATION);
        this.btnNewConditionTypeAdd = browser.getBrowserElement(OR_AREA,
                OR_ADD_CONDITION_BUTTON);
        this.elementValidateAddCondition = browser.getBrowserElement(OR_AREA,
                OR_VALIDATE_ADD_CONDITION).formatWithParms(getRuleNumber());
    }

    @Override
    protected boolean perform() {
        boolean result = true;

        if (!("Test Was Completed".equalsIgnoreCase(conditionType)
                || "Test Not Taken".equalsIgnoreCase(conditionType)
                || "Background Question".equalsIgnoreCase(conditionType)
                || "Single test Score".equalsIgnoreCase(conditionType)
                || "Multiple test Scores".equalsIgnoreCase(conditionType)
                || "Background Question".equalsIgnoreCase(conditionType) || "Major"
                    .equalsIgnoreCase(conditionType))) {
            LOGGER.error("Invalid Condition Type. Condition Type Should be either of the one {Test Was Completed,Test Not Taken,Background Question,Single test Score,Multiple Test Scores,Major} ");
            return false;
        }
        if (!new LeftClickOnModalPopupButton(getBrowser(),
                this.btnAddAnotherCondition).performWithStates()) {
            LOGGER.error("Not able to click on Add another Rule for Rule1 Button");
            result = false;
        }

        if (!new ElementExistsValidation(getBrowser(),
                this.optnValidationDrpNewRuleTypeCondition).performWithStates()) {
            LOGGER.error("Not able Find the element");
            result = false;
        }

        if (!new DropdownSelectionAction(getBrowser(),
                this.drpNewRuleTypeCondition, SelectorType.VISIBLE_TEXT,
                getConditionType()).performWithStates(false, true)) {
            LOGGER.error("Not able to select New Rule Type for Rule1");
            result = false;
        }

        if (!new LeftClickOnModalPopupButton(getBrowser(),
                this.btnNewConditionTypeAdd).performWithStates()) {
            LOGGER.error("Not able to click on Add popup button");
            result = false;
        }
        return result;
    }

    @Override
    protected boolean checkPostState() {
        boolean result = true;
        if (!new ElementExistsValidation(getBrowser(),
                this.elementValidateAddCondition).performWithStates()) {
            LOGGER.error("Not able to Find the Newly Added Rule");
            result = false;
        }
        return result;
    }

    private int getRuleNumber() {
        return ruleNumber;
    }

    private void setRuleNumber(int ruleNumber) {
        this.ruleNumber = ruleNumber;
    }

    private String getConditionType() {
        return conditionType;
    }

    private void setConditionType(String strNewConditionType) {
        this.conditionType = strNewConditionType;
    }
}
