package com.pearson.itautomation.accuplacer.testsetup.branchingprofiles;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.pearson.itautomation.accuplacer.core.Buttons;
import com.pearson.itautomation.bca.Browser;
import com.pearson.itautomation.bca.BrowserAction;
import com.pearson.itautomation.bca.BrowserElement;
import com.pearson.itautomation.bca.SelectorType;
import com.pearson.itautomation.bca.interaction.DropdownSelectionAction;
import com.pearson.itautomation.bca.interaction.LeftClickElementAction;
import com.pearson.itautomation.bca.validation.ElementExistsValidation;

/**
 * This Class will Add a branching profile of type provided as parameter<br>
 * <b>checkPrestate</b> - N/A <br>
 * <b>checkPostate</b> - This method will return true if Validation messages
 * appears successfully <br>
 * <b>perform</b> - This method will return true if the branching profile rule
 * has been added successfully
 * 
 * @author Deepak Radhakrishnan
 */

public class AddBranchingProfileRule extends BrowserAction {

    private static final Logger LOGGER = LoggerFactory
            .getLogger(AddBranchingProfileRule.class);

    private static final String OR_AREA = "OR_TestSetup";
    private static final String OR_ADD_ANOTHER_RULE1 = "AddAnotherRule1";
    private static final String OR_BELOW_THIS_RULE_RULE1 = "BelowThisRuleRule1";
    private static final String OR_NEW_RULETYPE_RULE = "NewRuleTypeRule";
    private static final String OR_VALIDATE_ADD_RULE = "elementValidateAddRule";

    private final BrowserElement btnAddAnotherRule;
    private final BrowserElement lnkBelowThisRule;
    private final BrowserElement drpNewRuleTypeRule;
    private final BrowserElement btnNewRuleTypeAdd;
    private final BrowserElement elementValidateAddRule;

    private String ruleType;
    private int ruleNumber;

    /**
     * This constructor will initialize the mandatory fields and the
     * BrowserElements from this class
     * 
     * @param browser
     * @param strBranchingProfileTitle
     *            The BranchingProfile title with timestamp
     * @param ruleNumber
     *            Rule Number position to be added
     * @param strNewRuleTypeRuleRule
     *            Type of Rule to be added
     */

    public AddBranchingProfileRule(final Browser browser, int ruleNumber,
            String strNewRuleTypeRuleRule) {
        super(browser);
        setRuleNumber(ruleNumber);
        setRuleType(strNewRuleTypeRuleRule);
        this.btnAddAnotherRule = browser.getBrowserElement(OR_AREA,
                OR_ADD_ANOTHER_RULE1).formatWithParms(getRuleNumber() - 1);
        this.lnkBelowThisRule = browser.getBrowserElement(OR_AREA,
                OR_BELOW_THIS_RULE_RULE1).formatWithParms(getRuleNumber() - 1);
        this.drpNewRuleTypeRule = browser.getBrowserElement(OR_AREA,
                OR_NEW_RULETYPE_RULE);
        this.btnNewRuleTypeAdd = Buttons.getButtonType1("Add");
        this.elementValidateAddRule = browser.getBrowserElement(OR_AREA,
                OR_VALIDATE_ADD_RULE).formatWithParms(getRuleNumber());
    }

    @Override
    protected boolean perform() {
        boolean result = true;
        if (!("Administer Test".equalsIgnoreCase(ruleType)
                || "Apply Test Settings".equalsIgnoreCase(ruleType) || "Administer Background Question Group"
                    .equalsIgnoreCase(ruleType))) {
            LOGGER.error("Invalid Rule Type. Rule Type Should be either of the one {Administer Test,Test Settings,Administer Background Question Group} ");
            return false;
        }
        if (!new LeftClickElementAction(getBrowser(), this.btnAddAnotherRule)
                .performWithStates()) {
            LOGGER.error("Not able to click on Add another Rule for Rule1 Button");
            return false;
        }
        if (!new LeftClickElementAction(getBrowser(), this.lnkBelowThisRule)
                .performWithStates()) {
            LOGGER.error("Not able to click on Below this Rule for Rule1 Link");
            return false;
        }
        try {
            Thread.sleep(500);
        } catch (InterruptedException e) {
            LOGGER.warn("Failed to wait for Modal Transition");
        }
        if (!new DropdownSelectionAction(getBrowser(), this.drpNewRuleTypeRule,
                SelectorType.VISIBLE_TEXT, getRuleType()).performWithStates(
                false, true)) {
            LOGGER.error("Not able to select New Rule Type for Rule1");
            return false;
        }

        if (!new LeftClickElementAction(getBrowser(), this.btnNewRuleTypeAdd)
                .performWithStates()) {
            LOGGER.error("Not able to Add New Rule for Rule1");
            return false;
        }
        return result;
    }

    @Override
    protected boolean checkPostState() {
        boolean result = true;
        if (!new ElementExistsValidation(getBrowser(),
                this.elementValidateAddRule).performWithStates()) {
            LOGGER.error("Not able to Find the Newly Added Rule");
            return false;
        }
        return result;
    }

    private int getRuleNumber() {
        return ruleNumber;
    }

    private void setRuleNumber(int ruleNumber) {
        this.ruleNumber = ruleNumber;
    }

    private String getRuleType() {
        return ruleType;
    }

    private void setRuleType(String strNewRuleTypeRuleRule) {
        this.ruleType = strNewRuleTypeRuleRule;
    }
}
