package com.pearson.itautomation.accuplacer.testsetup.branchingprofiles;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.pearson.itautomation.bca.Browser;
import com.pearson.itautomation.bca.BrowserAction;
import com.pearson.itautomation.bca.BrowserElement;
import com.pearson.itautomation.bca.interaction.LeftClickElementAction;
import com.pearson.itautomation.bca.validation.ElementExistsValidation;

/**
 * This Class will click on Verify button of BranchingProfile.<br>
 * <b>checkPrestate</b> - Return true if Verify button will available otherwise
 * return false <br>
 * <b>checkPostate</b> - <br>
 * <b>perform</b> -Returns true if Verify button will be clicked successfully.
 * 
 * @author Sujeet Chaudhary
 */
public class ClickOnVerifyBranchingProfile extends BrowserAction {

    private static final Logger LOGGER = LoggerFactory
            .getLogger(ClickOnVerifyBranchingProfile.class);

    private static final String OR_AREA = "OR_TestSetup";
    private static final String OR_VARIFY_BRANCHINGPROFILE_XPATH = "BranchingProfileVerifyXpath";

    private final BrowserElement btnVerifyBranchingProfile;

    /**
     * This constructor will initialize the mandatory fields and the
     * BrowserElements from this class
     * 
     * @param browser
     * @param strBranchingProfileTitle
     *            The BranchingProfile title to be Verify
     * 
     */
    public ClickOnVerifyBranchingProfile(final Browser browser,

    String strBranchingProfileTitle) {
        super(browser);

        this.btnVerifyBranchingProfile = browser.getBrowserElement(OR_AREA,
                OR_VARIFY_BRANCHINGPROFILE_XPATH).formatWithParms(
                strBranchingProfileTitle, strBranchingProfileTitle);
    }

    @Override
    protected boolean checkPreState() {
        boolean result = true;
        if (!new ElementExistsValidation(getBrowser(),
                this.btnVerifyBranchingProfile).performWithStates()) {
            LOGGER.error("Not able to Validate Verify Branching profile Btn");
            result = false;
        }

        return result;
    }

    @Override
    protected boolean perform() {
        boolean result = true;
        if (!new LeftClickElementAction(getBrowser(),
                this.btnVerifyBranchingProfile).performWithStates()) {
            LOGGER.error("Not able to click on verify Button");
            result = false;
        }

        return result;
    }

}
