package com.pearson.itautomation.accuplacer.testsetup.branchingprofiles;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.pearson.itautomation.bca.Browser;
import com.pearson.itautomation.bca.BrowserAction;
import com.pearson.itautomation.bca.BrowserElement;
import com.pearson.itautomation.bca.interaction.LeftClickElementAction;
import com.pearson.itautomation.bca.interaction.SendTextToElementAction;
import com.pearson.itautomation.bca.validation.ElementExistsValidation;

/**
 * This Class will fill out the Add Branching Profile Form with all the
 * mandatory details.<br>
 * <b>checkPrestate</b> - N/A <br>
 * <b>checkPostate</b> - This method will return true ATHe Branching Profile
 * Name has been added<br>
 * <b>perform</b> - This method will return true if all the mandatory fields
 * have been filled in and saved successfully.
 * 
 * @author Deepak Radhakrishnan
 */

public class CreateBranchingProfile extends BrowserAction {

    private static final Logger LOGGER = LoggerFactory
            .getLogger(CreateBranchingProfile.class);

    private static final String OR_AREA = "OR_TestSetup";
    private static final String OR_BRANCHINGPROFILE_TITLE = "OR_BracchingProfileTitle";
    private static final String OR_ADD_BRANCHINGPROFILE = "AddBranchingProfile";
    private static final String OR_SELECT_RULE = "selectTest";

    private final BrowserElement btnAddBranchingProfile;
    private final BrowserElement elementRule2;

    private final BrowserElement txtBranchingProfilTitle;
    private final String strBranchingProfileTitle;

    private final BrowserElement validateBPPageElement;

    /**
     * This constructor will initialize the mandatory fields and the
     * BrowserElements from this class
     * 
     * @param browser
     * @param strBranchingProfileTitle
     *            The BranchingProfile title with timestamp
     * @param strNewRuleTypeRuleRule1
     *            'New Rule Type Value' for Add Another Rule for Rule1
     * @param strNewRuleTypeRuleRule2
     *            'New Rule Type Value' for Add Another Rule for Rule2
     * @param strAdministerBackgroundQuestionGroup
     *            Value for 'Administer the Background Question Group'
     * 
     */

    public CreateBranchingProfile(final Browser browser,

    String strBranchingProfileTitle) {
        super(browser);

        this.strBranchingProfileTitle = strBranchingProfileTitle;

        this.btnAddBranchingProfile = browser.getBrowserElement(OR_AREA,
                OR_ADD_BRANCHINGPROFILE);
        this.validateBPPageElement = browser.getBrowserElement(OR_AREA,
                "ValidateBPPageElement");
        this.elementRule2 = browser.getBrowserElement(OR_AREA, OR_SELECT_RULE)
                .formatWithParms(2, "Administer the Background Question Group");
        elementRule2.setTimeout(20000);
        this.txtBranchingProfilTitle = browser.getBrowserElement(OR_AREA,
                OR_BRANCHINGPROFILE_TITLE);
    }

    @Override
    protected boolean checkPreState() {
        boolean result = true;
        if (!new ElementExistsValidation(getBrowser(),
                this.validateBPPageElement).performWithStates()) {
            LOGGER.error("Not able to find the Branching profile Page");
            result = false;
        }
        return result;
    }

    @Override
    protected boolean perform() {
        boolean result = true;

        if (!new LeftClickElementAction(getBrowser(),
                this.btnAddBranchingProfile).performWithStates()) {
            LOGGER.error("Not able to click on Add Button");
            result = false;
        }
        if (!new ElementExistsValidation(this.getBrowser(), this.elementRule2)
                .performWithStates()) {
            LOGGER.error("Not able find the Rules present by default for a branching profile");
            result = false;
        }
        if (!new SendTextToElementAction(getBrowser(),
                this.txtBranchingProfilTitle, this.strBranchingProfileTitle)
                .performWithStates()) {
            LOGGER.error("Not able to send text to Branching Profile field");
            result = false;
        }
        return result;
    }
}
