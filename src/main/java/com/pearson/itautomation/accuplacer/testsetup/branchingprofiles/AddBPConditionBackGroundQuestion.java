package com.pearson.itautomation.accuplacer.testsetup.branchingprofiles;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.pearson.itautomation.accuplacer.core.action.DropDownSelectByNormalizedRegEx;
import com.pearson.itautomation.accuplacer.interaction.ScrollToPageEnd;
import com.pearson.itautomation.bca.Browser;
import com.pearson.itautomation.bca.BrowserAction;
import com.pearson.itautomation.bca.BrowserElement;
import com.pearson.itautomation.bca.interaction.DropdownSelectionAction;

/**
 * This Class Add a condition <i><u>Background Question</i></u> <br>
 * <b>checkPrestate</b> - N/A <br>
 * <b>checkPostate<</b> - N/A <br>
 * <b>perform</b> - This method will return true if the Condition is added
 * successfully with the Test
 * 
 * @author Deepak Radhakrishnan
 */

public class AddBPConditionBackGroundQuestion extends BrowserAction {

    private static final Logger LOGGER = LoggerFactory
            .getLogger(AddBPConditionBackGroundQuestion.class);

    private static final String OR_AREA = "OR_TestSetup";
    private static final String OR_SELECT_CONDITION = "backgroundQUestionConditionDrp";
    private int ruleNumber;
    private String conditionType;
    private final BrowserElement drpTestNameSelect;

    private final String[] bQConditionAndOperator;
    private String bQName;
    private String strOperator;

    /**
     * This constructor will initialize the mandatory fields and the
     * BrowserElements from this class
     * 
     * @param browser
     * @param ruleNumber
     *            The Rule Number to be added
     * @param backGroundQuestionName
     *            THe Name of the Backround Question
     */
    public AddBPConditionBackGroundQuestion(final Browser browser,
            int ruleNumber, String backGroundQuestionName) {
        super(browser);
        bQConditionAndOperator = backGroundQuestionName.split(",");
        setbQName(bQConditionAndOperator[0]);
        strOperator = (bQConditionAndOperator[1]);
        setRuleNumber(ruleNumber);
        setConditionType("Background Question");
        this.drpTestNameSelect = browser.getBrowserElement(OR_AREA,
                OR_SELECT_CONDITION);
    }

    @Override
    protected boolean perform() {
        boolean result = true;
        if (bQConditionAndOperator.length < 2) {
            LOGGER.error("Condition +"
                    + getRuleNumber()
                    + " value should have 2 params seperated by comma {BQName , equal to OR not equal to}");
            return false;
        }
        if (!new AddBranchingProfileCondition(this.getBrowser(),
                getRuleNumber(), getConditionType()).performWithStates()) {
            LOGGER.error("Not able Add condition : " + getConditionType());
            return false;
        }

        if (!new ScrollToPageEnd(getBrowser()).performWithStates()) {
            LOGGER.error("Not able to Scroll to Page End");
            result = false;
        }
        if (!new DropDownSelectByNormalizedRegEx(getBrowser(),
                this.drpTestNameSelect.formatWithParms(getRuleNumber(), 2),
                getbQName()).performWithStates()) {

            LOGGER.error("Not able to select the Background Response");
            result = false;
        }

        if (!new DropdownSelectionAction(getBrowser(),
                this.drpTestNameSelect.formatWithParms(getRuleNumber(), 4), 2)
                .performWithStates(false, true)) {
            LOGGER.error("Not able to select the Background Response");
            result = false;
        }

        if (strOperator.equalsIgnoreCase("equal to")) {
            LOGGER.info("Operator selected to 'equal to'");
        }

        else if (!new DropDownSelectByNormalizedRegEx(getBrowser(),

        this.drpTestNameSelect.formatWithParms(getRuleNumber(), 3), strOperator)
                .performWithStates()) {
            LOGGER.error("Not able to select the Operator");
            result = false;
        }

        return result;

    }

    public int getRuleNumber() {
        return ruleNumber;
    }

    public void setRuleNumber(int ruleNumber) {
        this.ruleNumber = ruleNumber;
    }

    private String getConditionType() {
        return conditionType;
    }

    private void setConditionType(String conditionType) {
        this.conditionType = conditionType;
    }

    public String getbQName() {
        return bQName;
    }

    public void setbQName(String bQName) {
        this.bQName = bQName;
    }

}
