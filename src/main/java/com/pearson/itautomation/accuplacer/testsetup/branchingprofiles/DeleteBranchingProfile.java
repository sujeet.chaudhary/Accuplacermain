package com.pearson.itautomation.accuplacer.testsetup.branchingprofiles;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.pearson.itautomation.bca.Browser;
import com.pearson.itautomation.bca.BrowserAction;
import com.pearson.itautomation.bca.BrowserElement;
import com.pearson.itautomation.bca.interaction.LeftClickElementAction;
import com.pearson.itautomation.bca.validation.ElementExistsValidation;

/**
 * This Class will Delete the Branching Profile. <<br>
 * <b>checkPrestate</b> -This method will return true if Delete button is
 * available otherwise return falset<br>
 * <b>checkPostate</b> - This method will return true if Delete confirmation msg
 * will appear otherwise return false. <br>
 * <b>perform</b> - This method will return true if Branching Profile will be
 * Deleted successfully.
 * 
 * @author Sujeet Chaudhary
 */
public class DeleteBranchingProfile extends BrowserAction {

    private static final Logger LOGGER = LoggerFactory
            .getLogger(DeleteBranchingProfile.class);

    private static final String OR_AREA = "OR_TestSetup";
    private static final String OR_DELETE_BRANCHINGPROFILE_XPATH = "BranchingProfileDeleteXpath";
    private static final String OR_VALIDATION_MESSAGE_XPATH = "BranchingProfileDeleteValidatinMessage";
    private static final String OR_DELETE_YES = "BranchingProfileDeleteYesButton";

    private final BrowserElement btnDeleteBranchingProfile;
    private final BrowserElement txtDeleteValidationMsg;
    private final BrowserElement btnDeleteYes;

    /**
     * This constructor will initialize the mandatory fields and the
     * BrowserElements from this class
     * 
     * @param browser
     * @param strBranchingProfileTitle
     *            The BranchingProfile title which need to be Delete
     * 
     * 
     */

    public DeleteBranchingProfile(final Browser browser,
            String strBranchingProfileTitle) {
        super(browser);

        this.btnDeleteBranchingProfile = browser.getBrowserElement(OR_AREA,
                OR_DELETE_BRANCHINGPROFILE_XPATH).formatWithParms(
                strBranchingProfileTitle, strBranchingProfileTitle);

        this.txtDeleteValidationMsg = browser.getBrowserElement(OR_AREA,
                OR_VALIDATION_MESSAGE_XPATH);

        this.btnDeleteYes = browser.getBrowserElement(OR_AREA, OR_DELETE_YES);
    }

    @Override
    protected boolean checkPreState() {
        boolean result = true;
        if (!new ElementExistsValidation(getBrowser(),
                this.btnDeleteBranchingProfile).performWithStates()) {
            LOGGER.error("Not able to Validate Delete Btn");
            result = false;
        }

        return result;
    }

    @Override
    protected boolean checkPostState() {
        boolean result = true;

        if (!new ElementExistsValidation(getBrowser(),
                this.txtDeleteValidationMsg).performWithStates()) {
            LOGGER.error("Not able to Validate msg");
            result = false;
        }

        return result;
    }

    @Override
    protected boolean perform() {
        boolean result = true;

        if (!new LeftClickElementAction(getBrowser(),
                this.btnDeleteBranchingProfile).performWithStates()) {
            LOGGER.error("Not able to click on Delete Button");
            result = false;
        }

        if (!new LeftClickElementAction(getBrowser(), this.btnDeleteYes)
                .performWithStates()) {
            LOGGER.error("Not able to click on Yes Button");
            result = false;
        }

        return result;
    }
}
