package com.pearson.itautomation.accuplacer.testsetup.branchingprofiles;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.pearson.itautomation.bca.Browser;
import com.pearson.itautomation.bca.BrowserAction;
import com.pearson.itautomation.bca.BrowserElement;
import com.pearson.itautomation.bca.interaction.LeftClickElementAction;
import com.pearson.itautomation.bca.interaction.SendTextToElementAction;
import com.pearson.itautomation.bca.validation.ElementExistsValidation;

/**
 * This Class will Copy the Branching Profile. <br>
 * <b>checkPrestate</b> - N/A <br>
 * <b>checkPostate</b> - This method will validate the Message <br>
 * <b>perform</b> - This method will return true if Branching Profile will copy
 * successfully.
 * 
 * @author Sujeet Chaudhary
 */
public class CopyBranchingProfile extends BrowserAction {

    private static final Logger LOGGER = LoggerFactory
            .getLogger(CopyBranchingProfile.class);

    private static final String OR_AREA = "OR_TestSetup";
    private static final String OR_NEW_BRANCHINGPROFILE_NAME = "NewBranchingProfileTitle";
    private static final String OR_SAVE = "Save";
    private static final String OR_COPY_VALIDATION_MSG = "CopyValidationMsg";
    private static final String OR_COPY_BRANCHINGPROFILE_XPATH = "BranchingProfileCopyXpath";
    private static final String OR_CANCEL_BUTTON = "CancelButton";

    private static final String OR_SAVE_VALIDATE = "validateBPCopied";

    private final BrowserElement btnCopyBranchingProfile;
    private final BrowserElement txtNewBranchingProfilName;
    private final BrowserElement btnSave;
    private final BrowserElement txtCopyValidationMsg;
    private final BrowserElement btnCancel;
    private final BrowserElement elementExists;

    private final String strNewBranchingProfileName;

    /**
     * This constructor will initialize the mandatory fields and the
     * BrowserElements from this class
     * 
     * @param browser
     * @param strBranchingProfileTitle
     *            The BranchingProfile title which need to be Copy
     * @param strNewBranchingProfileName
     *            The BranchingProfile New Name for copy purpose
     * 
     */
    public CopyBranchingProfile(final Browser browser,
            String strBranchingProfileTitle, String strNewBranchingProfileName) {
        super(browser);

        this.strNewBranchingProfileName = strNewBranchingProfileName;

        this.txtCopyValidationMsg = browser.getBrowserElement(OR_AREA,
                OR_COPY_VALIDATION_MSG);

        this.btnCopyBranchingProfile = browser.getBrowserElement(OR_AREA,
                OR_COPY_BRANCHINGPROFILE_XPATH).formatWithParms(
                strBranchingProfileTitle, strBranchingProfileTitle);

        this.txtNewBranchingProfilName = browser.getBrowserElement(OR_AREA,
                OR_NEW_BRANCHINGPROFILE_NAME);
        this.elementExists = browser.getBrowserElement(OR_AREA,
                OR_SAVE_VALIDATE);

        this.btnSave = browser.getBrowserElement(OR_AREA, OR_SAVE);
        this.btnCancel = browser.getBrowserElement(OR_AREA, OR_CANCEL_BUTTON);

    }

    @Override
    protected boolean perform() {
        boolean result = true;
        if (!new LeftClickElementAction(getBrowser(),
                this.btnCopyBranchingProfile).performWithStates()) {
            LOGGER.error("Not able to click on Copy Button");
            result = false;
        }

        try {
            Thread.sleep(1000);
        } catch (InterruptedException e) {
            LOGGER.warn("Failed to wait for Save to be visible");
        }

        if (!new SendTextToElementAction(getBrowser(),
                this.txtNewBranchingProfilName, this.strNewBranchingProfileName)
                .performWithStates()) {
            LOGGER.debug("Not able to send text to New Branching Profile field");
            result = false;
        }

        try {
            Thread.sleep(500);
        } catch (InterruptedException e) {
            LOGGER.warn("Failed to wait for Save to be visible");
        }

        if (!new LeftClickElementAction(getBrowser(), this.btnSave)
                .performWithStates()) {
            LOGGER.error("Not able to click on Save Button");
            result = false;
        }

        if (new ElementExistsValidation(getBrowser(), this.elementExists)
                .performWithStates()) {
            LOGGER.info("Branching Profile Already Exists");
            if (!new SendTextToElementAction(getBrowser(),
                    this.txtNewBranchingProfilName,
                    (000 + (int) (Math.random() * 999) + ""))
                    .performWithStates()) {
                LOGGER.debug("Not able to send text to New Branching Profile field");
                result = false;
            }
            if (!new LeftClickElementAction(getBrowser(), this.btnSave)
                    .performWithStates()) {
                LOGGER.error("Not able to click on Save Button");
                result = false;
            }

        }
        return result;
    }

    @Override
    protected boolean checkPostState() {
        boolean result = true;
        txtCopyValidationMsg.setTimeout(10000);
        if (!new ElementExistsValidation(getBrowser(),
                this.txtCopyValidationMsg).performWithStates()) {
            LOGGER.debug("Not able to validate the Message ");
            result = false;
        }

        if (!result) {
            if (!new LeftClickElementAction(getBrowser(), this.btnCancel)
                    .performWithStates()) {
                LOGGER.error("Not able to click on Cancel");
                result = false;
            }
        }
        return result;
    }
}
