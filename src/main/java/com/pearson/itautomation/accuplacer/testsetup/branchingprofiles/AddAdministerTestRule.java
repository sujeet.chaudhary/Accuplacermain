package com.pearson.itautomation.accuplacer.testsetup.branchingprofiles;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.pearson.itautomation.accuplacer.interaction.ScrollToPageEnd;
import com.pearson.itautomation.bca.Browser;
import com.pearson.itautomation.bca.BrowserAction;
import com.pearson.itautomation.bca.BrowserElement;
import com.pearson.itautomation.bca.SelectorType;
import com.pearson.itautomation.bca.interaction.DropdownSelectionAction;
import com.pearson.itautomation.bca.validation.ElementExistsValidation;

/**
 * This Class Add an Administer Test Branching profile Rule<br>
 * <b>checkPrestate</b> - N/A <br>
 * <b>checkPostate<</b> - N/A <br>
 * <b>perform</b> - This method will return true if the Rule is added
 * successfully with the Test
 * 
 * @author Deepak Radhakrishnan
 */

public class AddAdministerTestRule extends BrowserAction {

    private static final Logger LOGGER = LoggerFactory
            .getLogger(AddAdministerTestRule.class);

    private static final String OR_AREA = "OR_TestSetup";
    private static final String OR_VALIDATE_ADD_RULE = "elementValidateAddRule";
    private static final String OR_SELECT_TEST = "selectTest";
    private static final String ADMINISTER_TEST_RULE = "Administer Test";
    private String testName;
    private int ruleNumber;

    private final BrowserElement drpTestNameSelect;
    private final BrowserElement elementValidateAddRule;

    /**
     * This constructor will initialize the mandatory fields and the
     * BrowserElements from this class
     * 
     * @param browser
     * @param ruleNumber
     *            Rule Number for the Branching profile
     * @param testName
     *            Name of the Test to be added
     */
    public AddAdministerTestRule(final Browser browser, int ruleNumber,
            String testName) {
        super(browser);
        setTestName(testName);
        setRuleNumber(ruleNumber);
        this.elementValidateAddRule = browser.getBrowserElement(OR_AREA,
                OR_VALIDATE_ADD_RULE).formatWithParms(getRuleNumber());
        this.drpTestNameSelect = browser.getBrowserElement(OR_AREA,
                OR_SELECT_TEST).formatWithParms(getRuleNumber(),
                "Administer the Test");
    }

    @Override
    protected boolean perform() {
        boolean result = true;

        if (!new AddBranchingProfileRule(this.getBrowser(), getRuleNumber(),
                ADMINISTER_TEST_RULE).performWithStates()) {
            LOGGER.error("Not able Add Administer Test Rule");
            return false;
        }
        if (!new ElementExistsValidation(this.getBrowser(),
                this.elementValidateAddRule).performWithStates()) {
            LOGGER.error("Not able validate the added Rule");
            result = false;
        }
        if (!new ScrollToPageEnd(getBrowser()).performWithStates()) {
            LOGGER.error("Not able to Scroll to Page End");
            result = false;
        }
        if (!new DropdownSelectionAction(getBrowser(), this.drpTestNameSelect,
                SelectorType.VISIBLE_TEXT, getTestName()).performWithStates(
                false, true)) {
            LOGGER.error("Not able to select THe Test" + getTestName());
            return false;
        }
        return result;
    }

    private String getTestName() {
        return testName;
    }

    private void setTestName(String testName) {
        this.testName = testName;
    }

    public int getRuleNumber() {
        return ruleNumber;
    }

    public void setRuleNumber(int ruleNumber) {
        this.ruleNumber = ruleNumber;
    }

}
