package com.pearson.itautomation.accuplacer.testsetup.branchingprofiles;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.pearson.itautomation.accuplacer.interaction.ScrollToPageEnd;
import com.pearson.itautomation.bca.Browser;
import com.pearson.itautomation.bca.BrowserAction;
import com.pearson.itautomation.bca.BrowserElement;
import com.pearson.itautomation.bca.SelectorType;
import com.pearson.itautomation.bca.interaction.DropdownSelectionAction;
import com.pearson.itautomation.bca.validation.ElementExistsValidation;

/**
 * This Class Add an Administer Background Question Group Rule<br>
 * <b>checkPrestate</b> - N/A <br>
 * <b>checkPostate<</b> - N/A <br>
 * <b>perform</b> - This method will return true if the dminister Background
 * Question Group Rule is added successfully with the Test
 * 
 * @author Deepak Radhakrishnan
 */

public class AddAdministerBackgroundQuestionGroupRule extends BrowserAction {

    private static final Logger LOGGER = LoggerFactory
            .getLogger(AddAdministerBackgroundQuestionGroupRule.class);

    private static final String OR_AREA = "OR_TestSetup";
    private static final String OR_VALIDATE_ADD_RULE = "elementValidateAddRule";
    private static final String OR_SELECT_BQ_GROUP = "selectTest";
    private static final String ADMINISTER_BQ_GROUP_RULE = "Administer Background Question Group";
    private String backGroundQuestionGroupName;
    private int ruleNumber;

    private final BrowserElement drpTestNameSelect;
    private final BrowserElement elementValidateAddRule;

    /**
     * This constructor will initialize the mandatory fields and the
     * BrowserElements from this class
     * 
     * @param browser
     * @param ruleNumber
     *            The Rule Number
     * @param bQGroupName
     *            The Background Question Name to be selected
     */
    public AddAdministerBackgroundQuestionGroupRule(final Browser browser,
            int ruleNumber, String bQGroupName) {

        super(browser);
        setBQGroupName(bQGroupName);
        setRuleNumber(ruleNumber);
        this.elementValidateAddRule = browser.getBrowserElement(OR_AREA,
                OR_VALIDATE_ADD_RULE).formatWithParms(getRuleNumber());
        this.drpTestNameSelect = browser.getBrowserElement(OR_AREA,
                OR_SELECT_BQ_GROUP).formatWithParms(getRuleNumber(),
                "Administer the Background Question Group");
    }

    @Override
    protected boolean perform() {
        boolean result = true;

        if (!new AddBranchingProfileRule(this.getBrowser(), getRuleNumber(),
                ADMINISTER_BQ_GROUP_RULE).performWithStates()) {
            LOGGER.error("Not able Add Administer Test Rule");
            result = false;
        }

        if (!new ElementExistsValidation(this.getBrowser(),
                this.elementValidateAddRule).performWithStates()) {
            LOGGER.error("Not able validate the added Rule");
            return false;
        }
        if (!new ScrollToPageEnd(getBrowser()).performWithStates()) {
            LOGGER.error("Not able to Scroll to Page End");
            result = false;
        }
        if (!new DropdownSelectionAction(getBrowser(), this.drpTestNameSelect,
                SelectorType.VISIBLE_TEXT, getBackGroundQuestionGroupName())
                .performWithStates(false, true)) {
            LOGGER.error("Not able to select THe Test"
                    + getBackGroundQuestionGroupName());
            result = false;
        }
        return result;
    }

    private String getBackGroundQuestionGroupName() {
        return backGroundQuestionGroupName;
    }

    private void setBQGroupName(String backGroundQuestionGroupName) {
        this.backGroundQuestionGroupName = backGroundQuestionGroupName;
    }

    public int getRuleNumber() {
        return ruleNumber;
    }

    public void setRuleNumber(int ruleNumber) {
        this.ruleNumber = ruleNumber;
    }

}
