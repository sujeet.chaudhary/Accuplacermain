package com.pearson.itautomation.accuplacer.interaction;

import org.openqa.selenium.ElementNotVisibleException;
import org.openqa.selenium.interactions.internal.Coordinates;
import org.openqa.selenium.internal.Locatable;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.pearson.itautomation.bca.Browser;
import com.pearson.itautomation.bca.BrowserAction;
import com.pearson.itautomation.bca.BrowserElement;

/**
 * 
 * This method will navigate to the Target Element
 * 
 * @author Deepak Radhakrishnan
 */
public class ScrollToElement extends BrowserAction {
    private static final Logger LOGGER = LoggerFactory
            .getLogger(ScrollToElement.class);
    private final BrowserElement targetElement;

    public ScrollToElement(Browser browser, BrowserElement targetElement) {
        super(browser);
        this.targetElement = targetElement;

    }

    @Override
    protected boolean perform() {
        try {

            Coordinates coordinates = ((Locatable) this.getBrowser()
                    .getElementWithWait(this.targetElement)).getCoordinates();
            coordinates.inViewPort();
            return true;
        } catch (ElementNotVisibleException e) {
            LOGGER.error("Exception occured while scrolling to element", e);
            return false;

        } catch (Exception e) {
            LOGGER.error("Exception occured while scrolling to element", e);
            return false;

        }
    }

}
