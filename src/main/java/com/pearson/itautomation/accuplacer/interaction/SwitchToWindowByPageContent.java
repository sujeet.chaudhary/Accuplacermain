package com.pearson.itautomation.accuplacer.interaction;

import java.util.Set;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.pearson.itautomation.accuplacer.core.AccuplacerBrowserUtils;
import com.pearson.itautomation.bca.Browser;
import com.pearson.itautomation.bca.BrowserAction;

/**
 * 
 * This method will Switch to window based on the page content
 * 
 * @author Deepak Radhakrishnan
 */
public class SwitchToWindowByPageContent extends BrowserAction {
    private static final Logger LOGGER = LoggerFactory
            .getLogger(SwitchToWindowByPageContent.class);
    private final String strContent;

    /**
     * @param browser
     * @param strContent
     *            Text Content on the Page
     */
    public SwitchToWindowByPageContent(Browser browser, String strContent) {
        super(browser);
        this.strContent = strContent;

    }

    @Override
    protected boolean perform() {

        Set<String> windowHandles;
        boolean pageFoundFlag = false;
        int counter = 0;
        do {
            try {
                Thread.sleep(100);
            } catch (InterruptedException e) {
                LOGGER.info("Failed to for page load", e);
            }
            counter++;
            windowHandles = getBrowser().getWindowHandles();
            for (String winHandle : windowHandles) {
                getBrowser().switchTo().window(winHandle);
                AccuplacerBrowserUtils.performWaits(getBrowser());
                if (getBrowser().getPageSource().contains(strContent)) {
                    LOGGER.info("Browser Window Found , return to code");
                    pageFoundFlag = true;
                    getBrowser().manage().window().maximize();
                    return pageFoundFlag;
                }
            }
        } while (!pageFoundFlag && (counter < 50));

        LOGGER.error("Window Not Found , Return to Code");
        return pageFoundFlag;
    }
}
