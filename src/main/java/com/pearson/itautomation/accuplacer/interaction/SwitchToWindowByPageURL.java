package com.pearson.itautomation.accuplacer.interaction;

import java.util.Set;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.pearson.itautomation.bca.Browser;
import com.pearson.itautomation.bca.BrowserAction;

/**
 * 
 * This method will Switch to window based on the page URL
 * 
 * @author Deepak Radhakrishnan
 */
public class SwitchToWindowByPageURL extends BrowserAction {
    private static final Logger LOGGER = LoggerFactory
            .getLogger(SwitchToWindowByPageURL.class);
    private final String stringURL;

    /**
     * @param browser
     * @param stringURL
     *            URL othe Page
     */
    public SwitchToWindowByPageURL(Browser browser, String stringURL) {
        super(browser);
        this.stringURL = stringURL;

    }

    @Override
    protected boolean perform() {

        Set<String> windowHandles;
        boolean pageFoundFlag = false;
        int counter = 0;
        do {
            try {
                Thread.sleep(100);
            } catch (InterruptedException e) {
                LOGGER.info("Failed to for page load", e);
            }
            counter++;
            windowHandles = getBrowser().getWindowHandles();
            for (String winHandle : windowHandles) {
                getBrowser().switchTo().window(winHandle);
                /* AccuplacerBrowserUtils.performWaits(getBrowser()); */
                System.out.println("URL" + getBrowser().getCurrentUrl());
                if (getBrowser().getCurrentUrl().contains(stringURL)) {
                    LOGGER.info("Browser Window Found , return to code");
                    pageFoundFlag = true;
                    getBrowser().manage().window().maximize();
                    return pageFoundFlag;
                }
            }
            LOGGER.info("Counter" + counter);
        } while (!pageFoundFlag && (counter < 50));

        LOGGER.error("Window Not Found , Return to Code");
        return pageFoundFlag;
    }
}
