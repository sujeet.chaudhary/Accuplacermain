package com.pearson.itautomation.accuplacer.interaction;

import org.openqa.selenium.JavascriptExecutor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.pearson.itautomation.bca.Browser;
import com.pearson.itautomation.bca.BrowserAction;

/**
 * This class will scroll to PAGE top of the browser
 * 
 * @author Deepak Radhakrishnan
 * 
 */
public class ScrollToPageTop extends BrowserAction {
    private static final Logger LOGGER = LoggerFactory
            .getLogger(ScrollToPageTop.class);

    public ScrollToPageTop(Browser browser) {
        super(browser);

    }

    @Override
    protected boolean perform() {
        try {
            JavascriptExecutor jse = (JavascriptExecutor) this.getBrowser()
                    .getWebDriver();
            jse.executeScript("scroll(250, 0)");
            Thread.sleep(200);
            return true;
        } catch (InterruptedException e) {
            LOGGER.error("Exception Occured while scrolling", e);
            return false;
        }
    }

}
