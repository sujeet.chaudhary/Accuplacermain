package com.pearson.itautomation.accuplacer.placementsetup.placementrules;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.pearson.itautomation.accuplacer.core.Buttons;
import com.pearson.itautomation.bca.Browser;
import com.pearson.itautomation.bca.BrowserAction;
import com.pearson.itautomation.bca.BrowserElement;
import com.pearson.itautomation.bca.interaction.LeftClickElementAction;
import com.pearson.itautomation.bca.interaction.SendTextToElementAction;

/**
 * This Class will fill the Placement rule Name,Description and Comment on
 * Plcement Rule Page
 * 
 * <b>checkPrestate</b> - N/A <br>
 * <b>checkPostate</b> - N/A <br>
 * <b>perform</b> - This method will return true if all the three fields have
 * been filled successfully.
 * 
 * @author Sujeet Chaudhary
 */
public class ChoosePlacementRulesName extends BrowserAction {

    private static final Logger LOGGER = LoggerFactory
            .getLogger(ChoosePlacementRulesName.class);

    private static final String OR_AREA = "OR_Placement_Setup";
    private static final String OR_RULE_NAME = "PlacementRulesName";
    private static final String OR_RULE_DESCRIPTION = "PlacementRulesDescription";
    private static final String OR_RULES_COMMENT = "PlacementRulesComment";

    private final BrowserElement txtPlacementRulesName;
    private final BrowserElement txtPlacementRulesDescription;
    private final BrowserElement txtPlacementRulesComment;
    private final BrowserElement btnAddPlacementRule = Buttons
            .getButtonType4("Add");

    private final String strRuleName;
    private final String strRuleDescription;
    private final String strRuleComment;

    /**
     * This constructor will initialize the mandatory fields and the
     * BrowserElements from this class
     * 
     * @param browser
     * @param strRuleName
     *            The Value of 'Rule Name' to be entered * @param strRuleName
     *            The
     * @param strRuleDescription
     *            Value of 'Rule Description' to be entered
     * @param strRuleComment
     *            Value of 'Rule comment' to be entered
     */
    public ChoosePlacementRulesName(final Browser browser, String strRuleName,
            String strRuleDescription, String strRuleComment) {
        super(browser);

        this.strRuleName = strRuleName;
        this.strRuleDescription = strRuleDescription;
        this.strRuleComment = strRuleComment;

        this.txtPlacementRulesName = browser.getBrowserElement(OR_AREA,
                OR_RULE_NAME);
        this.txtPlacementRulesDescription = browser.getBrowserElement(OR_AREA,
                OR_RULE_DESCRIPTION);
        this.txtPlacementRulesComment = browser.getBrowserElement(OR_AREA,
                OR_RULES_COMMENT);

    }

    @Override
    protected boolean perform() {
        boolean result = true;

        if (!new LeftClickElementAction(getBrowser(), this.btnAddPlacementRule)
                .performWithStates()) {
            LOGGER.error("Not able to click on Add Button");
            result = false;
        }

        if (!new SendTextToElementAction(getBrowser(),
                this.txtPlacementRulesName, strRuleName).performWithStates()) {
            LOGGER.error("Not able to send text to Placement Rule Name field");
            result = false;
        }

        if (!new SendTextToElementAction(getBrowser(),
                this.txtPlacementRulesDescription, strRuleDescription)
                .performWithStates()) {
            LOGGER.error("Not able to send text to Placement Description field");
            result = false;
        }

        if (!new SendTextToElementAction(getBrowser(),
                this.txtPlacementRulesComment, strRuleComment)
                .performWithStates()) {
            LOGGER.error("Not able to send text to Placement comment field");
            result = false;
        }
        return result;
    }
}
