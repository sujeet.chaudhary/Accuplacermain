package com.pearson.itautomation.accuplacer.placementsetup.compositescores;

import org.apache.commons.lang3.mutable.MutableInt;
import org.openqa.selenium.WebElement;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.pearson.itautomation.accuplacer.interaction.ScrollToPageTop;
import com.pearson.itautomation.bca.Browser;
import com.pearson.itautomation.bca.BrowserAction;
import com.pearson.itautomation.bca.BrowserElement;
import com.pearson.itautomation.bca.SelectorType;
import com.pearson.itautomation.bca.interaction.ClearTextElementAction;
import com.pearson.itautomation.bca.interaction.DropdownSelectionAction;
import com.pearson.itautomation.bca.interaction.LeftClickElementAction;
import com.pearson.itautomation.bca.interaction.SendTextToElementAction;

/**
 * This Class will responsible to add different condition types in linear way to
 * Composite Score <b>checkPrestate</b> - N/A <br>
 * <b>checkPostate</b> - N/A <br>
 * <b>perform</b> - This method will return true if different condition type
 * will added to Composite Score successfully.
 * 
 * @author Solomon Lingala
 */

public class AddConditionsToCompositeScore extends BrowserAction {

    private static final Logger LOGGER = LoggerFactory
            .getLogger(AddConditionsToCompositeScore.class);

    private static final String OR_AREA = "OR_Placement_Setup";
    private static final String OR_ADD_CONDITION_TO_SCORE_LINK = "AddConditionToThisScore";
    private static final String OR_SELECT_CONDITION = "SelectConditionCompositeScore";
    private static final String OR_SINGLE_TEST_SCORE_PREFIX = "SingleTestScorePrefix";
    private static final String OR_MUTLIPLE_WEIGHT_MEASURES = "MultipleWeightMeasuresPrefix";
    private static final String OR_FIXED_NUMBER = "FixedNumberPrefix";
    private static final String OR_SINGLE_TEST_SCORE_SELECT = "SelectTestPlacementRule";
    private static final String OR_SINGLE_STRAND_SCORE_SELECT = "SelectSingleTestScoreTest";
    private static final String OR_MULTIPLE_WEIGHT_MEASURES = "SelectMultipleWeightMeasuresTest";
    private static final String OR_SELECT_EQUAL_NOT_EQUAL = "SelectEqualAndNotEqual1";
    private static final String OR_SELECT_CONDITIONTEXT = "SelectConditionText";
    private static final String OR_BTN_ADD_CONDITION_TO_SCORE = "BTNAddScoreCondition";
    private static final String OR_BTN_ADD_SCORE = "AddScoreButton";
    private static final String OR_BTN_ADD_SCORE_MWSORAMWS = "AddScoreBelowForMWSAMWS";
    private static final String OR_BTN_ADD_SCORE_BELOW = "AddScoreBelowLink";
    private static final String OR_BTN_SAVE_COMPOSITE_SCORE = "SaveCompositeScoreButton";

    private final BrowserElement lnkAddConditionToScore;
    private final BrowserElement drpSelectNewScore;
    private final BrowserElement drpSelectSingleTestScoreTest;
    private final BrowserElement drpSelectSingleStrandScoreTest;
    private final BrowserElement drpSelectMultipleWeightMeasures;
    private final BrowserElement btnAddScoreCondition;
    private BrowserElement btnAddScoreB;
    private BrowserElement btnAddScoreBelow;
    private BrowserElement drpSelectEqualNotEqual;
    private BrowserElement drpSelectConditionText;
    private BrowserElement btnSaveScore;

    private String selectNewScoreType;
    private String selectConditionToScore;
    private String conditionValuesVector;

    /**
     * This Constructor will initialize the different mandatory fields and
     * browser elements for this class
     * 
     * @param browser
     * @param selectConditionToScore
     *            Condition Type to be added to CS with comma separated
     * @param conditionValuesVector
     *            Corresponding Values for each Condition Type.
     */

    public AddConditionsToCompositeScore(final Browser browser,
            String selectConditionToScore, String conditionValuesVector) {
        super(browser);
        this.selectConditionToScore = selectConditionToScore;
        this.conditionValuesVector = conditionValuesVector;
        this.btnAddScoreCondition = browser.getBrowserElement(OR_AREA,
                OR_BTN_ADD_CONDITION_TO_SCORE);
        this.lnkAddConditionToScore = browser.getBrowserElement(OR_AREA,
                OR_ADD_CONDITION_TO_SCORE_LINK);
        this.drpSelectMultipleWeightMeasures = browser.getBrowserElement(
                OR_AREA, OR_MULTIPLE_WEIGHT_MEASURES);
        this.drpSelectNewScore = browser.getBrowserElement(OR_AREA,
                OR_SELECT_CONDITION);
        this.drpSelectSingleTestScoreTest = browser.getBrowserElement(OR_AREA,
                OR_SINGLE_TEST_SCORE_SELECT);
        this.drpSelectSingleStrandScoreTest = browser.getBrowserElement(
                OR_AREA, OR_SINGLE_STRAND_SCORE_SELECT);
        this.drpSelectEqualNotEqual = browser.getBrowserElement(OR_AREA,
                OR_SELECT_EQUAL_NOT_EQUAL);
        this.drpSelectConditionText = browser.getBrowserElement(OR_AREA,
                OR_SELECT_CONDITIONTEXT);
    }

    @Override
    protected boolean perform() {
        boolean result = true;

        if (!new LeftClickElementAction(getBrowser(),
                this.lnkAddConditionToScore).performWithStates()) {
            LOGGER.error("Not able to click on Add a Condition to this score link");
            return false;
        }

        String[] arrselectConditionToScore = selectConditionToScore.split(";");
        String[] arrconditionValuesVector = conditionValuesVector.split(";");
        MutableInt counter = new MutableInt(0);
        int k = 0;
        for (String str1 : arrselectConditionToScore) {
            k = k + 1;
            for (int j = counter.getValue(); j < arrconditionValuesVector.length; j++) {
                WebElement checkElement = getBrowser().getElementWithWait(
                        this.drpSelectNewScore);

                switch (str1) {
                case "STS":
                    result = populateSingleTestScoreValues(
                            arrconditionValuesVector, j, checkElement, counter);

                    if (k != arrselectConditionToScore.length && result) {
                        this.btnAddScoreB = getBrowser().getBrowserElement(
                                OR_AREA, OR_BTN_ADD_SCORE).formatWithParms(1);
                        if (!new LeftClickElementAction(getBrowser(),
                                this.btnAddScoreB).performWithStates()) {
                            LOGGER.error("Not able to click on Add button after selecting Condition");
                            return false;
                        }
                        this.btnAddScoreBelow = getBrowser().getBrowserElement(
                                OR_AREA, OR_BTN_ADD_SCORE_BELOW);
                        if (!new LeftClickElementAction(getBrowser(),
                                this.btnAddScoreBelow).performWithStates()) {
                            LOGGER.error("Not able to click on Add button after selecting Condition");
                            return false;
                        }
                    }

                    break;

                case "SSS":
                    result = populateSingleStrandScoreValues(
                            arrconditionValuesVector, j, checkElement, counter);
                    if (k != arrselectConditionToScore.length && result) {
                        this.btnAddScoreB = getBrowser().getBrowserElement(
                                OR_AREA, OR_BTN_ADD_SCORE).formatWithParms(2);
                        if (!new LeftClickElementAction(getBrowser(),
                                this.btnAddScoreB).performWithStates()) {
                            LOGGER.error("Not able to click on Add button after selecting Condition");
                            return false;
                        }
                        this.btnAddScoreBelow = getBrowser().getBrowserElement(
                                OR_AREA, OR_BTN_ADD_SCORE_BELOW);
                        if (!new LeftClickElementAction(getBrowser(),
                                this.btnAddScoreBelow).performWithStates()) {
                            LOGGER.error("Not able to click on Add button after selecting Condition");
                            return false;
                        }
                    }
                    break;

                case "MWS":
                    result = populateMultiWeightedMeasuresValues(
                            arrconditionValuesVector, j, checkElement, counter);
                    if (k != arrselectConditionToScore.length && result) {
                        this.btnAddScoreB = getBrowser().getBrowserElement(
                                OR_AREA, OR_BTN_ADD_SCORE_MWSORAMWS)
                                .formatWithParms(3);
                        if (!new LeftClickElementAction(getBrowser(),
                                this.btnAddScoreB).performWithStates()) {
                            LOGGER.error("Not able to click on Add button after selecting Condition");
                            return false;
                        }
                        this.btnAddScoreBelow = getBrowser().getBrowserElement(
                                OR_AREA, OR_BTN_ADD_SCORE_BELOW);
                        if (!new LeftClickElementAction(getBrowser(),
                                this.btnAddScoreBelow).performWithStates()) {
                            LOGGER.error("Not able to click on Add button after selecting Condition");
                            return false;
                        }
                    }
                    break;

                case "AMWS":
                    result = populateAutoMultiWeightedScoreValues(
                            arrconditionValuesVector, j, checkElement, counter);
                    if (k != arrselectConditionToScore.length && result) {
                        this.btnAddScoreB = getBrowser().getBrowserElement(
                                OR_AREA, OR_BTN_ADD_SCORE_MWSORAMWS)
                                .formatWithParms(4);
                        if (!new LeftClickElementAction(getBrowser(),
                                this.btnAddScoreB).performWithStates()) {
                            LOGGER.error("Not able to click on Add button after selecting Condition");
                            return false;
                        }
                        this.btnAddScoreBelow = getBrowser().getBrowserElement(
                                OR_AREA, OR_BTN_ADD_SCORE_BELOW);
                        if (!new LeftClickElementAction(getBrowser(),
                                this.btnAddScoreBelow).performWithStates()) {
                            LOGGER.error("Not able to click on Add button after selecting Condition");
                            return false;
                        }
                    }
                    break;

                case "FN":
                    result = populateFixedNumberValues(
                            arrconditionValuesVector, j, checkElement, counter);
                    if (k != arrselectConditionToScore.length && result) {
                        this.btnAddScoreB = getBrowser().getBrowserElement(
                                OR_AREA, OR_BTN_ADD_SCORE).formatWithParms(4);
                        if (!new LeftClickElementAction(getBrowser(),
                                this.btnAddScoreB).performWithStates()) {
                            LOGGER.error("Not able to click on Add button after selecting Condition");
                            return false;
                        }
                        this.btnAddScoreBelow = getBrowser().getBrowserElement(
                                OR_AREA, OR_BTN_ADD_SCORE_BELOW);
                        if (!new LeftClickElementAction(getBrowser(),
                                this.btnAddScoreBelow).performWithStates()) {
                            LOGGER.error("Not able to click on Add button after selecting Condition");
                            return false;
                        }
                    }
                    break;

                default:
                    LOGGER.error("No value provided for Condition to be added to Composite Scores");
                    break;
                }
                if (result) {
                    break;
                }
            }
        }
        this.btnSaveScore = getBrowser().getBrowserElement(OR_AREA,
                OR_BTN_SAVE_COMPOSITE_SCORE);

        if (!new ScrollToPageTop(getBrowser()).performWithStates()) {
            LOGGER.error("Not able to Scroll to Page Top");
            result = false;
        }

        if (!new LeftClickElementAction(this.getBrowser(), btnSaveScore)
                .performWithStates()) {
            LOGGER.error("Failed to click on Save Score Button");
            result = false;
        }

        return result;
    }

    /**
     * @param arrconditionValuesVector
     *            different condition type values for that condition
     * @param i
     *            index for the similar browser elements
     * @param checkElement
     *            Check element exists on the page
     * @param counter
     *            Counter that keeps track of the index of the array
     * @return returns true if condition type and all value are populated
     *         correctly else returns false
     */

    private boolean populateSingleTestScoreValues(
            String[] arrconditionValuesVector, int i, WebElement checkElement,
            MutableInt counter) {
        boolean result = true;
        selectNewScoreType = "Single Test Score";
        if (checkElement != null) {
            if (!new DropdownSelectionAction(this.getBrowser(),
                    drpSelectNewScore, SelectorType.VISIBLE_TEXT,
                    selectNewScoreType).performWithStates()) {
                LOGGER.error("Failed to select from Select New Score drop down selection");
                result = false;
            }
        } else {
            result = false;
            LOGGER.warn("Could not find the Select New Score drop down");
        }
        if (!new LeftClickElementAction(getBrowser(), this.btnAddScoreCondition)
                .performWithStates()) {
            LOGGER.error("Not able to click on Add button after selecting Condition");
            return false;
        }
        if (!new ClearTextElementAction(getBrowser(), this.getBrowser()
                .getBrowserElement(OR_AREA, OR_SINGLE_TEST_SCORE_PREFIX)
                .formatWithParms(i + 1)).performWithStates()) {
            LOGGER.error("Not able to clear text from prefix field");
            result = false;
        }
        if (!new SendTextToElementAction(getBrowser(), this.getBrowser()
                .getBrowserElement(OR_AREA, OR_SINGLE_TEST_SCORE_PREFIX)
                .formatWithParms(i + 1), arrconditionValuesVector[i])
                .performWithStates()) {
            LOGGER.error("Not able to send text to prefix field");
            result = false;
        } else {
            counter.increment();
        }

        if (!new DropdownSelectionAction(getBrowser(),
                this.drpSelectSingleTestScoreTest.formatWithParms(i + 1),
                SelectorType.VISIBLE_TEXT_REGEX,
                arrconditionValuesVector[i + 1])
                .performWithStates(false, false)) {
            LOGGER.error("Not able to select New Condition Type from Dropdown");
            result = false;
        } else {
            counter.increment();
        }
        return result;
    }

    /**
     * @param arrconditionValuesVector
     *            different condition type values for that condition
     * @param i
     *            index for the similar browser elements
     * @param checkElement
     *            Check element exists on the page
     * @param counter
     *            Counter that keeps track of the index of the array
     * @return returns true if condition type and all value are populated
     *         correctly else returns false
     */

    private boolean populateSingleStrandScoreValues(
            String[] arrconditionValuesVector, int i, WebElement checkElement,
            MutableInt counter) {
        boolean result = true;
        selectNewScoreType = "Single Strand Score";
        if (checkElement != null) {
            if (!new DropdownSelectionAction(this.getBrowser(),
                    drpSelectNewScore, SelectorType.VISIBLE_TEXT,
                    selectNewScoreType).performWithStates()) {
                LOGGER.error("Failed to select from Select New Score drop down selection");
                result = false;
            }
        } else {
            result = false;
            LOGGER.warn("Could not find the Select New Score drop down");
        }

        if (!new LeftClickElementAction(getBrowser(), this.btnAddScoreCondition)
                .performWithStates()) {
            LOGGER.error("Not able to click on Add button after selecting Condition");
            return false;
        }

        if (!new ClearTextElementAction(getBrowser(), this.getBrowser()
                .getBrowserElement(OR_AREA, OR_SINGLE_TEST_SCORE_PREFIX)
                .formatWithParms(i)).performWithStates()) {
            LOGGER.error("Not able to clear text from prefix field");
            result = false;
        }

        if (!new SendTextToElementAction(getBrowser(), this.getBrowser()
                .getBrowserElement(OR_AREA, OR_SINGLE_TEST_SCORE_PREFIX)
                .formatWithParms(i), arrconditionValuesVector[i])
                .performWithStates()) {
            LOGGER.error("Not able to send text to prefix field");
            result = false;
        } else {
            counter.increment();
        }

        if (!new DropdownSelectionAction(getBrowser(),
                this.drpSelectSingleStrandScoreTest,
                SelectorType.VISIBLE_TEXT_REGEX,
                arrconditionValuesVector[i + 1])
                .performWithStates(false, false)) {
            LOGGER.error("Not able to select New Condition Type from Dropdown");
            result = false;
        } else {
            counter.increment();
        }

        if (!new DropdownSelectionAction(getBrowser(),
                this.drpSelectSingleTestScoreTest.formatWithParms(i),
                SelectorType.VISIBLE_TEXT_REGEX,
                arrconditionValuesVector[i + 2])
                .performWithStates(false, false)) {
            LOGGER.error("Not able to select New Condition Type from Dropdown");
            result = false;
        } else {
            counter.increment();
        }

        return result;
    }

    /**
     * @param arrconditionValuesVector
     *            different condition type values for that condition
     * @param i
     *            index for the similar browser elements
     * @param checkElement
     *            Check element exists on the page
     * @param counter
     *            Counter that keeps track of the index of the array
     * @return returns true if condition type and all value are populated
     *         correctly else returns false
     */

    private boolean populateMultiWeightedMeasuresValues(
            String[] arrconditionValuesVector, int i, WebElement checkElement,
            MutableInt counter) {
        boolean result = true;
        selectNewScoreType = "Multi Weighted Score";
        if (checkElement != null) {
            if (!new DropdownSelectionAction(this.getBrowser(),
                    drpSelectNewScore, SelectorType.VISIBLE_TEXT,
                    selectNewScoreType).performWithStates()) {
                LOGGER.error("Failed to select from Select New Score drop down selection");
                result = false;
            }
        } else {
            result = false;
            LOGGER.warn("Could not find the Select New Score drop down");
        }

        if (!new LeftClickElementAction(getBrowser(), this.btnAddScoreCondition)
                .performWithStates()) {
            LOGGER.error("Not able to click on Add button after selecting Condition");
            return false;
        }

        if (!new ClearTextElementAction(getBrowser(), this.getBrowser()
                .getBrowserElement(OR_AREA, OR_MUTLIPLE_WEIGHT_MEASURES)
                .formatWithParms(i - 4)).performWithStates()) {
            LOGGER.error("Not able to clear text from prefix field");
            result = false;
        }

        if (!new SendTextToElementAction(getBrowser(), this.getBrowser()
                .getBrowserElement(OR_AREA, OR_MUTLIPLE_WEIGHT_MEASURES)
                .formatWithParms(i - 4), arrconditionValuesVector[i])
                .performWithStates()) {
            LOGGER.error("Not able to send text to prefix field");
            result = false;
        } else {
            counter.increment();
        }

        if (!new DropdownSelectionAction(getBrowser(),
                this.drpSelectMultipleWeightMeasures.formatWithParms(1),
                SelectorType.VISIBLE_TEXT_REGEX,
                arrconditionValuesVector[i + 1])
                .performWithStates(false, false)) {
            LOGGER.error("Not able to select New Condition Type from Dropdown");
            result = false;
        } else {
            counter.increment();
        }

        if (!new ClearTextElementAction(getBrowser(), this.getBrowser()
                .getBrowserElement(OR_AREA, OR_MUTLIPLE_WEIGHT_MEASURES)
                .formatWithParms(i - 3)).performWithStates()) {
            LOGGER.error("Not able to clear text from prefix field");
            result = false;
        }

        if (!new SendTextToElementAction(getBrowser(), this.getBrowser()
                .getBrowserElement(OR_AREA, OR_MUTLIPLE_WEIGHT_MEASURES)
                .formatWithParms(i - 3), arrconditionValuesVector[i + 2])
                .performWithStates()) {
            LOGGER.error("Not able to send text to prefix field");
            result = false;
        } else {
            counter.increment();
        }

        if (!new DropdownSelectionAction(getBrowser(),
                this.drpSelectMultipleWeightMeasures.formatWithParms(2),
                SelectorType.VISIBLE_TEXT_REGEX,
                arrconditionValuesVector[i + 3])
                .performWithStates(false, false)) {
            LOGGER.error("Not able to select New Condition Type from Dropdown");
            result = false;
        } else {
            counter.increment();
        }

        if (!new DropdownSelectionAction(getBrowser(),
                this.drpSelectEqualNotEqual.formatWithParms(1),
                SelectorType.VISIBLE_TEXT_REGEX,
                arrconditionValuesVector[i + 4])
                .performWithStates(false, false)) {
            LOGGER.error("Not able to select New Condition Type from Dropdown");
            result = false;
        } else {
            counter.increment();
        }

        if (!new DropdownSelectionAction(getBrowser(),
                this.drpSelectConditionText, SelectorType.VISIBLE_TEXT_REGEX,
                arrconditionValuesVector[i + 5])
                .performWithStates(false, false)) {
            LOGGER.error("Not able to select New Condition Type from Dropdown");
            result = false;
        } else {
            counter.increment();
        }

        return result;
    }

    /**
     * @param arrconditionValuesVector
     *            different condition type values for that condition
     * @param i
     *            index for the similar browser elements
     * @param checkElement
     *            Check element exists on the page
     * @param counter
     *            Counter that keeps track of the index of the array
     * @return returns true if condition type and all value are populated
     *         correctly else returns false
     */

    private boolean populateAutoMultiWeightedScoreValues(
            String[] arrconditionValuesVector, int i, WebElement checkElement,
            MutableInt counter) {
        boolean result = true;
        selectNewScoreType = "Auto Multiple Weighted Score";
        if (checkElement != null) {
            if (!new DropdownSelectionAction(this.getBrowser(),
                    drpSelectNewScore, SelectorType.VISIBLE_TEXT,
                    selectNewScoreType).performWithStates()) {
                LOGGER.error("Failed to select from Select New Score drop down selection");
                result = false;
            }
        } else {
            result = false;
            LOGGER.warn("Could not find the Select New Score drop down");
        }

        if (!new LeftClickElementAction(getBrowser(), this.btnAddScoreCondition)
                .performWithStates()) {
            LOGGER.error("Not able to click on Add button after selecting Condition");
            return false;
        }

        if (!new ClearTextElementAction(getBrowser(), this.getBrowser()
                .getBrowserElement(OR_AREA, OR_MUTLIPLE_WEIGHT_MEASURES)
                .formatWithParms(i - 8)).performWithStates()) {
            LOGGER.error("Not able to clear text from prefix field");
            result = false;
        }

        if (!new SendTextToElementAction(getBrowser(), this.getBrowser()
                .getBrowserElement(OR_AREA, OR_MUTLIPLE_WEIGHT_MEASURES)
                .formatWithParms(i - 8), arrconditionValuesVector[i])
                .performWithStates()) {
            LOGGER.error("Not able to send text to prefix field");
            result = false;
        } else {
            counter.increment();
        }

        if (!new DropdownSelectionAction(getBrowser(),
                this.drpSelectMultipleWeightMeasures.formatWithParms(3),
                SelectorType.VISIBLE_TEXT_REGEX,
                arrconditionValuesVector[i + 1])
                .performWithStates(false, false)) {
            LOGGER.error("Not able to select New Condition Type from Dropdown");
            result = false;
        } else {
            counter.increment();
        }

        if (!new DropdownSelectionAction(getBrowser(),
                this.drpSelectMultipleWeightMeasures.formatWithParms(4),
                SelectorType.VISIBLE_TEXT_REGEX,
                arrconditionValuesVector[i + 2])
                .performWithStates(false, false)) {
            LOGGER.error("Not able to select New Condition Type from Dropdown");
            result = false;
        } else {
            counter.increment();
        }

        return result;
    }

    /**
     * @param arrconditionValuesVector
     *            different condition type values for that condition
     * @param i
     *            index for the similar browser elements
     * @param checkElement
     *            Check element exists on the page
     * @param counter
     *            Counter that keeps track of the index of the array
     * @return returns true if condition type and all value are populated
     *         correctly else returns false
     */

    private boolean populateFixedNumberValues(
            String[] arrconditionValuesVector, int i, WebElement checkElement,
            MutableInt counter) {
        boolean result = true;
        selectNewScoreType = "Fixed Number";
        if (checkElement != null) {
            if (!new DropdownSelectionAction(this.getBrowser(),
                    drpSelectNewScore, SelectorType.VISIBLE_TEXT,
                    selectNewScoreType).performWithStates()) {
                LOGGER.error("Failed to select from Select New Score drop down selection");
                result = false;
            }
        } else {
            result = false;
            LOGGER.warn("Could not find the Select New Score drop down");
        }

        if (!new LeftClickElementAction(getBrowser(), this.btnAddScoreCondition)
                .performWithStates()) {
            LOGGER.error("Not able to click on Add button after selecting Condition");
            return false;
        }

        if (!new ClearTextElementAction(getBrowser(), this.getBrowser()
                .getBrowserElement(OR_AREA, OR_FIXED_NUMBER)
                .formatWithParms(i - 8)).performWithStates()) {
            LOGGER.error("Not able to clear text from prefix field");
            result = false;
        }

        if (!new SendTextToElementAction(getBrowser(), this.getBrowser()
                .getBrowserElement(OR_AREA, OR_FIXED_NUMBER)
                .formatWithParms(i - 8), arrconditionValuesVector[i])
                .performWithStates()) {
            LOGGER.error("Not able to send text to prefix field");
            result = false;
        } else {
            counter.increment();
        }

        return result;
    }
}
