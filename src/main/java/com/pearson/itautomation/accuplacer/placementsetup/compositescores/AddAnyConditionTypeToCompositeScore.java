package com.pearson.itautomation.accuplacer.placementsetup.compositescores;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import org.openqa.selenium.WebElement;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.pearson.itautomation.accuplacer.core.Buttons;
import com.pearson.itautomation.accuplacer.interaction.ScrollToPageTop;
import com.pearson.itautomation.bca.Browser;
import com.pearson.itautomation.bca.BrowserAction;
import com.pearson.itautomation.bca.BrowserElement;
import com.pearson.itautomation.bca.SelectorType;
import com.pearson.itautomation.bca.interaction.ClearTextElementAction;
import com.pearson.itautomation.bca.interaction.DropdownSelectionAction;
import com.pearson.itautomation.bca.interaction.LeftClickElementAction;
import com.pearson.itautomation.bca.interaction.SendTextToElementAction;

/**
 * This Class will responsible to add different condition types to add different
 * Composite Score <b>checkPrestate</b> - N/A <br>
 * <b>checkPostate</b> - N/A <br>
 * <b>perform</b> - This method will return true if different condition type
 * will added to Composite Score successfully.
 * 
 * @author Solomon Lingala
 */

public class AddAnyConditionTypeToCompositeScore extends BrowserAction {
    private static final Logger LOGGER = LoggerFactory
            .getLogger(AddAnyConditionTypeToCompositeScore.class);

    private static final String AUTO_MULTIWEIGHTED_SCORE = "AMWS";
    private static final String FIXED_NUMBER = "FN";
    private static final String MULTIWEIGHTED_SCORE = "MWS";
    private static final String SINGLE_TEST_SCORE = "STS";
    private static final String SINGLE_STRAND_SCORE = "SSS";
    private static final String STRING_ERROR_MESSAGE = "Not able to click on Add button after selecting Condition";
    private static final int INTG_CONSTANT_FORNUM2 = 2;

    private static final String OR_AREA = "OR_Placement_Setup";
    private static final String OR_RULE_NAME = "CompositeScoreName";
    private static final String OR_ADD_CONDITION_TO_SCORE_LINK = "AddConditionToThisScore";
    private static final String OR_SELECT_CONDITION = "SelectConditionCompositeScore";
    private static final String OR_SINGLE_TEST_SCORE_PREFIX = "SingleTestScorePrefix";
    private static final String OR_MUTLIPLE_WEIGHT_MEASURES = "MultipleWeightMeasuresPrefix";
    private static final String OR_MUTLIPLE_WEIGHT_MEASURES_DYNAMIC = "MultipleWeightMeasuresDynamicPrefix";
    private static final String OR_FIXED_NUMBER = "FixedNumberPrefix";
    private static final String OR_SINGLE_TEST_SCORE_SELECT = "SelectTestPlacementRule";
    private static final String OR_SINGLE_STRAND_SCORE_SELECT = "SelectSingleTestScoreTest";
    private static final String OR_MULTIPLE_WEIGHT_MEASURES = "SelectMultipleWeightMeasuresTest";
    private static final String OR_SELECT_EQUAL_NOT_EQUAL = "SelectEqualAndNotEqual1";
    private static final String OR_SELECT_CONDITIONTEXT = "SelectConditionText";
    private static final String OR_BTN_ADD_CONDITION_TO_SCORE = "BTNAddScoreCondition";
    private static final String OR_BTN_ADD_SCORE = "AddScoreButton";
    private static final String OR_BTN_ADD_SCORE_BELOW = "AddScoreBelowLink";
    private static final String OR_MSG_SAVE_COMPOSITE_SCORE = "CompositeScoreSaveSameNameErrMsg";

    private final BrowserElement lnkAddConditionToScore;
    private final BrowserElement drpSelectNewScore;
    private final BrowserElement drpSelectSingleTestScoreTest;
    private final BrowserElement drpSelectSingleStrandScoreTest;
    private final BrowserElement drpSelectMultipleWeightMeasures;
    private final BrowserElement btnAddScoreCondition;
    private final BrowserElement txtCompositeScoreName;
    public final BrowserElement btnSaveCompositeScore = Buttons
            .getButtonType1("Save");
    private BrowserElement btnAddScoreB;
    private BrowserElement btnAddScoreBelow;
    private BrowserElement drpSelectEqualNotEqual;
    private BrowserElement drpSelectConditionText;
    private BrowserElement msgSameNameError;

    private String selectNewScoreType;
    private String selectConditionToScore;
    private String conditionValuesVector;

    /**
     * This Constructor will initialize the different mandatory fields and
     * browser elements for this class
     * 
     * @param browser
     * @param selectConditionToScore
     *            Condition Type to be added to CS with comma separated
     * @param conditionValuesVector
     *            Corresponding Values for each Condition Type.
     */
    public AddAnyConditionTypeToCompositeScore(final Browser browser,
            String selectConditionToScore, String conditionValuesVector) {
        super(browser);
        this.selectConditionToScore = selectConditionToScore;
        this.conditionValuesVector = conditionValuesVector;
        this.txtCompositeScoreName = browser.getBrowserElement(OR_AREA,
                OR_RULE_NAME);
        this.btnAddScoreCondition = browser.getBrowserElement(OR_AREA,
                OR_BTN_ADD_CONDITION_TO_SCORE);
        this.lnkAddConditionToScore = browser.getBrowserElement(OR_AREA,
                OR_ADD_CONDITION_TO_SCORE_LINK);
        this.drpSelectMultipleWeightMeasures = browser.getBrowserElement(
                OR_AREA, OR_MULTIPLE_WEIGHT_MEASURES);
        this.drpSelectNewScore = browser.getBrowserElement(OR_AREA,
                OR_SELECT_CONDITION);
        this.drpSelectSingleTestScoreTest = browser.getBrowserElement(OR_AREA,
                OR_SINGLE_TEST_SCORE_SELECT);
        this.drpSelectSingleStrandScoreTest = browser.getBrowserElement(
                OR_AREA, OR_SINGLE_STRAND_SCORE_SELECT);
        this.drpSelectEqualNotEqual = browser.getBrowserElement(OR_AREA,
                OR_SELECT_EQUAL_NOT_EQUAL);
        this.drpSelectConditionText = browser.getBrowserElement(OR_AREA,
                OR_SELECT_CONDITIONTEXT);
    }

    @Override
    protected boolean perform() {
        boolean result = true;

        if (!new LeftClickElementAction(getBrowser(),
                this.lnkAddConditionToScore).performWithStates()) {
            LOGGER.error("Not able to click on Add a Condition to this score link");
            return false;
        }
        String[] conditionTypes = selectConditionToScore.split(";");
        String[] conditionTypeValues = conditionValuesVector.split(";");
        int i = 0;
        int k = 0;
        for (String conditionType : conditionTypes) {
            int scoreMultiplierIndex = 2;
            if (conditionType.equals(SINGLE_STRAND_SCORE)) {
                scoreMultiplierIndex = 1;
            }
            k = k + 1;
            String conditionTypeValue = conditionTypeValues[i];
            String[] conditionTypeValueVectors = conditionTypeValue.split(":");
            WebElement checkElement = getBrowser().getElementWithWait(
                    this.drpSelectNewScore);
            switch (conditionType) {
            case SINGLE_TEST_SCORE:
                result = populateSingleTestScoreValues(
                        conditionTypeValueVectors, checkElement);

                if (k != conditionTypes.length && result) {
                    this.btnAddScoreB = getBrowser().getBrowserElement(OR_AREA,
                            OR_BTN_ADD_SCORE).formatWithParms(k);
                    if (!new LeftClickElementAction(getBrowser(),
                            this.btnAddScoreB).performWithStates()) {
                        LOGGER.error(STRING_ERROR_MESSAGE);
                        return false;
                    }
                    this.btnAddScoreBelow = getBrowser().getBrowserElement(
                            OR_AREA, OR_BTN_ADD_SCORE_BELOW);
                    if (!new LeftClickElementAction(getBrowser(),
                            this.btnAddScoreBelow).performWithStates()) {
                        LOGGER.error(STRING_ERROR_MESSAGE);
                        return false;
                    }
                }

                break;

            case SINGLE_STRAND_SCORE:

                result = populateSingleStrandScoreValues(
                        conditionTypeValueVectors, checkElement,
                        scoreMultiplierIndex);
                if (k != conditionTypes.length && result) {
                    this.btnAddScoreB = getBrowser().getBrowserElement(OR_AREA,
                            OR_BTN_ADD_SCORE).formatWithParms(k);
                    if (!new LeftClickElementAction(getBrowser(),
                            this.btnAddScoreB).performWithStates()) {
                        LOGGER.error(STRING_ERROR_MESSAGE);
                        return false;
                    }
                    this.btnAddScoreBelow = getBrowser().getBrowserElement(
                            OR_AREA, OR_BTN_ADD_SCORE_BELOW);
                    if (!new LeftClickElementAction(getBrowser(),
                            this.btnAddScoreBelow).performWithStates()) {
                        LOGGER.error(STRING_ERROR_MESSAGE);
                        return false;
                    }
                }
                break;

            case MULTIWEIGHTED_SCORE:
                result = populateMultiWeightedMeasuresValues(
                        conditionTypeValueVectors, checkElement);
                if (k != conditionTypes.length && result) {
                    this.btnAddScoreB = getBrowser().getBrowserElement(OR_AREA,
                            OR_BTN_ADD_SCORE).formatWithParms(k);
                    if (!new LeftClickElementAction(getBrowser(),
                            this.btnAddScoreB).performWithStates()) {
                        LOGGER.error(STRING_ERROR_MESSAGE);
                        return false;
                    }
                    this.btnAddScoreBelow = getBrowser().getBrowserElement(
                            OR_AREA, OR_BTN_ADD_SCORE_BELOW);
                    if (!new LeftClickElementAction(getBrowser(),
                            this.btnAddScoreBelow).performWithStates()) {
                        LOGGER.error(STRING_ERROR_MESSAGE);
                        return false;
                    }
                }
                break;

            case AUTO_MULTIWEIGHTED_SCORE:
                result = populateAutoMultiWeightedScoreValues(
                        conditionTypeValueVectors, checkElement);
                if (k != conditionTypes.length && result) {
                    this.btnAddScoreB = getBrowser().getBrowserElement(OR_AREA,
                            OR_BTN_ADD_SCORE).formatWithParms(k);
                    if (!new LeftClickElementAction(getBrowser(),
                            this.btnAddScoreB).performWithStates()) {
                        LOGGER.error(STRING_ERROR_MESSAGE);
                        return false;
                    }
                    this.btnAddScoreBelow = getBrowser().getBrowserElement(
                            OR_AREA, OR_BTN_ADD_SCORE_BELOW);
                    if (!new LeftClickElementAction(getBrowser(),
                            this.btnAddScoreBelow).performWithStates()) {
                        LOGGER.error(STRING_ERROR_MESSAGE);
                        return false;
                    }
                }
                break;

            case FIXED_NUMBER:
                result = populateFixedNumberValues(conditionTypeValueVectors,
                        checkElement);
                if (k != conditionTypes.length && result) {
                    this.btnAddScoreB = getBrowser().getBrowserElement(OR_AREA,
                            OR_BTN_ADD_SCORE).formatWithParms(k);
                    if (!new LeftClickElementAction(getBrowser(),
                            this.btnAddScoreB).performWithStates()) {
                        LOGGER.error(STRING_ERROR_MESSAGE);
                        return false;
                    }
                    this.btnAddScoreBelow = getBrowser().getBrowserElement(
                            OR_AREA, OR_BTN_ADD_SCORE_BELOW);
                    if (!new LeftClickElementAction(getBrowser(),
                            this.btnAddScoreBelow).performWithStates()) {
                        LOGGER.error(STRING_ERROR_MESSAGE);
                        return false;
                    }
                }
                break;

            default:
                LOGGER.error("No value provided for Condition to be added to Composite Scores");
                break;
            }

            i++;
        }
        if (!new ScrollToPageTop(getBrowser()).performWithStates()) {
            LOGGER.error("Not able to Scroll to Page Top");
            result = false;
        }

        if (!new LeftClickElementAction(getBrowser(),
                this.btnSaveCompositeScore).performWithStates()) {
            LOGGER.error("Not able to click on Save Button");
            return false;

        }

        this.msgSameNameError = getBrowser().getBrowserElement(OR_AREA,
                OR_MSG_SAVE_COMPOSITE_SCORE);
        if (this.msgSameNameError != null) {
            Date date = new Date();
            SimpleDateFormat sdf = new SimpleDateFormat("MMddyyyyhhmmss.SSS");
            String formattedDate = sdf.format(new Date(date.getTime()));
            WebElement element = getBrowser().getElementWithWait(
                    this.txtCompositeScoreName);
            if (element != null) {

                String textFound = element.getAttribute("value");
                String compositeScoreName = textFound + " " + formattedDate;

                if (!new ClearTextElementAction(getBrowser(),
                        this.txtCompositeScoreName).performWithStates()) {
                    LOGGER.error("Not able to clear text to Composite ScoreName field");
                    result = false;
                }

                if (!new SendTextToElementAction(getBrowser(),
                        this.txtCompositeScoreName, compositeScoreName)
                        .performWithStates()) {
                    LOGGER.error("Not able to send text to Composite ScoreName field");
                    result = false;
                }

            }

            if (!new ScrollToPageTop(getBrowser()).performWithStates()) {
                LOGGER.error("Not able to Scroll to Page Top");
                result = false;
            }

            if (!new LeftClickElementAction(getBrowser(),
                    this.btnSaveCompositeScore).performWithStates()) {
                LOGGER.error("Not able to click on Save Button");
                return false;

            }

        }
        return result;
    }

    /**
     * @param arrconditionValuesVector
     *            different condition type values for that condition
     * @param i
     *            index for the similar browser elements
     * @param checkElement
     *            Check element exists on the page
     * @param counter
     *            Counter that keeps track of the index of the array
     * @return returns true if condition type and all value are populated
     *         correctly else returns false
     */

    private boolean populateSingleTestScoreValues(
            String[] arrconditionValuesVector, WebElement checkElement) {
        boolean result = true;
        selectNewScoreType = "Single Test Score";
        if (checkElement != null) {
            if (!new DropdownSelectionAction(this.getBrowser(),
                    drpSelectNewScore, SelectorType.VISIBLE_TEXT,
                    selectNewScoreType).performWithStates()) {
                LOGGER.error("Failed to select from Select New Score drop down selection");
                result = false;
            }
        } else {
            result = false;
            LOGGER.warn("Could not find the Select New Score drop down");
        }
        if (!new LeftClickElementAction(getBrowser(), this.btnAddScoreCondition)
                .performWithStates()) {
            LOGGER.error(STRING_ERROR_MESSAGE);
            return false;
        }
        if (!new ClearTextElementAction(getBrowser(), this.getBrowser()
                .getBrowserElement(OR_AREA, OR_SINGLE_TEST_SCORE_PREFIX)
                .formatWithParms(1)).performWithStates()) {
            LOGGER.error("Not able to clear text from prefix field");
            result = false;
        }
        if (!new SendTextToElementAction(getBrowser(), this.getBrowser()
                .getBrowserElement(OR_AREA, OR_SINGLE_TEST_SCORE_PREFIX)
                .formatWithParms(1), arrconditionValuesVector[0])
                .performWithStates()) {
            LOGGER.error("Not able to send text to prefix field");
            result = false;
        }

        if (!new DropdownSelectionAction(getBrowser(),
                this.drpSelectSingleTestScoreTest.formatWithParms(1),
                SelectorType.VISIBLE_TEXT_REGEX, arrconditionValuesVector[1])
                .performWithStates(false, false)) {
            LOGGER.error("Not able to select New Condition Type from Dropdown");
            result = false;
        }
        return result;
    }

    /**
     * @param arrconditionValuesVector
     *            different condition type values for that condition
     * @param i
     *            index for the similar browser elements
     * @param checkElement
     *            Check element exists on the page
     * @param scoreMultiplierIndex
     * @param counter
     *            Counter that keeps track of the index of the array
     * @return returns true if condition type and all value are populated
     *         correctly else returns false
     */

    private boolean populateSingleStrandScoreValues(
            String[] arrconditionValuesVector, WebElement checkElement,
            int scoreMultiplierIndex) {
        boolean result = true;
        selectNewScoreType = "Single Strand Score";
        if (checkElement != null) {
            if (!new DropdownSelectionAction(this.getBrowser(),
                    drpSelectNewScore, SelectorType.VISIBLE_TEXT,
                    selectNewScoreType).performWithStates()) {
                LOGGER.error("Failed to select from Select New Score drop down selection");
                result = false;
            }
        } else {
            result = false;
            LOGGER.warn("Could not find the Select New Score drop down");
        }

        if (!new LeftClickElementAction(getBrowser(), this.btnAddScoreCondition)
                .performWithStates()) {
            LOGGER.error(STRING_ERROR_MESSAGE);
            return false;
        }

        if (!new ClearTextElementAction(getBrowser(), this.getBrowser()
                .getBrowserElement(OR_AREA, OR_SINGLE_TEST_SCORE_PREFIX)
                .formatWithParms(scoreMultiplierIndex)).performWithStates()) {
            LOGGER.error("Not able to clear text from prefix field");
            result = false;
        }

        if (!new SendTextToElementAction(getBrowser(), this.getBrowser()
                .getBrowserElement(OR_AREA, OR_SINGLE_TEST_SCORE_PREFIX)
                .formatWithParms(scoreMultiplierIndex),
                arrconditionValuesVector[0]).performWithStates()) {
            LOGGER.error("Not able to send text to prefix field");
            result = false;
        }

        if (!new DropdownSelectionAction(getBrowser(),
                this.drpSelectSingleStrandScoreTest,
                SelectorType.VISIBLE_TEXT_REGEX, arrconditionValuesVector[1])
                .performWithStates(false, true)) {
            LOGGER.error("Not able to select New Condition Type from Dropdown");
            result = false;
        }

        if (!new DropdownSelectionAction(getBrowser(),
                this.drpSelectSingleTestScoreTest
                        .formatWithParms(scoreMultiplierIndex),
                SelectorType.VISIBLE_TEXT_REGEX, arrconditionValuesVector[2])
                .performWithStates(false, true)) {
            LOGGER.error("Not able to select New Condition Type from Dropdown");
            result = false;
        }

        return result;
    }

    /**
     * @param arrconditionValuesVector
     *            different condition type values for that condition
     * @param i
     *            index for the similar browser elements
     * @param checkElement
     *            Check element exists on the page
     * @param counter
     *            Counter that keeps track of the index of the array
     * @return returns true if condition type and all value are populated
     *         correctly else returns false
     */
    private boolean populateMultiWeightedMeasuresValues(
            String[] arrconditionValuesVector, WebElement checkElement) {
        boolean result = true;
        selectNewScoreType = "Multi Weighted Score";
        if (checkElement != null) {
            if (!new DropdownSelectionAction(this.getBrowser(),
                    drpSelectNewScore, SelectorType.VISIBLE_TEXT,
                    selectNewScoreType).performWithStates()) {
                LOGGER.error("Failed to select from Select New Score drop down selection");
                result = false;
            }
        } else {
            result = false;
            LOGGER.warn("Could not find the Select New Score drop down");
        }

        if (!new LeftClickElementAction(getBrowser(), this.btnAddScoreCondition)
                .performWithStates()) {
            LOGGER.error(STRING_ERROR_MESSAGE);
            return false;
        }

        if (!clearAndSendTextToElements(
                this.getBrowser().getBrowserElement(OR_AREA,
                        OR_MUTLIPLE_WEIGHT_MEASURES_DYNAMIC),
                arrconditionValuesVector[0])) {
            LOGGER.error("Not able to send text to prefix field");
            result = false;
        }

        if (!new DropdownSelectionAction(getBrowser(),
                this.drpSelectMultipleWeightMeasures.formatWithParms(1),
                SelectorType.VISIBLE_TEXT_REGEX, arrconditionValuesVector[1])
                .performWithStates(false, false)) {
            LOGGER.error("Not able to select New Condition Type from Dropdown");
            result = false;
        }

        if (!new DropdownSelectionAction(getBrowser(),
                this.drpSelectMultipleWeightMeasures
                        .formatWithParms(INTG_CONSTANT_FORNUM2),
                SelectorType.VISIBLE_TEXT_REGEX, arrconditionValuesVector[3])
                .performWithStates(false, false)) {
            LOGGER.error("Not able to select New Condition Type from Dropdown");
            result = false;
        }

        if (!new DropdownSelectionAction(getBrowser(),
                this.drpSelectEqualNotEqual.formatWithParms(1),
                SelectorType.VISIBLE_TEXT_REGEX, arrconditionValuesVector[4])
                .performWithStates(false, false)) {
            LOGGER.error("Not able to select New Condition Type from Dropdown");
            result = false;
        }

        if (!new DropdownSelectionAction(getBrowser(),
                this.drpSelectConditionText, SelectorType.VISIBLE_TEXT_REGEX,
                arrconditionValuesVector[5]).performWithStates(false, false)) {
            LOGGER.error("Not able to select New Condition Type from Dropdown");
            result = false;
        }

        return result;
    }

    /**
     * @param arrconditionValuesVector
     *            different condition type values for that condition
     * @param i
     *            index for the similar browser elements
     * @param checkElement
     *            Check element exists on the page
     * @param counter
     *            Counter that keeps track of the index of the array
     * @return returns true if condition type and all value are populated
     *         correctly else returns false
     */

    private boolean populateAutoMultiWeightedScoreValues(
            String[] arrconditionValuesVector, WebElement checkElement) {
        boolean result = true;
        selectNewScoreType = "Auto Multiple Weighted Score";
        if (checkElement != null) {
            if (!new DropdownSelectionAction(this.getBrowser(),
                    drpSelectNewScore, SelectorType.VISIBLE_TEXT,
                    selectNewScoreType).performWithStates()) {
                LOGGER.error("Failed to select from Select New Score drop down selection");
                result = false;
            }
        } else {
            result = false;
            LOGGER.warn("Could not find the Select New Score drop down");
        }

        if (!new LeftClickElementAction(getBrowser(), this.btnAddScoreCondition)
                .performWithStates()) {
            LOGGER.error(STRING_ERROR_MESSAGE);
            return false;
        }

        if (!new ClearTextElementAction(getBrowser(), this.getBrowser()
                .getBrowserElement(OR_AREA, OR_MUTLIPLE_WEIGHT_MEASURES)
                .formatWithParms(1)).performWithStates()) {
            LOGGER.error("Not able to clear text from prefix field");
            result = false;
        }

        if (!new SendTextToElementAction(getBrowser(), this.getBrowser()
                .getBrowserElement(OR_AREA, OR_MUTLIPLE_WEIGHT_MEASURES)
                .formatWithParms(1), arrconditionValuesVector[0])
                .performWithStates()) {
            LOGGER.error("Not able to send text to prefix field");
            result = false;
        }

        if (!new DropdownSelectionAction(getBrowser(),
                this.drpSelectMultipleWeightMeasures.formatWithParms(1),
                SelectorType.VISIBLE_TEXT_REGEX, arrconditionValuesVector[1])
                .performWithStates(false, false)) {
            LOGGER.error("Not able to select New Condition Type from Dropdown");
            result = false;
        }

        if (!new DropdownSelectionAction(getBrowser(),
                this.drpSelectMultipleWeightMeasures.formatWithParms(2),
                SelectorType.VISIBLE_TEXT_REGEX, arrconditionValuesVector[2])
                .performWithStates(false, false)) {
            LOGGER.error("Not able to select New Condition Type from Dropdown");
            result = false;
        }
        return result;
    }

    /**
     * @param arrconditionValuesVector
     *            different condition type values for that condition
     * @param i
     *            index for the similar browser elements
     * @param checkElement
     *            Check element exists on the page
     * @param counter
     *            Counter that keeps track of the index of the array
     * @return returns true if condition type and all value are populated
     *         correctly else returns false.
     */

    private boolean populateFixedNumberValues(
            String[] arrconditionValuesVector, WebElement checkElement) {
        boolean result = true;
        selectNewScoreType = "Fixed Number";
        if (checkElement != null) {
            if (!new DropdownSelectionAction(this.getBrowser(),
                    drpSelectNewScore, SelectorType.VISIBLE_TEXT,
                    selectNewScoreType).performWithStates()) {
                LOGGER.error("Failed to select from Select New Score drop down selection");
                result = false;
            }
        } else {
            result = false;
            LOGGER.warn("Could not find the Select New Score drop down");
        }

        if (!new LeftClickElementAction(getBrowser(), this.btnAddScoreCondition)
                .performWithStates()) {
            LOGGER.error(STRING_ERROR_MESSAGE);
            return false;
        }

        if (!new ClearTextElementAction(getBrowser(), this.getBrowser()
                .getBrowserElement(OR_AREA, OR_FIXED_NUMBER).formatWithParms(1))
                .performWithStates()) {
            LOGGER.error("Not able to clear text from prefix field");
            result = false;
        }

        if (!new SendTextToElementAction(getBrowser(),
                this.getBrowser().getBrowserElement(OR_AREA, OR_FIXED_NUMBER)
                        .formatWithParms(1), arrconditionValuesVector[0])
                .performWithStates()) {
            LOGGER.error("Not able to send text to prefix field");
            result = false;
        }

        return result;
    }

    /**
     * @param be
     *            Browser Element for which text will be sent
     * @param text
     *            text that will be sent
     * @return true or false
     */
    private boolean clearAndSendTextToElements(BrowserElement be, String text) {
        boolean result = false;
        List<WebElement> list = getBrowser().getElementsWithWait(be);
        for (WebElement element : list) {
            element.clear();
            element.sendKeys(text);
            result = true;
        }

        return result;
    }
}
