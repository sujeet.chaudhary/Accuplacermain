package com.pearson.itautomation.accuplacer.placementsetup.majors;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.pearson.itautomation.bca.Browser;
import com.pearson.itautomation.bca.BrowserAction;
import com.pearson.itautomation.bca.BrowserElement;
import com.pearson.itautomation.bca.interaction.LeftClickElementAction;
import com.pearson.itautomation.bca.validation.ElementExistsValidation;

/**
 * This Class will Activate/Inactivate Created Major. <<br>
 * <b>perform</b> - This method will return true if Major will be
 * Activated/Inactivated.
 * 
 * @author Rupal Jain
 */
public class ActivateInactivateMajor extends BrowserAction {

    private static final Logger LOGGER = LoggerFactory
            .getLogger(ActivateInactivateMajor.class);

    private static final String OR_AREA = "OR_Placement_Setup";
    private static final String OR_ACTIVATE_INACTIVATE_MAJOR_XPATH = "MajorActivateInactivateXpath";
    private static final String OR_VALIDATION_MESSAGE_XPATH = "MajorActiveInactiveValidatinMessage";

    private final BrowserElement btnActiveInactiveMajor;
    private final BrowserElement txtActiveInactiveValidationMsg;

    /**
     * This constructor will initialize the mandatory fields and the
     * BrowserElements from this class
     * 
     * @param browser
     * @param strMajorName
     *            The Major Name which need to be View
     * @param index
     *            Indexes required to select correct XPath
     */

    public ActivateInactivateMajor(final Browser browser, String strMajorName,
            int index) {
        super(browser);

        this.btnActiveInactiveMajor = browser.getBrowserElement(OR_AREA,
                OR_ACTIVATE_INACTIVATE_MAJOR_XPATH).formatWithParms(
                strMajorName, strMajorName, index);

        this.txtActiveInactiveValidationMsg = browser.getBrowserElement(
                OR_AREA, OR_VALIDATION_MESSAGE_XPATH);

    }

    @Override
    protected boolean perform() {
        boolean result = true;

        if (!new ElementExistsValidation(getBrowser(),
                this.btnActiveInactiveMajor).performWithStates()) {
            LOGGER.error("Not able to Validate Activate/Inactivate Btn");
            return false;
        }

        if (!new LeftClickElementAction(getBrowser(),
                this.btnActiveInactiveMajor).performWithStates()) {
            LOGGER.error("Cannot click on Activate/Inactivate Button");
            return false;
        }

        if (!new ElementExistsValidation(getBrowser(),
                this.txtActiveInactiveValidationMsg).performWithStates()) {
            LOGGER.error("Can not Validate msg");
            return false;
        }

        return result;
    }
}
