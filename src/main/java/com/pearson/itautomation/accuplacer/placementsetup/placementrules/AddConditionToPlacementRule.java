package com.pearson.itautomation.accuplacer.placementsetup.placementrules;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.pearson.itautomation.accuplacer.core.Buttons;
import com.pearson.itautomation.accuplacer.core.action.LeftClickOnModalPopupButton;
import com.pearson.itautomation.accuplacer.interaction.ScrollToPageTop;
import com.pearson.itautomation.bca.Browser;
import com.pearson.itautomation.bca.BrowserAction;
import com.pearson.itautomation.bca.BrowserElement;
import com.pearson.itautomation.bca.SelectorType;
import com.pearson.itautomation.bca.interaction.ClearTextElementAction;
import com.pearson.itautomation.bca.interaction.DropdownSelectionAction;
import com.pearson.itautomation.bca.interaction.LeftClickElementAction;
import com.pearson.itautomation.bca.interaction.SendTextToElementAction;
import com.pearson.itautomation.bca.validation.ElementExistsValidation;

/**
 * This Class will responsible to add different conditions on different courses
 * to add different Placement Rules <b>checkPrestate</b> - N/A <br>
 * <b>checkPostate</b> - Will Validate the confirmation Msg <br>
 * <b>perform</b> - This method will return true if different conditions will
 * applied on different courses successfully.
 * 
 * @author Sujeet Chaudhary
 */
public class AddConditionToPlacementRule extends BrowserAction {

    private static final Logger LOGGER = LoggerFactory
            .getLogger(AddConditionToPlacementRule.class);

    private static final String OR_AREA = "OR_Placement_Setup";
    private static final String OR_ADD_CONDITION_TO_RULE_LINK = "AddConditionToRule";
    private static final String OR_SELECT_CONDITION = "SelectConditionPlacementRule";
    private static final String OR_BTN_ADD_CONDITION = "BTNAddCondition";
    private static final String OR_SELECT_STARTING_BRACKET = "SelectStartingBracket";
    private static final String OR_SELECT_CLOSING_BRACKET = "SelectClosingBracket";
    private static final String OR_SELECT_TEST = "SelectTestPlacementRule";
    private static final String OR_SELECT_LOGICAL_OPERATOR = "SelectLogicalOperator";
    private static final String OR_ADD_ANOTHER_CONDITION = "AddAnotherCondition";
    private static final String OR_ADD_CONDITION_BELOW = "AddConditionBelow";
    private static final String OR_SELECT_EQUAL_AND_NOT_EQUAL = "SelectEqualAndNotEqual";
    private static final String OR_SELECT_ANSWER_OF_BACKGROUND_QUESTION = "SelectAnswerOfBackgroundQuestion";
    private static final String OR_SINGLE_TEST_SCORE_PREFIX = "SingleTestScorePrefix";
    private static final String OR_SINGLE_TEST_SCORE_FINAL = "SingleTestScoreFinal";
    private static final String OR_SINGLE_STRAND_SCORE = "SingleStrandScore";
    private static final String OR_MAJOR_LIST_VIEW = "MajorListView";
    private static final String OR_SELECTED_MAJOR = "SelectedMajor";
    private static final String OR_ADD_SELECTED_MAJOR = "AddSelectedMajor";
    private static final String OR_MULTIPLE_TEST_SCORE_PREFIX = "MultipleTestScorePrefix";
    private static final String OR_SELECT_VALUE_FOR_MULTIPLE_TEST_SCORE = "SelectValueForMultipleTestScore";
    private static final String OR_PLACEMENT_RULE_CONFIRMATION_MSG = "PlacementRuleConfirmationMsg";
    private static final String OR_VALIDATE_DROP_DOWN_DATA = "ValidateDropDownData";

    private final BrowserElement txtAddConditionToRule;
    private final BrowserElement drpSelectConditionPlacementRule;
    private final BrowserElement btnAddCondition;
    private final BrowserElement drpSelectedMajor;
    private final BrowserElement btnAddSelectedMajor;
    private final BrowserElement txtPlacementRuleConfirmationMsg;
    private final BrowserElement optionValidateDropDownData;
    private final BrowserElement btnSavePlacementRule = Buttons
            .getButtonType1("Save");

    private final String[] strConditionArray;
    private final String[] strAllBracketArray;
    private final String[] strAlltestArray;
    private final String[] strLogicalOperatorArray;
    private final String[] strEqualNotEqualArray;
    private final String[] strAnswerOfBackgroundQuestionArray;
    private final String[] strSingleTestScorePrefixArray;
    private final String[] strSingleTestScoreFinalArray;
    private final String[] strSingleStrandScoreArray;
    private final String[] strSelectedMajorArray;
    private final String[] strSelectValueForMultipleTestScoreArray;
    private final String[] strSelectValueForAutoMultipleWeightedMajorArray;
    private String[] strSingleConditionBracketArray;
    private final int strNoOfCondition;

    private int equalNotEqual = 1;
    private int finalAndPrefixIndex = 1;
    private int singleStrandScore = 1;
    private int backgroundQuestion = 1;
    private int majorIndex = 1;
    private int selectTest = 1;
    private int multipleTestScore = 1;
    private int addCondition = 1;
    private int singleTestScore = 1;
    private int multipleTestScoreValue = 1;
    private int multipleTestScorePrefix = 1;
    private int multipletestScoreIndex = 1;

    /**
     * This constructor will initialize the mandatory fields and the
     * BrowserElements from this class
     * 
     * @param browser
     * @param strConditionAll
     *            Conditions to be applied with comma separated
     * @param strNoOfCondition
     *            No of conditions to be addad at a time with comma separated
     * @param strBracketAll
     *            Type of All bracket which require for enclosing the condition
     *            with comma separated
     * @param strTestAll
     *            Value to be selected from dropdown for selected condition with
     *            comma separated
     * @param strLogicalOperatorAll
     *            OR/AND which required to Separate two conditions with comma
     *            separated
     * @param strEqualNotEqualAll
     *            equal/not equal to .. etc which needs to be selected with
     *            comma separated
     * @param strAnswerOfBackgroundQuestionAll
     *            Answer of selected BackGround question with comma separated
     * @param strSingleTestScorePrefixAll
     *            Prefix score to be selected for the required conditions with
     *            comma separate
     * @param strSingleStrandScoreAll
     *            score need to entered for Single Stands Score with comma
     *            separated
     * @param strSelectedMajorAll
     *            value of the major conditions with comma separated
     * @param strSelectValueForMultipleTestScoreAll
     *            Value needs to be selected for Multiple Test Score with comma
     *            separated
     * @param strSelectValueForAutoMultipleWeightedMajorAll
     *            Value needs to be selected for Auto Multiple Weighted Major
     *            with comma separated
     * 
     */
    public AddConditionToPlacementRule(final Browser browser,
            String strConditionAll, String strNoOfCondition,
            String strBracketAll, String strTestAll,
            String strLogicalOperatorAll, String strEqualNotEqualAll,
            String strAnswerOfBackgroundQuestionAll,
            String strSingleTestScorePrefixAll,
            String strSingleTestScoreFinalAll, String strSingleStrandScoreAll,
            String strSelectedMajorAll,
            String strSelectValueForMultipleTestScoreAll,
            String strSelectValueForAutoMultipleWeightedMajorAll) {
        super(browser);
        this.strConditionArray = strConditionAll.split(",");
        this.strAllBracketArray = strBracketAll.split(",");
        this.strAlltestArray = strTestAll.split(",");
        this.strEqualNotEqualArray = strEqualNotEqualAll.split(",");
        this.strLogicalOperatorArray = strLogicalOperatorAll.split(",");
        this.strSingleTestScorePrefixArray = strSingleTestScorePrefixAll
                .split(",");
        this.strSingleTestScoreFinalArray = strSingleTestScoreFinalAll
                .split(",");
        this.strSingleStrandScoreArray = strSingleStrandScoreAll.split(",");

        this.strSelectedMajorArray = strSelectedMajorAll.split(",");

        this.strSelectValueForMultipleTestScoreArray = strSelectValueForMultipleTestScoreAll
                .split(",");

        this.strSelectValueForAutoMultipleWeightedMajorArray = strSelectValueForAutoMultipleWeightedMajorAll
                .split(",");

        this.strAnswerOfBackgroundQuestionArray = strAnswerOfBackgroundQuestionAll
                .split(",");
        this.strNoOfCondition = Integer.parseInt(strNoOfCondition);
        this.txtAddConditionToRule = browser.getBrowserElement(OR_AREA,
                OR_ADD_CONDITION_TO_RULE_LINK);
        this.drpSelectConditionPlacementRule = browser.getBrowserElement(
                OR_AREA, OR_SELECT_CONDITION);
        this.btnAddCondition = browser.getBrowserElement(OR_AREA,
                OR_BTN_ADD_CONDITION);
        this.drpSelectedMajor = browser.getBrowserElement(OR_AREA,
                OR_SELECTED_MAJOR);
        this.btnAddSelectedMajor = browser.getBrowserElement(OR_AREA,
                OR_ADD_SELECTED_MAJOR);
        this.txtPlacementRuleConfirmationMsg = browser.getBrowserElement(
                OR_AREA, OR_PLACEMENT_RULE_CONFIRMATION_MSG);

        this.optionValidateDropDownData = browser.getBrowserElement(OR_AREA,
                OR_VALIDATE_DROP_DOWN_DATA);

    }

    @Override
    protected boolean checkPostState() {

        boolean result = true;

        if (!new ScrollToPageTop(getBrowser()).performWithStates()) {
            LOGGER.error("Not able to go to Up");
            return false;
        }

        if (!new LeftClickElementAction(getBrowser(), this.btnSavePlacementRule)
                .performWithStates()) {
            LOGGER.error("Not able to click on Save Button");
            return false;

        }

        if (!new ElementExistsValidation(getBrowser(),
                this.txtPlacementRuleConfirmationMsg).performWithStates()) {
            LOGGER.error("Not able to validate message");
            result = false;
        }

        return result;
    }

    @Override
    protected boolean perform() {
        boolean result = true;

        for (int i = 1; i <= strNoOfCondition; i++) {
            if (i > 1 && i <= strNoOfCondition) {

                if (!new LeftClickElementAction(getBrowser(), this.getBrowser()
                        .getBrowserElement(OR_AREA, OR_ADD_ANOTHER_CONDITION)
                        .formatWithParms(addCondition)).performWithStates()) {
                    LOGGER.error("Not able to click on Add Another Condition");
                    return false;
                }

                if (!new LeftClickElementAction(getBrowser(), this.getBrowser()
                        .getBrowserElement(OR_AREA, OR_ADD_CONDITION_BELOW)
                        .formatWithParms(addCondition)).performWithStates()) {
                    LOGGER.error("Not able to click on Add a Condition below");
                    return false;
                }
                addCondition++;

            }

            if (i == 1) {
                if (!new LeftClickOnModalPopupButton(getBrowser(),
                        this.txtAddConditionToRule).performWithStates()) {
                    LOGGER.error("Not able to click on Add Condition Link");
                    return false;
                }
            }

            if (!new ElementExistsValidation(getBrowser(),
                    this.optionValidateDropDownData).performWithStates(false,
                    true)) {
                LOGGER.error("Not able to validate the Drop Down data");
                return false;
            }

            if (!new DropdownSelectionAction(getBrowser(),
                    this.drpSelectConditionPlacementRule,
                    SelectorType.VISIBLE_TEXT, this.strConditionArray[i - 1])
                    .performWithStates(false, true)) {
                LOGGER.error("Not able to select New Condition Type from Dropdown");
                return false;
            }

            if (!new ElementExistsValidation(getBrowser(), this.btnAddCondition)
                    .performWithStates()) {
                LOGGER.error("Not able to Validate Add button after selecting Condition");
                return false;
            }

            if (!new LeftClickOnModalPopupButton(getBrowser(),
                    this.btnAddCondition).performWithStates()) {
                LOGGER.error("Not able to click on Add button after selecting Condition");
                return false;
            }

            if (i > 1) {
                if (!new DropdownSelectionAction(getBrowser(), this
                        .getBrowser()
                        .getBrowserElement(OR_AREA, OR_SELECT_LOGICAL_OPERATOR)
                        .formatWithParms(i - 1), SelectorType.VISIBLE_TEXT,
                        this.strLogicalOperatorArray[i - 2]).performWithStates(
                        false, true)) {
                    LOGGER.error("Not able to select Operator from Dropdown");
                    return false;
                }
            }

            strSingleConditionBracketArray = strAllBracketArray[i - 1]
                    .split(" ");

            if (!new DropdownSelectionAction(getBrowser(), this.getBrowser()
                    .getBrowserElement(OR_AREA, OR_SELECT_STARTING_BRACKET)
                    .formatWithParms(i), SelectorType.VISIBLE_TEXT,
                    this.strSingleConditionBracketArray[0]).performWithStates(
                    false, true)) {
                LOGGER.error("Not able to select starting bracket from Dropdown");
                return false;
            }

            if ("Auto Multiple Weighted Measures"
                    .equals(strConditionArray[i - 1])) {

                if (!new ClearTextElementAction(getBrowser(), this
                        .getBrowser()
                        .getBrowserElement(OR_AREA,
                                OR_MULTIPLE_TEST_SCORE_PREFIX)
                        .formatWithParms(multipleTestScorePrefix))
                        .performWithStates()) {
                    LOGGER.error("Not able to clear Text from Auto Multiple Weighted Measures Prefix field");
                    return false;
                }
                if (!new SendTextToElementAction(
                        getBrowser(),
                        this.getBrowser()
                                .getBrowserElement(OR_AREA,
                                        OR_MULTIPLE_TEST_SCORE_PREFIX)
                                .formatWithParms(multipleTestScorePrefix),
                        this.strSingleTestScorePrefixArray[finalAndPrefixIndex - 1])
                        .performWithStates()) {
                    LOGGER.error("Not able to send text to Auto Multiple Weighted Measures Prefix field");
                    result = false;
                }

                if (!new DropdownSelectionAction(
                        getBrowser(),
                        this.getBrowser()
                                .getBrowserElement(OR_AREA,
                                        OR_SELECT_VALUE_FOR_MULTIPLE_TEST_SCORE)
                                .formatWithParms(multipleTestScore),
                        SelectorType.VISIBLE_TEXT,
                        this.strSelectValueForAutoMultipleWeightedMajorArray[multipleTestScoreValue - 1])
                        .performWithStates(false, true)) {
                    LOGGER.error("Not able to select value from Auto Multiple Weighted Measures Dropdown1");
                    return false;
                }

                if (!new DropdownSelectionAction(
                        getBrowser(),
                        this.getBrowser()
                                .getBrowserElement(OR_AREA,
                                        OR_SELECT_VALUE_FOR_MULTIPLE_TEST_SCORE)
                                .formatWithParms(++multipleTestScore),
                        SelectorType.VISIBLE_TEXT,
                        this.strSelectValueForAutoMultipleWeightedMajorArray[multipleTestScoreValue])
                        .performWithStates(false, true)) {
                    LOGGER.error("Not able to select value from Auto Multiple Weighted Measures Dropdown2");
                    return false;
                }

                if (!new ClearTextElementAction(getBrowser(), this.getBrowser()
                        .getBrowserElement(OR_AREA, OR_SINGLE_TEST_SCORE_FINAL)
                        .formatWithParms(finalAndPrefixIndex))
                        .performWithStates()) {
                    LOGGER.error("Not able to clear Auto Multiple Weighted Measures Final Score field");
                    return false;
                }

                if (!new SendTextToElementAction(
                        getBrowser(),
                        this.getBrowser()
                                .getBrowserElement(OR_AREA,
                                        OR_SINGLE_TEST_SCORE_FINAL)
                                .formatWithParms(finalAndPrefixIndex),
                        this.strSingleTestScoreFinalArray[finalAndPrefixIndex - 1])
                        .performWithStates()) {
                    LOGGER.error("Not able to send text to Auto Multiple Weighted Measures Final Score field");
                    return false;
                }

                if (!new DropdownSelectionAction(getBrowser(), this
                        .getBrowser()
                        .getBrowserElement(OR_AREA,
                                OR_SELECT_EQUAL_AND_NOT_EQUAL)
                        .formatWithParms(equalNotEqual),
                        SelectorType.VISIBLE_TEXT,
                        this.strEqualNotEqualArray[equalNotEqual - 1])
                        .performWithStates(false, true)) {
                    LOGGER.error("Not able to select Equal to Or Not Equal from Auto Multiple Weighted Measures Dropdown");
                    return false;
                }
                equalNotEqual++;
                finalAndPrefixIndex++;
                multipleTestScore++;
                addCondition++;
                addCondition++;
                multipleTestScoreValue++;
                multipleTestScoreValue++;
                multipleTestScorePrefix++;
            }

            if ("Multiple Test Score".equals(strConditionArray[i - 1])) {

                if (!new ClearTextElementAction(getBrowser(), this
                        .getBrowser()
                        .getBrowserElement(OR_AREA,
                                OR_MULTIPLE_TEST_SCORE_PREFIX)
                        .formatWithParms(multipleTestScorePrefix))
                        .performWithStates()) {
                    LOGGER.error("Not able to clear Text from Multiple Test Prefix field");
                    return false;
                }
                if (!new SendTextToElementAction(
                        getBrowser(),
                        this.getBrowser()
                                .getBrowserElement(OR_AREA,
                                        OR_MULTIPLE_TEST_SCORE_PREFIX)
                                .formatWithParms(multipleTestScorePrefix),
                        this.strSingleTestScorePrefixArray[finalAndPrefixIndex - 1])
                        .performWithStates()) {
                    LOGGER.error("Not able to send text to Multiple Test Prefix field");
                    result = false;
                }

                if (!new DropdownSelectionAction(
                        getBrowser(),
                        this.getBrowser()
                                .getBrowserElement(OR_AREA,
                                        OR_SELECT_VALUE_FOR_MULTIPLE_TEST_SCORE)
                                .formatWithParms(multipleTestScore),
                        SelectorType.VISIBLE_TEXT,
                        this.strSelectValueForMultipleTestScoreArray[multipletestScoreIndex - 1])
                        .performWithStates(false, true)) {
                    LOGGER.error("Not able to select value from Multiple Test Score Dropdown");
                    return false;
                }

                if (!new ClearTextElementAction(getBrowser(), this.getBrowser()
                        .getBrowserElement(OR_AREA, OR_SINGLE_TEST_SCORE_FINAL)
                        .formatWithParms(finalAndPrefixIndex))
                        .performWithStates()) {
                    LOGGER.error("Not able to clear Multiple Test Score Final Score field");
                    return false;
                }

                if (!new SendTextToElementAction(
                        getBrowser(),
                        this.getBrowser()
                                .getBrowserElement(OR_AREA,
                                        OR_SINGLE_TEST_SCORE_FINAL)
                                .formatWithParms(finalAndPrefixIndex),
                        this.strSingleTestScoreFinalArray[finalAndPrefixIndex - 1])
                        .performWithStates()) {
                    LOGGER.error("Not able to send Multiple Test Score Final Score field");
                    return false;
                }

                if (!new DropdownSelectionAction(getBrowser(), this
                        .getBrowser()
                        .getBrowserElement(OR_AREA,
                                OR_SELECT_EQUAL_AND_NOT_EQUAL)
                        .formatWithParms(equalNotEqual),
                        SelectorType.VISIBLE_TEXT,
                        this.strEqualNotEqualArray[equalNotEqual - 1])
                        .performWithStates(false, true)) {
                    LOGGER.error("Not able to select Equal to Or Not Equal from Multiple Test Score Dropdown");
                    return false;
                }
                equalNotEqual++;
                finalAndPrefixIndex++;
                multipleTestScore++;
                addCondition++;
                multipleTestScorePrefix++;
                multipletestScoreIndex++;
            }

            if ("Single Strand Score".equals(strConditionArray[i - 1])) {
                if (!new DropdownSelectionAction(getBrowser(), this
                        .getBrowser()
                        .getBrowserElement(OR_AREA, OR_SINGLE_STRAND_SCORE)
                        .formatWithParms(singleStrandScore),
                        SelectorType.VISIBLE_TEXT,
                        this.strSingleStrandScoreArray[singleStrandScore - 1])
                        .performWithStates(false, true)) {
                    LOGGER.error("Not able to select Equal to Or Not Equal from  Single Strand Score Dropdown");
                    return false;
                }
                singleStrandScore++;
            }

            if ("Major".equals(strConditionArray[i - 1])) {
                if (!new LeftClickElementAction(getBrowser(), this.getBrowser()
                        .getBrowserElement(OR_AREA, OR_MAJOR_LIST_VIEW)
                        .formatWithParms(majorIndex)).performWithStates()) {
                    LOGGER.error("Not able to click on Major List button");
                    return false;
                }

                if (!new DropdownSelectionAction(getBrowser(),
                        this.drpSelectedMajor, SelectorType.VISIBLE_TEXT,
                        this.strSelectedMajorArray[majorIndex - 1])
                        .performWithStates(true, true)) {
                    LOGGER.error("Not able to select Equal to Or Not Equal from Major Dropdown");
                    return false;
                }

                if (!new LeftClickElementAction(getBrowser(),
                        this.btnAddSelectedMajor).performWithStates()) {
                    LOGGER.error("Not able to click on Add button after selecting Major");
                    return false;
                }
                majorIndex++;
            }

            if (!"Major".equals(strConditionArray[i - 1])
                    && !"Multiple Test Score".equals(strConditionArray[i - 1])
                    && !"Auto Multiple Weighted Measures"
                            .equals(strConditionArray[i - 1])) {
                if (!new DropdownSelectionAction(getBrowser(), this
                        .getBrowser()
                        .getBrowserElement(OR_AREA, OR_SELECT_TEST)
                        .formatWithParms(selectTest),
                        SelectorType.VISIBLE_TEXT,
                        this.strAlltestArray[selectTest - 1])
                        .performWithStates(false, true)) {
                    LOGGER.error("Not able to select text value from Dropdown");
                    return false;
                }
                selectTest++;
            }

            if ("Background Question".equals(strConditionArray[i - 1])) {
                if (!new DropdownSelectionAction(getBrowser(), this
                        .getBrowser()
                        .getBrowserElement(OR_AREA,
                                OR_SELECT_EQUAL_AND_NOT_EQUAL)
                        .formatWithParms(equalNotEqual),
                        SelectorType.VISIBLE_TEXT,
                        this.strEqualNotEqualArray[equalNotEqual - 1])
                        .performWithStates(false, true)) {
                    LOGGER.error("Not able to select Equal to Or Not Equal from Background Question Dropdown");
                    return false;
                }

                if (!new DropdownSelectionAction(
                        getBrowser(),
                        this.getBrowser()
                                .getBrowserElement(OR_AREA,
                                        OR_SELECT_ANSWER_OF_BACKGROUND_QUESTION)
                                .formatWithParms(backgroundQuestion),
                        SelectorType.VISIBLE_TEXT,
                        this.strAnswerOfBackgroundQuestionArray[backgroundQuestion - 1])
                        .performWithStates(false, true)) {
                    LOGGER.error("Not able to select answer from Background Question Dropdown");
                    return false;
                }
                equalNotEqual++;
                backgroundQuestion++;
            }

            if ("Single Test Score".equals(strConditionArray[i - 1])
                    || "Composite Score".equals(strConditionArray[i - 1])
                    || "Single Strand Score".equals(strConditionArray[i - 1])) {

                if (!new ClearTextElementAction(getBrowser(),
                        this.getBrowser()
                                .getBrowserElement(OR_AREA,
                                        OR_SINGLE_TEST_SCORE_PREFIX)
                                .formatWithParms(singleTestScore))
                        .performWithStates()) {
                    LOGGER.error("Not able to clear text from prefix field field");
                    result = false;
                }

                if (!new SendTextToElementAction(
                        getBrowser(),
                        this.getBrowser()
                                .getBrowserElement(OR_AREA,
                                        OR_SINGLE_TEST_SCORE_PREFIX)
                                .formatWithParms(singleTestScore),
                        this.strSingleTestScorePrefixArray[finalAndPrefixIndex - 1])
                        .performWithStates()) {
                    LOGGER.error("Not able to send text to prefix field");
                    result = false;
                }

                if (!new DropdownSelectionAction(getBrowser(), this
                        .getBrowser()
                        .getBrowserElement(OR_AREA,
                                OR_SELECT_EQUAL_AND_NOT_EQUAL)
                        .formatWithParms(equalNotEqual),
                        SelectorType.VISIBLE_TEXT,
                        this.strEqualNotEqualArray[equalNotEqual - 1])
                        .performWithStates(false, true)) {
                    LOGGER.error("Not able to select Equal to Or Not Equal from Dropdown");
                    return false;
                }

                if (!new ClearTextElementAction(getBrowser(), this.getBrowser()
                        .getBrowserElement(OR_AREA, OR_SINGLE_TEST_SCORE_FINAL)
                        .formatWithParms(finalAndPrefixIndex))
                        .performWithStates()) {
                    LOGGER.error("Not able to clear final score field");
                    return false;
                }

                if (!new SendTextToElementAction(
                        getBrowser(),
                        this.getBrowser()
                                .getBrowserElement(OR_AREA,
                                        OR_SINGLE_TEST_SCORE_FINAL)
                                .formatWithParms(finalAndPrefixIndex),
                        this.strSingleTestScoreFinalArray[finalAndPrefixIndex - 1])
                        .performWithStates()) {
                    LOGGER.error("Not able to send text to final field");
                    return false;
                }
                equalNotEqual++;
                finalAndPrefixIndex++;
                singleTestScore++;
            }

            if ("User Defined Field".equals(strConditionArray[i - 1])) {

                if (!new DropdownSelectionAction(getBrowser(), this
                        .getBrowser()
                        .getBrowserElement(OR_AREA,
                                OR_SELECT_EQUAL_AND_NOT_EQUAL)
                        .formatWithParms(equalNotEqual),
                        SelectorType.VISIBLE_TEXT,
                        this.strEqualNotEqualArray[equalNotEqual - 1])
                        .performWithStates(false, true)) {
                    LOGGER.error("Not able to select Equal to Or Not Equal from User Defined Field Dropdown");
                    return false;
                }

                if (!new ClearTextElementAction(getBrowser(), this.getBrowser()
                        .getBrowserElement(OR_AREA, OR_SINGLE_TEST_SCORE_FINAL)
                        .formatWithParms(finalAndPrefixIndex))
                        .performWithStates()) {
                    LOGGER.error("Not able to clear text from User Defined Field final score field");
                    return false;
                }

                if (!new SendTextToElementAction(
                        getBrowser(),
                        this.getBrowser()
                                .getBrowserElement(OR_AREA,
                                        OR_SINGLE_TEST_SCORE_FINAL)
                                .formatWithParms(finalAndPrefixIndex),
                        this.strSingleTestScoreFinalArray[finalAndPrefixIndex - 1])
                        .performWithStates()) {
                    LOGGER.error("Not able to send text to User Defined Field final field");
                    return false;
                }
                equalNotEqual++;
                finalAndPrefixIndex++;
            }

            if (!new DropdownSelectionAction(getBrowser(), this.getBrowser()
                    .getBrowserElement(OR_AREA, OR_SELECT_CLOSING_BRACKET)
                    .formatWithParms(i), SelectorType.VISIBLE_TEXT,
                    this.strSingleConditionBracketArray[1]).performWithStates(
                    false, true)) {
                LOGGER.error("Not able to Close the bracket");
                return false;
            }
        }

        return result;
    }
}
