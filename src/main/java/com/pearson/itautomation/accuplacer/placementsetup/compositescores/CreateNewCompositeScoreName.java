package com.pearson.itautomation.accuplacer.placementsetup.compositescores;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.pearson.itautomation.bca.Browser;
import com.pearson.itautomation.bca.BrowserAction;
import com.pearson.itautomation.bca.BrowserElement;
import com.pearson.itautomation.bca.interaction.SendTextToElementAction;

/**
 * This Class will fill the Composite Score Name,Description and Comment on
 * Composite Score Name Page
 * 
 * <b>checkPrestate</b> - N/A <br>
 * <b>checkPostate</b> - N/A <br>
 * <b>perform</b> - This method will return true if all the three fields have
 * been filled successfully.
 * 
 * @author Solomon Lingala
 */

public class CreateNewCompositeScoreName extends BrowserAction {
    private static final Logger LOGGER = LoggerFactory
            .getLogger(CreateNewCompositeScoreName.class);

    private static final String OR_AREA = "OR_Placement_Setup";
    private static final String OR_RULE_NAME = "CompositeScoreName";
    private static final String OR_RULE_DESCRIPTION = "CompositeScoreDescription";
    private static final String OR_RULES_COMMENT = "CompositeScoreComment";

    private final BrowserElement txtCompositeScoreName;
    private final BrowserElement txtCompositeScoreDescription;
    private final BrowserElement txtCompositeScoreComments;

    private final String compositeScoreName;
    private final String compositeScoreDescription;
    private final String compositeScoreComments;

    /**
     * This constructor will initialize the mandatory fields and the
     * BrowserElements from this class
     * 
     * @param browser
     * @param compositeScoreName
     *            The Value of 'Composite Score Name' to be entered
     * @param compositeScoreDescription
     *            Value of 'Composite Score Description' to be entered
     * @param compositeScoreComments
     *            Value of 'Composite Score comment' to be entered
     */

    public CreateNewCompositeScoreName(final Browser browser,
            String compositeScoreName, String compositeScoreDescription,
            String compositeScoreComments) {
        super(browser);
        this.compositeScoreName = compositeScoreName;
        this.compositeScoreDescription = compositeScoreDescription;
        this.compositeScoreComments = compositeScoreComments;
        this.txtCompositeScoreName = browser.getBrowserElement(OR_AREA,
                OR_RULE_NAME);
        this.txtCompositeScoreDescription = browser.getBrowserElement(OR_AREA,
                OR_RULE_DESCRIPTION);
        this.txtCompositeScoreComments = browser.getBrowserElement(OR_AREA,
                OR_RULES_COMMENT);

    }

    @Override
    protected boolean perform() {
        boolean result = true;

        if (!new SendTextToElementAction(getBrowser(),
                this.txtCompositeScoreName, compositeScoreName)
                .performWithStates()) {
            LOGGER.error("Not able to send text to Composite ScoreName field");
            result = false;
        }

        if (!new SendTextToElementAction(getBrowser(),
                this.txtCompositeScoreDescription, compositeScoreDescription)
                .performWithStates()) {
            LOGGER.error("Not able to send text to Composite Score Description field");
            result = false;
        }

        if (!new SendTextToElementAction(getBrowser(),
                this.txtCompositeScoreComments, compositeScoreComments)
                .performWithStates()) {
            LOGGER.error("Not able to send text to Composite Score Comments field");
            result = false;
        }
        return result;
    }
}
