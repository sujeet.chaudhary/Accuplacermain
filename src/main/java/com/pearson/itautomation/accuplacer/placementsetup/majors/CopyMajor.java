package com.pearson.itautomation.accuplacer.placementsetup.majors;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.pearson.itautomation.accuplacer.core.action.LeftClickOnModalPopupButton;
import com.pearson.itautomation.bca.Browser;
import com.pearson.itautomation.bca.BrowserAction;
import com.pearson.itautomation.bca.BrowserElement;
import com.pearson.itautomation.bca.interaction.LeftClickElementAction;
import com.pearson.itautomation.bca.interaction.SendTextToElementAction;

/**
 * This Class will Copy the Major. <br>
 * <b>perform</b> - This method will return true if Major will copy
 * successfully.
 * 
 * @author Rupal Jain
 */
public class CopyMajor extends BrowserAction {

    private static final Logger LOGGER = LoggerFactory
            .getLogger(CopyMajor.class);

    private static final String OR_AREA = "OR_Placement_Setup";
    private static final String OR_NEW_MAJOR_NAME = "NewMajorName";
    private static final String OR_SAVE = "Save";
    private static final String OR_COPY_MAJOR_XPATH = "MajorCopyXpath";

    private final BrowserElement btnCopyMajor;
    private final BrowserElement txtNewMajorName;
    private final BrowserElement btnSave;

    private final String strCopyMajorName;

    /**
     * This constructor will initialize the mandatory fields and the
     * BrowserElements from this class
     * 
     * @param browser
     * @param strMajorName
     *            The Major which need to be Copy
     * @param strCopyMajorName
     *            The Major New Name for copy purpose
     * 
     */
    public CopyMajor(final Browser browser, String strMajorName,
            String strCopyMajorName) {
        super(browser);

        this.strCopyMajorName = strCopyMajorName;

        this.btnCopyMajor = browser.getBrowserElement(OR_AREA,
                OR_COPY_MAJOR_XPATH)
                .formatWithParms(strMajorName, strMajorName);

        this.txtNewMajorName = browser.getBrowserElement(OR_AREA,
                OR_NEW_MAJOR_NAME);

        this.btnSave = browser.getBrowserElement(OR_AREA, OR_SAVE);

    }

    @Override
    protected boolean perform() {
        boolean result = true;

        if (!new LeftClickElementAction(getBrowser(), this.btnCopyMajor)
                .performWithStates()) {
            LOGGER.error("Not able to click on Copy Button");
            return false;
        }

        if (!new SendTextToElementAction(getBrowser(), this.txtNewMajorName,
                strCopyMajorName).performWithStates()) {
            LOGGER.debug("Not able to send text to New Major field");
            return false;
        }

        if (!new LeftClickOnModalPopupButton(getBrowser(), this.btnSave)
                .performWithStates()) {
            LOGGER.error("Not able to click on Save Button");
            return false;
        }

        return result;
    }
}
