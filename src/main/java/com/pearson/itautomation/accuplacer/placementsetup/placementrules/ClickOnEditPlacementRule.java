package com.pearson.itautomation.accuplacer.placementsetup.placementrules;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.pearson.itautomation.bca.Browser;
import com.pearson.itautomation.bca.BrowserAction;
import com.pearson.itautomation.bca.BrowserElement;
import com.pearson.itautomation.bca.interaction.LeftClickElementAction;

/**
 * This Class will Clic on Edit Button of Placement Rule <br>
 * <b>checkPrestate</b> - N/A <br>
 * <b>checkPostate</b> - N/A <br>
 * <b>perform</b> - This method will return true if Edit button of Placement
 * Rule will clicked successfully will be edited successfully.
 * 
 * @author Sujeet Chaudhary
 */
public class ClickOnEditPlacementRule extends BrowserAction {

    private static final Logger LOGGER = LoggerFactory
            .getLogger(ClickOnEditPlacementRule.class);

    private static final String OR_AREA = "OR_TestSetup";

    private static final String OR_EDIT_PLACEMENT_RULE_XPATH = "BranchingProfileEditXpath";

    private final BrowserElement btnEditPlacementRule;

    /**
     * This constructor will initialize the mandatory fields and the
     * BrowserElements from this class
     * 
     * @param browser
     * @param strPlacementRule
     *            The Placement Rule Name which need to be Edit
     * 
     * 
     */
    public ClickOnEditPlacementRule(final Browser browser,
            String strPlacementRule) {
        super(browser);

        this.btnEditPlacementRule = browser.getBrowserElement(OR_AREA,
                OR_EDIT_PLACEMENT_RULE_XPATH).formatWithParms(strPlacementRule,
                strPlacementRule);
    }

    @Override
    protected boolean perform() {
        boolean result = true;
        if (!new LeftClickElementAction(getBrowser(), this.btnEditPlacementRule)
                .performWithStates()) {
            LOGGER.error("Not able to click on Edit Button");
            result = false;
        }

        return result;

    }
}
