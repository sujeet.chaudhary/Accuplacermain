package com.pearson.itautomation.accuplacer.placementsetup.majors;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.pearson.itautomation.bca.Browser;
import com.pearson.itautomation.bca.BrowserAction;
import com.pearson.itautomation.bca.BrowserElement;
import com.pearson.itautomation.bca.interaction.LeftClickElementAction;
import com.pearson.itautomation.bca.validation.ElementExistsValidation;

/**
 * This Class will Delete the Major. <<br>
 * <b>perform</b> - This method will return true if Major will be Deleted
 * successfully.
 * 
 * @author Rupal Jain
 */
public class DeleteMajor extends BrowserAction {

    private static final Logger LOGGER = LoggerFactory
            .getLogger(DeleteMajor.class);

    private static final String OR_AREA = "OR_Placement_Setup";
    private static final String OR_DELETE_MAJOR_XPATH = "MajorDeleteXpath";
    private static final String OR_VALIDATION_MESSAGE_XPATH = "MajorDeleteValidationMessage";
    private static final String OR_DELETE_YES = "MajorDeleteYesButton";
    private static final int WAIT_FOR_MSG = 20000;
    private final BrowserElement btnDeleteMajor;
    private final BrowserElement txtDeleteValidationMsg;
    private final BrowserElement btnDeleteYes;

    /**
     * This constructor will initialize the mandatory fields and the
     * BrowserElements from this class
     * 
     * @param browser
     * @param strMajorName
     *            The Major Name which need to be Delete
     * 
     * 
     */

    public DeleteMajor(final Browser browser, String strMajorName) {
        super(browser);

        this.btnDeleteMajor = browser.getBrowserElement(OR_AREA,
                OR_DELETE_MAJOR_XPATH).formatWithParms(strMajorName,
                strMajorName);

        this.txtDeleteValidationMsg = browser.getBrowserElement(OR_AREA,
                OR_VALIDATION_MESSAGE_XPATH);

        this.btnDeleteYes = browser.getBrowserElement(OR_AREA, OR_DELETE_YES);
    }

    @Override
    protected boolean perform() {
        boolean result = true;

        if (!new ElementExistsValidation(getBrowser(), this.btnDeleteMajor)
                .performWithStates()) {
            LOGGER.error("Not able to Validate Delete Btn");
            return false;
        }

        if (!new LeftClickElementAction(getBrowser(), this.btnDeleteMajor)
                .performWithStates()) {
            LOGGER.error("Not able to click on Delete Button");
            return false;
        }

        if (!new LeftClickElementAction(getBrowser(), this.btnDeleteYes)
                .performWithStates()) {
            LOGGER.error("Not able to click on Yes Button");
            return false;
        }
        
        txtDeleteValidationMsg.setTimeout(WAIT_FOR_MSG);
        
        if (!new ElementExistsValidation(getBrowser(),
                this.txtDeleteValidationMsg).performWithStates()) {
            LOGGER.error("Not able to Validate msg");
            return false;
        }

        return result;
    }
}
