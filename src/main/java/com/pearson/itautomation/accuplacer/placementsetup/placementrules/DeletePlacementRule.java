package com.pearson.itautomation.accuplacer.placementsetup.placementrules;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.pearson.itautomation.bca.Browser;
import com.pearson.itautomation.bca.BrowserAction;
import com.pearson.itautomation.bca.BrowserElement;
import com.pearson.itautomation.bca.interaction.LeftClickElementAction;
import com.pearson.itautomation.bca.validation.ElementExistsValidation;

/**
 * This Class will Delete the Placement Rule. <<br>
 * <b>checkPrestate</b> -This method will return true if Delete button is
 * available otherwise return false<br>
 * <b>checkPostate</b> - This method will return true if Delete confirmation msg
 * will appear otherwise return false. <br>
 * <b>perform</b> - This method will return true if Placement Rule will be
 * Deleted successfully.
 * 
 * @author Sujeet Chaudhary
 */
public class DeletePlacementRule extends BrowserAction {

    private static final Logger LOGGER = LoggerFactory
            .getLogger(DeletePlacementRule.class);

    private static final String OR_AREA = "OR_TestSetup";
    private static final String OR_AREA1 = "OR_Placement_Setup";
    private static final String OR_DELETE_PLACEMENT_RULE_XPATH = "BranchingProfileDeleteXpath";
    private static final String OR_VALIDATION_MESSAGE_XPATH = "PlacementRuleDeleteValidatinMessage";
    private static final String OR_DELETE_YES = "BranchingProfileDeleteYesButton";

    private final BrowserElement btnDeletePlacementRule;
    private final BrowserElement txtDeleteValidationMsg;
    private final BrowserElement btnDeleteYes;

    /**
     * This constructor will initialize the mandatory fields and the
     * BrowserElements from this class
     * 
     * @param browser
     * @param strPlacementRuleTitle
     *            The Placement Rule title which need to be Deleted.
     * 
     * 
     */

    public DeletePlacementRule(final Browser browser,
            String strPlacementRuleTitle) {
        super(browser);

        this.btnDeletePlacementRule = browser.getBrowserElement(OR_AREA,
                OR_DELETE_PLACEMENT_RULE_XPATH).formatWithParms(
                strPlacementRuleTitle, strPlacementRuleTitle);

        this.txtDeleteValidationMsg = browser.getBrowserElement(OR_AREA1,
                OR_VALIDATION_MESSAGE_XPATH);

        this.btnDeleteYes = browser.getBrowserElement(OR_AREA, OR_DELETE_YES);
    }

    @Override
    protected boolean checkPreState() {
        boolean result = true;
        if (!new ElementExistsValidation(getBrowser(),
                this.btnDeletePlacementRule).performWithStates()) {
            LOGGER.error("Not able to Validate Delete Btn");
            result = false;
        }

        return result;
    }

    @Override
    protected boolean checkPostState() {
        boolean result = true;

        if (!new ElementExistsValidation(getBrowser(),
                this.txtDeleteValidationMsg).performWithStates()) {
            LOGGER.error("Not able to Validate msg");
            result = false;
        }

        return result;
    }

    @Override
    protected boolean perform() {
        boolean result = true;

        if (!new LeftClickElementAction(getBrowser(),
                this.btnDeletePlacementRule).performWithStates()) {
            LOGGER.error("Not able to click on Delete Button");
            result = false;
        }

        if (!new LeftClickElementAction(getBrowser(), this.btnDeleteYes)
                .performWithStates()) {
            LOGGER.error("Not able to click on Yes Button");
            result = false;
        }

        return result;
    }
}
