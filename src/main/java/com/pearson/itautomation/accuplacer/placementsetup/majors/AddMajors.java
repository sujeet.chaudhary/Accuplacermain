package com.pearson.itautomation.accuplacer.placementsetup.majors;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.pearson.itautomation.accuplacer.core.AccuplacerBrowserUtils;
import com.pearson.itautomation.accuplacer.core.CommonUtilities;
import com.pearson.itautomation.accuplacer.core.action.LeftClickOnModalPopupButton;
import com.pearson.itautomation.bca.Browser;
import com.pearson.itautomation.bca.BrowserAction;
import com.pearson.itautomation.bca.BrowserElement;
import com.pearson.itautomation.bca.interaction.SendTextToElementAction;
import com.pearson.itautomation.bca.validation.ElementExistsValidation;

/**
 * This Class will fill out the Add Major Form with all the mandatory details.<br>
 * <b>perform</b> - This method will return true if all the mandatory fields
 * have been filled in and saved successfully.
 * 
 * @author Rupal Jain
 */

public class AddMajors extends BrowserAction {

    private static final Logger LOGGER = LoggerFactory
            .getLogger(AddMajors.class);
    
    private static final String OR_AREA = "OR_Placement_Setup";
    private static final String OR_MAJOR_CODE = "MajorCode";
    private static final String OR_MAJOR_NAME = "MajorName";
    private static final String OR_MAJOR_COMMENT = "MajorComment";
    private static final String OR_DISABLED_SAVE = "DisabledSave";
    private static final String OR_ADD_MAJOR = "AddMajorButton";

    private final BrowserElement txtMajorCode;
    private final BrowserElement txtMajorName;
    private final BrowserElement txtMajorComment;
    private final BrowserElement btnDisabledSave;
    private final BrowserElement btnAddMajor;

    private String majorName;
    private String majorCode;
    private static final String MAJOR_COMMENT = "Comment";

    private static final Integer WAIT_FOR_LOAD = 500;

    /**
     * This constructor will initialize the mandatory fields and the
     * BrowserElements from this class
     * 
     * @param browser
     * @param strMajorName
     *            Major name
     *	@param strMajorCode
     *            Major Code 
     */

    public AddMajors(final Browser browser, String strMajorName, String strMajorCode) {
        super(browser);

        this.btnAddMajor = browser.getBrowserElement(OR_AREA, OR_ADD_MAJOR);
        this.txtMajorCode = browser.getBrowserElement(OR_AREA, OR_MAJOR_CODE);
        this.txtMajorName = browser.getBrowserElement(OR_AREA, OR_MAJOR_NAME);
        this.txtMajorComment = browser.getBrowserElement(OR_AREA,
                OR_MAJOR_COMMENT);
        this.btnDisabledSave = browser.getBrowserElement(OR_AREA,
                OR_DISABLED_SAVE);
        this.majorName = strMajorName;
        this.majorCode = strMajorCode;

    }

    @Override
    protected boolean perform() {
        boolean result = true;

        if (!new LeftClickOnModalPopupButton(getBrowser(), this.btnAddMajor)
                .performWithStates()) {
            LOGGER.error("Not able to click on Add Button");
            return false;
        }

        AccuplacerBrowserUtils.WaitInMilliSeconds(WAIT_FOR_LOAD);
        
    	if (this.majorCode == "CODE") {
    		this.majorCode = this.majorCode + CommonUtilities.getCurrentTime();
    	}
        
        if (!new SendTextToElementAction(getBrowser(), this.txtMajorCode,
        		this.majorCode)
                .performWithStates()) {
            LOGGER.error("Not able to send text to First Name field");
            result = false;
        }

        if (!new SendTextToElementAction(getBrowser(), this.txtMajorName,
                this.majorName).performWithStates()) {
            LOGGER.error("Not able to send text to Major Name field");
            result = false;
        }

        if (!new SendTextToElementAction(getBrowser(), this.txtMajorComment,
                MAJOR_COMMENT + CommonUtilities.getCurrentTime())
                .performWithStates()) {
            LOGGER.error("Not able to send text to Major Comment field");
            result = false;
        }

        if (new ElementExistsValidation(getBrowser(), this.btnDisabledSave)
                .performWithStates()
                && !new ReFillMajor(getBrowser(), this.majorName)
                        .performWithStates()) {
            LOGGER.error("Save button is not enabled");
            return false;

        }

        return result;
    }
}
