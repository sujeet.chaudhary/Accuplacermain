package com.pearson.itautomation.accuplacer.placementsetup.placementrules;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.pearson.itautomation.accuplacer.core.Buttons;
import com.pearson.itautomation.accuplacer.core.action.LeftClickOnModalPopupButton;
import com.pearson.itautomation.bca.Browser;
import com.pearson.itautomation.bca.BrowserAction;
import com.pearson.itautomation.bca.BrowserElement;
import com.pearson.itautomation.bca.SelectorType;
import com.pearson.itautomation.bca.interaction.ClearTextElementAction;
import com.pearson.itautomation.bca.interaction.DropdownSelectionAction;
import com.pearson.itautomation.bca.interaction.LeftClickElementAction;
import com.pearson.itautomation.bca.interaction.SendTextToElementAction;
import com.pearson.itautomation.bca.validation.ElementExistsValidation;

/**
 * This Class will responsible to add Multi Weighted Measure conditions on
 * different courses to add different Placement Rules <b>checkPrestate</b> - N/A <br>
 * <b>checkPostate</b> - Will Validate the confirmation Msg<br>
 * <b>perform</b> - This method will return true if Multi Weighted Measure will
 * applied on different courses successfully.
 * 
 * @author Sujeet Chaudhary
 */
public class AddMultiWeightedMeasureCondition extends BrowserAction {

    private static final Logger LOGGER = LoggerFactory
            .getLogger(AddMultiWeightedMeasureCondition.class);

    private static final String OR_AREA = "OR_Placement_Setup";
    private static final String OR_ADD_CONDITION_TO_RULE_LINK = "AddConditionToRule";
    private static final String OR_SELECT_STARTING_BRACKET = "SelectStartingBracket";
    private static final String OR_SELECT_CLOSING_BRACKET = "SelectClosingBracket";
    private static final String OR_BTN_ADD_CONDITION = "BTNAddCondition";
    private static final String OR_SELECT_CONDITION = "SelectConditionPlacementRule";
    private static final String OR_MULTI_WEIGHTED_VALUE = "SelectValueForMultipleTestScore";
    private static final String OR_MULTI_WEIGHTED_MEASURE_PREFIX_SCORE = "MultipleTestScorePrefix";
    private static final String OR_MULTI_WEIGHTED_MEASURE_FINAL_SCORE = "SingleTestScoreFinal";
    private static final String OR_SELECT_EQUAL_AND_NOT_EQUAL = "SelectEqualAndNotEqual";
    private static final String OR_SELECT_ANSWER_FOR_AUTO_MULTIPLE_WEIGHTED_MEASURE = "SelectAnsForAutoMultipleWeightedMeasures";
    private static final String OR_PLACEMENT_RULE_CONFIRMATION_MSG = "PlacementRuleConfirmationMsg";
    private static final String OR_VALIDATE_DROP_DOWN_DATA = "ValidateDropDownData";

    private final BrowserElement txtAddConditionToRule;
    private final BrowserElement drpSelectConditionPlacementRule;
    private final BrowserElement btnAddCondition;
    private final BrowserElement txtPlacementRuleConfirmationMsg;
    private final BrowserElement optionValidateDropDownData;
    private final BrowserElement btnSavePlacementRule = Buttons
            .getButtonType1("Save");
    private final String[] strMultiWeightedValueArray;
    private final String[] strPrefixValueArray;
    private final String strFinalValueArray;
    private final String strConditionArray;
    private final String[] strEqualOrNotEqualArray;
    private final String[] strBracketArray;
    private final String strAnsForMultipleWeightedMeasuresArray;

    private  final static int INDEX = 1;

    /**
     * This constructor will initialize the mandatory fields and the
     * BrowserElements from this class
     * 
     * @param browser
     * @param strConditionAll
     *            Conditions to be applied with comma separated
     * @param strBracketAll
     *            Type of All bracket which require for enclosing the condition
     *            with comma separated
     * @param strEqualNotEqualAll
     *            equal/not equal to .. etc which needs to be selected with
     *            comma separated
     * @param strAnsForMultipleWeightedMeasuresAll
     *            Answer of selected Multiple Weighted Measures with comma
     *            separated
     * @param strPrefixValueAll
     *            Prefix score to be selected for Multiple Weighted Measures
     *            conditions with comma separate
     * @param strMultiWeightedValueAll
     *            Value needs to be selected for Multi Weighted Measure with
     *            comma separated
     * @param strFinalValueAll
     *            Final score to be selected for Multiple Weighted Measures
     *            conditions with comma separate
     * 
     */
    public AddMultiWeightedMeasureCondition(final Browser browser,
            String StrBracketAll, String strConditionAll,
            String strMultiWeightedValueAll, String strPrefixValueAll,
            String strFinalValueAll, String strEqualOrNotEqualAll,
            String strAnsForMultipleWeightedMeasuresAll) {
        super(browser);

        this.strConditionArray = strConditionAll;
        this.strMultiWeightedValueArray = strMultiWeightedValueAll.split(",");
        this.strBracketArray = StrBracketAll.split(" ");
        this.strPrefixValueArray = strPrefixValueAll.split(",");
        this.strFinalValueArray = strFinalValueAll;
        this.strEqualOrNotEqualArray = strEqualOrNotEqualAll.split(",");
        this.strAnsForMultipleWeightedMeasuresArray = strAnsForMultipleWeightedMeasuresAll;

        this.txtAddConditionToRule = browser.getBrowserElement(OR_AREA,
                OR_ADD_CONDITION_TO_RULE_LINK);
        this.drpSelectConditionPlacementRule = browser.getBrowserElement(
                OR_AREA, OR_SELECT_CONDITION);
        this.btnAddCondition = browser.getBrowserElement(OR_AREA,
                OR_BTN_ADD_CONDITION);
        this.txtPlacementRuleConfirmationMsg = browser.getBrowserElement(
                OR_AREA, OR_PLACEMENT_RULE_CONFIRMATION_MSG);
        this.optionValidateDropDownData = browser.getBrowserElement(OR_AREA,
                OR_VALIDATE_DROP_DOWN_DATA);

    }

    @Override
    protected boolean checkPostState() {

        boolean result = true;

        if (!new ElementExistsValidation(getBrowser(),
                this.txtPlacementRuleConfirmationMsg).performWithStates()) {
            LOGGER.error("Not able to validate message");
            result = false;
        }

        return result;
    }

    @Override
    protected boolean perform() {
        boolean result = true;

        if (!new LeftClickElementAction(getBrowser(),
                this.txtAddConditionToRule).performWithStates()) {
            LOGGER.error("Not able to click on Add Condition Link");
            return false;
        }

        if (!new ElementExistsValidation(getBrowser(),
                this.optionValidateDropDownData).performWithStates(false, true)) {
            LOGGER.error("Not able to validate the Drop Down data");
            return false;
        }

        if (!new DropdownSelectionAction(getBrowser(),
                this.drpSelectConditionPlacementRule,
                SelectorType.VISIBLE_TEXT, this.strConditionArray)
                .performWithStates(false, true)) {
            LOGGER.error("Not able to select New Condition Type from Dropdown");
            return false;
        }

        if (!new LeftClickOnModalPopupButton(getBrowser(), this.btnAddCondition)
                .performWithStates()) {
            LOGGER.error("Not able to click on Add button after selecting Condition");
            return false;
        }

        if (!new DropdownSelectionAction(getBrowser(), this.getBrowser()
                .getBrowserElement(OR_AREA, OR_SELECT_STARTING_BRACKET)
                .formatWithParms(1), SelectorType.VISIBLE_TEXT,
                this.strBracketArray[INDEX - 1]).performWithStates(false, true)) {
            LOGGER.error("Not able to select opening bracket from Dropdown");
            return false;
        }

        if (!new ClearTextElementAction(getBrowser(), this
                .getBrowser()
                .getBrowserElement(OR_AREA,
                        OR_MULTI_WEIGHTED_MEASURE_PREFIX_SCORE)
                .formatWithParms(1)).performWithStates()) {
            LOGGER.error("Not able to clear Text from Multi Weighted Measures Prefix field");
            return false;
        }
        if (!new SendTextToElementAction(getBrowser(), this
                .getBrowser()
                .getBrowserElement(OR_AREA,
                        OR_MULTI_WEIGHTED_MEASURE_PREFIX_SCORE)
                .formatWithParms(1), this.strPrefixValueArray[INDEX - 1])
                .performWithStates()) {
            LOGGER.error("Not able to send text to Multi Weighted Measures Prefix field");
            result = false;
        }

        if (!new ClearTextElementAction(getBrowser(), this
                .getBrowser()
                .getBrowserElement(OR_AREA,
                        OR_MULTI_WEIGHTED_MEASURE_PREFIX_SCORE)
                .formatWithParms(2)).performWithStates()) {
            LOGGER.error("Not able to clear Text from  Multi Weighted Measures Prefix field2");
            return false;
        }
        if (!new SendTextToElementAction(getBrowser(), this
                .getBrowser()
                .getBrowserElement(OR_AREA,
                        OR_MULTI_WEIGHTED_MEASURE_PREFIX_SCORE)
                .formatWithParms(2), this.strPrefixValueArray[INDEX])
                .performWithStates()) {
            LOGGER.error("Not able to send text to Multi Weighted Measures Prefix field2");
            result = false;
        }

        if (!new DropdownSelectionAction(getBrowser(), this.getBrowser()
                .getBrowserElement(OR_AREA, OR_MULTI_WEIGHTED_VALUE)
                .formatWithParms(1), SelectorType.VISIBLE_TEXT,
                this.strMultiWeightedValueArray[INDEX - 1]).performWithStates(
                false, true)) {
            LOGGER.error("Not able to select value from Multi Weighted Measures Dropdown1");
            return false;
        }

        if (!new DropdownSelectionAction(getBrowser(), this.getBrowser()
                .getBrowserElement(OR_AREA, OR_MULTI_WEIGHTED_VALUE)
                .formatWithParms(2), SelectorType.VISIBLE_TEXT,
                this.strMultiWeightedValueArray[INDEX]).performWithStates(false,
                true)) {
            LOGGER.error("Not able to select value from Multi Weighted Measures Dropdown2");
            return false;
        }

        if (!new DropdownSelectionAction(getBrowser(), this.getBrowser()
                .getBrowserElement(OR_AREA, OR_SELECT_EQUAL_AND_NOT_EQUAL)
                .formatWithParms(1), SelectorType.VISIBLE_TEXT,
                this.strEqualOrNotEqualArray[INDEX - 1]).performWithStates(false,
                true)) {
            LOGGER.error("Not able to select equal/not equal to from Multi  Weighted Measures Dropdown");
            return false;
        }

        if (!new DropdownSelectionAction(getBrowser(), this
                .getBrowser()
                .getBrowserElement(OR_AREA,
                        OR_SELECT_ANSWER_FOR_AUTO_MULTIPLE_WEIGHTED_MEASURE)
                .formatWithParms(1), SelectorType.VISIBLE_TEXT,
                this.strAnsForMultipleWeightedMeasuresArray).performWithStates(
                false, true)) {
            LOGGER.error("Not able to select answer from Multi Weighted Measures Dropdown");
            return false;
        }

        if (!new ClearTextElementAction(getBrowser(), this
                .getBrowser()
                .getBrowserElement(OR_AREA,
                        OR_MULTI_WEIGHTED_MEASURE_FINAL_SCORE)
                .formatWithParms(1)).performWithStates()) {
            LOGGER.error("Not able to clear Text from  Multi Weighted Measures Final field");
            return false;
        }
        if (!new SendTextToElementAction(getBrowser(), this
                .getBrowser()
                .getBrowserElement(OR_AREA,
                        OR_MULTI_WEIGHTED_MEASURE_FINAL_SCORE)
                .formatWithParms(1), this.strFinalValueArray)
                .performWithStates()) {
            LOGGER.error("Not able to send text to Multi Weighted Measures Final field");
            result = false;
        }

        if (!new DropdownSelectionAction(getBrowser(), this.getBrowser()
                .getBrowserElement(OR_AREA, OR_SELECT_CLOSING_BRACKET)
                .formatWithParms(1), SelectorType.VISIBLE_TEXT,
                this.strBracketArray[INDEX]).performWithStates(false, true)) {
            LOGGER.error("Not able to select closing bracket from Dropdown");
            return false;
        }

        if (!new LeftClickElementAction(getBrowser(), this.btnSavePlacementRule)
                .performWithStates()) {
            LOGGER.error("Not able to click on Save Button");
            return false;

        }

        return result;
    }
}
