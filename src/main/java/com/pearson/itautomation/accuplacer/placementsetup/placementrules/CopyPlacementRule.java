package com.pearson.itautomation.accuplacer.placementsetup.placementrules;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.pearson.itautomation.bca.Browser;
import com.pearson.itautomation.bca.BrowserAction;
import com.pearson.itautomation.bca.BrowserElement;
import com.pearson.itautomation.bca.interaction.LeftClickElementAction;
import com.pearson.itautomation.bca.interaction.SendTextToElementAction;
import com.pearson.itautomation.bca.validation.ElementExistsValidation;

/**
 * This Class will Copy the Placement rule. <br>
 * <b>checkPrestate</b> - N/A <br>
 * <b>checkPostate</b> - This method will validate the Message <br>
 * <b>perform</b> - This method will return true if Placement Rule will copied
 * successfully.
 * 
 * @author Sujeet Chaudhary
 */
public class CopyPlacementRule extends BrowserAction {

    private static final Logger LOGGER = LoggerFactory
            .getLogger(CopyPlacementRule.class);

    private static final String OR_AREA = "OR_TestSetup";
    private static final String OR_AREA1 = "OR_Placement_Setup";
    private static final String OR_NEW_PLACEMENT_RULE_NAME = "NewPlacementRuleName";
    private static final String OR_SAVE = "Save";
    private static final String OR_COPY_VALIDATION_MSG = "CopyValidationMsgPlacementRule";
    private static final String OR_COPY_PLACEMENT_RULE_XPATH = "BranchingProfileCopyXpath";
    private static final String OR_CANCEL_BUTTON = "CancelButton";

    private final BrowserElement btnCopyPlacementRule;
    private final BrowserElement txtNewPlacementRuleName;
    private final BrowserElement btnSave;
    private final BrowserElement txtCopyValidationMsg;
    private final BrowserElement btnCancel;

    private final String strNewPlacementRuleName;

    /**
     * This constructor will initialize the mandatory fields and the
     * BrowserElements from this class
     * 
     * @param browser
     * @param strPlacementRuleTitle
     *            The Placement Rule title which need to be Copy
     * @param strNewPlacementRuleName
     *            The Placement Rule New Name for copy purpose
     * 
     */
    public CopyPlacementRule(final Browser browser,
            String strPlacementRuleTitle, String strNewPlacementRuleName) {
        super(browser);

        this.strNewPlacementRuleName = strNewPlacementRuleName;

        this.txtCopyValidationMsg = browser.getBrowserElement(OR_AREA1,
                OR_COPY_VALIDATION_MSG);

        this.btnCopyPlacementRule = browser.getBrowserElement(OR_AREA,
                OR_COPY_PLACEMENT_RULE_XPATH).formatWithParms(
                strPlacementRuleTitle, strPlacementRuleTitle);

        this.txtNewPlacementRuleName = browser.getBrowserElement(OR_AREA1,
                OR_NEW_PLACEMENT_RULE_NAME);

        this.btnSave = browser.getBrowserElement(OR_AREA, OR_SAVE);
        this.btnCancel = browser.getBrowserElement(OR_AREA, OR_CANCEL_BUTTON);

    }

    @Override
    protected boolean perform() {
        boolean result = true;
        if (!new LeftClickElementAction(getBrowser(), this.btnCopyPlacementRule)
                .performWithStates()) {
            LOGGER.error("Not able to click on Copy Button");
            result = false;
        }

        try {
            Thread.sleep(500);
        } catch (InterruptedException e) {
            LOGGER.warn("Failed to wait for Save to be visible");
        }

        if (!new SendTextToElementAction(getBrowser(),
                this.txtNewPlacementRuleName, this.strNewPlacementRuleName)
                .performWithStates()) {
            LOGGER.debug("Not able to send text to New Placement Rule field");
            result = false;
        }

        try {
            Thread.sleep(1000);
        } catch (InterruptedException e) {
            LOGGER.warn("Failed to wait for Save to be visible");
        }

        if (!new LeftClickElementAction(getBrowser(), this.btnSave)
                .performWithStates()) {
            LOGGER.error("Not able to click on Save Button");
            result = false;
        }

        return result;
    }

    @Override
    protected boolean checkPostState() {
        boolean result = true;

        if (!new ElementExistsValidation(getBrowser(),
                this.txtCopyValidationMsg).performWithStates()) {
            LOGGER.debug("Not able to validate the Message ");
            result = false;
        }

        if (!result) {
            if (!new LeftClickElementAction(getBrowser(), this.btnCancel)
                    .performWithStates()) {
                LOGGER.error("Not able to click on Cancle");
                result = false;
            }
        }
        return result;
    }
}
