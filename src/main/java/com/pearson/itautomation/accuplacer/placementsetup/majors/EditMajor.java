package com.pearson.itautomation.accuplacer.placementsetup.majors;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.pearson.itautomation.accuplacer.core.CommonUtilities;
import com.pearson.itautomation.bca.Browser;
import com.pearson.itautomation.bca.BrowserAction;
import com.pearson.itautomation.bca.BrowserElement;
import com.pearson.itautomation.bca.interaction.LeftClickElementAction;
import com.pearson.itautomation.bca.interaction.SendTextToElementAction;

/**
 * This Class will fill out the Edit Major Form with all the mandatory details.<br>
 * <b>perform</b> - This method will return true if all the mandatory fields
 * have been filled in and saved successfully.
 * 
 * @author Rupal Jain
 */

public class EditMajor extends BrowserAction {

    private static final Logger LOGGER = LoggerFactory
            .getLogger(EditMajor.class);

    private static final String OR_AREA = "OR_Placement_Setup";
    private static final String OR_MAJOR_COMMENT = "MajorComment";
    private static final String OR_MAJOR_EDIT = "EditMajor";
    private final String timeStamp;

    private final BrowserElement btnEditMajor;
    private final BrowserElement txtMajorComment;

    /**
     * This constructor will initialize the mandatory fields and the
     * BrowserElements from this class
     * 
     * @param browser
     */
    public EditMajor(final Browser browser, String strMajorName) {
        super(browser);

        this.btnEditMajor = browser.getBrowserElement(OR_AREA, OR_MAJOR_EDIT)
                .formatWithParms(strMajorName, strMajorName);

        this.txtMajorComment = browser.getBrowserElement(OR_AREA,
                OR_MAJOR_COMMENT);
        this.timeStamp = CommonUtilities.getCurrentDateTime();

    }

    @Override
    protected boolean perform() {
        boolean result = true;

        if (!new LeftClickElementAction(getBrowser(), this.btnEditMajor)
                .performWithStates()) {
            LOGGER.error("Not able to click on Edit Button");
            return false;
        }
        if (!new SendTextToElementAction(getBrowser(), this.txtMajorComment,
                timeStamp).performWithStates()) {
            LOGGER.error("Not able to send text to Major Comment field");
            return false;
        }
        return result;
    }

}
