package com.pearson.itautomation.accuplacer.placementsetup.majors;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.pearson.itautomation.accuplacer.core.CommonUtilities;
import com.pearson.itautomation.bca.Browser;
import com.pearson.itautomation.bca.BrowserAction;
import com.pearson.itautomation.bca.BrowserElement;
import com.pearson.itautomation.bca.interaction.ClearTextElementAction;
import com.pearson.itautomation.bca.interaction.SendTextToElementAction;
import com.pearson.itautomation.bca.validation.ElementExistsValidation;

/**
 * This Class will fill out the Major Form with all the mandatory details.<br>
 * <b>perform</b> - This method will return true if all the fields are filled in
 * successfully.
 * 
 * @author Rupal Jain
 */

public class ReFillMajor extends BrowserAction {

    private static final Logger LOGGER = LoggerFactory
            .getLogger(ReFillMajor.class);

    private static final String OR_AREA = "OR_Placement_Setup";
    private static final String OR_MAJOR_CODE = "MajorCode";
    private static final String OR_MAJOR_NAME = "MajorName";
    private static final String OR_MAJOR_COMMENT = "MajorComment";
    private static final String OR_DISABLED_SAVE = "DisabledSave";

    private final BrowserElement txtMajorCode;
    private final BrowserElement txtMajorName;
    private final BrowserElement txtMajorComment;
    private final BrowserElement btnDisabledSave;

    private static final String MAJOR_CODE = "Code";
    private final String majorName;
    private static final String MAJOR_COMMENT = "Comment";
    private static final String DISABLED_STRING = "Save Button is DISABLED";

    /**
     * This constructor will initialize the Major name field and the
     * BrowserElements from this class
     * 
     * @param browser
     */
    public ReFillMajor(final Browser browser, String strMajorName) {
        super(browser);

        majorName = strMajorName;
        this.txtMajorCode = browser.getBrowserElement(OR_AREA, OR_MAJOR_CODE);
        this.txtMajorName = browser.getBrowserElement(OR_AREA, OR_MAJOR_NAME);
        this.txtMajorComment = browser.getBrowserElement(OR_AREA,
                OR_MAJOR_COMMENT);
        this.btnDisabledSave = browser.getBrowserElement(OR_AREA,
                OR_DISABLED_SAVE);

    }

    @Override
    protected boolean perform() {
        if (!checkSaveButtonDisabledStatus()) {
            LOGGER.info("Save Button is Enabled");
            return true;
        }

        if (reFillMajorName()) {
            return true;
        } else if (reFillMajorCode()) {
            return true;
        } else if (reFillMajorComment()) {            
            return true;
        } else {
        	return !checkSaveButtonDisabledStatus();
        }
    }

    /**
     * This Method will return true if save button is disabled
     * 
     * @return
     */
    public boolean checkSaveButtonDisabledStatus() {
        return new ElementExistsValidation(getBrowser(), this.btnDisabledSave)
                .performWithStates();

    }
    
    
    /**
     * Enter value Major name text box if it is Blank
     * 
     * @return Boolean
     */
    public boolean reFillMajorName() {
        boolean result = true;

        if (!new ClearTextElementAction(getBrowser(), this.txtMajorName)
                .performWithStates()) {
            LOGGER.error("Not able to Clear Major Name field");
            result = false;
        }
        if (!new SendTextToElementAction(getBrowser(), this.txtMajorName,
                majorName).performWithStates()) {
            LOGGER.error("Not able to send text to Major Name field");
            result = false;
        }        

        return result;
    }
    
    /**
     * Enter value Major Code text box if it is Blank
     * 
     * @return Boolean
     */
    public boolean reFillMajorCode() {
        boolean result = true;

        if (checkSaveButtonDisabledStatus()) {
            LOGGER.info(DISABLED_STRING);
            if (!new ClearTextElementAction(getBrowser(), this.txtMajorCode)
                    .performWithStates()) {
                LOGGER.error("Not able to Clear Major Code field");
                result = false;
            }
            if (!new SendTextToElementAction(getBrowser(), this.txtMajorCode,
                    MAJOR_CODE + CommonUtilities.getCurrentTime())
                    .performWithStates()) {
                LOGGER.error("Not able to send text to Major Code field");
                result = false;

            }
        } else {
            result = true;
        }
        return result;
    }
    
    /**
     * Enter value Major Comment text box if it is Blank
     * 
     * @return Boolean
     */
    
    public boolean reFillMajorComment() {
        boolean result = true;

        if (checkSaveButtonDisabledStatus()) {
            LOGGER.info(DISABLED_STRING);
            if (!new ClearTextElementAction(getBrowser(), this.txtMajorComment)
                    .performWithStates()) {
                LOGGER.error("Not able to Clear Major Comment field");
                result = false;
            }
            if (!new SendTextToElementAction(getBrowser(),
                    this.txtMajorComment, MAJOR_COMMENT
                            + CommonUtilities.getCurrentTime())
                    .performWithStates()) {
                LOGGER.error("Not able to send text to Major Comment field");
                result = false;

            }
        } else {
            result = true;
        }
        return result;
    }
}
