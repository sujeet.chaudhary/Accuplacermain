package com.pearson.itautomation.accuplacer.placementsetup.placementrules;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.pearson.itautomation.bca.Browser;
import com.pearson.itautomation.bca.BrowserAction;
import com.pearson.itautomation.bca.BrowserElement;
import com.pearson.itautomation.bca.SelectorType;
import com.pearson.itautomation.bca.interaction.DropdownSelectionAction;
import com.pearson.itautomation.bca.interaction.LeftClickElementAction;

/**
 * This Class will Select the Course on which Rule has to be applied
 * 
 * <b>checkPrestate</b> - N/A <br>
 * <b>checkPostate</b> - N/A <br>
 * <b>perform</b> - This method will return true if Course will selected
 * successfully
 * 
 * @author Sujeet Chaudhary
 */
public class SelectCoursePlacement extends BrowserAction {

    private static final Logger LOGGER = LoggerFactory
            .getLogger(SelectCoursePlacement.class);

    private static final String OR_AREA = "OR_Placement_Setup";
    private static final String OR_COURSE_PLACEMENT = "CoursePlacement";
    private static final String OR_ADD_COURSE_SYMBOL = "AddCourseSymbol";
    private static final String OR_ADD_COURSE = "AddCourse";

    private final BrowserElement txtAddCourse;

    private final String[] strCoursePlacementArray;
    private final int strnoOfCoursePlacement;

    /**
     * This constructor will initialize the mandatory fields and the
     * BrowserElements from this class
     * 
     * @param browser
     * @param strCoursePlacementAll
     *            The Value of 'Course' to be selected
     * @param strNoOfCoursePlacement
     *            No of courses to be selected
     */
    public SelectCoursePlacement(final Browser browser,
            String strCoursePlacementAll, String strNoOfCoursePlacement) {
        super(browser);
        this.strCoursePlacementArray = strCoursePlacementAll.split(",");
        this.strnoOfCoursePlacement = Integer.parseInt(strNoOfCoursePlacement);
        this.txtAddCourse = browser.getBrowserElement(OR_AREA, OR_ADD_COURSE);

    }

    @Override
    protected boolean perform() {
        boolean result = true;
        for (int i = 1; i <= strnoOfCoursePlacement; i++) {
            if (i > 1 && i <= strnoOfCoursePlacement) {
                if (!new LeftClickElementAction(getBrowser(), getBrowser()
                        .getBrowserElement(OR_AREA, OR_ADD_COURSE_SYMBOL)
                        .formatWithParms(i - 1)).performWithStates()) {
                    LOGGER.error("Not able to click on Add course btn");
                    return false;
                }

                if (!new LeftClickElementAction(getBrowser(), this.txtAddCourse)
                        .performWithStates()) {
                    LOGGER.error("Not able to click on Add course Link");
                    return false;
                }
            }

            if (!new DropdownSelectionAction(getBrowser(), getBrowser()
                    .getBrowserElement(OR_AREA, OR_COURSE_PLACEMENT)
                    .formatWithParms(i), SelectorType.VISIBLE_TEXT,
                    this.strCoursePlacementArray[i - 1]).performWithStates(
                    false, true)) {
                LOGGER.error("Not able to select Course from Dropdown");
                return false;
            }

        }

        return result;
    }
}
