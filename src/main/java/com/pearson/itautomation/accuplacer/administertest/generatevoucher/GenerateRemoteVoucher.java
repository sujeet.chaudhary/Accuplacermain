package com.pearson.itautomation.accuplacer.administertest.generatevoucher;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.pearson.itautomation.accuplacer.core.Buttons;
import com.pearson.itautomation.bca.Browser;
import com.pearson.itautomation.bca.BrowserAction;
import com.pearson.itautomation.bca.BrowserElement;
import com.pearson.itautomation.bca.SelectorType;
import com.pearson.itautomation.bca.interaction.DropdownSelectionAction;
import com.pearson.itautomation.bca.interaction.LeftClickElementAction;
import com.pearson.itautomation.bca.interaction.SendTextToElementAction;

/**
 * This Class will Generate a Remote Voucher for a particular Student<br>
 * <b>checkPrestate</b> - N/A <br>
 * <b>checkPostate</b> - N/A <br>
 * <b>perform</b> - This method will return true a student voucher is created
 * successfully
 * 
 * 
 * @author Deepak Radhakrishnan
 */
public class GenerateRemoteVoucher extends BrowserAction {
    private static final Logger LOGGER = LoggerFactory
            .getLogger(GenerateRemoteVoucher.class);

    private final BrowserElement drpCountry;
    private final BrowserElement txtCity;
    private final BrowserElement btnSearch;

    private final String country;
    private final String city;

    /**
     * @param browser
     * @param studentID
     *            Primary Student ID
     * @param lastName
     * @param dOB
     *            Date of Birth
     * @param mOB
     *            Month of Birth
     * @param yOB
     *            Year of Birth
     * @param branchingProfile
     *            Branching Profile
     * @param country
     *            Country
     * @param city
     *            City
     * @param siteName
     *            Name of the Remote site to register
     */
    public GenerateRemoteVoucher(Browser browser, String studentID,
            String lastName, String dOB, String mOB, String yOB,
            String branchingProfile, String country, String city,
            String siteName) {
        super(browser);

        drpCountry = browser.getBrowserElement("OR_Vouchers",
                "countryRemoteVoucher");
        txtCity = browser.getBrowserElement("OR_Vouchers", "cityRemoteVoucher");
        this.country = country;
        this.city = city;
        this.btnSearch = Buttons.getButtonType1("Search");

    }

    protected boolean perform() {
        boolean result = true;

        if (!new DropdownSelectionAction(this.getBrowser(), this.drpCountry,
                SelectorType.VISIBLE_TEXT, this.country).performWithStates()) {
            LOGGER.error("Unable to select country");
            result = false;
        }
        if (!new SendTextToElementAction(this.getBrowser(), this.txtCity, city)
                .performWithStates()) {
            LOGGER.error("Unable to type city text");
            result = false;
        }
        if (!new LeftClickElementAction(this.getBrowser(), this.btnSearch)
                .performWithStates()) {
            LOGGER.error("Unable to click on search button");
            result = false;
        }
        return result;
    }

}
