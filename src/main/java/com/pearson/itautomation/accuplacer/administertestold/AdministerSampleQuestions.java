package com.pearson.itautomation.accuplacer.administertestold;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.pearson.itautomation.accuplacer.core.Buttons;
import com.pearson.itautomation.bca.Browser;
import com.pearson.itautomation.bca.BrowserAction;
import com.pearson.itautomation.bca.BrowserElement;
import com.pearson.itautomation.bca.interaction.LeftClickElementAction;

/**
 * This Class Will 2 Administer Sample Questions<br>
 * <b>checkPrestate</b> - N/A <br>
 * <b>checkPostate</b> - N/A <br>
 * <b>perform</b> - This method will return true if the sample questions have
 * been administered successfully
 * 
 * 
 * @author Deepak Radhakrishnan
 */
public class AdministerSampleQuestions extends BrowserAction {
    private static final Logger LOGGER = LoggerFactory
            .getLogger(AdministerSampleQuestions.class);
    private final BrowserElement btnSampleQuestions;
    private final BrowserElement btnConfirm;
    private final BrowserElement btnBeginTest;
    private final BrowserElement optionA;
    private final BrowserElement btnNextSampleQuestion;

    /**
     * This constructor will initialize the confirmation message
     * 
     * @param browser
     * 
     * @author Deepak Radhakrishnan
     */
    public AdministerSampleQuestions(Browser browser) {
        super(browser);
        this.btnSampleQuestions = Buttons.getButtonType1("Sample Questions");
        this.btnConfirm = Buttons.getButtonType1("Confirm");
        this.btnBeginTest = Buttons.getButtonType1("Begin Test");
        this.btnNextSampleQuestion = Buttons
                .getButtonType1("Next Sample Question");
        this.optionA = getBrowser().getBrowserElementWithFormat(
                "OR_Administer_Test_OLD", "optionByValue", "A");

    }

    @Override
    protected boolean perform() {
        boolean result = true;
        if (getBrowser().getPageSource().contains("Sample Questions")) {
            LOGGER.info("Answering Sample Questions");

            if (!new LeftClickElementAction(getBrowser(),
                    this.btnSampleQuestions).performWithStates()) {
                LOGGER.error("Not able to click SampleQuestions Button");
                return false;
            }
            if (!(answerSampleQuestion1() && answerSampleQuestion2())) {
                LOGGER.info("Failed While answering sample question");
                return false;
            }

        } else if (getBrowser().getPageSource().contains("Begin Test")) {
            LOGGER.info("No Sample Questions , Begin Test Page Found");
            if (!new LeftClickElementAction(getBrowser(), this.btnBeginTest)
                    .performWithStates()) {
                LOGGER.error("Not able to click Begin Test Button");
                return false;
            }

        } else {
            LOGGER.info("No Sample Questions , return to code");
            return true;
        }
        return result;
    }

    /**
     * Answer the 1st Sample Question
     * 
     * @return
     */
    private boolean answerSampleQuestion1() {
        boolean result = true;
        if (!new LeftClickElementAction(getBrowser(), this.optionA)
                .performWithStates()) {
            LOGGER.error("Not able to click option A of Sample Question 1");
            result = false;
        }
        if (!new LeftClickElementAction(getBrowser(),
                this.btnNextSampleQuestion).performWithStates()) {
            LOGGER.error("Not able to click Next SampleQuestions Button");
            return false;
        }
        if (!new LeftClickElementAction(getBrowser(), this.btnConfirm)
                .performWithStates()) {
            LOGGER.error("Not able to click Confirm Button");
            return false;
        }
        return result;
    }

    /**
     * Answer the 2nd Sample Question
     * 
     * @return
     */
    private boolean answerSampleQuestion2() {
        boolean result = true;
        if (!new LeftClickElementAction(getBrowser(), this.optionA)
                .performWithStates()) {
            LOGGER.error("Not able to click option A of Sample Question 2");
            result = false;
        }
        if (!new LeftClickElementAction(getBrowser(), this.btnBeginTest)
                .performWithStates()) {
            LOGGER.error("Not able to click Begin Test Button");
            return false;
        }
        if (!new LeftClickElementAction(getBrowser(), this.btnConfirm)
                .performWithStates()) {
            LOGGER.error("Not able to click Start Button");
            return false;
        }
        return result;
    }
}
