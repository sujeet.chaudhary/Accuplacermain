package com.pearson.itautomation.accuplacer.administertestold;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.pearson.itautomation.accuplacer.core.Buttons;
import com.pearson.itautomation.accuplacer.core.action.LeftClickOnModalPopupButton;
import com.pearson.itautomation.accuplacer.interaction.SwitchToWindowByPageContent;
import com.pearson.itautomation.bca.Browser;
import com.pearson.itautomation.bca.BrowserAction;
import com.pearson.itautomation.bca.BrowserElement;
import com.pearson.itautomation.bca.interaction.ClearTextElementAction;
import com.pearson.itautomation.bca.interaction.LeftClickElementAction;
import com.pearson.itautomation.bca.interaction.SendTextToElementAction;

/**
 * This Class will fill in the User Name and password to login to the active
 * session of <i>SM/PROR/PROC</i><br>
 * <b>checkPrestate</b> - N/A <br>
 * <b>checkPostate</b> - N/A <br>
 * <b>perform</b> - This method will return true if the UserName Authentication
 * is done successfully
 * 
 * @author Deepak Radhakrishnan
 */
public class RetestAttempt extends BrowserAction {
    private static final Logger LOGGER = LoggerFactory
            .getLogger(RetestAttempt.class);
    private final BrowserElement txtPassword;
    private final String password;
    private final BrowserElement btnSubmit;
    private final BrowserElement txtUserName;
    private final String userName;
    private final boolean submitSession;
    private final BrowserElement btnCloseSession;
    private final BrowserElement btnYesCloseSession;
    private static final String OR_AREA = "OR_Administer_Test_OLD";
    private AdministerTestUtils adminTURetest;

    /**
     * This constructor will initialize the confirmation message
     * 
     * @param browser
     * 
     * @author Deepak Radhakrishnan
     */
    public RetestAttempt(Browser browser, String userName, String password,
            boolean submitSession) {
        super(browser);

        this.btnSubmit = getBrowser().getBrowserElement(OR_AREA,
                "enterProcSubmitButton");
        this.btnCloseSession = Buttons.getButtonType1("Close Session");
        this.btnYesCloseSession = getBrowser().getBrowserElement(OR_AREA,
                "closeRetestAttempt");
        this.txtPassword = getBrowser().getBrowserElement(OR_AREA,
                "enterProcPassword");
        this.txtUserName = getBrowser().getBrowserElement(OR_AREA,
                "enterProcUserName");
        this.password = password;
        this.userName = userName;
        this.submitSession = submitSession;

    }

    @Override
    protected boolean perform() {
        boolean result = true;
        adminTURetest = new AdministerTestUtils(getBrowser());
        if (submitSession) {
            LOGGER.info("Retest Session to be Submitted");
            if (!new ClearTextElementAction(getBrowser(), this.txtUserName)
                    .performWithStates()) {
                LOGGER.error("Not able to clear the UserName");
                result = false;
            }
            if (!new SendTextToElementAction(getBrowser(), this.txtUserName,
                    this.userName).performWithStates()) {
                LOGGER.error("Not able to type the UserName");
                result = false;
            }

            if (!new ClearTextElementAction(getBrowser(), this.txtPassword)
                    .performWithStates()) {
                LOGGER.error("Not able to clear the password");
                result = false;
            }
            if (!new SendTextToElementAction(getBrowser(), this.txtPassword,
                    password).performWithStates()) {
                LOGGER.error("Not able to type the password");
                result = false;
            }
            if (!new LeftClickElementAction(getBrowser(), this.btnSubmit)
                    .performWithStates()) {
                LOGGER.error("Not able to click on Submit Button");
                return false;
            }
        } else {
            LOGGER.info("Closing retest attempt Test session");
            if (!new LeftClickElementAction(getBrowser(), this.btnCloseSession)
                    .performWithStates()) {
                LOGGER.error("Not able to click on Close Button");
                result = false;
            }
            if (!new LeftClickOnModalPopupButton(getBrowser(),
                    this.btnYesCloseSession).performWithStates()) {
                LOGGER.error("Not able to click on Yes confirmation Button");
                return false;
            }
            if (!new SwitchToWindowByPageContent(this.getBrowser(),
                    "If you are attempting to administer a test and are not presented with")
                    .performWithStates()) {
                LOGGER.error("Unable to switch to main window");
                return false;
            }
            result = adminTURetest.switchToMainWindowAndConclude(password);
        }
        return result;

    }
}
