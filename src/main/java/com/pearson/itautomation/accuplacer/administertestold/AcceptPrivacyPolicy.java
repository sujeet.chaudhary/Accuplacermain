package com.pearson.itautomation.accuplacer.administertestold;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.pearson.itautomation.accuplacer.core.AccuplacerBrowserUtils;
import com.pearson.itautomation.accuplacer.core.Buttons;
import com.pearson.itautomation.accuplacer.interaction.ScrollToPageEnd;
import com.pearson.itautomation.accuplacer.interaction.SwitchToWindowByPageContent;
import com.pearson.itautomation.bca.Browser;
import com.pearson.itautomation.bca.BrowserAction;
import com.pearson.itautomation.bca.BrowserElement;
import com.pearson.itautomation.bca.interaction.LeftClickElementAction;

/**
 * This Class will switch to the privacy policy window and click on accept<br>
 * <b>checkPrestate</b> - N/A <br>
 * <b>checkPostate</b> - N/A <br>
 * <b>perform</b> - This method will return true privacy policy is accepted
 * successfully
 * 
 * 
 * @author Deepak Radhakrishnan
 */
public class AcceptPrivacyPolicy extends BrowserAction {
    private static final Logger LOGGER = LoggerFactory
            .getLogger(AcceptPrivacyPolicy.class);
    private final BrowserElement btnAccept;

    /**
     * This constructor will initialize the elements
     * 
     * @param browser
     * @author Deepak Radhakrishnan
     */
    public AcceptPrivacyPolicy(Browser browser) {
        super(browser);
        this.btnAccept = Buttons.getButtonType1("Accept");

    }

    @Override
    protected boolean perform() {
        boolean result = true;
        if (!new SwitchToWindowByPageContent(this.getBrowser(),
                "The College Board System Student Privacy Policy")
                .performWithStates()) {
            LOGGER.error("Not able to switch to Privact Policy Page");
            return false;
        }
        if (!new ScrollToPageEnd(this.getBrowser()).performWithStates()) {
            LOGGER.error("Not able to scroll to Page End of Privacy Policy");
            result = false;
        }
        if (!new LeftClickElementAction(getBrowser(), this.btnAccept)
                .performWithStates()) {
            LOGGER.error("Not able to click Accept Button");
            return false;
        }

        return result;
    }

    @Override
    protected boolean checkPostState() {
        boolean result = true;
        if (!AccuplacerBrowserUtils.checkPageSourceForString(this.getBrowser(),
                "Student Instructions")) {
            LOGGER.error("Not Find the Student Instruction Page");
            return false;
        }
        return result;

    }
}
