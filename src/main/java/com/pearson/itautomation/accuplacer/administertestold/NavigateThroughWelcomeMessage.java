package com.pearson.itautomation.accuplacer.administertestold;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.pearson.itautomation.accuplacer.core.Buttons;
import com.pearson.itautomation.bca.Browser;
import com.pearson.itautomation.bca.BrowserAction;
import com.pearson.itautomation.bca.BrowserElement;
import com.pearson.itautomation.bca.interaction.LeftClickElementAction;
import com.pearson.itautomation.bca.validation.ElementExistsValidation;

/**
 * This Class Navigate Through the welcome page while administering a test<br>
 * <b>checkPrestate</b> - N/A <br>
 * <b>checkPostate</b> - N/A <br>
 * <b>perform</b> - This method will return true if the welcome message has been
 * successfully traversed through
 * 
 * 
 * @author Deepak Radhakrishnan
 */
public class NavigateThroughWelcomeMessage extends BrowserAction {
    private static final Logger LOGGER = LoggerFactory
            .getLogger(NavigateThroughWelcomeMessage.class);
    private final BrowserElement btnNext;
    private final BrowserElement btnStart;
    private final BrowserElement welcomeHeader;

    /**
     * This constructor will initialize the confirmation message
     * 
     * @param browser
     * 
     * @author Deepak Radhakrishnan
     */
    public NavigateThroughWelcomeMessage(Browser browser) {
        super(browser);
        this.btnNext = Buttons.getButtonType1("Next");
        this.btnStart = Buttons.getButtonType1("Start");
        this.welcomeHeader = getBrowser()
                .getObjectRepository()
                .getBrowserElement("OR_Administer_Test_OLD", "welcomeMsgHeader");

    }

    @Override
    protected boolean checkPreState() {
        boolean result = true;
        if (!new ElementExistsValidation(this.getBrowser(), welcomeHeader)
                .performWithStates()) {
            LOGGER.error("Could not find the welcome header");
            return false;
        }
        return result;
    }

    @Override
    protected boolean perform() {
        boolean result = true;

        if (!new LeftClickElementAction(getBrowser(), this.btnNext)
                .performWithStates()) {
            LOGGER.error("Not able to click Next Button");
            return false;
        }
        if (!new LeftClickElementAction(getBrowser(), this.btnStart)
                .performWithStates()) {
            LOGGER.error("Not able to click Start Button");
            return false;
        }

        return result;
    }
}
