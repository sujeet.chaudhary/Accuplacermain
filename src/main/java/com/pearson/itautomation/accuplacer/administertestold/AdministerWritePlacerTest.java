package com.pearson.itautomation.accuplacer.administertestold;

import java.sql.Connection;

import org.openqa.selenium.WebElement;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.pearson.itautomation.accuplacer.core.AccuplacerBrowserUtils;
import com.pearson.itautomation.accuplacer.core.Buttons;
import com.pearson.itautomation.accuplacer.core.action.LeftClickOnModalPopupButton;
import com.pearson.itautomation.bca.Browser;
import com.pearson.itautomation.bca.BrowserAction;
import com.pearson.itautomation.bca.BrowserElement;
import com.pearson.itautomation.bca.interaction.LeftClickElementAction;
import com.pearson.itautomation.bca.interaction.SendTextToElementAction;
import com.pearson.itautomation.bca.validation.ElementExistsValidation;

/**
 * This Class Will Administer a Writeplacer Prompt<br>
 * <b>checkPrestate</b> - N/A <br>
 * <b>checkPostate</b> - N/A <br>
 * <b>perform</b> - This method will return true if the sample questions have
 * been administered successfully
 * 
 * 
 * @author Deepak Radhakrishnan
 */
public class AdministerWritePlacerTest extends BrowserAction {
    private static final Logger LOGGER = LoggerFactory
            .getLogger(AdministerWritePlacerTest.class);
    String itemid, testName, itemKey = null;
    int numberOfQuestion, savedQuesCount = 0;
    Boolean isSave = false;
    String savedArr[] = null;
    AdministerTestUtils adminTUWP;
    boolean saveResumeFlag = true;
    Connection con;

    private static final String OR_AREA = "OR_Administer_Test_OLD";

    private final BrowserElement btnSubmit;
    private final BrowserElement btnConfirm;

    private final BrowserElement txtStudentResponse;

    private final String password;

    private final BrowserElement btnBeginTest;
    private String essayResponse;
    private final BrowserElement btnYes;
    private final BrowserElement wpAuthModal;
    private String authModalClass;

    /**
     * This constructor will initialize the confirmation message
     * 
     * @param browser
     * @param password
     * 
     * @author Deepak Radhakrishnan
     * @param essayResponse
     */
    public AdministerWritePlacerTest(Browser browser, String password,
            String essayResponse) {
        super(browser);
        adminTUWP = new AdministerTestUtils(browser);

        this.btnSubmit = Buttons.getButtonType1("Submit");
        this.btnBeginTest = Buttons.getButtonType1("Begin Test");
        this.btnConfirm = Buttons.getButtonType1("Confirm");
        this.btnYes = browser.getBrowserElement(OR_AREA,
                "writeplacerauthYesButton");
        this.password = password;
        this.txtStudentResponse = browser.getBrowserElement(OR_AREA,
                "WriteplacerText");
        this.wpAuthModal = browser.getBrowserElement(OR_AREA,
                "writePlacerAuthModal");
        this.essayResponse = essayResponse;
    }

    @Override
    protected boolean perform() {
        boolean result = true;
        if (getBrowser().getPageSource().contains("You have completed your")) {
            LOGGER.info("Test Session is COMPLETE , return to the main code");
            return adminTUWP.concludeSession(password);
        }
        if (!adminTUWP.checkIfItemIsErrorFree("Before WP Test Begins")) {
            return false;
        }
        if (!getBrowser().getPageSource().contains("Instructions to Students")) {
            LOGGER.error("WritePlacer Begin Test Page Not Found");
            return false;
        } else {
            LOGGER.info("WritePlacer Begin Test Page Found");
            if (!new LeftClickElementAction(this.getBrowser(),
                    this.btnBeginTest).performWithStates()) {
                LOGGER.error("Could not click Click On Begin Test Answer");
                return false;
            }
            if (!adminTUWP
                    .checkIfItemIsErrorFree("After Clicking on Begin Test")) {
                return false;
            }
            if (!new SendTextToElementAction(getBrowser(),
                    this.txtStudentResponse, essayResponse).performWithStates()) {
                LOGGER.error("Not able to type From Date");
                return false;
            }
            if (!new LeftClickElementAction(this.getBrowser(), this.btnSubmit)
                    .performWithStates()) {
                LOGGER.error("Could not click Click On Submit Answer");
                result = false;
            }
            if (!new LeftClickOnModalPopupButton(this.getBrowser(),
                    this.btnConfirm).performWithStates()) {
                LOGGER.error("Could not click Click On Confirm Answer");
                result = false;
            }
            // Check for Blank essay validation if the response is blank
            if ("".equalsIgnoreCase(essayResponse)) {
                LOGGER.info("Checking For Blank essay Validation");
                AccuplacerBrowserUtils.WaitInMilliSeconds(1000);
                if (!new ElementExistsValidation(this.getBrowser(),
                        this.wpAuthModal).performWithStates()) {
                    LOGGER.error("Could not Find the WP confirmation Modal");
                    return false;
                } else {
                    LOGGER.info("WP confirmation Modal element found");
                }
                WebElement authModalElement = getBrowser().getElementWithWait(
                        wpAuthModal);

                // To Check the blank essay validation popup
                if (authModalElement != null) {
                    authModalClass = authModalElement.getAttribute("class");
                    if ("modal hide fade in".equalsIgnoreCase(authModalClass)) {
                        LOGGER.info("Popup Found  ");
                        if (!new LeftClickOnModalPopupButton(this.getBrowser(),
                                this.btnYes).performWithStates()) {
                            LOGGER.error("Could not click Click On Yes Answer");
                            result = false;
                        }
                    }
                    // Auth Modal Not Found
                    else {
                        LOGGER.error("Popup NOT Found to validate the Blank Essay");
                        return false;
                    }
                }
                // Auth Modal Element Not Found
                else {
                    LOGGER.error("Element '{}' Not Found", authModalElement);
                }
            } else {
                LOGGER.info("Essay Is Not Blank");
            }
        }
        if (!adminTUWP.checkIfItemIsErrorFree("After WP Test completes")) {
            return false;
        }
        if (getBrowser().getPageSource().contains("You have completed your")) {
            LOGGER.info("Test Session is COMPLETE , return to the main code");
            return adminTUWP.concludeSession(password);
        } else
            return result;
    }
}