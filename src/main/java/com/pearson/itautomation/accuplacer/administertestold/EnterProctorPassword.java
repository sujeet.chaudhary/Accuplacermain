package com.pearson.itautomation.accuplacer.administertestold;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.pearson.itautomation.accuplacer.core.Buttons;
import com.pearson.itautomation.bca.Browser;
import com.pearson.itautomation.bca.BrowserAction;
import com.pearson.itautomation.bca.BrowserElement;
import com.pearson.itautomation.bca.interaction.ClearTextElementAction;
import com.pearson.itautomation.bca.interaction.LeftClickElementAction;
import com.pearson.itautomation.bca.interaction.SendTextToElementAction;

/**
 * This Class will fill in the User Name and password to login to the active
 * session of <i>SM/PROR/PROC</i><br>
 * <b>checkPrestate</b> - N/A <br>
 * <b>checkPostate</b> - N/A <br>
 * <b>perform</b> - This method will return true if the UserName Authentication
 * is done successfully
 * 
 * @author Deepak Radhakrishnan
 */
public class EnterProctorPassword extends BrowserAction {
    private static final Logger LOGGER = LoggerFactory
            .getLogger(EnterProctorPassword.class);
    private final BrowserElement btnUnlock;
    private final BrowserElement txtPassword;
    // private final BrowserElement btnContinue;
    private final String password;
    private final BrowserElement btnSubmit;
    private static final String OR_AREA = "OR_Administer_Test_OLD";

    /**
     * This constructor will initialize the confirmation message
     * 
     * @param browser
     * 
     * @author Deepak Radhakrishnan
     */
    public EnterProctorPassword(Browser browser, String password) {
        super(browser);

        this.btnUnlock = Buttons.getButtonType4("Unlock");
        this.btnSubmit = getBrowser().getBrowserElement(OR_AREA,
                "enterProcSubmitButton");
        this.txtPassword = getBrowser().getBrowserElement(OR_AREA,
                "enterProcPassword");

        this.password = password;

    }

    @Override
    protected boolean perform() {
        boolean result = true;

        if (!new ClearTextElementAction(getBrowser(), this.txtPassword)
                .performWithStates()) {
            LOGGER.error("Not able to clear the password");
            result = false;
        }
        if (!new SendTextToElementAction(getBrowser(), this.txtPassword,
                password).performWithStates()) {
            LOGGER.error("Not able to type the password");
            result = false;
        }

        // Click on Continue on Proctor Password page

        if (!getBrowser().getPageSource().contains(
                "You have completed your test session")) {

            if (!new LeftClickElementAction(getBrowser(), this.btnUnlock)
                    .performWithStates()) {
                LOGGER.error("Not able to click Unlock Button Button");
                return false;

            }
        } else {
            if (!new LeftClickElementAction(getBrowser(), this.btnSubmit)
                    .performWithStates()) {
                LOGGER.error("Not able to click Submit Button Button");
                return false;
            }

        }

        return result;
    }

}
