package com.pearson.itautomation.accuplacer.administertestold;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.pearson.itautomation.accuplacer.core.AccuplacerBrowserUtils;
import com.pearson.itautomation.accuplacer.core.Buttons;
import com.pearson.itautomation.accuplacer.core.action.LeftClickOnModalPopupButton;
import com.pearson.itautomation.accuplacer.interaction.ScrollToPageTop;
import com.pearson.itautomation.bca.Browser;
import com.pearson.itautomation.bca.BrowserAction;
import com.pearson.itautomation.bca.BrowserElement;
import com.pearson.itautomation.bca.SelectorType;
import com.pearson.itautomation.bca.interaction.DropdownSelectionAction;
import com.pearson.itautomation.bca.interaction.LeftClickElementAction;
import com.pearson.itautomation.bca.interaction.SendTextToElementAction;
import com.pearson.itautomation.bca.validation.ElementExistsValidation;

/**
 * This Class will Fill and Save the Student Information <br>
 * <b>checkPrestate</b> - N/A <br>
 * <b>checkPostate</b> - This method will validate the Welcome Page <br>
 * <b>perform</b> - This method will Fill the Required data of the student.<br>
 * 
 * @author Sujeet Chaudhary
 */
public class FillAndSaveStudentInformation extends BrowserAction {

    private static final Logger LOGGER = LoggerFactory
            .getLogger(FillAndSaveStudentInformation.class);

    private static final String OR_AREA = "OR_Administer_Test_OLD";
    private static final String OR_STRUDENT_ID = "StudentIdAdministerTest";
    private static final String OR_CONFIRM_STRUDENT_ID = "ConfirmStudentIdAdministerTest";
    private static final String OR_LAST_NAME = "LastNameAdministerTest";
    private static final String OR_DATE_OF_BIRTH_MONTH = "DateOfBirthMonth";
    private static final String OR_DATE_OF_BIRTH_DATE = "DateOfBirthDate";
    private static final String OR_DATE_OF_BIRTH_YEAR = "DateOfBirthYear";
    private static final String OR_NEXT_ADMINISTER_TEST = "NextAdministerTest";
    private static final String OR_FIRST_NAME_ADMINISTER_TEST = "FirstNameAdministerTest";
    private static final String OR_ADDRESS1_ADMINISTER_TEST = "Address1AdministerTest";
    private static final String OR_COUNTRY_ADMINISTER_TEST = "CountryAdministerTest";
    private static final String OR_STATE_ADMINISTER_TEST = "StateAdministerTest";
    private static final String OR_CITY_ADMINISTER_TEST = "CityAdministerTest";
    private static final String OR_ZIPPOSTAL_CODE_ADMINISTER_TEST = "ZipPostalCodeAdministerTest";
    private static final String OR_EMAIL_ADDRESS_ADMINISTER_TEST = "EmailAddressAdministerTest";
    private static final String OR_GENDER_ADMINISTER_TEST = "GenderAdministerTest";
    private static final String OR_HOW_DO_YOU_DESCRIBE_YOURSELF_ADMINISTER_TEST = "HowDoYouDescribeYourselfAdministerTest";
    private static final String OR_SAVE_ADMINISTER_TEST = "SaveAdministerTest";
    private static final String OR_YES_ADMINISTER_TEST = "YesAdministerTest";
    private static final String OR_WELCOME_ADMINISTER_TEST = "WelcomeAdministerTest";
    private static final String OR_HOME_PHONE = "adminhomePhone";
    private static final String OR_CELL_PHONE = "admincellPhone";
    private static final String OR_ADDRESS2 = "adminAddress2";
    private static final String OR_MIDDLE_NAME = "adminMiddleName";

    private final BrowserElement txtStudentId;
    private final BrowserElement txtConfirmStudentId;
    private final BrowserElement txtLastName;
    private final BrowserElement drpDateOfBirthMonth;
    private final BrowserElement drpDateOfBirthDate;
    private final BrowserElement drpDateOfBirthYear;
    private final BrowserElement btnNextAdministerTest;
    private final BrowserElement btnYesAdministerTest;
    private final BrowserElement txtFirstNameAdministerTest;
    private final BrowserElement txtAddress1AdministerTest;
    private final BrowserElement drpCountryAdministerTest;
    private final BrowserElement drpStateAdministerTest;
    private final BrowserElement txtCityAdministerTest;
    private final BrowserElement txtZipPostalCodeAdministerTest;
    private final BrowserElement txtEmailAddressAdministerTest;
    private final BrowserElement drpGenderAdministerTest;
    private final BrowserElement drpHowDoYouDescribeYourselfAdministerTest;
    private final BrowserElement btnSave;
    private final BrowserElement txtWelcomeAdministerTest;
    private final BrowserElement txtHomePhone;
    private final BrowserElement txtCellPhone;
    private final BrowserElement txtAddress2;
    private final BrowserElement txtMiddleName;
    private final BrowserElement btnClosePopUp = Buttons
            .getButtonType1("Close");

    private final String strStudentId;
    private final String strLastName;
    private final String strDateOfBirthDate;
    private final String strDateOfBirthMonth;
    private final String strDateOfBirthYear;

    private static final String PHONE = "1111111111";
    private static final String MIDDLE_NAME = "MiddleName";
    private static final String FIRST_NAME = "FirstName";
    private static final String ADDRESS = "Address";
    private static final String COUNTRY = "United States";
    private static final String STATE = "Alabama";
    private static final String CITY = "City";
    private static final String ZIP_CODE = "123456";
    private static final String EMAIL_ADDRESS = "Email@abc.com";
    private static final String GENDER = "Male";
    private static final String HOW_YOU_DESCRIBE_YOURSELF = "Other";

    /**
     * This constructor will initialize the mandatory fields and the
     * BrowserElements from this class
     * 
     * @param browser
     * @param strStudentId
     *            The StudentId of the Student
     * @param strLastName
     *            LastName of the Student
     * @param strLastName
     *            LastName of the Student
     * @param strDateOfBirthDate
     *            Date of Student BirthDay
     * @param strDateOfBirthMonth
     *            Month of Student BirthDay
     * @param strDateOfBirthYear
     *            Year of Student BirthDay
     */
    public FillAndSaveStudentInformation(final Browser browser,
            String strStudentId, String strLastName, String strDateOfBirthDate,
            String strDateOfBirthMonth, String strDateOfBirthYear) {
        super(browser);

        this.strStudentId = strStudentId;
        this.strLastName = strLastName;
        this.strDateOfBirthDate = strDateOfBirthDate;
        this.strDateOfBirthMonth = strDateOfBirthMonth;
        this.strDateOfBirthYear = strDateOfBirthYear;

        this.txtStudentId = browser.getBrowserElement(OR_AREA, OR_STRUDENT_ID);

        this.txtConfirmStudentId = browser.getBrowserElement(OR_AREA,
                OR_CONFIRM_STRUDENT_ID);

        this.txtLastName = browser.getBrowserElement(OR_AREA, OR_LAST_NAME);

        this.drpDateOfBirthMonth = browser.getBrowserElement(OR_AREA,
                OR_DATE_OF_BIRTH_MONTH);

        this.drpDateOfBirthDate = browser.getBrowserElement(OR_AREA,
                OR_DATE_OF_BIRTH_DATE);

        this.drpDateOfBirthYear = browser.getBrowserElement(OR_AREA,
                OR_DATE_OF_BIRTH_YEAR);

        this.btnNextAdministerTest = browser.getBrowserElement(OR_AREA,
                OR_NEXT_ADMINISTER_TEST);

        this.btnYesAdministerTest = browser.getBrowserElement(OR_AREA,
                OR_YES_ADMINISTER_TEST);
        this.txtFirstNameAdministerTest = browser.getBrowserElement(OR_AREA,
                OR_FIRST_NAME_ADMINISTER_TEST);

        this.txtAddress1AdministerTest = browser.getBrowserElement(OR_AREA,
                OR_ADDRESS1_ADMINISTER_TEST);

        this.drpCountryAdministerTest = browser.getBrowserElement(OR_AREA,
                OR_COUNTRY_ADMINISTER_TEST);

        this.drpStateAdministerTest = browser.getBrowserElement(OR_AREA,
                OR_STATE_ADMINISTER_TEST);

        this.txtCityAdministerTest = browser.getBrowserElement(OR_AREA,
                OR_CITY_ADMINISTER_TEST);

        this.txtZipPostalCodeAdministerTest = browser.getBrowserElement(
                OR_AREA, OR_ZIPPOSTAL_CODE_ADMINISTER_TEST);

        this.txtEmailAddressAdministerTest = browser.getBrowserElement(OR_AREA,
                OR_EMAIL_ADDRESS_ADMINISTER_TEST);

        this.drpGenderAdministerTest = browser.getBrowserElement(OR_AREA,
                OR_GENDER_ADMINISTER_TEST);

        this.drpHowDoYouDescribeYourselfAdministerTest = browser
                .getBrowserElement(OR_AREA,
                        OR_HOW_DO_YOU_DESCRIBE_YOURSELF_ADMINISTER_TEST);

        this.btnSave = browser.getBrowserElement(OR_AREA,
                OR_SAVE_ADMINISTER_TEST);

        this.txtWelcomeAdministerTest = browser.getBrowserElement(OR_AREA,
                OR_WELCOME_ADMINISTER_TEST);
        this.txtHomePhone = browser.getBrowserElement(OR_AREA, OR_HOME_PHONE);
        this.txtCellPhone = browser.getBrowserElement(OR_AREA, OR_CELL_PHONE);
        this.txtAddress2 = browser.getBrowserElement(OR_AREA, OR_ADDRESS2);
        this.txtMiddleName = browser.getBrowserElement(OR_AREA, OR_MIDDLE_NAME);

    }

    @Override
    protected boolean perform() {
        boolean result = true;
        // Fill From 1
        if (!new LeftClickOnModalPopupButton(getBrowser(), this.btnClosePopUp)
                .performWithStates()) {
            LOGGER.error("Not able to Click on Close popuu Button");
            result = false;
        }
        AccuplacerBrowserUtils.WaitInMilliSeconds(500);
        if (!new SendTextToElementAction(getBrowser(), this.txtStudentId,
                this.strStudentId).performWithStates()) {
            LOGGER.info("Not able to send text Student Id field");
            result = false;
        }

        if (!new SendTextToElementAction(getBrowser(),
                this.txtConfirmStudentId, this.strStudentId)
                .performWithStates()) {
            LOGGER.info("Not able to send text to Confirm Student Id field");
            result = false;
        }

        if (!new SendTextToElementAction(getBrowser(), this.txtLastName,
                this.strLastName).performWithStates()) {
            LOGGER.info("Not able to send text to Last Name field");
            result = false;
        }

        if (!new DropdownSelectionAction(getBrowser(),
                this.drpDateOfBirthMonth, SelectorType.VISIBLE_TEXT,
                this.strDateOfBirthMonth).performWithStates(false, false)) {
            LOGGER.error("Not able to select Month from Drop Down");
            result = false;
        }

        if (!new DropdownSelectionAction(getBrowser(), this.drpDateOfBirthDate,
                SelectorType.VISIBLE_TEXT, this.strDateOfBirthDate)
                .performWithStates(false, false)) {
            LOGGER.error("Not able to select Date from Drop Down");
            result = false;
        }

        if (!new DropdownSelectionAction(getBrowser(), this.drpDateOfBirthYear,
                SelectorType.VISIBLE_TEXT, this.strDateOfBirthYear)
                .performWithStates(false, false)) {
            LOGGER.error("Not able to select Year from Drop Down");
            result = false;
        }

        if (!new LeftClickElementAction(getBrowser(),
                this.btnNextAdministerTest).performWithStates()) {
            LOGGER.error("Not able to Click on Next Button");
            result = false;
        }

        if (!new LeftClickOnModalPopupButton(getBrowser(),
                this.btnYesAdministerTest).performWithStates()) {
            LOGGER.error("Not able to Click on Yes Button");
            result = false;
        }
        // Fill Form 2
        if (getBrowser()
                .getPageSource()
                .contains(
                        "NOTE: If you have taken an ACCUPLACER test previously and your identifying information is not displayed below")) {
            LOGGER.info("Filling Student info Form 2");
            if (!new SendTextToElementAction(getBrowser(),
                    this.txtFirstNameAdministerTest, FIRST_NAME)
                    .performWithStates()) {
                LOGGER.info("Not able to send text to First Name field");
                result = false;
            }

            if (!new SendTextToElementAction(getBrowser(),
                    this.txtAddress1AdministerTest, ADDRESS)
                    .performWithStates()) {
                LOGGER.info("Not able to send text to Address1 field");
                result = false;
            }

            if (!new DropdownSelectionAction(getBrowser(),
                    this.drpCountryAdministerTest, SelectorType.VISIBLE_TEXT,
                    COUNTRY).performWithStates()) {
                LOGGER.error("Not able to select Country from Drop Down");
                result = false;
            }

            if (!new DropdownSelectionAction(getBrowser(),
                    this.drpStateAdministerTest, SelectorType.VISIBLE_TEXT,
                    STATE).performWithStates()) {
                LOGGER.error("Not able to select State from Drop Down");
                result = false;
            }

            if (!new SendTextToElementAction(getBrowser(),
                    this.txtCityAdministerTest, CITY).performWithStates()) {
                LOGGER.info("Not able to send text to City field");
                result = false;
            }

            if (!new SendTextToElementAction(getBrowser(),
                    this.txtZipPostalCodeAdministerTest, ZIP_CODE)
                    .performWithStates()) {
                LOGGER.info("Not able to send text to Zip/Postal code field");
                result = false;
            }

            if (!new SendTextToElementAction(getBrowser(),
                    this.txtEmailAddressAdministerTest, EMAIL_ADDRESS)
                    .performWithStates()) {
                LOGGER.info("Not able to send text to Email Address field");
                result = false;
            }

            if (!new SendTextToElementAction(getBrowser(), this.txtHomePhone,
                    PHONE).performWithStates()) {
                LOGGER.info("Not able to send text to Home Phone  field");
                result = false;
            }

            if (!new SendTextToElementAction(getBrowser(), this.txtCellPhone,
                    PHONE).performWithStates()) {
                LOGGER.info("Not able to send text to Cell Phone  field");
                result = false;
            }

            if (!new SendTextToElementAction(getBrowser(), this.txtMiddleName,
                    MIDDLE_NAME).performWithStates()) {
                LOGGER.info("Not able to send text to MiddleName field");
                result = false;
            }

            if (!new SendTextToElementAction(getBrowser(), this.txtAddress2,
                    ADDRESS).performWithStates()) {
                LOGGER.info("Not able to send text to Address field");
                result = false;
            }

            if (!new DropdownSelectionAction(getBrowser(),
                    this.drpGenderAdministerTest, SelectorType.VISIBLE_TEXT,
                    GENDER).performWithStates()) {
                LOGGER.error("Not able to select Gender from Drop Down");
                result = false;
            }
            if (!new DropdownSelectionAction(getBrowser(),
                    this.drpHowDoYouDescribeYourselfAdministerTest,
                    SelectorType.VISIBLE_TEXT, HOW_YOU_DESCRIBE_YOURSELF)
                    .performWithStates()) {
                LOGGER.error("Not able to select HowDoYouDescribeYourself from Drop Down");
                result = false;
            }
        }

        if (!new LeftClickOnModalPopupButton(getBrowser(), this.btnSave)
                .performWithStates()) {
            LOGGER.error("Not able to Click on Save Button");
            return false;
        }
        if (!new ScrollToPageTop(getBrowser()).performWithStates()) {
            LOGGER.error("Not able to Scroll to Page Top");
            result = false;
        }

        if (!new LeftClickOnModalPopupButton(getBrowser(),
                this.btnYesAdministerTest).performWithStates()) {
            LOGGER.error("Not able to Click on Yes Button");
            return false;
        }

        return result;

    }

    @Override
    protected boolean checkPostState() {
        boolean result = true;

        if (!new ElementExistsValidation(getBrowser(),
                this.txtWelcomeAdministerTest).performWithStates()) {
            LOGGER.error("Not able to validate the Welcome Msg ");
            result = false;
        }
        return result;

    }
}
