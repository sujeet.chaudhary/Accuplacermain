package com.pearson.itautomation.accuplacer.administertestold;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.pearson.itautomation.accuplacer.core.Buttons;
import com.pearson.itautomation.bca.Browser;
import com.pearson.itautomation.bca.BrowserAction;
import com.pearson.itautomation.bca.BrowserElement;
import com.pearson.itautomation.bca.SelectorType;
import com.pearson.itautomation.bca.interaction.DropdownSelectionAction;
import com.pearson.itautomation.bca.interaction.LeftClickElementAction;
import com.pearson.itautomation.bca.validation.ElementExistsValidation;

/**
 * This Class will select a Branching Profile based on the parameter<br>
 * <b>checkPrestate</b> - N/A <br>
 * <b>checkPostate</b> - N/A <br>
 * <b>perform</b> - This method will return true if the Branching Profile is
 * selected and the sessions is begun
 * 
 * 
 * @author Deepak Radhakrishnan
 */
public class SelectBranchingProfile extends BrowserAction {
    private static final Logger LOGGER = LoggerFactory
            .getLogger(SelectBranchingProfile.class);
    private static final String OR_AREA = "OR_Administer_Test";
    private static final String OR_SELECT_BRANCHING_PROFILE = "branchingProfileSelect";
    private static final String OR_CHECK_OPTION_EXISTS = "optionBySimpleText";
    private final String strBranchingProfile;

    private final BrowserElement drpNewRuleTypeCondition;
    private final BrowserElement btnAdministerTest;
    private final BrowserElement bpSelectElementCheck;

    /**
     * This constructor will initialize the confirmation message
     * 
     * @param browser
     * @param strBranchingProfile
     *            Save confirmation message
     * @author Deepak Radhakrishnan
     */
    public SelectBranchingProfile(Browser browser, String strBranchingProfile) {
        super(browser);
        this.strBranchingProfile = strBranchingProfile;
        this.drpNewRuleTypeCondition = browser.getBrowserElement(OR_AREA,
                OR_SELECT_BRANCHING_PROFILE);
        this.bpSelectElementCheck = browser.getBrowserElement("OR_Global",
                OR_CHECK_OPTION_EXISTS).formatWithParms(strBranchingProfile);
        btnAdministerTest = Buttons.getButtonType1("Administer Test");
    }

    @Override
    protected boolean perform() {
        boolean result = true;
        if (!new ElementExistsValidation(getBrowser(), bpSelectElementCheck)
                .performWithStates()) {
            LOGGER.error("Not able Find the option {}", bpSelectElementCheck);
            result = false;
        }
        if (!new DropdownSelectionAction(getBrowser(), drpNewRuleTypeCondition,
                SelectorType.VISIBLE_TEXT, strBranchingProfile)
                .performWithStates(false, false)) {
            LOGGER.error("Not able to click on Select the Branching Profile");
            result = false;
        }
        if (!new LeftClickElementAction(getBrowser(), btnAdministerTest)
                .performWithStates()) {
            LOGGER.error("Not able to click Administer Test Button");
            result = false;
        }

        return result;
    }
}
