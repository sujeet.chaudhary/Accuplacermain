package com.pearson.itautomation.accuplacer.administertestold;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Properties;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.pearson.itautomation.accuplacer.core.CommonUtilities;
import com.pearson.itautomation.accuplacer.core.action.NavigateToMenu;
import com.pearson.itautomation.accuplacer.interaction.SwitchToWindowByPageContent;
import com.pearson.itautomation.bca.Browser;
import com.pearson.itautomation.bca.BrowserElement;
import com.pearson.itautomation.bca.interaction.LeftClickElementAction;
import com.pearson.itautomation.bca.validation.ElementExistsValidation;

/**
 * This Class will consist of all the Administer Test utility methods
 * 
 * @author Deepak Radhakrishnan
 * 
 */
public class AdministerTestUtils {
    private final Browser browser;
    private static final Logger LOGGER = LoggerFactory
            .getLogger(AdministerTestUtils.class);
    protected Properties sqlQueries = CommonUtilities
            .readPropertyFile("//config//SQL.properties");
    private static final int FAILURE_CODE = 999;
    private static final String FAILURE_STRING = "ERROR";
    private static final int POSITION_OF_QUESTION_COUNT = 4;
    private static final int ARRSIZE = 5;
    private static final String OR_AREA = "OR_Administer_Test_OLD";

    public AdministerTestUtils(Browser browser) {
        this.browser = browser;
    }

    /**
     * This method will return the Question Count of the current Test
     * 
     * @return 999 if the method fails to find the element else it would return
     *         the actual Number
     * @author Deepak Radhakrishnan
     */
    public int getQuestionCount() {
        LOGGER.info("In getQuestionCount Method");
        BrowserElement headerElementQuestionCount = this.browser
                .getBrowserElement(OR_AREA, "headerQuestionQuestionCount");
        headerElementQuestionCount.setTimeout(5000);
        if (!new ElementExistsValidation(browser, headerElementQuestionCount)
                .performWithStates()) {
            LOGGER.error("Could Not Find the Browser Element {}",
                    headerElementQuestionCount);
            return FAILURE_CODE;
        }
        String questionCount = this.browser.getElementWithWait(
                headerElementQuestionCount).getText();
        if (questionCount == null) {
            LOGGER.error("Could Not Find the Number of questions");
            return FAILURE_CODE;
        }
        String[] countArr = questionCount.split(" ");
        // the actual count is at position 4
        if (countArr[POSITION_OF_QUESTION_COUNT] == null) {
            LOGGER.error("Could Not Find the Number of questions");
            return FAILURE_CODE;
        } else {
            return Integer.parseInt(countArr[POSITION_OF_QUESTION_COUNT]);
        }
    }

    /**
     * This Method will return true of the option value is selected
     * 
     * @param optionChar
     * 
     * @return True if answer is selected
     */
    public boolean selectANS(String optionChar) {
        boolean result = true;
        BrowserElement optionValue = this.browser.getBrowserElementWithFormat(
                OR_AREA, "optionValueAnswer", optionChar);
        if (!new LeftClickElementAction(browser, optionValue)
                .performWithStates()) {
            LOGGER.error("Could not click Click On Option Answer");
            result = false;
        }
        return result;
    }

    public String getTestName(final Connection con) {
        ResultSet rset = null;
        String lTemp = null;
        Statement stmt = null;
        BrowserElement questionItemId = this.browser.getBrowserElement(OR_AREA,
                "itemId");
        if (questionItemId == null) {
            LOGGER.warn("Could Not Find the Browser Element '{}'",
                    questionItemId);
        }

        String itemid = this.browser.getElementWithWait(questionItemId)
                .getAttribute("Value");

        String strInitQuery = "select test_name from test_detail where test_detail_id in "
                + ""
                + "(select test_detail_id from cat_item_parameter where item_id ="
                + itemid + ")";
        try {
            stmt = con.createStatement();
            rset = stmt.executeQuery(strInitQuery);
            if (!rset.next()) {
                LOGGER.error("No records were found for the item_id: " + itemid
                        + " in the database");
            } else {
                do {
                    lTemp = rset.getString("TEST_NAME");
                } while (rset.next());

            }
        } catch (SQLException e) {
            LOGGER.error("Exception occured while executing the query", e);
        } finally {
            try {
                if (stmt != null && !con.isClosed()) {
                    stmt.close();
                }
                if (rset != null && !con.isClosed()) {
                    rset.close();
                }
            } catch (SQLException e) {
                LOGGER.error("Exception occured while closing the statement", e);
            }
        }

        return lTemp;

    }

    /**
     * This method will return itemID from the UI
     * 
     * @return itemid
     */
    public String getItemIDFromUI() {
        BrowserElement questionItemId = this.browser.getBrowserElement(OR_AREA,
                "itemId");
        if (questionItemId == null) {
            LOGGER.error("Could Not Find the Browser Element {}",
                    questionItemId);
            return FAILURE_STRING;
        }

        if (this.browser.getElementWithWait(questionItemId) == null) {
            LOGGER.error("Could Not Find the {} element", questionItemId);
            return FAILURE_STRING;

        }
        String itemid = this.browser.getElementWithWait(questionItemId)
                .getAttribute("Value");
        return itemid;
    }

    /**
     * @param con
     *            DB connection
     * @param itemid
     *            Item ID from UI
     * @return The correct answer from the DB , Failure code in case of error
     */
    public String getCorrectAnswer(final Connection con, String itemid) {
        ResultSet rset = null;
        String lTemp = null;
        Statement stmt = null;

        if (itemid == null) {
            LOGGER.error("Could Not Find the item ID");
            return FAILURE_STRING;
        }
        String strInitQuery = "select item_key from cat_item_parameter where item_id ="
                + itemid;
        try {
            stmt = con.createStatement();
            rset = stmt.executeQuery(strInitQuery);
            if (!rset.next()) {
                LOGGER.error("No records were found for the item_id: " + itemid
                        + " in the database");
                return FAILURE_STRING;
            } else {
                do {
                    lTemp = rset.getString("item_key");
                } while (rset.next());

            }
        } catch (SQLException e) {
            LOGGER.error(
                    "Exception occured while executing the query for item id "
                            + itemid, e);
        } finally {
            try {
                if (stmt != null && !con.isClosed()) {
                    stmt.close();
                }
                if (rset != null && !con.isClosed()) {
                    rset.close();
                }
            } catch (SQLException e) {
                LOGGER.error("Exception occured while closing the statement", e);
            }
        }
        return lTemp;
    }

    /**
     * This Method will choose incorrect answer
     * 
     * @param itemKey
     * @return
     */
    public boolean chooseIncorrectAns(String itemKey) {
        if ("A".equals(itemKey)) {
            return selectANS("B");
        } else {
            return selectANS("A");
        }
    }

    /**
     * This Method will choose correct answer
     * 
     * @param itemKey
     */
    public boolean chooseCorrectAns(String itemKey) {
        return selectANS(itemKey);
    }

    public boolean concludeSession(String password) {
        boolean result = true;
        if (!new EnterProctorPassword(this.browser, password)
                .performWithStates()) {
            LOGGER.error("Could not enter proctor password nad submit");
            return false;
        }
        // Closing the ISR window
        browser.close();

        // Switching to the ISR window
        result = switchToMainWindowAndConclude(password);
        return result;
    }

    public boolean switchToMainWindowAndConclude(String password) {
        boolean result = true;
        if (!new SwitchToWindowByPageContent(this.browser,
                "Logged in as someone else?").performWithStates()) {
            LOGGER.error("Could not Switch to the main session window");
            result = false;
        }
        if (!new EnterProctorPassword(browser, password).performWithStates()) {
            LOGGER.error("Could not enter proctor password and submit");
            return false;
        }
        if ((!this.browser.getPageSource().contains(
                "POP-UP BLOCKERS MUST BE DISABLED TO"))
                && (!new NavigateToMenu(browser, "Administer Test",
                        "Administer New Test Session").performWithStates())) {
            LOGGER.error("Failed to navigate to Administer Test Page");
            return false;
        }

        return result;
    }

    public String[][] getWriteplacerResponse(String studentId,
            final Connection con) {
        String[][] arrNull = { { "NULL" }, { "NULL" } };
        try {
            ResultSet rset = null;
            Statement stmt = null;

            String sqlwriteplacerresponse = sqlQueries
                    .getProperty("sqlwriteplacerresponse1")
                    + studentId
                    + sqlQueries.getProperty("sqlwriteplacerresponse2");
            stmt = con.createStatement();
            rset = stmt.executeQuery(sqlwriteplacerresponse);
            String[][] strWriteplacerPrompt = new String[ARRSIZE][ARRSIZE];
            if (rset.getFetchSize() != 0) {
                strWriteplacerPrompt = getStringRecordSetArray(rset,
                        "writeplacer_prompt_title,created_on");
                return strWriteplacerPrompt;

            } else {
                return arrNull;
            }

        } catch (SQLException e) {
            LOGGER.error("Error Occured while fetching the Writeplacer Report",
                    e);
            return new String[0][0];
        }
    }

    /*
     * Name - getStringRecordSetArray This method will return top 5 entries from
     * the column
     */
    public String[][] getStringRecordSetArray(ResultSet rset,
            String strDBColumnName) {
        String[] dBcolumns = strDBColumnName.split(",");

        String lTemp = null;
        int i = 0;
        String[][] strArr = new String[ARRSIZE][dBcolumns.length];
        try {
            while (rset.next() && (i < ARRSIZE)) {
                int count = 0;
                do {
                    lTemp = rset.getString(dBcolumns[count]);
                    strArr[i][count] = lTemp;
                    count++;
                } while (count < dBcolumns.length);
                i++;

            }
            if (strArr[1] != null) {
                LOGGER.info("Responses found");
                return strArr;
            } else {
                LOGGER.error("No Responses found");
                return new String[0][0];
            }
        } catch (SQLException e) {
            LOGGER.error("Exception occured in getStringRecordSetArray", e);
        } finally {
            try {

                if (rset != null) {
                    rset.close();
                }
            } catch (SQLException e) {
                LOGGER.error("Exception occured while closing the reslutset", e);
            }
        }
        return strArr;
    }

    /**
     * Check for item errors
     * 
     * @param locationToCheckForError
     *            Check for errors at location
     * @return
     */
    public boolean checkIfItemIsErrorFree(String locationToCheckForError) {
        boolean isErrorFreeFlag = true;
        if (browser.getPageSource().contains(
                "The following error(s) must be corrected")) {
            LOGGER.error("Error Found for the Item At "
                    + locationToCheckForError + " , timestamp - "
                    + CommonUtilities.getCurrentDateTime());
            isErrorFreeFlag = false;
        }
        return isErrorFreeFlag;

    }
}