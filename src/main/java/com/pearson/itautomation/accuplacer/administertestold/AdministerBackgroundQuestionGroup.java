package com.pearson.itautomation.accuplacer.administertestold;

import org.openqa.selenium.WebElement;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.pearson.itautomation.accuplacer.core.Buttons;
import com.pearson.itautomation.bca.Browser;
import com.pearson.itautomation.bca.BrowserAction;
import com.pearson.itautomation.bca.BrowserElement;
import com.pearson.itautomation.bca.interaction.LeftClickElementAction;

/**
 * This Class will answer the all Background questions <br>
 * <b>checkPrestate</b> - N/A <br>
 * <b>checkPostate</b> - This method will validate the Welcome Page <br>
 * <b>perform</b> - This method will select the answer for every Background
 * question.<br>
 * 
 * @author Sujeet Chaudhary
 */
public class AdministerBackgroundQuestionGroup extends BrowserAction {

    private static final Logger LOGGER = LoggerFactory
            .getLogger(AdministerBackgroundQuestionGroup.class);
    private static final int INDEX_OF_BQ_TITLE_POSITION = 4;
    private final BrowserElement txtSubmitButtonBackgroundQuestion = Buttons
            .getButtonType1("Submit");;
    private final BrowserElement txtConfirmSubmitButtonBackgroundQuestion = Buttons
            .getButtonType1("Confirm");
    private final BrowserElement bgQuestionTitle;
    private final BrowserElement bgQuestionResponse1;

    /**
     * This constructor will initialize the mandatory fields and the
     * BrowserElements from this class
     * 
     */
    public AdministerBackgroundQuestionGroup(final Browser browser) {
        super(browser);
        this.bgQuestionTitle = getBrowser().getBrowserElement(
                "OR_Administer_Test_OLD", "bgQuestionTitle");
        this.bgQuestionResponse1 = getBrowser().getBrowserElement(
                "OR_Administer_Test_OLD", "bqResponseNo1");
    }

    @Override
    protected boolean perform() {
        boolean result = true;
        try {
            if (this.getBrowser().getWebDriver().getPageSource()
                    .contains("Background Questions")) {

                WebElement elementToCheckBQ = getBrowser().getElementWithWait(
                        bgQuestionTitle);
                if (elementToCheckBQ != null) {
                    String s1 = elementToCheckBQ.getText();
                    String[] str = s1.split(" ");
                    int noOfBackgroungQuestion = Integer
                            .parseInt(str[INDEX_OF_BQ_TITLE_POSITION]);
                    for (int i = 1; i <= noOfBackgroungQuestion; i++) {
                        if (!new LeftClickElementAction(getBrowser(),
                                this.bgQuestionResponse1).performWithStates()) {
                            LOGGER.error("Not able to Click on BQ Response Button");
                            return false;
                        }
                        if (!new LeftClickElementAction(getBrowser(),
                                this.txtSubmitButtonBackgroundQuestion)
                                .performWithStates()) {
                            LOGGER.error("Not able to Click on submit Button");
                            result = false;
                        }
                        if (!new LeftClickElementAction(getBrowser(),
                                this.txtConfirmSubmitButtonBackgroundQuestion)
                                .performWithStates()) {
                            LOGGER.error("Not able to Click on Confirm Button");
                            result = false;
                        }
                    }
                    return result;
                } else {
                    LOGGER.error("Not able to find {}", elementToCheckBQ);
                    return false;
                }
            }
        } catch (NumberFormatException e) {
            LOGGER.error("String Cannot be resolve into a Number", e);
            return false;
        }
        return result;
    }
}
