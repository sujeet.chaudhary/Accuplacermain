package com.pearson.itautomation.accuplacer.administertestold;

import java.io.File;
import java.io.IOException;
import java.sql.Connection;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;

import org.apache.commons.io.FileUtils;
import org.openqa.selenium.By;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebElement;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.pearson.itautomation.accuplacer.core.AccuplacerBrowserUtils;
import com.pearson.itautomation.accuplacer.core.Buttons;
import com.pearson.itautomation.accuplacer.core.CommonUtilities;
import com.pearson.itautomation.accuplacer.core.action.LeftClickOnModalPopupButton;
import com.pearson.itautomation.accuplacer.core.action.NavigateToMenu;
import com.pearson.itautomation.accuplacer.interaction.SwitchToWindowByPageContent;
import com.pearson.itautomation.accuplacer.interaction.SwitchToWindowByPageURL;
import com.pearson.itautomation.bca.Browser;
import com.pearson.itautomation.bca.BrowserAction;
import com.pearson.itautomation.bca.BrowserElement;
import com.pearson.itautomation.bca.SelectorType;
import com.pearson.itautomation.bca.interaction.ClearTextElementAction;
import com.pearson.itautomation.bca.interaction.DropdownSelectionAction;
import com.pearson.itautomation.bca.interaction.LeftClickElementAction;
import com.pearson.itautomation.bca.interaction.SendTextToElementAction;

/**
 * This Class Will Administer CAT Tests Based on the Parameters<br>
 * <b>checkPrestate</b> - N/A <br>
 * <b>checkPostate</b> - N/A <br>
 * <b>perform</b> - This method will return true if the sample questions have
 * been administered successfully
 * 
 * 
 * @author Deepak Radhakrishnan
 */
public class AdministerSaveAndResumeCATTest extends BrowserAction {
    private static final Logger LOGGER = LoggerFactory
            .getLogger(AdministerSaveAndResumeCATTest.class);
    String itemid, testName, itemKey = null;
    String correctAnswer = null;
    int numberOfQuestion, savedQuesCount = 0;
    boolean isSave = false;
    private String[] savedArr = null;
    private final AdministerTestUtils adminTU;
    private boolean saveResumeFlag = true;
    private boolean submitSession;
    private final Connection con;
    private boolean takeScreenshot = false;

    private static final String OR_AREA = "OR_Administer_Test_OLD";
    private static final String TEST_COMPLETE_TEXT = "You have completed your";
    private static final int FAILURE_CODE = 999;
    private static final String FAILURE_STRING = "ERROR";
    private static final int PAGE_LOAD_BLOCK = 90;
    private static final int BEGIN_TEST_TIMEOUT = 30000;
    private final BrowserElement btnSubmit;
    private final BrowserElement btnConfirm;
    private final String[] answerKeys;
    private final BrowserElement lnkSaveResume;
    private final BrowserElement btnYes;
    private final BrowserElement btnClose;
    private final BrowserElement txtStudentID;
    private final BrowserElement txtFromDate;
    private final BrowserElement txtToDate;
    private final BrowserElement btnPerformSearch;

    private String classNameOfEXNUM;
    private final String password;
    private final String studentId;
    private final BrowserElement lnkExnum;
    private final BrowserElement lnkResume;
    private final BrowserElement btnBeginTest;
    private final BrowserElement drpSelectSessionStatus;
    private HashMap<String, String> itemHashMap = new HashMap<String, String>();
    private String userName;
    private final String RESTEST_ATTEMPT_TEXT = "You have reached the re-test limit for the following tests";
    private String itemListString;

    /**
     * This constructor will initialize the confirmation message
     * 
     * @param browser
     * @param password
     *            Password of logged in user
     * @param studentId
     *            Student Id
     * @param strBranchingProfile
     *            Branching Profile of the Student
     * @param answerKeys
     *            AnswerKeys Parameter
     * @param savedQuestion
     *            SaveAndResumeAtQuestion
     * 
     * @param con
     *            database connection
     * 
     * @author Deepak Radhakrishnan
     */
    public AdministerSaveAndResumeCATTest(Browser browser, String password,
            String studentId, String answerKeys, String savedQuestion,
            Connection con) {
        super(browser);
        this.con = con;
        adminTU = new AdministerTestUtils(browser);
        if ("".equalsIgnoreCase(savedQuestion)) {
            saveResumeFlag = false;
        } else {
            savedArr = savedQuestion.split(",");
        }
        this.btnSubmit = Buttons.getButtonType1("Submit");
        this.btnBeginTest = Buttons.getButtonType1("Begin Test");
        this.btnConfirm = Buttons.getButtonType1("Confirm");
        this.answerKeys = answerKeys.split(",");
        this.lnkSaveResume = browser.getBrowserElement(OR_AREA,
                "saveAndResumeLink");
        this.btnYes = Buttons.getButtonType1("Yes");
        this.btnClose = Buttons.getButtonType1("Close");
        this.password = password;
        this.txtStudentID = browser.getBrowserElement(OR_AREA,
                "StudentIdBoxManageOpen");
        this.txtFromDate = browser.getBrowserElement(OR_AREA,
                "FromDateManageOpen");
        this.txtToDate = browser.getBrowserElement(OR_AREA,
                "ToDateBoxManageOpen");
        this.btnPerformSearch = Buttons.getButtonType1("Search");
        this.studentId = studentId;
        this.lnkExnum = browser.getBrowserElement(OR_AREA,
                "manageOpenExnumLink");
        this.lnkResume = browser.getBrowserElement(OR_AREA,
                "manageOpenResumeLink");
        this.drpSelectSessionStatus = browser.getBrowserElement(OR_AREA,
                "ManageOpenStatusDrp");
    }

    public AdministerSaveAndResumeCATTest(Browser browser, String password,
            String studentId, String answerKeys, String savedQuestion,
            Connection con, boolean submitSession, String userName) {
        this(browser, password, studentId, answerKeys, savedQuestion, con);
        this.submitSession = submitSession;
        this.userName = userName;

    }

    public AdministerSaveAndResumeCATTest(Browser browser, String password,
            String studentId, String answerKeys, String savedQuestion,
            Connection con, boolean takeScreenshot) {
        this(browser, password, studentId, answerKeys, savedQuestion, con);
        this.takeScreenshot = takeScreenshot;

    }

    @Override
    protected boolean perform() {
        boolean result = true;
        // To Check the Number of questions

        for (int testNumberN = 0; testNumberN < answerKeys.length; testNumberN++) {
            savedQuesCount = 0;
            if (getBrowser().getPageSource().contains(RESTEST_ATTEMPT_TEXT)) {
                LOGGER.info("Maximum Retest Attempts Reached");
                result = (new RetestAttempt(getBrowser(), userName, password,
                        submitSession).performWithStates());
                if (result) {
                    LOGGER.info("Retest Attempt Successfull!!");
                    if ((!submitSession)
                            && (AccuplacerBrowserUtils
                                    .checkPageSourceForString(getBrowser(),
                                            "Administer New Test Session") || AccuplacerBrowserUtils
                                    .checkPageSourceForString(getBrowser(),
                                            "Test Session Search Results"))) {
                        LOGGER.info("Retest Attempt Closed Successfully");
                        return true;

                    }
                } else {
                    LOGGER.info("Retest Attempt UNSuccessfull!!");
                    return false;
                }
                AccuplacerBrowserUtils.WaitInMilliSeconds(BEGIN_TEST_TIMEOUT);
                if (getBrowser().getPageSource().contains(TEST_COMPLETE_TEXT)) {
                    LOGGER.info("Test Session is COMPLETE , return to the main code");
                    displayItems(itemHashMap);
                    return adminTU.concludeSession(password);
                } else {
                    LOGGER.info("Test Session is NOT COMPLETE");

                }
            }
            if (getBrowser().getPageSource().contains(TEST_COMPLETE_TEXT)) {
                LOGGER.info("Test Session is COMPLETE , return to the main code");
                displayItems(itemHashMap);
                return adminTU.concludeSession(password);
            }
            if (!adminTU.checkIfItemIsErrorFree("Before Sample Question")) {
                return false;
            }
            if (!new AdministerSampleQuestions(this.getBrowser())
                    .performWithStates()) {
                LOGGER.error("Failed While Answering Sample Questions");
                return false;
            }
            numberOfQuestion = adminTU.getQuestionCount();
            // Checking of the Question count was found successfully
            if (numberOfQuestion == FAILURE_CODE) {
                LOGGER.error("Failed to find the Question Count");
                return false;
            }
            if (!(this.answerKeys[testNumberN].length() == numberOfQuestion)) {
                LOGGER.info("AnswerKey pattern Does NOT match with UI content");
                if (this.answerKeys[testNumberN].length() > numberOfQuestion) {
                    LOGGER.info("AnswerKey patteren is greated than the number of Questions");
                } else {
                    LOGGER.info("AnswerKey patteren is LESS than the number of Questions");
                    int appendNoOfChar = (numberOfQuestion - this.answerKeys[testNumberN]
                            .length());
                    for (int i = 0; i < appendNoOfChar; i++) {
                        this.answerKeys[testNumberN] = this.answerKeys[testNumberN]
                                + "1";
                    }
                }
            }

            // For Loop Running for Test administration
            for (int i = 1; i <= numberOfQuestion; i++) {
                LOGGER.info("Answer Question - " + i + " of "
                        + numberOfQuestion);

                // Checking for Save Resume
                if ((saveResumeFlag && savedQuesCount < savedArr.length)
                        && (Integer.parseInt(savedArr[savedQuesCount]) == (i))) {
                    LOGGER.info("In Save and Resume Loop Saving at Questions no "
                            + savedArr[savedQuesCount]);
                    savedQuesCount++;

                    if (!new LeftClickElementAction(this.getBrowser(),
                            this.lnkSaveResume).performWithStates()) {
                        LOGGER.error("Could not  Click On Save and Resume Link");
                        result = false;
                    }

                    if (!new LeftClickOnModalPopupButton(this.getBrowser(),
                            this.btnYes).performWithStates()) {
                        LOGGER.error("Could not  Click On Yes Button");
                        result = false;
                    }

                    if (!new LeftClickElementAction(this.getBrowser(),
                            this.btnClose).performWithStates()) {
                        LOGGER.error("Could not  Click On Close Button");
                        result = false;
                    }

                    if (!new SwitchToWindowByPageContent(this.getBrowser(),
                            "Logged in as someone else?").performWithStates()) {
                        LOGGER.error("Could not Switch to the main session window");
                        result = false;
                    }

                    if (!new EnterProctorPassword(this.getBrowser(),
                            this.password).performWithStates()) {
                        LOGGER.error("Could not enter proctor password nad submit");
                        return false;
                    }

                    if (!this.getBrowser().getPageSource()
                            .contains("Test Session Search Results")) {
                        LOGGER.info("User is NOT on Manage open search Page");
                        if (!new NavigateToMenu(this.getBrowser(),
                                "Administer Test", "Manage Test Sessions")
                                .performWithStates()) {
                            LOGGER.error("Failed to navigate to Manage open sessions page");
                            return false;
                        }
                        result = searchForSession();
                    } else {
                        LOGGER.info("User is ALREADY on Manage open search Page");
                        result = searchForSession();

                    }

                    if (!new LeftClickElementAction(getBrowser(),
                            this.btnPerformSearch).performWithStates()) {
                        LOGGER.error("Not able to Click on Perform Search Button");
                        return false;
                    }
                    AccuplacerBrowserUtils.performAccuplacerPageLoadWaitBlock(
                            getBrowser(), PAGE_LOAD_BLOCK);
                    if (getBrowser().getPageSource().contains(
                            "Date value cannot be in the future")) {
                        LOGGER.info("Rentering the To Date");
                        if (!new ClearTextElementAction(getBrowser(),
                                this.txtToDate).performWithStates()) {
                            LOGGER.error("Not able clear \"To\" Date");
                            result = false;
                        }

                        if (!new SendTextToElementAction(getBrowser(),
                                this.txtToDate,
                                CommonUtilities
                                        .getDayFromTodayUSCentralTimeZone(-1))
                                .performWithStates()) {
                            LOGGER.error("Not able to type to Date");
                            result = false;
                        }
                        if (!new LeftClickElementAction(getBrowser(),
                                this.btnPerformSearch).performWithStates()) {
                            LOGGER.error("Not able to Click on Perform Search Button");
                            return false;
                        }
                    }
                    if (this.lnkExnum != null) {
                        classNameOfEXNUM = getBrowser().getElementWithWait(
                                this.lnkExnum).getAttribute("class");
                        if (!classNameOfEXNUM.contains("sorting_desc")) {
                            LOGGER.info("Sorting By Exnum");

                            if (!new LeftClickOnModalPopupButton(getBrowser(),
                                    this.lnkExnum).performWithStates()) {
                                LOGGER.error("Not able to Click on Exnum Link");
                                result = false;
                            }
                        } else {
                            LOGGER.info("Exnum Already sorted");
                        }
                    }

                    if (!new LeftClickElementAction(getBrowser(),
                            this.lnkResume).performWithStates()) {
                        LOGGER.error("Not able to Click on Resume Link");
                        return false;
                    }

                    if (!new SwitchToWindowByPageURL(this.getBrowser(),
                            "resumeTestWithToken.do").performWithStates()) {
                        LOGGER.error("Could not Switch to the resume session window");
                        result = false;
                    }
                    this.btnBeginTest.setTimeout(BEGIN_TEST_TIMEOUT);
                    if (!result) {
                        LOGGER.error("Not able to find the resume window");
                        return false;
                    }

                    if (!new LeftClickElementAction(getBrowser(),
                            this.btnBeginTest).performWithStates()) {
                        LOGGER.error("Not able to Click on Begin TestButton Link");
                        result = false;
                    }
                    LOGGER.info("Resuming at Questions no"
                            + savedArr[savedQuesCount - 1]);
                }

                if (!adminTU.checkIfItemIsErrorFree("Question " + i)) {
                    return false;
                }
                // changes
                itemid = adminTU.getItemIDFromUI();
                if (itemid.equalsIgnoreCase(FAILURE_STRING)) {
                    LOGGER.error("Failed to Find the Item ID");
                    return false;
                }
                if (takeScreenshot) {
                    List<WebElement> list = getBrowser().getWebDriver()
                            .findElements(By.tagName("img"));
                    if (list.size() > 0) {
                        LOGGER.info("Underline found!!");
                        File dir = new File("c:\\ImagesItems\\Images\\"
                                + itemid + ".png");
                        if (dir.exists()) {
                            LOGGER.info("Item screenshot Already exists");
                        } else if (getBrowser() != null) {
                            LOGGER.info("Taking screeenshot");

                            File scrFile = ((TakesScreenshot) getBrowser()
                                    .getWebDriver())
                                    .getScreenshotAs(OutputType.FILE);
                            try {
                                String screenshotName = "c:\\ImagesItems\\Images\\"
                                        + itemid + ".png";
                                FileUtils.copyFile(scrFile, new File(
                                        screenshotName));
                            } catch (IOException e) {
                                LOGGER.error("Error while taking screenshot", e);
                            }
                        }
                    }
                }

                correctAnswer = adminTU.getCorrectAnswer(con, itemid);
                if (correctAnswer.equalsIgnoreCase(FAILURE_STRING)) {
                    LOGGER.error("Failed to Find the correct answer");
                    return false;
                }
                if (answerKeys[testNumberN].charAt(i - 1) == '1') {
                    LOGGER.info("Selecting Correct Answer");
                    if (!adminTU.chooseCorrectAns(correctAnswer)) {
                        LOGGER.error("Could NOT Choose the InCorrect answer");
                        return false;
                    }
                } else {
                    LOGGER.info("Selecting Incorrect Answer");
                    if (!adminTU.chooseIncorrectAns(correctAnswer)) {
                        LOGGER.error("Could NOT Choose the InCorrect answer");
                        return false;
                    }
                }
                result = checkForDuplicates(i + "", itemid);
                if (!result) {
                    LOGGER.error("DUPLICATE ITEM FOUND");
                    return false;
                }
                if (!new LeftClickElementAction(this.getBrowser(),
                        this.btnSubmit).performWithStates()) {
                    LOGGER.error("Could not click Click On Submit Answer");
                    result = false;
                }
                if (!new LeftClickElementAction(this.getBrowser(),
                        this.btnConfirm).performWithStates()) {
                    LOGGER.error("Could not click Click On Confirm Answer");
                    result = false;
                }
            }
            // check if the test is complete
            if (result) {
                if (getBrowser().getPageSource().contains(TEST_COMPLETE_TEXT)) {
                    LOGGER.info("Test Session is COMPLETE , return to the main code");
                    displayItems(itemHashMap);
                    return adminTU.concludeSession(password);
                }
                // check for errors on Page
                if (!adminTU.checkIfItemIsErrorFree("End of Test")) {
                    return false;
                } else if (AccuplacerBrowserUtils.checkPageSourceForString(
                        getBrowser(), TEST_COMPLETE_TEXT)) {
                    // wait and check if the test is complete
                    displayItems(itemHashMap);
                    return adminTU.concludeSession(password);
                }
            }
        }
        return result;
    }

    private boolean searchForSession() {
        boolean resultOfSearchSessions = true;

        if (!new ClearTextElementAction(getBrowser(), this.txtStudentID)
                .performWithStates()) {
            LOGGER.error("Not able clear Student ID");
            resultOfSearchSessions = false;
        }

        if (!new SendTextToElementAction(getBrowser(), this.txtStudentID,
                this.studentId).performWithStates()) {
            LOGGER.error("Not able type Student ID");
            resultOfSearchSessions = false;
        }
        if (!new ClearTextElementAction(getBrowser(), this.txtFromDate)
                .performWithStates()) {
            LOGGER.error("Not able clear From Date");
            resultOfSearchSessions = false;
        }

        if (!new SendTextToElementAction(getBrowser(), this.txtFromDate,
                CommonUtilities.getDayFromTodayUSCentralTimeZone(-1))
                .performWithStates()) {
            LOGGER.error("Not able to type \"From\" Date");
            resultOfSearchSessions = false;
        }

        if (!new ClearTextElementAction(getBrowser(), this.txtToDate)
                .performWithStates()) {
            LOGGER.error("Not able clear \"To\" Date");
            resultOfSearchSessions = false;
        }

        if (!new SendTextToElementAction(getBrowser(), this.txtToDate,
                CommonUtilities.getDayFromTodayUSCentralTimeZone(0))
                .performWithStates()) {
            LOGGER.error("Not able to type to Date");
            resultOfSearchSessions = false;
        }

        if (!new DropdownSelectionAction(getBrowser(), drpSelectSessionStatus,
                SelectorType.VISIBLE_TEXT, "Open").performWithStates(false,
                true)) {
            LOGGER.error("Not able Select Status as Open");
            resultOfSearchSessions = false;
        }
        return resultOfSearchSessions;
    }

    /**
     * This method will check it the current item has already been administered
     * in the test
     * 
     * 
     * @param itemNumber
     *            Question Number
     * @param itemID
     *            itemID of the exposed item
     * @return true if no duplicates are found
     */
    public boolean checkForDuplicates(String itemNumber, String itemID) {
        String itemString;
        if (itemHashMap != null && itemHashMap.containsValue(itemID)) {
            LOGGER.error(String.format(
                    "SAME ITEM %s FOUND IN THE TEST SESSION AT QUESTION %s",
                    itemid, itemNumber));
            displayItems(itemHashMap);
            return false;
        } else {
            if (itemNumber.length() == 1) {
                itemString = "0" + itemNumber;
            } else {
                itemString = itemNumber;
            }
            itemHashMap.put("Question" + itemString, itemID);
            return true;
        }
    }

    /**
     * This method will display the entore item id list for the test session
     * 
     * @param itemHashMap
     *            - HashMap for all the item entries
     */
    public void displayItems(HashMap<String, String> itemHashMap) {
        List<String> itemList = new ArrayList<String>(itemHashMap.keySet());
        Collections.sort(itemList);

        List<String> v = new ArrayList<String>(itemHashMap.keySet());
        Collections.sort(v);

        String itemListString = "";
        for (String str : v) {
            itemListString = itemListString + str + " - "
                    + (String) itemHashMap.get(str) + " ;";
        }
        LOGGER.info(itemListString);
        this.setItemListString(itemListString);

    }

    /**
     * Getter method List of item ids
     */
    public String getItemListString() {
        return itemListString;
    }

    /**
     * Setter method List of item ids
     */
    public void setItemListString(String itemListString) {
        this.itemListString = itemListString;
    }
}