package com.pearson.itautomation.accuplacer.core.action;

import org.openqa.selenium.By;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.pearson.itautomation.bca.Browser;
import com.pearson.itautomation.bca.BrowserAction;
import com.pearson.itautomation.bca.BrowserElement;
import com.pearson.itautomation.bca.interaction.LeftClickElementAction;
import com.pearson.itautomation.bca.validation.ElementExistsValidation;

/**
 * This Class will navigate the user to a specific MainMenu <br>
 * <b>checkPrestate</b> - This method will return true of the Main menu link is
 * present<br>
 * <b>checkPostate</b> - N/A <br>
 * <b>perform</b> - This method will return true if the Main Menu Link is
 * clicked successfully
 * 
 * @author Deepak Radhakrishnan
 * 
 */
public class NavigateToTab extends BrowserAction {
    private static final Logger LOGGER = LoggerFactory
            .getLogger(NavigateToTab.class);
    private final BrowserElement lnkTab;
    private String strTabName;

    /**
     * This constructor will initialize the browser elements and the constant
     * variables
     * 
     * @param browser
     * @param strTabName
     *            Left Panel Link Text value
     * @param strSubMenuName
     *            Link Text Value *
     * 
     * @author Deepak Radhakrishnan
     */
    public NavigateToTab(Browser browser, String strTabName) {
        super(browser);
        this.lnkTab = new BrowserElement("MenuTab",
                By.xpath("//span[contains(text(),'" + strTabName + "')]"));
        this.lnkTab.setTimeout(30000);
        this.strTabName = strTabName;

    }

    @Override
    protected boolean checkPreState() {
        boolean result = true;

        if (!new ElementExistsValidation(this.getBrowser(), this.lnkTab)
                .performWithStates()) {
            LOGGER.error("Could not find the " + strTabName + " Tab");
            result = false;
        }
        return result;

    }

    @Override
    protected boolean perform() {
        boolean result = true;
        if (!new LeftClickElementAction(this.getBrowser(), lnkTab)
                .performWithStates()) {
            LOGGER.error("Could NOT click on " + strTabName + " Tab");
            result = false;
        }

        return result;
    }

}
