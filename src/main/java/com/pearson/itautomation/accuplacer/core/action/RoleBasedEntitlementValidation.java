package com.pearson.itautomation.accuplacer.core.action;

import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * The Role Based Entitlement Validation will validate the permissions read from
 * database with permissions from input data excel sheet by comparing them for
 * all the roles and write the results to and output excel file
 * 
 * @author Solomon Lingala
 * @since
 * 
 */
public class RoleBasedEntitlementValidation {
    private Logger LOGGER = LoggerFactory
            .getLogger(RoleBasedEntitlementValidation.class);
    private Map<Integer, String> dbMap;
    private Map<Integer, String> xlMap;
    private Map<Integer, String> resultMap;
    private final Map<String, Integer> trackingMap = new HashMap<>();
    private Workbook workbook;

    /**
     * Read the data from database for that role ID
     * 
     * @param con
     *            The database connection
     * @param roleId
     *            The role id the user is assigned to.
     */
    public final void readDataFromDB(final Connection con, final String roleId) {

        dbMap = new HashMap<>();
        String userId = "";
        String strInitQuery = "SELECT user_id FROM user_master WHERE role_id = "
                + roleId + " and  delete_status='Y' and rownum <=1";
        try (Statement stmt = con.createStatement();
                ResultSet rs = stmt.executeQuery(strInitQuery);) {
            if (!rs.next()) {
                LOGGER.info("No records were found for the roleId: " + roleId
                        + " in the database");

            } else {
                do {
                    // Get data from the current row and use it
                    userId = rs.getString("user_id");
                } while (rs.next());
                String resultString = "";
                String strQuery = "select fn_get_acl(" + userId
                        + ") json from dual";
                try (Statement stmt1 = con.createStatement();
                        ResultSet rs1 = stmt1.executeQuery(strQuery);) {

                    while (rs1.next()) {
                        resultString = rs1.getString("json");
                    }
                } catch (SQLException e) {
                    LOGGER.error("Exception occured while executing the query",
                            e.getCause());
                }
                JSONParser parser = new JSONParser();
                JSONObject modObj = null;
                boolean allDone = false;

                try {
                    modObj = (JSONObject) parser.parse(resultString);
                } catch (ParseException e) {
                    allDone = true;
                    LOGGER.error("Exception occured while parsing JSON object",
                            e.getCause());
                }

                int count = 1;
                while (!allDone) {
                    String key = String.valueOf(count);
                    String value = (String) modObj.get(key);
                    if (value != null) {
                        dbMap.put(Integer.valueOf(key), value);
                    } else {
                        allDone = true;
                    }
                    ++count;
                }
            }
        } catch (SQLException e) {
            LOGGER.error("Exception occured while executing the query",
                    e.getCause());
        }
    }

    /**
     * Read the data from input spreadsheet for that roleId.
     * 
     * @param roleId
     *            The role id the user is assigned to.
     */
    public final void readDataFromXlFile(final String roleId) {
        xlMap = new HashMap<>();
        Sheet sheet = workbook.getSheetAt(Integer.parseInt(roleId) - 1);
        // Iterate through each rows one by one
        Iterator<Row> rowIterator = sheet.iterator();
        // skip header
        rowIterator.next();
        while (rowIterator.hasNext()) {
            Row row = rowIterator.next();
            // For each row, iterate through relevant columns
            Iterator<Cell> cellIterator = row.cellIterator();
            String key = readCell(cellIterator.next());
            String value = readCell(cellIterator.next());
            xlMap.put(Integer.valueOf(key), value);
        }
    }

    /**
     * Compare the data fetched from database and spreadsheet with each other.
     * 
     * @return The result map after comparing the two data sources
     */
    public final Map<Integer, String> comparison(final Connection con,
            final String roleId) {
        int counter = 0;
        resultMap = new HashMap<>();
        // if (!dbMap.equals(xlMap)) {
        for (Map.Entry<Integer, String> e : xlMap.entrySet()) {
            Integer key = e.getKey();
            String expected = e.getValue();
            String actual = dbMap.get(key);
            String msg = "";
            if (actual == null) {
                msg = "No data found for the user.";
            } else {
                if (!expected.equals(actual)) {
                    msg = "Value mismatch. actual = '" + actual
                            + "' expected = '" + expected + "'";
                    counter++;
                    trackingMap.put(getRoleName(con, roleId), counter);
                } else {
                    msg = "Value matched. actual = '" + actual
                            + "' expected = '" + expected + "'";
                }
            }

            resultMap.put(key, msg);
        }
        for (Map.Entry<Integer, String> e : dbMap.entrySet()) {
            Integer key = e.getKey();
            if (!xlMap.containsKey(key)) {
                resultMap.put(key, "The function id " + key
                        + " is missing in excel file.");
            }
        }

        // }
        return resultMap;
    }

    private String getRoleName(final Connection con, final String roleId) {
        String roleName = "";
        String strInitQuery = "SELECT role_name FROM role_master WHERE role_id = "
                + roleId;
        try (Statement stmt = con.createStatement();
                ResultSet rs = stmt.executeQuery(strInitQuery)) {
            if (rs.next()) {
                roleName = rs.getString("role_name");
            } else {
                LOGGER.info("No records were found for the roleId: " + roleId
                        + " in the database");
            }
        } catch (SQLException e) {
            LOGGER.error("Exception occured while executing the query",
                    e.getCause());
        }
        return roleName;
    }

    /**
     * @return the trackingMap
     */
    public final Map<String, Integer> getTrackingMap() {
        return trackingMap;
    }

    /**
     * Write the results of compared data to output results Excel file
     * 
     * @param out
     *            The output stream of the file
     * @param i
     *            The Sheet index based on roleId
     */
    public final void writeResultstoXL(final FileOutputStream out, final int i) {
        Sheet s = workbook.getSheetAt(i);

        Iterator<Row> iterator = s.iterator();
        // skip the header
        iterator.next();
        int rowCount = 1;
        while (iterator.hasNext()) {
            Row r = iterator.next();
            rowCount++;
            Cell cell = r.getCell(0);
            cell.setCellType(Cell.CELL_TYPE_STRING);
            Integer key = Integer.parseInt(cell.getStringCellValue());
            if (resultMap.containsKey(key)) {
                String value = resultMap.get(key);
                resultMap.remove(key);
                Cell cell1 = r.createCell(4);
                cell1.setCellValue(key);
                Cell cell2 = r.createCell(5);
                if (value.startsWith("Value matched.")) {
                    cell2.setCellValue("false");
                } else {
                    cell2.setCellValue("true");
                    Cell cell3 = r.createCell(6);
                    cell3.setCellValue(value);
                }
            }
        }

        for (Integer key : resultMap.keySet()) {
            String value = resultMap.get(key);
            Row r = s.createRow(rowCount);
            rowCount++;
            Cell cell1 = r.createCell(4);
            cell1.setCellValue(key);
            Cell cell2 = r.createCell(5);
            if (value.startsWith("Value matched.")) {
                cell2.setCellValue("false");
            } else {
                cell2.setCellValue("true");
                Cell cell3 = r.createCell(6);
                cell3.setCellValue(value);
            }

        }

    }

    /**
     * @return workbook of the active spreadsheet
     */
    public final Workbook getWorkbook() {
        return workbook;
    }

    /**
     * 
     * @param cell
     *            in sheet
     * @return String value for the active cell
     */
    private String readCell(Cell cell) {
        cell.setCellType(Cell.CELL_TYPE_STRING);
        return cell.getStringCellValue();
    }

    /**
     * Load the input file that holds the role based entitlements.
     * 
     * @param fileName
     *            Input data file that needs to be loaded
     */
    public void loadXlFile(final String fileName) {
        try (InputStream file = RoleBasedEntitlementValidation.class
                .getResourceAsStream(fileName)) {
            workbook = new XSSFWorkbook(file);
        } catch (IOException e) {
            LOGGER.error("Exception occured while loading input data file",
                    e.getCause());
        }
    }

}
