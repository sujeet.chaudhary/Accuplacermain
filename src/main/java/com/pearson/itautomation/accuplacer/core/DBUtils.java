package com.pearson.itautomation.accuplacer.core;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.HashMap;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class DBUtils {
    private static final Logger LOGGER = LoggerFactory.getLogger(DBUtils.class);
    private static final int LAST_RECORD = 999;
    private final Connection con;

    /**
     * This constructor will set the connection to be used
     * 
     * @param con
     *            Connection String
     */
    public DBUtils(final Connection con) {
        this.con = con;
    }

    /**
     * This method will return a single String result for the input <i>query</i>
     * and <i>column Name</i>
     * 
     * 
     * @param query
     *            complete SQL query without formats
     * @param column
     *            value from a particular column to be returned
     * 
     * @author Deepak Radhakrishnan
     * @return
     */
    public String executeQueryforOneField(String query, String column) {
        ResultSet rset = null;
        String lTemp = null;
        Statement stmt = null;

        try {
            stmt = con.createStatement();
            rset = stmt.executeQuery(query);
            if (!rset.next()) {
                LOGGER.error("No records were found ");
            } else {
                do {
                    lTemp = rset.getString(column);
                    if (lTemp != null) {
                        lTemp = lTemp.trim();
                    }
                } while (rset.next());

            }
        } catch (SQLException e) {
            LOGGER.error("Exception occured while executing the query", e);
        } finally {

            closeStatements(stmt, rset);
        }
        return lTemp;

    }

    /**
     * This method will return multidimensional array result for the input
     * <i>query</i> and <i>column Name</i>
     * 
     * @param con
     *            connection object
     * @param query
     *            complete SQL query without formats
     * 
     * @return multidimentional array for the result
     * @author Deepak Radhakrishnan
     */
    public String[][] runAndReturnMultiDimentionalArray(String query) {
        String[][] rValue = null;
        ResultSet rset = null;
        Statement stmt = null;
        int rowCount = 0;
        int nCol = 0;
        HashMap<Integer, String[]> hmap = new HashMap<Integer, String[]>();

        try {

            stmt = con.createStatement();
            LOGGER.info("Executing Query : " + query);
            rset = stmt.executeQuery(query);
            if (!rset.next()) {
                LOGGER.warn(String.format("No records were found"));
            } else {
                do {
                    // number of rows
                    nCol = rset.getMetaData().getColumnCount();
                    rowCount++;
                    // go to the last record to parse again in the subsequent
                    // loop
                    String[] tempRow = new String[nCol];
                    for (int i = 0; i < nCol; i++) {
                        tempRow[i] = rset.getString(i + 1);
                        if (tempRow[i] != null) {
                            tempRow[i] = tempRow[i].trim();
                        }
                    }
                    hmap.put(rowCount, tempRow);

                } while (rset.next());
                rValue = new String[rowCount][nCol];

                for (int i = 1; i <= hmap.size(); i++) {
                    if (hmap.containsKey((Integer) i)) {
                        rValue[i - 1] = hmap.get(i);

                    }
                }
            }

        } catch (SQLException e) {
            LOGGER.error("Exception occured while executing the query" + query,
                    e);
        } finally {
            closeStatements(stmt, rset);
        }

        return rValue;
    }

    /**
     * 
     * This Method will return the Nth Record
     * 
     * @param query
     *            complete SQL query without formats
     * @param n
     *            position of record to be returned (0 for first record , 999
     *            for the last record)
     * @return nth row
     * @author Deepak Radhakrishnan
     */
    public String[] runAndReturnNTHRecord(String query, int n) {
        String[][] rValue = runAndReturnMultiDimentionalArray(query);
        if (n == LAST_RECORD) {
            return rValue[rValue.length - 1];
        } else if (n <= 0) {
            throw new IllegalArgumentException(
                    "Row should br greate than zero ,actual parameter is : "
                            + n);
        } else if (rValue != null) {
            return rValue[n - 1];

        } else {
            LOGGER.warn("Row number '" + n + "' is NULL present in the DB");
            return null;
        }
    }

    /**
     * 
     * @param query
     *            Query to be executed
     * @param n
     *            column position(starts with n>=0)
     * @return nth column of the Multi dimentional Query output
     */
    public String[] runAndReturnNthColumn(String query, int n) {
        String[][] rValue = runAndReturnMultiDimentionalArray(query);
        String[] column = new String[rValue.length];
        for (int i = 0; i < rValue.length; i++) {
            column[i] = rValue[i][n];
        }
        return column;
    }

    /**
     * This method is to be used in the finally block to close an active <br>
     * <b>1.Statement</b><br>
     * <b>2.Result Set</b> <br>
     * 
     * @param stmt
     *            active SQL statement
     * @param rset
     *            active SQL Resultset
     * 
     * 
     * @author Deepak Radhakrishnan
     */

    public void closeStatements(Statement stmt, ResultSet rset) {
        try {
            if (stmt != null && !con.isClosed()) {
                stmt.close();
            }
            if (rset != null && !con.isClosed()) {
                rset.close();
            }
        } catch (SQLException e) {
            LOGGER.error("Exception occured while closing the statement", e);
        }
    }
}
