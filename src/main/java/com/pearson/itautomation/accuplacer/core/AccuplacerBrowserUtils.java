package com.pearson.itautomation.accuplacer.core;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.support.ui.WebDriverWait;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.pearson.itautomation.bca.Browser;
import com.pearson.itautomation.bca.WaitForAjaxPredicate;
import com.pearson.itautomation.bca.WaitForJqueryPredicate;

public class AccuplacerBrowserUtils {

    private static final Logger LOGGER = LoggerFactory
            .getLogger(AccuplacerBrowserUtils.class);
    private static final long PAGE_SOURCE_POLL_TIME = 100;

    private AccuplacerBrowserUtils() {
    }

    /**
     * This Method will find the String on the Page Source w
     * 
     * @param browser
     * @param charSequence
     * @return true if char sequence is found
     * 
     * @author Deepak Radhakrishnan
     */
    public static boolean checkPageSourceForString(Browser browser,
            String charSequence) {
        int pageSourcePollCounter = 0;
        boolean result = false;
        do {
            pageSourcePollCounter++;
            if (browser.getPageSource().contains(charSequence)) {
                result = true;
                break;
            }

            else {
                try {
                    Thread.sleep(PAGE_SOURCE_POLL_TIME);
                } catch (InterruptedException e) {
                    LOGGER.info(
                            "Error occured in page source check wait block", e);
                }
            }
        } while (pageSourcePollCounter < 50);

        return result;

    }

    public static void performWaits(Browser browser) {
        WebDriverWait waitForAjax = new WebDriverWait(browser.getWebDriver(),
                60);
        waitForAjax.until(new WaitForAjaxPredicate());

        WebDriverWait waitForJQuery = new WebDriverWait(browser.getWebDriver(),
                60);
        waitForJQuery.until(new WaitForJqueryPredicate());

        WebDriverWait waitForPageLoad = new WebDriverWait(
                browser.getWebDriver(), 60);
        waitForPageLoad.until(new WaitForPageLoadPredicate());
        browser.getWebDriver().manage().timeouts()
                .pageLoadTimeout(90, TimeUnit.SECONDS);
    }

    public static void performAccuplacerPageLoadWaitBlock(Browser browser,
            final int time) {

        WebDriverWait waitForPageLoad = new WebDriverWait(
                browser.getWebDriver(), time);
        waitForPageLoad.until(new WaitForPageLoadPredicate());

    }

    /**
     * Hard wait for specified Duration
     * 
     * @param time
     */
    public static void WaitInMilliSeconds(long time) {
        try {
            Thread.sleep(time);
        } catch (InterruptedException e) {
            LOGGER.error("Exception Occured while waiting", e);
        }
    }

}
