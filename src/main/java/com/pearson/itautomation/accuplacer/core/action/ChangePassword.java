package com.pearson.itautomation.accuplacer.core.action;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.pearson.itautomation.accuplacer.core.Buttons;
import com.pearson.itautomation.bca.Browser;
import com.pearson.itautomation.bca.BrowserAction;
import com.pearson.itautomation.bca.BrowserElement;
import com.pearson.itautomation.bca.interaction.LeftClickElementAction;
import com.pearson.itautomation.bca.interaction.SendTextToElementAction;
import com.pearson.itautomation.bca.validation.ElementExistsValidation;

/**
 * This Class will change password of a User(any 1/16 roles)<br>
 * <b>checkPrestate</b> - N/A <br>
 * <b>checkPostate</b> - N/A <br>
 * <b>perform</b> - This method will return true once the password has been
 * changed successfully
 * 
 * @author Deepak Radhakrishnan
 * 
 */
public class ChangePassword extends BrowserAction {
    private static final Logger LOGGER = LoggerFactory
            .getLogger(ChangePassword.class);

    private static final String OR_AREA = "OR_Global";
    private static final String OR_USER_DROPDOWN = "UserDropdown";
    private static final String OR_CHANGE_PASSWORD = "ChangePasswordLink";
    private static final String OR_OLD_PASSWORD = "OldPasswordTextbox";
    private static final String OR_NEW_PASSWORD = "NewPasswordTextBox";
    private static final String OR_CONFIRM_NEW_PASSWORD = "ConfirmNewPasswordTextBox";

    private final BrowserElement lnkUserDrpDown;
    private final BrowserElement lnkChangePassword;
    private final BrowserElement btnReset;
    private final BrowserElement btnSubmit;
    private final BrowserElement txtOldPassword;
    private final BrowserElement txtNewPassword;
    private final BrowserElement txtConfimNewPassword;
    private final String strOldPassword;
    private final String strNewPassword;

    /**
     * This constructor will initialize the browser elements and the constant
     * variables
     * 
     * @param browser
     * @param strOldPassword
     *            Existing Password of the user
     * @param strNewPassword
     *            New Password to be set
     */
    public ChangePassword(Browser browser, String strOldPassword,
            String strNewPassword) {
        super(browser);
        this.lnkUserDrpDown = browser.getBrowserElement(OR_AREA,
                OR_USER_DROPDOWN);
        this.lnkChangePassword = browser.getBrowserElement(OR_AREA,
                OR_CHANGE_PASSWORD);
        this.txtOldPassword = browser.getBrowserElement(OR_AREA,
                OR_OLD_PASSWORD);
        this.txtNewPassword = browser.getBrowserElement(OR_AREA,
                OR_NEW_PASSWORD);
        this.txtConfimNewPassword = browser.getBrowserElement(OR_AREA,
                OR_CONFIRM_NEW_PASSWORD);
        this.btnReset = Buttons.getButtonType1("Reset");
        this.btnSubmit = Buttons.getButtonType1("Submit");
        this.strOldPassword = strOldPassword;
        this.strNewPassword = strNewPassword;

    }

    @Override
    protected boolean perform() {
        boolean result = true;
        if (!new LeftClickElementAction(this.getBrowser(), this.lnkUserDrpDown)
                .performWithStates()) {
            LOGGER.error("Could not click on User Dropdown");
            return false;
        }

        if (!new LeftClickOnModalPopupButton(this.getBrowser(),
                this.lnkChangePassword).performWithStates()) {
            LOGGER.error("Could not click on Change Password Link");
            return false;
        }
        if (!new ElementExistsValidation(this.getBrowser(), this.btnReset)
                .performWithStates()) {
            LOGGER.error("Could not Find Reset Button");
            result = false;
        }
        try {
            Thread.sleep(1000);
        } catch (InterruptedException e) {
            LOGGER.info("Error occured while waitng for the page load", e);
        }
        if (!new SendTextToElementAction(this.getBrowser(),
                this.txtOldPassword, this.strOldPassword).performWithStates()) {
            LOGGER.error("Could not type OLD user Password");
            result = false;
        }
        if (!new SendTextToElementAction(this.getBrowser(),
                this.txtNewPassword, this.strNewPassword).performWithStates()) {
            LOGGER.error("Could not type New user Password");
            result = false;
        }
        if (!new SendTextToElementAction(this.getBrowser(),
                this.txtConfimNewPassword, this.strNewPassword)
                .performWithStates()) {
            LOGGER.error("Could not type Confirm New user Password");
            result = false;
        }
        if (!new LeftClickElementAction(this.getBrowser(), this.btnSubmit)
                .performWithStates()) {
            LOGGER.error("Could not click ClickOn Submit");
            result = false;
        }
        return result;
    }

}
