package com.pearson.itautomation.accuplacer.core.action;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.pearson.itautomation.bca.Browser;
import com.pearson.itautomation.bca.BrowserAction;
import com.pearson.itautomation.bca.BrowserElement;
import com.pearson.itautomation.bca.interaction.ClearTextElementAction;
import com.pearson.itautomation.bca.interaction.LeftClickElementAction;
import com.pearson.itautomation.bca.interaction.SendTextToElementAction;
import com.pearson.itautomation.bca.validation.ElementExistsValidation;

/**
 * This Class will Login to the Accuplacer application based on the Username and
 * Password parameter <br>
 * <b>checkPrestate</b> - N/A<br>
 * <b>checkPostate</b> - Will return true if the logout link is available <br>
 * <b>perform</b> - This method will return true if Modal Button is clicked
 * successfully
 * 
 * @author Deepak Radhakrishnan
 * 
 */
public class LoginAction extends BrowserAction {
    private static final Logger LOGGER = LoggerFactory
            .getLogger(LoginAction.class);
    private static final String OR_AREA = "OR_Login";
    private static final String OR_LOGINFIELD = "LoginField";
    private static final String OR_PASSWORDFIELD = "PasswordField";
    private static final String OR_LOGIN_BUTTON = "LoginButton";
    private static final String OR_LOGOUT_LINK = "Logout";
    private static final String OR_WHATS_NEW_CLOSE = "WhatsNewClose";
    private static final String OR_WHATS_NEW_VISIBLIE = "WhatsNewVisible";
    private final BrowserElement txtLoginField;
    private final BrowserElement txtPasswordField;
    private final BrowserElement btnLogin;
    private final BrowserElement lnkLogout;
    private final BrowserElement btnWhatsNewClose;
    private final BrowserElement btnWhatsNewVisible;

    private final String userName;
    private final String password;

    /**
     * This constructor will initialize the browser elements and the constant
     * variables
     * 
     * @param browser
     * @param userName
     * @param password
     * 
     * @author Deepak Radhakrishnan
     */
    public LoginAction(final Browser browser, String userName, String password) {
        super(browser);
        this.userName = userName;
        this.password = password;
        this.txtLoginField = browser.getBrowserElement(OR_AREA, OR_LOGINFIELD);
        this.txtPasswordField = browser.getBrowserElement(OR_AREA,
                OR_PASSWORDFIELD);
        this.btnLogin = browser.getBrowserElement(OR_AREA, OR_LOGIN_BUTTON);
        this.lnkLogout = browser.getBrowserElement(OR_AREA, OR_LOGOUT_LINK);
        this.btnWhatsNewClose = browser.getBrowserElement(OR_AREA,
                OR_WHATS_NEW_CLOSE);
        this.btnWhatsNewVisible = browser.getBrowserElement(OR_AREA,
                OR_WHATS_NEW_VISIBLIE);
    }

    @Override
    protected boolean perform() {
        boolean result = true;

        if (!new ClearTextElementAction(this.getBrowser(), this.txtLoginField)
                .performWithStates()) {
            LOGGER.error("LoginAction failed to Clear the login field");
            result = false;
        }
        if (!new SendTextToElementAction(this.getBrowser(), this.txtLoginField,
                this.userName).performWithStates()) {
            LOGGER.error("LoginAction failed to populate the login field");
            result = false;
        }
        if (!new ClearTextElementAction(this.getBrowser(),
                this.txtPasswordField).performWithStates()) {
            LOGGER.error("LoginAction failed to Clear the Password field");
            result = false;
        }
        if (!new SendTextToElementAction(this.getBrowser(),
                this.txtPasswordField, this.password).performWithStates()) {
            LOGGER.error("LoginAction failed to populate the Password field");
            result = false;
        }

        if (!new LeftClickElementAction(this.getBrowser(), btnLogin)
                .performWithStates()) {
            LOGGER.error("LoginAction failed ,unable to click on Submit Button");
            result = false;
        }

        return result;
    }

    @Override
    protected boolean checkPostState() {
        boolean result = true;
        boolean whatsNewWindowFlag = true;
        this.lnkLogout.setTimeout(30000);
        if (!new ElementExistsValidation(this.getBrowser(), lnkLogout)
                .performWithStates()) {
            LOGGER.error("LoginAction Failed , unalble to locate Logout Link");
            result = false;
        }
        whatsNewWindowFlag = new ElementExistsValidation(this.getBrowser(),
                btnWhatsNewVisible).performWithStates();

        if (whatsNewWindowFlag) {
            LOGGER.info("Whats New Window Found");
            if (!new LeftClickOnModalPopupButton(this.getBrowser(),
                    btnWhatsNewClose).performWithStates()) {
                LOGGER.error("LoginAction failed ,unable to click on Close Button on the WHatsNew Window");
                result = false;
            }
        }
        return result;
    }
}
