package com.pearson.itautomation.accuplacer.core;

public class HTMLUtils {

    private static final String BOLD_TAG_START = "<b>";
    private static final String BOLD_TAG_END = "</b>";
    private static final String LINEBREAK = "</b>";

    private static final String HTML_HEADER_DYNAMIC_HEADER = "<table border=1><tr><td style=\"max-width:400px;word-wrap: break-word\">"
            + getBoldTagStart()
            + "Test_Alias"
            + getBoldTagEnd()
            + "</td><td style=\"max-width:400px;word-wrap: break-word\">"
            + getBoldTagStart()
            + "Dynamic Vector"
            + getBoldTagEnd()
            + "</td><td style=\"max-width:400px;word-wrap: break-word\">"
            + getBoldTagStart()
            + "Occurrence"
            + getBoldTagEnd()
            + "</td><td style=\"max-width:400px;word-wrap: break-word\">"
            + getBoldTagStart()
            + "test_Session_Detail_ID"
            + getBoldTagEnd()
            + "</td></tr>";
    private static final String HTML_DYNAMIC_HEADER_ROW = "<tr><td style=\"max-width:400px;word-wrap: break-word\">"
            + getBoldTagStart()
            + "%s"
            + getBoldTagEnd()
            + "</td><td style=\"max-width:400px;word-wrap: break-word\">%s</td><td style=\"max-width:400px;word-wrap: break-word\">%s</td><td style=\"max-width:400px;word-wrap: break-word\">%s</td></tr>";
    private static final String HTML_SET_ID_HEADER = "<table border=1><tr><td style=\"max-width:400px;word-wrap: break-word\">"
            + getBoldTagStart()
            + "SetID"
            + getBoldTagEnd()
            + "</td><td style=\"max-width:400px;word-wrap: break-word\">"
            + getBoldTagStart() + "Count" + getBoldTagEnd() + "</td></tr>";
    private static final String HTML_SET_ID_ROW = "<tr><td style=\"max-width:400px;word-wrap: break-word\">%s</td><td style=\"max-width:400px;word-wrap: break-word\">%d</td></tr>";
    private static final String HTML_TABLE_END = "</table>";

    private HTMLUtils() {
        // private constructor
    }

    public static String getHtmlSetIdRow() {
        return HTML_SET_ID_ROW;
    }

    public static String getHtmlHeaderDynamicHeader() {
        return HTML_HEADER_DYNAMIC_HEADER;
    }

    public static String getHtmlDynamicHeaderRow() {
        return HTML_DYNAMIC_HEADER_ROW;
    }

    public static String getHtmlSetIdHeader() {
        return HTML_SET_ID_HEADER;
    }

    public static String getHtmlTableEnd() {
        return HTML_TABLE_END;
    }

    public static String getBoldTagStart() {
        return BOLD_TAG_START;
    }

    public static String getBoldTagEnd() {
        return BOLD_TAG_END;
    }

    public static String getLinebreak() {
        return LINEBREAK;
    }
}
