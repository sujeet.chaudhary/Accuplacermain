package com.pearson.itautomation.accuplacer.core.action;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.pearson.itautomation.bca.Browser;
import com.pearson.itautomation.bca.BrowserAction;
import com.pearson.itautomation.bca.BrowserElement;
import com.pearson.itautomation.bca.interaction.LeftClickElementAction;
import com.pearson.itautomation.bca.validation.ElementExistsValidation;

/**
 * This Class will click on a Modal pop-up button <br>
 * <b>checkPrestate</b> - N/A<br>
 * <b>checkPostate</b> - N/A <br>
 * <b>perform</b> - This method will return true if Modal Button is click
 * successfully
 * 
 * @author Deepak Radhakrishnan
 * 
 */

public class LeftClickOnModalPopupButton extends BrowserAction {

    private static final Logger LOGGER = LoggerFactory
            .getLogger(LeftClickOnModalPopupButton.class);
    private static final long SLEEP_FOR_MODAL_POPUP_TRANSITION = 600;
    private final BrowserElement btnElement;

    /**
     * * This constructor will initialize the browser elements and the constant
     * variables
     * 
     * @author Deepak Radhakrishnan
     * 
     */
    public LeftClickOnModalPopupButton(Browser browser,
            BrowserElement btnElement) {
        super(browser);
        this.btnElement = btnElement;
    }

    @Override
    protected boolean perform() {
        boolean result = true;
        if (!new ElementExistsValidation(this.getBrowser(), this.btnElement)
                .performWithStates()) {
            LOGGER.error("Unable to find the Modal button to click on");
            result = false;
        }
        try {
            Thread.sleep(SLEEP_FOR_MODAL_POPUP_TRANSITION);
        } catch (InterruptedException ex) {
            LOGGER.error("Error while waiting for the modal window transition");
        }
        if (!new LeftClickElementAction(this.getBrowser(), this.btnElement)
                .performWithStates()) {
            LOGGER.error("Unable to click on the Modal Button Button");
            result = false;
        }

        return result;
    }

}
