package com.pearson.itautomation.accuplacer.core.action;

import org.openqa.selenium.By;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.pearson.itautomation.accuplacer.core.Buttons;
import com.pearson.itautomation.bca.Browser;
import com.pearson.itautomation.bca.BrowserAction;
import com.pearson.itautomation.bca.BrowserElement;
import com.pearson.itautomation.bca.interaction.ClearTextElementAction;
import com.pearson.itautomation.bca.interaction.LeftClickElementAction;
import com.pearson.itautomation.bca.interaction.SendTextToElementAction;
import com.pearson.itautomation.bca.validation.ElementExistsValidation;

/**
 * This Class will Seach for a desired value on the search page<br>
 * <b>checkPrestate</b> - This Method will return true if the search box is
 * found <br>
 * <b>checkPostate</b> - N/A <br>
 * <b>perform</b> - This method will return true if The Search is successful
 * 
 * @author Deepak Radhakrishnan
 * 
 */
public class GenericSearchAction extends BrowserAction {

    private static final Logger LOGGER = LoggerFactory
            .getLogger(GenericSearchAction.class);

    private final BrowserElement txtSearchBox;
    private final BrowserElement btnSearch;

    private final String strValuetoSearch;
    private final String strLabelOfInputbox;

    /**
     * This constructor will initialize the mandatory fields and the
     * BrowserElements from this class
     * 
     * @param browser
     * @param strValuetoSearch
     *            The Text value to be searched
     * @param strLabelOfInputbox
     *            THe Label of the input Box
     * 
     * @author Deepak Radhakrishnan
     */
    public GenericSearchAction(final Browser browser, String strValuetoSearch,
            String strLabelOfInputbox) {
        super(browser);

        this.strValuetoSearch = strValuetoSearch;
        this.strLabelOfInputbox = strLabelOfInputbox;

        this.txtSearchBox = new BrowserElement(strValuetoSearch,
                By.xpath("(//label[contains(text(),'" + this.strLabelOfInputbox
                        + "')]/following-sibling::input[1])[1]"));
        this.btnSearch = Buttons.getButtonType1("Search");
    }

    @Override
    protected boolean checkPreState() {
        boolean result = true;

        if (!new ElementExistsValidation(getBrowser(), this.txtSearchBox)
                .performWithStates()) {
            LOGGER.error("Not able to load " + strLabelOfInputbox
                    + " search Name field");
            result = false;
        }

        return result;
    }

    @Override
    protected boolean perform() {
        boolean result = true;
        if (!new ClearTextElementAction(getBrowser(), this.txtSearchBox)
                .performWithStates()) {
            LOGGER.error("Not able to clear the textbox");
            result = false;
        }
        if (!new SendTextToElementAction(getBrowser(), this.txtSearchBox,
                this.strValuetoSearch).performWithStates()) {
            LOGGER.error("Not able to send text to " + strLabelOfInputbox
                    + " field");
            result = false;
        }

        if (!new LeftClickElementAction(getBrowser(), this.btnSearch)
                .performWithStates()) {
            LOGGER.error("Not able to click on Search Button");
            result = false;
        }
        return result;
    }
}
