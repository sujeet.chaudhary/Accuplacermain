package com.pearson.itautomation.accuplacer.core;

import java.util.List;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.pearson.itautomation.bca.Browser;
import com.pearson.itautomation.bca.BrowserElement;
import com.pearson.itautomation.bca.WaitForAjaxPredicate;
import com.pearson.itautomation.bca.WaitForJqueryPredicate;

/**
 * This Class has the override the wait methods from the bca libraries. The
 * Instance of this class will be intiated from the Test Cases
 * 
 * 
 * @author Deepak Radhakrishnan
 * 
 */
public class AccuplacerBrowser extends Browser {
    private long timeoutsForPredicate;

    public long getTimeoutsForPredicate() {
        return timeoutsForPredicate;
    }

    public void setTimeoutsForPredicate(long timeoutsForPredicate) {
        this.timeoutsForPredicate = timeoutsForPredicate;
    }

    public AccuplacerBrowser(WebDriver webDriver) {
        super(webDriver);
        setTimeoutsForPredicate(60);
        getWebDriver().manage().timeouts()
                .pageLoadTimeout(90, TimeUnit.SECONDS);
    }

    /*
     * (non-Javadoc)
     * 
     * @see com.pearson.itautomation.bca.Browser#getElementWithWait(com.pearson.
     * itautomation.bca.BrowserElement)
     */
    @Override
    public WebElement getElementWithWait(BrowserElement browserElement) {
        performWaits();

        return super.getElementWithWait(browserElement);
    }

    /*
     * (non-Javadoc)
     * 
     * @see
     * com.pearson.itautomation.bca.Browser#getElementsWithWait(com.pearson.
     * itautomation.bca.BrowserElement)
     */
    @Override
    public List<WebElement> getElementsWithWait(BrowserElement browserElement) {
        performWaits();

        return super.getElementsWithWait(browserElement);
    }

    public void performWaits() {
        WebDriverWait waitForAjax = new WebDriverWait(getWebDriver(),
                timeoutsForPredicate);
        waitForAjax.until(new WaitForAjaxPredicate());

        WebDriverWait waitForJQuery = new WebDriverWait(getWebDriver(),
                timeoutsForPredicate);
        waitForJQuery.until(new WaitForJqueryPredicate());

        WebDriverWait waitForPageLoad = new WebDriverWait(getWebDriver(),
                timeoutsForPredicate);
        waitForPageLoad.until(new WaitForPageLoadPredicate());
    }
}
