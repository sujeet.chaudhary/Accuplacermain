package com.pearson.itautomation.accuplacer.core.action;

import org.openqa.selenium.By;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.pearson.itautomation.bca.Browser;
import com.pearson.itautomation.bca.BrowserAction;
import com.pearson.itautomation.bca.BrowserElement;
import com.pearson.itautomation.bca.interaction.LeftClickElementAction;
import com.pearson.itautomation.bca.validation.ElementExistsValidation;

/**
 * This Class will navigate the user to a specific SubMenu through the MainMenu <br>
 * <b>checkPrestate</b> - This method will return true of the Main menu link is
 * present<br>
 * <b>checkPostate</b> - This method will return true if the validation element
 * on the subsequent page is found. The post state will not check anything if
 * the validation element is null. <br>
 * <b>perform</b> - This method will return true if the Main Menu and the Sub
 * Menu Link is clicked successfully
 * 
 * @author Deepak Radhakrishnan
 * 
 */
public class NavigateToMenu extends BrowserAction {
    private static final Logger LOGGER = LoggerFactory
            .getLogger(NavigateToMenu.class);

    private static final String OR_GLOBAL = "OR_Global";
    private static final String LEFT_MENU_ELEMENT = "LeftMenuTopElement";
    private static final String LEFT_MENU_ELEMENT_OPENED = "LeftMenuTopElementOpened";
    private static final String LEFT_SUB_MENU_ELEMENT_OPENED = "LeftMenuSubElementOpened";

    private final BrowserElement lnkTab;
    private final BrowserElement lnkTabOpened;
    private final BrowserElement lnkSubMenu;
    private final BrowserElement lnkSubMenuOpened;
    private BrowserElement lnkChildSubMenu;
    private final BrowserElement elementCheckPoint;
    private final String strTabName;
    private final String strSubMenuName;

    private String childSubmenu = null;

    /**
     * This constructor will initialize the browser elements and the constant
     * variables
     * 
     * @param browser
     * @param strTabName
     *            Left Panel Link Text value
     * @param strSubMenuName
     *            Link Text Value
     * @param navCheckPoint
     *            Checkpoint Element
     * 
     * @author Deepak Radhakrishnan
     */
    public NavigateToMenu(Browser browser, String strTabName,
            String strSubMenuName, BrowserElement navCheckPoint) {
        super(browser);
        this.lnkTab = browser.getBrowserElementWithFormat(OR_GLOBAL,
                LEFT_MENU_ELEMENT, strTabName);
        this.lnkTabOpened = browser.getBrowserElementWithFormat(OR_GLOBAL,
                LEFT_MENU_ELEMENT_OPENED, strTabName);
        this.lnkSubMenuOpened = browser.getBrowserElementWithFormat(OR_GLOBAL,
                LEFT_SUB_MENU_ELEMENT_OPENED, strSubMenuName);
        this.lnkSubMenu = new BrowserElement("SubMenuTab",
                By.partialLinkText(strSubMenuName));
        this.elementCheckPoint = navCheckPoint;
        this.strTabName = strTabName;
        this.strSubMenuName = strSubMenuName;
    }

    /**
     * Create a NavigateToMenu object that will use the Browser to navigate to a
     * given tab and submenu of that tab. No post state validation is performed
     * with this constructor.
     * 
     * @param browser
     *            The browser to use.
     * @param tabName
     *            The primary tab name from the left hand menu.
     * @param subMenuName
     *            The submenu which should exist as an item under the primary
     *            tab.
     */
    public NavigateToMenu(Browser browser, String tabName, String subMenuName) {
        this(browser, tabName, subMenuName, null);
    }

    public NavigateToMenu(String childSubmenu, Browser browser, String tabName,
            String subMenuName, BrowserElement navCheckPoint) {
        this(browser, tabName, subMenuName, navCheckPoint);
        this.childSubmenu = childSubmenu;
        this.lnkChildSubMenu = new BrowserElement("childSubMenuTab",
                By.linkText(childSubmenu));

    }

    @Override
    protected boolean checkPreState() {
        boolean result = true;

        if (!new ElementExistsValidation(this.getBrowser(), this.lnkTab)
                .performWithStates()) {
            LOGGER.error("Could not find the " + strTabName + " Tab");
            result = false;
        }
        return result;

    }

    @Override
    protected boolean perform() {
        boolean result = true;

        // if the link exists and is not opened, we should try to click on it.
        // we check to see if it exists first because if it exists, then the
        // check to see if it's opened should go fast.
        if (new ElementExistsValidation(getBrowser(), lnkTab)
                .performWithStates()) {
            if (!new ElementExistsValidation(getBrowser(), lnkTabOpened)
                    .performWithStates()) {
                if (!new LeftClickElementAction(this.getBrowser(), lnkTab)
                        .performWithStates()) {
                    LOGGER.error("Could NOT click on " + strTabName + " Tab");
                    result = false;
                }
            }
        } else {
            LOGGER.error("The primary tab give by " + lnkTab
                    + " does not exist and could not be clicked.");
            result = false;
        }

        if (childSubmenu != null) {
            if (new ElementExistsValidation(getBrowser(), lnkSubMenu)
                    .performWithStates()) {
                if (!new ElementExistsValidation(getBrowser(), lnkSubMenuOpened)
                        .performWithStates()) {
                    if (!new LeftClickElementAction(this.getBrowser(),
                            lnkSubMenu).performWithStates()) {
                        LOGGER.error("Could NOT click on " + strSubMenuName
                                + " Sub Menu");
                        result = false;
                    }
                    if (result
                            && !new LeftClickElementAction(this.getBrowser(),
                                    lnkChildSubMenu).performWithStates()) {
                        LOGGER.error("Could NOT click on " + lnkChildSubMenu
                                + "Child Link");
                        result = false;
                    }
                }
            } else {
                LOGGER.error("The primary tab give by " + lnkTab
                        + " does not exist and could not be clicked.");
                result = false;
            }
        }

        else if (result
                && !new LeftClickElementAction(this.getBrowser(), lnkSubMenu)
                        .performWithStates()) {
            LOGGER.error("Could NOT click on " + strSubMenuName + " Link");
            result = false;
        }

        return result;
    }

    @Override
    protected boolean checkPostState() {
        boolean result = true;
        if (elementCheckPoint != null
                && !new ElementExistsValidation(this.getBrowser(),
                        elementCheckPoint).performWithStates()) {
            LOGGER.error("Could NOT find the Checkpoint element");
            result = false;
        }
        return result;

    }

}
