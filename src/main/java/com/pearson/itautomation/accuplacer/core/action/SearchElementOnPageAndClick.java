package com.pearson.itautomation.accuplacer.core.action;

import java.util.List;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.pearson.itautomation.accuplacer.core.AccuplacerBrowserUtils;
import com.pearson.itautomation.bca.Browser;
import com.pearson.itautomation.bca.BrowserAction;
import com.pearson.itautomation.bca.BrowserElement;
import com.pearson.itautomation.bca.interaction.LeftClickElementAction;
import com.pearson.itautomation.bca.validation.ElementExistsValidation;

/**
 * This Class will Search for a desired entity(branchingprofile/institution/test
 * setting etc) and clicks the desired action(Delete/edit) <br>
 * <b>checkPrestate</b> - This method will return true if the records per page
 * text is displayed<br>
 * <b>checkPostate</b> - N/A <br>
 * <b>perform</b> - This method will return true if the desired button is
 * clicked for the corresponding record
 * 
 * @author Deepak Radhakrishnan
 * 
 */
public class SearchElementOnPageAndClick extends BrowserAction {
    private static final Logger LOGGER = LoggerFactory
            .getLogger(SearchElementOnPageAndClick.class);

    private static final long WAIT_FOR_SEARCH = 500;
    private static final int NUMBER_OF_ATTEMPTS_TO_SEARCH = 20;

    private final BrowserElement lblPagination;
    private final BrowserElement btnNext;
    private String strPaginationText;
    private int numberOfRecords;
    private final String strElementText;
    private final String strAction;
    private final BrowserElement btnYes;
    private final int indexOfElementToFind;
    private final String strTableRowString;
    private boolean labelPaginationFlag = false;

    /**
     * This constructor will initialize the browser elements and the constant
     * variables
     * 
     * @param browser
     * @param strElementText
     * @param strAction
     * @param strTableRowString
     * @param indexOfElementToFind
     * 
     * @author Deepak Radhakrishnan
     */
    public SearchElementOnPageAndClick(Browser browser, String strElementText,
            String strAction, String strTableRowString, int indexOfElementToFind) {
        super(browser);
        this.strElementText = strElementText;
        this.strAction = strAction;
        this.lblPagination = new BrowserElement("labelPagination1",
                By.xpath("//div[@id='ft_pagerecords']/span[3]"));
        this.strPaginationText = getBrowser().getElementWithWait(lblPagination)
                .getText();
        int counter = 0;
        LabelSearch: do {
            if ("".equalsIgnoreCase(strPaginationText)) {
                LOGGER.info("Pagination Text NOT Found , Waiting For Search to Complete");
                AccuplacerBrowserUtils.WaitInMilliSeconds(WAIT_FOR_SEARCH);
                this.strPaginationText = getBrowser().getElementWithWait(
                        lblPagination).getText();
                counter++;
            } else {
                labelPaginationFlag = true;
                break LabelSearch;

            }
        } while (counter < NUMBER_OF_ATTEMPTS_TO_SEARCH);
        if (!"".equalsIgnoreCase(strPaginationText)) {
            this.numberOfRecords = Integer.parseInt(strPaginationText);

        }
        this.btnNext = new BrowserElement("NextPage", By.linkText("Next"));
        this.btnYes = browser.getBrowserElement("OR_TestSetup",
                "DeleteBPRuleYesBtn");
        this.strTableRowString = strTableRowString;
        this.indexOfElementToFind = indexOfElementToFind;

    }

    @Override
    protected boolean checkPreState() {
        boolean result = true;
        if (!labelPaginationFlag) {
            LOGGER.error("Failed to find the Search the User");
            return false;
        }
        if (!new ElementExistsValidation(this.getBrowser(), this.lblPagination)
                .performWithStates()) {
            LOGGER.error("Failed find the prestate Element");
            result = false;
        }
        return result;
    }

    @Override
    protected boolean perform() {
        boolean result = false;
        boolean paginationFlag = true;
        // to track pagination
        int counter = 0;

        do {
            counter++;
            // Dynamic xpath validation for next page
            if (!new ElementExistsValidation(
                    this.getBrowser(),
                    new BrowserElement(
                            counter + "NewPage",
                            By.xpath("//div[@id='ft_pagerecords']/span[contains(text(),'"
                                    + (((counter - 1) * 10) + 1) + "')]")))
                    .performWithStates()) {
                LOGGER.error("Failed to find the Pagination Info");
            }
            getBrowser().getWebDriver().manage().timeouts()
                    .pageLoadTimeout(60, TimeUnit.SECONDS);
            if (getBrowser().getWebDriver().getPageSource()
                    .contains(strElementText)) {
                LOGGER.info("Element Found to click");
                paginationFlag = false;
                result = true;
                getBrowser().getWebDriver().manage().timeouts()
                        .pageLoadTimeout(60, TimeUnit.SECONDS);
                List<WebElement> list = getBrowser().getWebDriver()
                        .findElements(
                                By.xpath("(//tr[contains(@ng-repeat,'"
                                        + strTableRowString + "')])/td["
                                        + indexOfElementToFind + "]"));
                for (int i = 0; i < list.size(); i++) {
                    String strListItem = list.get(i).getText();
                    if (strListItem.equalsIgnoreCase(strElementText)) {
                        LOGGER.info("Element Found to " + strAction);
                        if (!new LeftClickElementAction(
                                this.getBrowser(),
                                new BrowserElement(
                                        "Element" + strAction,
                                        By.xpath("(//button[@data-original-title='"
                                                + strAction
                                                + "'])["
                                                + (i + 1)
                                                + "]"))).performWithStates()) {
                            LOGGER.error("Failed to perform click action on for the element on the page");
                            result = false;
                        }
                        if (getBrowser().getWebDriver().getPageSource()
                                .contains("Are you sure ")
                                && strAction.equalsIgnoreCase("Delete")) {
                            LOGGER.info("Deleting item");
                            if (!new LeftClickOnModalPopupButton(
                                    this.getBrowser(), this.btnYes)
                                    .performWithStates()) {
                                LOGGER.error("Unable to click on Yes Button");
                                result = false;
                            }
                        }
                        // since the element is found
                        break;
                    }

                }
            } else {

                if (!new LeftClickElementAction(this.getBrowser(), this.btnNext)
                        .performWithStates()) {
                    LOGGER.error("Failed to click on next paginattion Button");
                    result = false;
                }

            }

        } while ((paginationFlag && counter < numberOfRecords / 10));
        return result;
    }
}
