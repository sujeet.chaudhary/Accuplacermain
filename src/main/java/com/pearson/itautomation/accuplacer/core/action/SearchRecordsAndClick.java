package com.pearson.itautomation.accuplacer.core.action;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.NoSuchElementException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.pearson.itautomation.accuplacer.core.AccuplacerBrowserUtils;
import com.pearson.itautomation.bca.Browser;
import com.pearson.itautomation.bca.BrowserAction;
import com.pearson.itautomation.bca.BrowserElement;
import com.pearson.itautomation.bca.validation.ElementExistsValidation;

/**
 * This method will search the created record in searched result section
 * throughout the entire page.If record found, return 'true' otherwise return
 * 'false' with confirmation msg <b>checkPrestate</b> - checking for
 * preCondition <b>checkPostate</b> - N/A <br>
 * <b>perform</b> - This method will return true if the Records available on the
 * page other wise returns false
 * 
 * @author Sujeet Chaudhary
 * 
 */
public class SearchRecordsAndClick extends BrowserAction {

    private static final Logger LOGGER = LoggerFactory
            .getLogger(SearchRecordsAndClick.class);

    private final String Xpath;
    private final BrowserElement preCondition;

    int counter = 1;

    /**
     * This constructor will initialize the mandatory fields and the
     * BrowserElements from this class
     * 
     * @param browser
     * @param XpathOfElement
     *            The XPATH of the Element need to be search
     */
    public SearchRecordsAndClick(final Browser browser, String XpathOfElement) {

        super(browser);
        this.Xpath = XpathOfElement;
        this.preCondition = new BrowserElement("PreCondition",
                By.id("ft_pagerecords"));
        preCondition.setTimeout(10000);
    }

    @Override
    protected boolean checkPreState() {
        if (!new ElementExistsValidation(this.getBrowser(), this.preCondition)
                .performWithStates()) {
            LOGGER.error("Unable to Load Page");
            return false;
        }

        return true;
    }

    @Override
    protected boolean perform() {
        boolean result = false;
        String s1 = this.getBrowser().getWebDriver()
                .findElement(By.xpath("//div[@id='ft_pagerecords']/span[2]"))
                .getText();
        String s2 = this.getBrowser().getWebDriver()
                .findElement(By.xpath("//div[@id='ft_pagerecords']/span[3]"))
                .getText();
        int noOfElementOnPage = Integer.parseInt(s1);
        int noOfElement = Integer.parseInt(s2);
        int noOFPages = noOfElement / noOfElementOnPage;
        while (!result) {
            try {

                this.getBrowser().getWebDriver().findElement(By.xpath(Xpath))
                        .isEnabled();
                result = true;

            } catch (NoSuchElementException ex) {

                if (counter <= noOFPages) {
                    long waitingTime = 500;
                    boolean flag = true;
                    while (!flag && waitingTime <= 20000) {
                        try {
                            this.getBrowser().getWebDriver()
                                    .findElement(By.linkText("Next"))
                                    .isEnabled();
                            flag = true;
                        } catch (NoSuchElementException e) {
                            AccuplacerBrowserUtils
                                    .WaitInMilliSeconds(waitingTime);
                            waitingTime = waitingTime + 500;
                            flag = false;
                        }
                    }
                    this.getBrowser().getWebDriver()
                            .findElement(By.linkText("Next")).click();
                    this.getBrowser().getWebDriver().manage().timeouts()
                            .implicitlyWait(5, TimeUnit.SECONDS);
                    counter++;

                    result = false;
                } else {
                    result = true;
                }
            }
        }
        return result;
    }
}
