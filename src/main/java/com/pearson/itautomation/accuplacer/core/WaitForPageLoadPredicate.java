package com.pearson.itautomation.accuplacer.core;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.common.base.Predicate;

public class WaitForPageLoadPredicate implements Predicate<WebDriver> {

    private static final Logger LOGGER = LoggerFactory
            .getLogger(WaitForPageLoadPredicate.class);

    @Override
    public boolean apply(WebDriver webdriver) {
        if (webdriver.getPageSource().contains("Loading data...")) {
            WebElement loadingBlock = webdriver.findElement(By.id("cover"));

            if (loadingBlock == null) {
                LOGGER.info("Loading Element not found, return to code");
                return true;
            }

            boolean doneBlocking = !loadingBlock.getAttribute("style")
                    .contains("block");
            if (!doneBlocking) {
                LOGGER.info("Waiting for blocking element");
            }
            return doneBlocking;
        }
        return true;
    }

}
