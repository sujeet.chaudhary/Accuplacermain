package com.pearson.itautomation.accuplacer.core;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Calendar;
import java.util.Properties;
import java.util.TimeZone;

import org.apache.commons.lang3.time.FastDateFormat;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class CommonUtilities {
    private static final Logger LOGGER = LoggerFactory
            .getLogger(CommonUtilities.class);

    private CommonUtilities() {
        // private constructor for utility class

    }

    protected static final Properties UTILITIES_PROPERTIES = CommonUtilities
            .readPropertyFile("//config//Utilities.properties");
    public static final Properties EMAIL_PROPERTIES = CommonUtilities
            .readPropertyFile("//config//emailProperties.properties");
    private static final FastDateFormat DATE_WITH_SLASHES = FastDateFormat
            .getInstance("MM/dd/yyyy");
    private static final FastDateFormat DATE_WITH_SPACES = FastDateFormat
            .getInstance("MMM dd yyyy");
    private static final FastDateFormat DATE_WITHOUT_SPACES = FastDateFormat
            .getInstance("ddmmss");
    private static final FastDateFormat DATE_WITHOUT_SPACES2 = FastDateFormat
            .getInstance("ddhhmmss");
    private static final FastDateFormat DATE_WITH_TIME = FastDateFormat
            .getInstance("MMMdd hh-mm ss aa");
    private static final FastDateFormat TIME_FORMAT = FastDateFormat
            .getInstance("hh-mm:ss aa");

    /**
     * Returns the Application absolute path
     * 
     * @author Deepak Radhakrishnan
     * 
     * @return strAppPath
     */
    public static final String appPath() {
        String strAppPath = null;

        File currentDir = new File("");
        strAppPath = currentDir.getAbsolutePath();

        return strAppPath;
    }

    /* Name - readPropertyFile */
    public static final Properties readPropertyFile(String pFileName) {
        Properties propertyLoad = new Properties();
        String fileName = CommonUtilities.appPath() + pFileName;

        try (FileInputStream fileSource = new FileInputStream(fileName)) {
            propertyLoad.load(fileSource);
        } catch (IOException e) {
            LOGGER.info("There was an error reading the properties file.", e);
        }

        return propertyLoad;
    }

    /**
     * This method returns txt file as a string
     * 
     * @param fileName
     * @return
     */
    public static String readTxtAsString(String fileName) {
        BufferedReader br = null;
        String valArr = "";

        // read from first file
        try {
            br = new BufferedReader(
                    new java.io.FileReader(new File((fileName))));

            String lineContent = "";
            while ((lineContent = br.readLine()) != null) {
                valArr = valArr + lineContent + "\n";
            }
        } catch (IOException e) {
            LOGGER.info("There was an error reading the text file.", e);
        }

        finally {
            if (br != null) {
                try {
                    br.close();
                } catch (IOException e) {
                    LOGGER.info("There was an error closing the txt file.", e);
                }
            }
        }
        return valArr;
    }

    /**
     * @param txtContent
     * @param filePath
     */
    public static void writeToTextFile(String txtContent, String filePath) {
        BufferedWriter writer = null;
        try {
            // create a temporary file

            File logFile = new File(filePath);

            // This will output the full path where the file will be written
            // to...
            writer = new BufferedWriter(new FileWriter(logFile));
            writer.write(txtContent);
        } catch (IOException e) {
            LOGGER.info("Exception occured", e);
        } finally {
            try {
                // Close the writer regardless of what happens...
                writer.close();
            } catch (IOException e) {
                LOGGER.info("Exception occured", e);
            }
        }
    }

    /**
     * @param noOfDaysAhead
     *            The number of days ahead of today. Negative integer in case of
     *            days behind. 0 to get the current date
     * @return
     * 
     * @author Deepak Radhakrishnan
     */
    public static final String getDayFromToday(int noOfDaysAhead) {
        Calendar cal = Calendar.getInstance();
        cal.add(Calendar.DATE, noOfDaysAhead);
        return DATE_WITH_SLASHES.format(cal.getTime());
    }

    /**
     * @param noOfDaysAhead
     *            The number of days ahead of today. Negative integer in case of
     *            days behind. 0 to get the current date
     * @return Date as per US/Central Timezone
     * 
     * @author Deepak Radhakrishnan
     */
    public static final String getDayFromTodayUSCentralTimeZone(
            int noOfDaysAhead) {
        Calendar cal = Calendar.getInstance();
        TimeZone usCentraltimeZone = TimeZone.getTimeZone("CST");
        FastDateFormat centralTimeDateWIthSlashes = FastDateFormat.getInstance(
                "MM/dd/yyyy", usCentraltimeZone);
        cal.add(Calendar.DATE, noOfDaysAhead);
        return centralTimeDateWIthSlashes.format(cal.getTime());
    }

    public static final String getCurrentDayMonthYear() {
        Calendar cal = Calendar.getInstance();
        return DATE_WITH_SPACES.format(cal.getTime());
    }

    public static final String getCurrentddmmss() {
        Calendar cal = Calendar.getInstance();
        return DATE_WITHOUT_SPACES.format(cal.getTime());
    }

    public static final String getCurrentddhhmmss() {
        Calendar cal = Calendar.getInstance();
        return DATE_WITHOUT_SPACES2.format(cal.getTime());
    }

    public static final String getCurrentDateTime() {
        Calendar cal = Calendar.getInstance();
        return DATE_WITH_TIME.format(cal.getTime());
    }

    public static final String getCurrentTime() {
        Calendar cal = Calendar.getInstance();
        return TIME_FORMAT.format(cal.getTime());
    }

    public static final Properties getUtilitiesProperties() {
        return UTILITIES_PROPERTIES;
    }

    public static void writeToTextFile(String txtContent) {
        BufferedWriter writer = null;
        try {
            // create a temporary file

            File logFile = new File(appPath()
                    + "//src//test//resources//temp.txt");

            // This will output the full path where the file will be written
            // to...
            writer = new BufferedWriter(new FileWriter(logFile));
            writer.write(txtContent);
        } catch (IOException e) {
            LOGGER.info("Exception occured", e);
        } finally {
            try {
                // Close the writer regardless of what happens...
                writer.close();
            } catch (IOException e) {
                LOGGER.info("Exception occured", e);
            }
        }

    }
}
