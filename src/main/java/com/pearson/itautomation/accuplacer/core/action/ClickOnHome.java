package com.pearson.itautomation.accuplacer.core.action;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.pearson.itautomation.bca.Browser;
import com.pearson.itautomation.bca.BrowserAction;
import com.pearson.itautomation.bca.BrowserElement;
import com.pearson.itautomation.bca.interaction.LeftClickElementAction;

/**
 * This Class will click on Home. <br>
 * <b>checkPrestate</b> - N/A<br>
 * <b>checkPostate</b> - N/A<br>
 * <b>perform</b> - This method will return true if Home will be clicked
 * successfully.
 * 
 * @author Sujeet Chaudhary
 */
public class ClickOnHome extends BrowserAction {

    private static final Logger LOGGER = LoggerFactory
            .getLogger(ClickOnHome.class);

    private static final String OR_AREA = "OR_TestSetup";
    private static final String OR_HOME = "Home";

    private final BrowserElement txtHome;

    /**
     * This constructor will initialize the mandatory fields and the
     * BrowserElements from this class
     * 
     * @param browser
     * 
     */
    public ClickOnHome(final Browser browser) {
        super(browser);
        this.txtHome = browser.getBrowserElement(OR_AREA, OR_HOME);
    }

    @Override
    protected boolean perform() {
        boolean result = true;
        if (!new LeftClickElementAction(getBrowser(), this.txtHome)
                .performWithStates()) {
            LOGGER.info("Not able to click on Home Button");
            result = false;
        }

        return result;
    }

}
