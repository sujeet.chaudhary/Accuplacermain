package com.pearson.itautomation.accuplacer.core;

import org.openqa.selenium.By;

import com.pearson.itautomation.bca.BrowserElement;

/**
 * 
 * This method will return the xpath of generic buttons available in the
 * Accuplacer application. This Reduces the overhead of writing xpaths
 * individual elements.
 * 
 * @author Deepak Radhakrishnan
 *         <table border='1'>
 *         <tr>
 *         <td><b>Button Type</td>
 *         <td><b>Xpath RegEX</td>
 *         </tr>
 *         <tr>
 *         <td>Button1</td>
 *         <td>//button[contains(text(), <i>ButtonLabel)]</td>
 *         </tr>
 *         <tr>
 *         <td>Button2</td>
 *         <td>//*[contains(@title, <i>ButtonLabel)]</td>
 *         </tr>
 *         <tr>
 *         <td>Button3</td>
 *         <td>//*[contains(text(), <i>ButtonLabel)])[2]</td>
 *         </tr>
 *         <tr>
 *         <td>Button4</td>
 *         <td>//button[contains(., <i>ButtonLabel)]</td>
 *         </tr>
 *         <tr>
 *         <td>Button5</td>
 *         <td>(//*[contains(@data-original-title,<i>ButtonLabel)])</td>
 *         </tr>
 *         </table>
 * 
 */
public class Buttons {
    private Buttons() {
        // private constructor for utility class

    }

    /**
     * This Method will return BrowserElement enclosed within <i>'button'</i>
     * html tag
     * 
     * @author Deepak Radhakrishnan
     * @param strBtnName
     *            Text Attribute
     */
    public static BrowserElement getButtonType1(String strBtnName) {

        return new BrowserElement(strBtnName,
                By.xpath("//button[contains(text(),'" + strBtnName + "')]"));
    }

    /**
     * This Method will return BrowserElement enclosed within <i>'a'</i>. The
     * User needs to pass the 'Title' attribute value
     * 
     * @author Deepak Radhakrishnan
     * @param strBtnName
     *            'Title' attribute value
     */
    public static BrowserElement getButtonType2(String strBtnName) {

        return new BrowserElement(strBtnName, By.xpath("//a[contains(@title,'"
                + strBtnName + "')]"));

    }

    /**
     * This Method will return BrowserElement enclosed within any of the tag.
     * The User needs to pass the 'text' attribute value
     * 
     * @author Deepak Radhakrishnan
     * @param strBtnName
     *            Text Value
     */
    public static BrowserElement getButtonType3(String strBtnName) {

        return new BrowserElement(strBtnName, By.xpath("(//*[contains(text(),'"
                + strBtnName + "')])[2]"));

    }

    /**
     * This Method will return BrowserElement enclosed within <i>'button'</i>
     * html tag
     * 
     * @author Deepak Radhakrishnan
     * @param strBtnName
     *            - Value of any unique attribute
     */
    public static BrowserElement getButtonType4(String strBtnName) {

        return new BrowserElement(strBtnName, By.xpath("(//button[contains(.,'"
                + strBtnName + "')])"));
    }

    /**
     * Returns type 5 button with the data-original tag
     * 
     * @author Deepak Radhakrishnan
     * @param strBtnName
     */
    public static BrowserElement getButtonType5(String strBtnName) {

        return new BrowserElement(strBtnName,
                By.xpath("(//*[contains(@data-original-title,'" + strBtnName
                        + "')])"));

    }

}
