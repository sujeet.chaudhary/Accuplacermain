package com.pearson.itautomation.accuplacer.core.action;

import java.util.Set;

import org.openqa.selenium.By;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.pearson.itautomation.accuplacer.interaction.SwitchToWindowByPageContent;
import com.pearson.itautomation.bca.Browser;
import com.pearson.itautomation.bca.BrowserAction;
import com.pearson.itautomation.bca.BrowserElement;
import com.pearson.itautomation.bca.interaction.LeftClickElementAction;
import com.pearson.itautomation.bca.interaction.SendTextToElementAction;

/**
 * This Class will Login to the Accuplacer application based on the Username and
 * Password parameter <br>
 * <b>checkPrestate</b> - N/A<br>
 * <b>checkPostate</b> - Will return true if the logout link is available <br>
 * <b>perform</b> - This method will return true if Modal Button is clicked
 * successfully
 * 
 * @author Deepak Radhakrishnan
 * 
 */
public class CATOldLoginAction extends BrowserAction {
    private static final Logger LOGGER = LoggerFactory
            .getLogger(CATOldLoginAction.class);
    private static final String OR_AREA = "OR_Login_OLD";
    private static final String OR_LOGINFIELD = "LoginFieldOLD";
    private static final String OR_PASSWORDFIELD = "PasswordFieldOLD";
    private static final String OR_LOGIN_BUTTON = "LoginButtonOLD";

    private final BrowserElement txtLoginField;
    private final BrowserElement txtPasswordField;
    private final BrowserElement btnLogin;

    private final String userName;
    private final String password;

    /**
     * This constructor will initialize the browser elements and the constant
     * variables
     * 
     * @param browser
     * @param userName
     * @param password
     * 
     * @author Deepak Radhakrishnan
     */
    public CATOldLoginAction(final Browser browser, String userName,
            String password) {
        super(browser);
        this.userName = userName;
        this.password = password;
        this.txtLoginField = browser.getBrowserElement(OR_AREA, OR_LOGINFIELD);
        this.txtPasswordField = browser.getBrowserElement(OR_AREA,
                OR_PASSWORDFIELD);
        this.btnLogin = browser.getBrowserElement(OR_AREA, OR_LOGIN_BUTTON);
    }

    @Override
    protected boolean perform() {
        boolean result = true;

        boolean present = true;

        try {

            getBrowser().getWebDriver().findElement(
                    By.xpath("//Button[contains(text(),'Information')]"));
        }

        catch (org.openqa.selenium.NoSuchElementException e) {
            LOGGER.error("Element Not Found, User navigated to the login Page",
                    e);
            present = false;

        }

        if (present == true) {

            LOGGER.info("Clicking on Login Button , Navigating to login page");

            getBrowser().getWebDriver()
                    .findElement(By.className("login_button")).click();

        }

        if (!new SendTextToElementAction(this.getBrowser(), this.txtLoginField,
                this.userName).performWithStates()) {
            LOGGER.error("LoginAction failed to populate the login field");
            result = false;
        }
        if (!new SendTextToElementAction(this.getBrowser(),
                this.txtPasswordField, this.password).performWithStates()) {
            LOGGER.error("LoginAction failed to populate the Password field");
            result = false;
        }

        if (!new LeftClickElementAction(this.getBrowser(), btnLogin)
                .performWithStates()) {
            LOGGER.error("LoginAction failed ,unable to click on Submit Button");
            result = false;
        }

        if (!new SwitchToWindowByPageContent(this.getBrowser(),
                "Do not display this notification again until a new notice is posted")
                .performWithStates()) {
            LOGGER.error("Could Not switch to the Whats New Window");
            result = false;
        }
        this.getBrowser().close();
        Set<String> windowHandles = getBrowser().getWindowHandles();
        for (String winHandle : windowHandles) {
            getBrowser().switchTo().window(winHandle);
        }

        if (!new SwitchToWindowByPageContent(this.getBrowser(),
                "System Requirements").performWithStates()) {
            LOGGER.error("Could Not Switch to the main Window");
            result = false;
        }
        return result;
    }
    /*
     * @Override protected boolean checkPostState() { boolean result = true;
     * this.lnkLogout.setTimeout(10000); if (!new
     * ElementExistsValidation(this.getBrowser(), lnkLogout)
     * .performWithStates()) {
     * LOGGER.error("LoginAction Failed , unalble to locate Logout Link");
     * result = false; } return result; }
     */

}
