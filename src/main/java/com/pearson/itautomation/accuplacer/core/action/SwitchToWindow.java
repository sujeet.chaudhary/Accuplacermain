package com.pearson.itautomation.accuplacer.core.action;

import java.util.Set;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.pearson.itautomation.bca.Browser;
import com.pearson.itautomation.bca.BrowserAction;

/**
 * This Class will send the Control from Parent window to child or Vice Versa .<br>
 * <b>checkPrestate</b> - N/A <br>
 * <b>checkPostsate</b> - N/A <br>
 * <b>perform</b> - This method will return true if control will switch from one
 * window to other window successfully .
 * 
 * @author Sujeet Chaudhary
 */
public class SwitchToWindow extends BrowserAction {

    private static final Logger LOGGER = LoggerFactory
            .getLogger(SwitchToWindow.class);

    private final String url;

    /**
     * This constructor will initialize the mandatory fields and the
     * BrowserElements from this class
     * 
     * @param browser
     * @param url
     *            The URL of the target Window.
     */
    public SwitchToWindow(final Browser browser, String url) {

        super(browser);

        this.url = url;
    }

    @Override
    protected boolean perform() {
        boolean result = false;
        Set<String> windowHandles = this.getBrowser().getWebDriver()
                .getWindowHandles();

        for (String winHandle : windowHandles) {
            this.getBrowser().getWebDriver().switchTo().window(winHandle);
            if (this.getBrowser().getWebDriver().getCurrentUrl().contains(url)) {
                this.getBrowser().getWebDriver().manage().window().maximize();
                result = true;
            } else {
                LOGGER.error("target window are not available for switch");
            }

        }
        return result;
    }
}
