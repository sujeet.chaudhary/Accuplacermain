package com.pearson.itautomation.accuplacer.core.action;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.pearson.itautomation.accuplacer.core.AccuplacerBrowserUtils;
import com.pearson.itautomation.accuplacer.core.Buttons;
import com.pearson.itautomation.bca.Browser;
import com.pearson.itautomation.bca.BrowserAction;
import com.pearson.itautomation.bca.BrowserElement;
import com.pearson.itautomation.bca.interaction.LeftClickElementAction;

/**
 * This Class will click on save button and check the presence of the
 * confirmation message<br>
 * <b>checkPrestate</b> - N/A <br>
 * <b>checkPostate</b> - N/A <br>
 * <b>perform</b> - This method will return true if the saving action is
 * confirmed successfully successfully.
 * 
 * @author Deepak Radhakrishnan
 */
public class SaveAndAssert extends BrowserAction {
    private static final Logger LOGGER = LoggerFactory
            .getLogger(SaveAndAssert.class);
    private final String strConfirmation;

    public final BrowserElement btnSave = Buttons.getButtonType1("Save");
    private Browser browser = null;

    /**
     * This constructor will initialize the confirmation message
     * 
     * @param browser
     * @param strConfirmation
     *            Save confirmation message
     * @author Deepak Radhakrishnan
     */
    public SaveAndAssert(Browser browser, String strConfirmation) {
        super(browser);
        this.browser = browser;
        this.strConfirmation = strConfirmation;

    }

    @Override
    protected boolean perform() {
        boolean result = true;
        boolean flag = browser.getElementWithWait(btnSave).isEnabled();
        long waitingTime = 500;
        long waitingTimeForMsg = 500;
        while (!flag && waitingTime <= 20000) {

            AccuplacerBrowserUtils.WaitInMilliSeconds(waitingTime);
            flag = browser.getElementWithWait(btnSave).isEnabled();
            waitingTime = waitingTime + 500;

        }

        if (!new LeftClickElementAction(getBrowser(), btnSave)
                .performWithStates()) {
            LOGGER.error("Not able to click on save button");
            result = false;
        }
        boolean existance = AccuplacerBrowserUtils.checkPageSourceForString(
                this.getBrowser(), this.strConfirmation);
        while (!existance && waitingTimeForMsg <= 20000) {
            AccuplacerBrowserUtils.WaitInMilliSeconds(waitingTime);
            existance = AccuplacerBrowserUtils.checkPageSourceForString(
                    this.getBrowser(), this.strConfirmation);
            waitingTimeForMsg = waitingTimeForMsg + 500;
        }
        if (AccuplacerBrowserUtils.checkPageSourceForString(this.getBrowser(),
                this.strConfirmation))
            LOGGER.info("Saved successfully");
        else {
            LOGGER.error("Save and assert fail");
            return false;
        }

        return result;
    }
}
