package com.pearson.itautomation.accuplacer.core.action;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.pearson.itautomation.accuplacer.core.Buttons;
import com.pearson.itautomation.bca.Browser;
import com.pearson.itautomation.bca.BrowserAction;
import com.pearson.itautomation.bca.BrowserElement;
import com.pearson.itautomation.bca.interaction.ClearTextElementAction;
import com.pearson.itautomation.bca.interaction.LeftClickElementAction;
import com.pearson.itautomation.bca.interaction.SendTextToElementAction;
import com.pearson.itautomation.bca.validation.ElementExistsValidation;

/**
 * This Class will the security questions on Page 2 of <i>Change password</i><br>
 * <b>checkPrestate</b> - This method will validate whether the password has
 * been changed successfully in order to edit the <i>Security questions</i> <br>
 * <b>checkPostate</b> - N/A <br>
 * <b>perform</b> - This method will return true once the <i>security questions
 * </i> have been changed successfully
 * 
 * @author Deepak Radhakrishnan
 * 
 */
public class ChangeSecurityQuestions extends BrowserAction {
    private static final Logger LOGGER = LoggerFactory
            .getLogger(ChangeSecurityQuestions.class);

    private static final String OR_AREA = "OR_Global";
    private static final String OR_SECURITY_QUESTIONS_RESPONSE1 = "SecurityResponse1";
    private static final String OR_SECURITY_QUESTIONS_RESPONSE2 = "SecurityResponse2";
    private static final String OR_SECURITY_QUESTIONS_RESPONSE3 = "SecurityResponse3";

    private final BrowserElement txtResponse1;
    private final BrowserElement txtResponse2;
    private final BrowserElement txtResponse3;
    private final BrowserElement btnSubmit;

    private final String strResponse1;
    private final String strResponse2;
    private final String strResponse3;

    /**
     * This constructor will initialize the browser elements and the constant
     * variables
     * 
     * @param browser
     * @param strResponse1
     *            Text response 1 for the first security question
     * @param strResponse2
     *            Text response 2 for the second security question
     * @param strResponse3
     *            Text response 3 for the third security question
     */
    public ChangeSecurityQuestions(Browser browser, String strResponse1,
            String strResponse2, String strResponse3) {
        super(browser);
        this.txtResponse1 = browser.getBrowserElement(OR_AREA,
                OR_SECURITY_QUESTIONS_RESPONSE1);
        this.txtResponse2 = browser.getBrowserElement(OR_AREA,
                OR_SECURITY_QUESTIONS_RESPONSE2);
        this.txtResponse3 = browser.getBrowserElement(OR_AREA,
                OR_SECURITY_QUESTIONS_RESPONSE3);
        this.strResponse1 = strResponse1;
        this.strResponse2 = strResponse2;
        this.strResponse3 = strResponse3;
        this.btnSubmit = Buttons.getButtonType1("Save");
    }

    @Override
    protected boolean checkPreState() {
        boolean result = true;
        if (!new ElementExistsValidation(this.getBrowser(), getBrowser()
                .getBrowserElement("OR_Global", "ChangePasswordValidate1"))
                .performWithStates()) {
            LOGGER.error("Unable to find the password change confirmation");
            result = false;
        }
        if (!new ElementExistsValidation(this.getBrowser(), this.txtResponse1)
                .performWithStates()) {
            LOGGER.error("Unable to find the Security response textbox");
            result = false;
        }
        return result;
    }

    @Override
    protected boolean perform() {
        boolean result = true;
        if (!new ClearTextElementAction(this.getBrowser(), this.txtResponse1)
                .performWithStates()) {
            LOGGER.error("Unable to clear Response 1 text");
            result = false;
        }
        if (!new SendTextToElementAction(this.getBrowser(), this.txtResponse1,
                this.strResponse1).performWithStates()) {
            LOGGER.error("Unable to send Response 1 text");
            result = false;
        }
        if (!new ClearTextElementAction(this.getBrowser(), this.txtResponse2)
                .performWithStates()) {
            LOGGER.error("Unable to clear Response 2 text");
            result = false;
        }
        if (!new SendTextToElementAction(this.getBrowser(), this.txtResponse2,
                this.strResponse2).performWithStates()) {
            LOGGER.error("Unable to send Response 2 text");
            result = false;
        }
        if (!new ClearTextElementAction(this.getBrowser(), this.txtResponse3)
                .performWithStates()) {
            LOGGER.error("Unable to clear Response 3 text");
            result = false;
        }
        if (!new SendTextToElementAction(this.getBrowser(), this.txtResponse3,
                this.strResponse3).performWithStates()) {
            LOGGER.error("Unable to send Response 3 text");
            result = false;
        }

        if (!new LeftClickElementAction(this.getBrowser(), this.btnSubmit)
                .performWithStates()) {
            LOGGER.error("Unable to Click on Submit Button");
            result = false;
        }
        return result;

    }

}
