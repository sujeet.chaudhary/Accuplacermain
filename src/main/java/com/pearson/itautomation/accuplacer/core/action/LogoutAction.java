package com.pearson.itautomation.accuplacer.core.action;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.pearson.itautomation.bca.Browser;
import com.pearson.itautomation.bca.BrowserAction;
import com.pearson.itautomation.bca.BrowserElement;
import com.pearson.itautomation.bca.interaction.LeftClickElementAction;
import com.pearson.itautomation.bca.validation.ElementExistsValidation;

/**
 * This Class will Logout from the Accuplacer application <br>
 * <b>checkPrestate</b> - N/A<br>
 * <b>checkPostate</b> - Will return true if the login page is available <br>
 * <b>perform</b> - This method will return true if logout link is clicked
 * successfully
 * 
 * @author Sujeet Chaudhary
 * 
 */
public class LogoutAction extends BrowserAction {

    private static final Logger LOGGER = LoggerFactory
            .getLogger(LoginAction.class);

    private static final String OR_AREA = "OR_Login";
    private static final String OR_LOGOUT = "Logout";
    private static final String OR_USERNAME = "LoginField";
    private static final String OR_YES = "YesLogout";

    private final BrowserElement lnkLogout;
    private final BrowserElement txtUserName;
    private final BrowserElement btnYesLogout;

    /**
     * This constructor will initialize the browser elements and the constant
     * variables
     * 
     * @param browser
     * 
     * @author Sujeet Chaudhary
     */
    public LogoutAction(final Browser browser) {
        super(browser);
        this.lnkLogout = browser.getBrowserElement(OR_AREA, OR_LOGOUT);
        this.btnYesLogout = browser.getBrowserElement(OR_AREA, OR_YES);
        this.txtUserName = browser.getBrowserElement(OR_AREA, OR_USERNAME);
    }

    @Override
    protected boolean perform() {
        boolean result = true;

        if (!new LeftClickElementAction(this.getBrowser(), lnkLogout)
                .performWithStates()) {
            LOGGER.error("Could not click on logout link");
            result = false;
        }

        if (!new LeftClickElementAction(this.getBrowser(), btnYesLogout)
                .performWithStates()) {
            LOGGER.error("Couldclick on yes button");
            result = false;
        }

        try {
            Thread.sleep(1000);
        } catch (InterruptedException e) {
            LOGGER.warn("Failed to wait for Login page to visible");
        }
        return result;
    }

    @Override
    protected boolean checkPostState() {
        boolean result = true;

        if (!new ElementExistsValidation(getBrowser(), this.txtUserName)
                .performWithStates()) {
            LOGGER.error("Not able to Logout");
            result = false;
        }

        return result;
    }

}
