package com.pearson.itautomation.accuplacer.core.action;

import org.openqa.selenium.By;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.pearson.itautomation.bca.Browser;
import com.pearson.itautomation.bca.BrowserAction;
import com.pearson.itautomation.bca.BrowserElement;
import com.pearson.itautomation.bca.interaction.LeftClickElementAction;
import com.pearson.itautomation.bca.validation.ElementExistsValidation;

/**
 * This Class will navigate the user to a specific Link <br>
 * <b>checkPrestate</b> - This method will return true of the link is present<br>
 * <b>checkPostate</b> - This method will return true if the validation element
 * on the subsequent page is found <br>
 * <b>perform</b> - This method will return true if the Sub Menu Link is clicked
 * successfully
 * 
 * @author Deepak Radhakrishnan
 * 
 */
public class NavigateToLink extends BrowserAction {
    private static final Logger LOGGER = LoggerFactory
            .getLogger(NavigateToLink.class);
    private final BrowserElement lnkSubMenu;
    private final BrowserElement elementCheckPoint;
    private String strSubMenuName;

    /**
     * This constructor will initialize the browser elements and the constant
     * variables
     * 
     * @param browser
     * @param strSubMenuName
     *            Link Text Value
     * @param navCheckPoint
     *            Checkpoint Element
     * 
     * @author Deepak Radhakrishnan
     */
    public NavigateToLink(Browser browser, String strSubMenuName,
            BrowserElement navCheckPoint) {
        super(browser);

        this.lnkSubMenu = new BrowserElement("SubMenuTab",
                By.linkText(strSubMenuName));
        this.elementCheckPoint = navCheckPoint;
        this.strSubMenuName = strSubMenuName;

    }

    @Override
    protected boolean checkPreState() {
        boolean result = true;

        if (!new ElementExistsValidation(this.getBrowser(), this.lnkSubMenu)
                .performWithStates()) {
            LOGGER.error("Could not find the " + strSubMenuName + " Tab");
            result = false;
        }
        return result;

    }

    @Override
    protected boolean perform() {
        boolean result = true;

        if (result
                && !new LeftClickElementAction(this.getBrowser(), lnkSubMenu)
                        .performWithStates()) {
            LOGGER.error("Could NOT click on " + strSubMenuName + " Link");
            result = false;
        }

        return result;
    }

    @Override
    protected boolean checkPostState() {
        boolean result = true;
        if (!new ElementExistsValidation(this.getBrowser(), elementCheckPoint)
                .performWithStates()) {
            LOGGER.error("Could NOT find the Checkpoint element");
            result = false;
        }
        return result;

    }

}
