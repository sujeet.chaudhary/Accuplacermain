package com.pearson.itautomation.accuplacer.core.action;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.pearson.itautomation.bca.Browser;
import com.pearson.itautomation.bca.BrowserAction;
import com.pearson.itautomation.bca.BrowserElement;
import com.pearson.itautomation.bca.SelectorType;
import com.pearson.itautomation.bca.interaction.DropdownSelectionAction;

/**
 * This Class will Select the option based on the partial text parameter<br>
 * <b>checkPrestate</b> - N/A <br>
 * <b>checkPostate</b> - N/A <br>
 * <b>perform</b> - This method will return true if all the mandatory fields
 * have been filled in and saved successfully.
 * 
 * @author Deepak Radhakrishnan
 * 
 */
public class DropDownSelectByNormalizedRegEx extends BrowserAction {
    private static final Logger LOGGER = LoggerFactory
            .getLogger(DropDownSelectByNormalizedRegEx.class);
    private final String regExConvertedString;


    private final BrowserElement dropDownElement;

    /**
     * This constructor will initialize the mandatory fields and the
     * BrowserElements from this class
     * 
     * @param browser
     * @param dropDownElement
     *            The Dropdown Element
     * @param txtValueofOption
     *            The Text Value of the option to be selected
     * @author Deepak Radhakrishnan
     */

    public DropDownSelectByNormalizedRegEx(Browser browser,
            BrowserElement dropDownElement, String txtValueofOption) {
        super(browser);
        txtValueofOption = txtValueofOption.replace("*", "");
        // Regex patter [starts with 0 or more spaces] , [starts with 1 or more Asterisks] , [replace 1 or more spaces with one space]
        this.regExConvertedString =  "\\s*.*"+txtValueofOption.replace(" ", "\\s+") + "\\s*";
        LOGGER.info("------------"+regExConvertedString);
        this.dropDownElement = dropDownElement;
    
        
    }

    @Override
    protected boolean perform() {
        boolean result = true;
        LOGGER.info("Matching "+regExConvertedString);
        if (!new DropdownSelectionAction(this.getBrowser(),
                this.dropDownElement, SelectorType.VISIBLE_TEXT_REGEX,
                regExConvertedString).performWithStates(false,true)) {
            LOGGER.error("Failed to select " + regExConvertedString + " Option");
            return false;
        }
        return result;
    }
}
