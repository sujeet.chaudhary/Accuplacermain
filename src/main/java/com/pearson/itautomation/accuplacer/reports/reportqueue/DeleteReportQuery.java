package com.pearson.itautomation.accuplacer.reports.reportqueue;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.pearson.itautomation.accuplacer.core.AccuplacerBrowserUtils;
import com.pearson.itautomation.accuplacer.core.action.LeftClickOnModalPopupButton;
import com.pearson.itautomation.bca.Browser;
import com.pearson.itautomation.bca.BrowserAction;
import com.pearson.itautomation.bca.BrowserElement;
import com.pearson.itautomation.bca.validation.ElementExistsValidation;

public class DeleteReportQuery extends BrowserAction {
    private static final Logger LOGGER = LoggerFactory
            .getLogger(DeleteReportQuery.class);

    private BrowserElement btnDeleteQuery;
    private BrowserElement btnClose;

    /**
     * This class will delete all the submitted queries present in the Report
     * Queue, regardless of the processing state <br>
     * <b>checkPreState</b> - N/A<br>
     * <b>checkPostState</b> - N/A <b>perform</b> This method will return true
     * if all the items under the report queue are deleted
     * 
     * @author Deepak Radhakrishnan
     * @param browser
     *            Browser instance
     */
    public DeleteReportQuery(Browser browser) {
        super(browser);

        btnDeleteQuery = browser.getBrowserElement("OR_Reports", "deleteQuery");
        btnClose = browser.getBrowserElement("OR_Reports", "yesButtonQueue");
        btnDeleteQuery.setTimeout(5000);

    }

    protected boolean perform() {
        boolean result = true;
        do {
            if (new ElementExistsValidation(getBrowser(), btnDeleteQuery)
                    .performWithStates()) {
                LOGGER.info("Deleting reports");
                if (!new LeftClickOnModalPopupButton(this.getBrowser(),
                        btnDeleteQuery).performWithStates()) {
                    LOGGER.error("Unable Click on delete button");
                    return false;
                }
                if (!new LeftClickOnModalPopupButton(this.getBrowser(),
                        btnClose).performWithStates()) {
                    LOGGER.error("Unable Click on delete confirmation");
                    result = false;
                }
                AccuplacerBrowserUtils.WaitInMilliSeconds(2000);
            }
        } while (new ElementExistsValidation(getBrowser(), btnDeleteQuery)
                .performWithStates());

        return result;
    }
}
