/**
 * 
 */
package com.pearson.itautomation.accuplacer.reports.core;

/**
 * @author Deepak Radhakrishnan
 *
 */
public class Reports {
    private final String exnum;
    private final Student student;

    public Reports(String exnum, Student student) {
        this.exnum = exnum;
        this.student = student;
    }

    public String getExnum() {
        return exnum;
    }

    public Student getStudent() {
        return student;
    }

}
