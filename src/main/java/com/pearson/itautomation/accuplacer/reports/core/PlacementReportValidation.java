package com.pearson.itautomation.accuplacer.reports.core;

import java.util.HashMap;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class PlacementReportValidation extends ReportValidateFacory {
    private static final Logger LOGGER = LoggerFactory
            .getLogger(PlacementReportValidation.class);
    HashMap<String, TestScores> testScores;
    private final String testName;
    private String expected = null;
    private String actual = null;

    public PlacementReportValidation(String currentExnum,
            String typeOfCurrentColumn, String valueOfCurrentColumn,
            ReadExcelReport reportToBeVerified, int columnIndex,
            HashMap<String, TestScores> testScores) {
        super(currentExnum, typeOfCurrentColumn, valueOfCurrentColumn,
                columnIndex, reportToBeVerified);
        this.testScores = testScores;
        this.testName = getValueOfCurrentColumn();

    }

    @Override
    public boolean evaluateReportFields() {
        boolean validatedScore = true;
        if (testScores.get(getCurrentExnum()).isPlacementScoresPresent()) {
            LOGGER.debug("Verify Placement Score");
            String placementScoreInExcel = getReportToBeVerified()
                    .getArrReportExcel()[getReportToBeVerified()
                    .getExnumPosition(getCurrentExnum())][getColumnIndex()];
            String placementScoreInDB = testScores.get(getCurrentExnum())
                    .getPlacementScore(testName);
            if (testScores.get(getCurrentExnum()).getPlacementTestsMap()
                    .containsKey(testName)) {
                LOGGER.debug("TestName : " + testName + "Test Score"
                        + placementScoreInDB);
                LOGGER.debug("--" + placementScoreInExcel + " "
                        + Math.round(Double.parseDouble(placementScoreInDB)));
                LOGGER.info("Exnum " + getCurrentExnum() + " TestName : "
                        + testName + ACTUAL_SCORE_MESSAGE
                        + placementScoreInExcel + EXPECTED_VALUE_MESSAGE
                        + Math.round(Double.parseDouble(placementScoreInDB)));
                validatedScore = Double.parseDouble(placementScoreInExcel) == Math
                        .round(Double.parseDouble(placementScoreInDB));
                try {
                    validatedScore = Double.parseDouble(placementScoreInExcel) == Math
                            .round(Double.parseDouble(placementScoreInDB));
                } catch (Exception e) {
                    LOGGER.error("Exception occured while comparing "
                            + placementScoreInExcel + " and"
                            + placementScoreInDB, e);
                    expected = placementScoreInDB;
                    actual = placementScoreInExcel;
                    return false;
                }

            }
        }
        return validatedScore;
    }

    @Override
    public String returnValidationMessage() {
        return "Comparing Placement Scores for Test - " + testName
                + " , Exnum :" + getCurrentExnum();
    }

    @Override
    public String failedValidationMessage() {
        return String
                .format("Expected Placement Score (for exnum =%s) is '%s' Actual Score = '%s'",
                        getCurrentExnum(), expected, actual);
    }

}
