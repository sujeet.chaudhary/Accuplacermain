package com.pearson.itautomation.accuplacer.reports.core;

import java.sql.Connection;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * @author Deepak Radhakrishnan
 *
 */
public class ColumnSequence {
    private static final Logger LOGGER = LoggerFactory
            .getLogger(ColumnSequence.class);

    private int countColumn;
    private String columnSequence;
    private String columnTypeSequence;
    private final String[] headerArrayInput;
    private final Connection con;
    private boolean isHeaderGeneratedCorrectly;

    /**
     * @param headerArrayInput
     *            Selected Header Fields
     * @param con
     *            Databse connection
     */
    public ColumnSequence(String[] headerArrayInput, Connection con) {
        this.con = con;
        countColumn = 0;
        columnSequence = "";
        columnTypeSequence = "";
        this.headerArrayInput = headerArrayInput;
        generateColumnSequence();
        setHeaderGeneratedCorrectly(true);
        validateColumnSequence();
    }

    /**
     * This method will generate the header sequence as per the input sequence
     */
    public void generateColumnSequence() {
        for (int i = 0; i < headerArrayInput.length; i++) {
            Column column = new Column(headerArrayInput[i], con);
            String columnValues = column.readColumns();
            String columnTypeValues = column.getColumnTypeSequence();

            if (columnValues == null) {
                LOGGER.error("Header columns Returned as Null");
            } else {
                countColumn = countColumn + columnValues.split(";").length;

                if (columnSequence.equalsIgnoreCase("")) {
                    columnSequence = columnValues;
                } else {
                    columnSequence = columnSequence + ";" + columnValues;

                }
            }
            if (columnTypeSequence == null
                    || columnTypeSequence.equalsIgnoreCase("")) {
                this.columnTypeSequence = columnTypeValues;
            } else {
                columnTypeSequence = columnTypeSequence + ";"
                        + columnTypeValues;

            }
        }

    }

    private void validateColumnSequence() {
        if (!(columnSequence.split(";").length == columnTypeSequence.split(";").length)) {
            LOGGER.error("Number of Columns in the report Header, does NOT Match with the associated Type of Each Field");
            setHeaderGeneratedCorrectly(false);
        }

    }

    public String getColumnTypeSequence() {
        return columnTypeSequence;
    }

    public void setColumnTypeSequence(String columnTypeSequence) {
        this.columnTypeSequence = columnTypeSequence;
    }

    public String getColumnSequence() {
        return columnSequence;
    }

    public void setColumnSequence(String columnSequence) {
        this.columnSequence = columnSequence;
    }

    /**
     * @return the isHeaderGeneratedCorrectly
     */
    public boolean isHeaderGeneratedCorrectly() {
        return isHeaderGeneratedCorrectly;
    }

    /**
     * @param isHeaderGeneratedCorrectly
     *            the isHeaderGeneratedCorrectly to set the header sequence
     *            along with Header Type Sequence
     */
    public void setHeaderGeneratedCorrectly(boolean isHeaderGeneratedCorrectly) {
        this.isHeaderGeneratedCorrectly = isHeaderGeneratedCorrectly;
    }
}
