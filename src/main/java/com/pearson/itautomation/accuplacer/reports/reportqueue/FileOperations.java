package com.pearson.itautomation.accuplacer.reports.reportqueue;

import java.io.File;
import java.io.FileFilter;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class FileOperations {
    private static final Logger LOGGER = LoggerFactory
            .getLogger(FileOperations.class);

    private static final String SOURCE_LOCATION = "D:\\Eclipse\\Repo\\Test";
    private static final String DESTINATION_LOCATION = "D:\\Downloads_sel2\\";

    public static void main(String args[]) {
        // File file;
        // do {
        // file = lastFileModified(SOURCE_LOCATION);
        // if (file != null) {
        // LOGGER.info("Copying File '" + file.getName() + "'");
        // copyFile(file, DESTINATION_LOCATION);
        // }
        // } while (file != null);

        // System.out.println(TestBase.AUTOMATION_FILE_DOWNLOAD);
        // boolean deleteFile2 = FileOperations.deleteFile(new File(
        // TestBase.AUTOMATION_FILE_DOWNLOAD1));
        // System.out.println("BOOLEANS" + deleteFile1 + deleteFile2);

        // String dest = "D:\\DELETEWORKSPACE";
        // System.out.println(lastFileModified(dest));
        // System.out.println(deleteFile(new File(dest)));

    }

    public static boolean deleteFolder(String folderName) {
        while (FileOperations.lastFileModified(folderName) != null) {
            if (!FileOperations.deleteFile(FileOperations
                    .lastFileModified(folderName))) {
                LOGGER.info("Folder is empty");
                break;
            }
            System.out.println("a");
        }
        boolean deleteFile1 = FileOperations.deleteFile(new File(folderName));
        return deleteFile1;
    }

    public static void moveFiles() {
        File file;
        do {
            file = lastFileModified(SOURCE_LOCATION);
            if (file != null) {
                LOGGER.info("Copying File '" + file.getName() + "'");
                copyFile(file, DESTINATION_LOCATION);
            }
        } while (file != null);
    }

    public static void copyFile(File file, String destination) {
        if (!((destination.charAt(destination.length() - 1) + "")
                .equalsIgnoreCase("\\") || (destination.charAt(destination
                .length() - 1) + "").equalsIgnoreCase("/"))) {
            destination = destination + "\\";
        }
        if (!new File(destination).exists()) {
            LOGGER.info("Creating Folder");
            new File(destination).mkdirs();
        }
        System.out
                .println(file.renameTo(new File(destination + file.getName())));
    }

    public static File lastFileModified(String dir) {
        File fl = new File(dir);
        File[] files = readAllFilesFromDirectory(fl);
        long lastMod = Long.MIN_VALUE;
        File choice = null;
        if (files != null) {
            for (File file : files) {
                if (file.lastModified() > lastMod) {
                    choice = file;
                    lastMod = file.lastModified();
                }
            }
        } else {
            LOGGER.error("File Not Found at '" + dir + "'");
            return null;
        }
        return choice;
    }

    public static File[] readAllFilesFromDirectory(File folder) {
        File[] files = folder.listFiles(new FileFilter() {
            public boolean accept(File file) {
                return file.isFile();
            }
        });
        return files;
    }

    public static boolean deleteFile(File file) {
        if (file.getPath().contains("DELETEWORKSPACE")
                || file.getPath().contains("Automation")) {
            if (file.exists()) {
                return file.delete();
            } else {
                LOGGER.error(String
                        .format("File cannot be deleted as the file %s does not exists",
                                file.getPath()));
                return false;
            }
        } else {
            LOGGER.error("File cannot be deleted as the desired files are not in the delete workspace");
            return false;
        }
    }

}
