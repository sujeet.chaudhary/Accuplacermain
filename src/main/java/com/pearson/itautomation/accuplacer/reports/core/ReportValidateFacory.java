package com.pearson.itautomation.accuplacer.reports.core;

public abstract class ReportValidateFacory {
    private final String currentExnum;
    private final String typeOfCurrentColumn;
    private final String valueOfCurrentColumn;
    private final ReadExcelReport reportToBeVerified;
    private final int columnIndex;

    public static final String ACTUAL_SCORE_MESSAGE = " Actual Score as per the Excel :";
    public static final String EXPECTED_VALUE_MESSAGE = " Expected Value as per the DB reference ";

    ReportValidateFacory(String currentExnum, String typeOfCurrentColumn,
            String valueOfCurrentColumn, int columnIndex,
            ReadExcelReport reportToBeVerified) {
        this.currentExnum = currentExnum;
        this.typeOfCurrentColumn = typeOfCurrentColumn;
        this.valueOfCurrentColumn = valueOfCurrentColumn;
        this.reportToBeVerified = reportToBeVerified;
        this.columnIndex = columnIndex;
    }

    public String getCurrentExnum() {
        return currentExnum;
    }

    public String getTypeOfCurrentColumn() {
        return typeOfCurrentColumn;
    }

    public String getValueOfCurrentColumn() {
        return valueOfCurrentColumn;
    }

    public ReadExcelReport getReportToBeVerified() {
        return reportToBeVerified;
    }

    public int getColumnIndex() {
        return columnIndex;
    }

    public abstract boolean evaluateReportFields();

    public abstract String returnValidationMessage();

    public abstract String failedValidationMessage();
}
