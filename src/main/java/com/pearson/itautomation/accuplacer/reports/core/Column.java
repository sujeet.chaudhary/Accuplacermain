package com.pearson.itautomation.accuplacer.reports.core;

import java.sql.Connection;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.pearson.itautomation.accuplacer.core.DBUtils;

/**
 * @author Deepak Radhakrishnan
 *
 *         This Class will create a column based on the input field value
 */
public class Column {
    private static final Logger LOGGER = LoggerFactory.getLogger(Column.class);
    private final String column;
    private String columnValue;
    private String columnType;
    private String columnTypeSequence = "";
    private String columnSequence = "";
    private String diagnosticColumnSequenceQuery = "select Roster_Summary_Strand_name from test_strand_mapping where test_Detail_id in (select test_Detail_id from test_Detail where trim(test_name) = '%s') order by domain_sequence";
    private final DBUtils dbu;

    /**
     * Construct a Column based on One Field
     * 
     * @param column
     *            column Name and Type
     * @param con
     */
    public Column(String column, Connection con) {
        dbu = new DBUtils(con);
        this.column = column;
    }

    public String readColumns() {
        String columnArr[] = column.split(";");
        if (columnArr.length != 2) {
            LOGGER.error("Column parameter is in incorrect format , exepected format 'Columnname;Column Type'");
            return null;
        }

        else {
            setColumnValue(columnArr[0]);
            setColumnType(columnArr[1]);
        }

        // Switch Case to Identify the Type of Column

        switch (getColumnType()) {
        case ReportConstants.PLACEMENT_TEST: {
            LOGGER.debug("Generating Placement Test Column Sequence");
            columnSequence = getColumnValue();
            columnTypeSequence = getColumnType();
            break;
        }
        case ReportConstants.DIAGNOSTIC_TEST: {
            LOGGER.debug("Generating Diagnostic Test Column Sequence");
            columnSequence = generateDiagnosticColummHeader();
            createDiagnosticColumnTypeSequence();

            break;
        }
        case ReportConstants.PLACEMENT_DIAGNOSTIC_TEST: {
            LOGGER.debug("Generating Diagnostic Test Column Sequence");
            columnSequence = getColumnValue() + ";"
                    + generateDiagnosticColummHeader();
            columnTypeSequence = columnTypeSequence
                    + ReportConstants.PLACEMENT_TEST;
            createDiagnosticColumnTypeSequence();

            /**
             * removing the last diagnostic keyword , as the column sequence
             * count considers the first placement keyword for the
             * PLACEMNT_DIAGNOSTIC Type
             */

            columnTypeSequence = columnTypeSequence.substring(0,
                    columnTypeSequence.length()
                            - (";" + ReportConstants.DIAGNOSTIC_TEST).length());
            break;
        }
        case ReportConstants.TEST_SESSSION: {
            LOGGER.debug("Generating TEST_SESSSION Test Column Sequence");
            columnSequence = getColumnValue();
            columnTypeSequence = getColumnType();
            break;
        }
        case ReportConstants.STUDENT_INFORMATION: {
            LOGGER.debug("Generating STUDENT_INFORMATION Test Column Sequence");
            columnSequence = getColumnValue();
            columnTypeSequence = getColumnType();
            break;
        }
        default: {
            LOGGER.error("Unknown Column Type : " + getColumnType());
            break;
        }

        }
        return columnSequence;
    }

    /**
     * This Method will generate Diagnostic Header sequence
     * 
     */
    public String generateDiagnosticColummHeader() {
        String columnHeader = null;
        String arrColumnHeader[][] = dbu
                .runAndReturnMultiDimentionalArray(String.format(
                        diagnosticColumnSequenceQuery, getColumnValue()));
        for (int i = 0; i < arrColumnHeader.length; i++) {
            if (i != 0) {
                columnHeader = columnHeader + ";" + arrColumnHeader[i][0];
            } else {
                columnHeader = arrColumnHeader[i][0];
            }

        }
        return columnHeader;

    }

    public String getColumnValue() {
        return columnValue;
    }

    public void setColumnValue(String columnValue) {
        this.columnValue = columnValue;
    }

    public String getColumnType() {
        return columnType;
    }

    public void setColumnType(String columnType) {
        this.columnType = columnType;
    }

    public String getColumnTypeSequence() {
        return columnTypeSequence;
    }

    /**
     * This Method will create a Column sequence for a Selected Diagnostic Field
     */
    public void createDiagnosticColumnTypeSequence() {
        if (!(getColumnSequence() == null || ""
                .equalsIgnoreCase(getColumnSequence()))) {
            int lengthOfSequence = getColumnSequence().split(";").length;
            for (int i = 0; i < lengthOfSequence; i++) {
                if ("".equalsIgnoreCase(columnTypeSequence)) {
                    this.columnTypeSequence = ""
                            + ReportConstants.DIAGNOSTIC_TEST;
                } else {
                    this.columnTypeSequence = columnTypeSequence + ";"
                            + ReportConstants.DIAGNOSTIC_TEST;
                }
            }
        } else {
            LOGGER.error("Column Sequence is either empty OR a Null object");
        }

    }

    /**
     * @return The Column Sequence Constructed by this class
     */
    public String getColumnSequence() {
        return columnSequence;
    }

}
