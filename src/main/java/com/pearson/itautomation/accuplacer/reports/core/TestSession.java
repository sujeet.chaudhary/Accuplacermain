package com.pearson.itautomation.accuplacer.reports.core;

/**
 * This class will create Test Session
 * 
 * 
 * @author Deepak Radhakrishnan
 *
 */
public abstract class TestSession {

    private final String exnum;

    public TestSession(String exnum) {
        this.exnum = exnum;
    }

    public String getExnum() {
        return exnum;
    }

}
