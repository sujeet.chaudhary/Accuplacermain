package com.pearson.itautomation.accuplacer.reports.isr;

import java.io.File;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.pearson.itautomation.accuplacer.core.AccuplacerBrowserUtils;
import com.pearson.itautomation.accuplacer.core.Buttons;
import com.pearson.itautomation.bca.Browser;
import com.pearson.itautomation.bca.BrowserAction;
import com.pearson.itautomation.bca.BrowserElement;
import com.pearson.itautomation.bca.SelectorType;
import com.pearson.itautomation.bca.interaction.ClearTextElementAction;
import com.pearson.itautomation.bca.interaction.DropdownSelectionAction;
import com.pearson.itautomation.bca.interaction.LeftClickElementAction;
import com.pearson.itautomation.bca.interaction.SendTextToElementAction;
import com.pearson.itautomation.bca.validation.ElementExistsValidation;

public class SubmitReport extends BrowserAction {
    private static final Logger LOGGER = LoggerFactory
            .getLogger(SubmitReport.class);

    private final String automationQuery;
    private final BrowserElement drpSavedQuery;
    private final BrowserElement btnSubmit;
    private final BrowserElement txtdescription;
    private final BrowserElement optionText;
    private final BrowserElement widgetButton;
    private final BrowserElement widgetClose;
    /**
     * This constant is used for transition waits
     */
    private static final long TIMEOUT = 3000;
    /**
     * This constant is used for Number of attempts to check if the Repor export
     * is downloaded
     */

    private static final int TIMEOUT_WINDOW = 20000;
    private final boolean isOffline;
    private final String submittedDescription;
    private String reportName;
    private File downloadFileName;
    private static String executionTimestamp;

    /**
     * @param browser
     *            Browser instance
     * @param reportName
     *            Name of the Report
     * @param automationQuery
     *            Name of the Saved Query
     * @param isOffline
     *            Offline/Online Report
     * @param executionTimestamp
     *            execution time stamp
     */
    public SubmitReport(Browser browser, String reportName,
            String automationQuery, boolean isOffline, String executionTimestamp) {
        super(browser);
        this.automationQuery = automationQuery;
        drpSavedQuery = browser.getBrowserElement("OR_Reports", "savedQuery");
        this.btnSubmit = Buttons.getButtonType1("Submit");
        this.isOffline = isOffline;
        this.setReportName(reportName);
        this.txtdescription = browser.getBrowserElement("OR_Reports",
                "description");
        SubmitReport.executionTimestamp = executionTimestamp;
        submittedDescription = reportName.replaceAll(" ", "")
                + SubmitReport.executionTimestamp;
        this.optionText = browser.getBrowserElement("OR_Global",
                "optionBySimpleText").formatWithParms(this.automationQuery);
        this.optionText.setTimeout(TIMEOUT_WINDOW);

        widgetButton = browser.getBrowserElement("OR_Reports", "widgetButton");
        widgetClose = browser.getBrowserElement("OR_Reports", "widgetclose");

    }

    public String toString() {
        return String
                .format("SubmitReport [reportName=%s, submittedDescription=%s, isOffline=%s ,automationQuery=%s]",
                        reportName, submittedDescription, isOffline,
                        getAutomationQuery());

    }

    /*
     * (non-Javadoc)
     * 
     * @see java.lang.Object#hashCode()
     */
    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result
                + ((automationQuery == null) ? 0 : automationQuery.hashCode());
        result = prime * result
                + ((btnSubmit == null) ? 0 : btnSubmit.hashCode());
        result = prime
                * result
                + ((downloadFileName == null) ? 0 : downloadFileName.hashCode());
        result = prime * result
                + ((drpSavedQuery == null) ? 0 : drpSavedQuery.hashCode());
        result = prime * result + (isOffline ? 1231 : 1237);
        result = prime * result
                + ((optionText == null) ? 0 : optionText.hashCode());
        result = prime * result
                + ((reportName == null) ? 0 : reportName.hashCode());
        result = prime
                * result
                + ((submittedDescription == null) ? 0 : submittedDescription
                        .hashCode());
        result = prime * result
                + ((txtdescription == null) ? 0 : txtdescription.hashCode());
        result = prime * result
                + ((widgetButton == null) ? 0 : widgetButton.hashCode());
        result = prime * result
                + ((widgetClose == null) ? 0 : widgetClose.hashCode());
        return result;
    }

    /*
     * (non-Javadoc)
     * 
     * @see java.lang.Object#equals(java.lang.Object)
     */
    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (!(obj instanceof SubmitReport)) {
            return false;
        }
        SubmitReport other = (SubmitReport) obj;
        if (automationQuery == null) {
            if (other.automationQuery != null) {
                return false;
            }
        } else if (!automationQuery.equals(other.automationQuery)) {
            return false;
        }
        if (btnSubmit == null) {
            if (other.btnSubmit != null) {
                return false;
            }
        } else if (!btnSubmit.equals(other.btnSubmit)) {
            return false;
        }
        if (downloadFileName == null) {
            if (other.downloadFileName != null) {
                return false;
            }
        } else if (!downloadFileName.equals(other.downloadFileName)) {
            return false;
        }
        if (drpSavedQuery == null) {
            if (other.drpSavedQuery != null) {
                return false;
            }
        } else if (!drpSavedQuery.equals(other.drpSavedQuery)) {
            return false;
        }
        if (isOffline != other.isOffline) {
            return false;
        }
        if (optionText == null) {
            if (other.optionText != null) {
                return false;
            }
        } else if (!optionText.equals(other.optionText)) {
            return false;
        }
        if (reportName == null) {
            if (other.reportName != null) {
                return false;
            }
        } else if (!reportName.equals(other.reportName)) {
            return false;
        }
        if (submittedDescription == null) {
            if (other.submittedDescription != null) {
                return false;
            }
        } else if (!submittedDescription.equals(other.submittedDescription)) {
            return false;
        }
        if (txtdescription == null) {
            if (other.txtdescription != null) {
                return false;
            }
        } else if (!txtdescription.equals(other.txtdescription)) {
            return false;
        }
        if (widgetButton == null) {
            if (other.widgetButton != null) {
                return false;
            }
        } else if (!widgetButton.equals(other.widgetButton)) {
            return false;
        }
        if (widgetClose == null) {
            if (other.widgetClose != null) {
                return false;
            }
        } else if (!widgetClose.equals(other.widgetClose)) {
            return false;
        }
        return true;
    }

    public boolean isOffline() {
        return isOffline;
    }

    public String getSubmittedDescription() {
        return submittedDescription;
    }

    protected boolean perform() {
        boolean result = true;

        result = selectSavedQuery();

        if (result == false) {
            LOGGER.error("Failed Select the Saved Query");
            return false;
        }

        result = submitSavedQuery();
        if (result == false) {
            LOGGER.error("Failed Submit the Saved Query");
            return false;
        }
        // while (new ElementExistsValidation(this.getBrowser(), blockDownload)
        // .performWithStates() && counter < COUNTER_ATTEMPTS) {
        // LOGGER.info("Waiting for report Download");
        //
        // counter++;
        // AccuplacerBrowserUtils.WaitInMilliSeconds(WAIT_FOR_DOWNLOAD);
        //
        // }
        //
        // if (counter >= COUNTER_ATTEMPTS) {
        // LOGGER.info("Download Time Exceed");
        //
        // }
        //
        // if (new ElementExistsValidation(this.getBrowser(), downloadSuccess)
        // .performWithStates()) {
        // LOGGER.info("Report Downloaded successfully");
        // if (!new LeftClickOnModalPopupButton(this.getBrowser(),
        // downloadReportExport).performWithStates()) {
        // LOGGER.error("Unable Click on DOwnload Button");
        // result = false;
        // }
        // if (!new LeftClickOnModalPopupButton(this.getBrowser(), downloadXLS)
        // .performWithStates()) {
        // LOGGER.error("Unable Click on Download XLS");
        // result = false;
        // }
        //
        // }

        String classOFWidget = null;
        if (widgetButton != null) {
            classOFWidget = this.getBrowser().getElementWithWait(widgetButton)
                    .getAttribute("class").toString();
        }

        if (!(classOFWidget == null)) {
            LOGGER.info("Class of Widget Found");
            if (!classOFWidget.equalsIgnoreCase("ng-hide")) {
                LOGGER.info("Widget Found");
                if (new ElementExistsValidation(this.getBrowser(), widgetClose)
                        .performWithStates()) {
                    if (!new LeftClickElementAction(this.getBrowser(),
                            widgetClose).performWithStates()) {
                        LOGGER.error("Unable close the widget");
                        result = false;
                    }
                } else
                    LOGGER.info("Widget is not displayed");

            } else {
                LOGGER.info("Widget is not displayed");
            }

        }

        // }
        /*
         * String interimOption = String.format(OPTIONS_REGEX,
         * selectedValue[i]);
         */
        // optionBranchingProfileFilter = getBrowser()
        // .getBrowserElementWithFormat("OR_Reports", "isrBPOPtions",
        // selectedValue[i]);
        // Actions oAction = new Actions(this.getBrowser().getWebDriver());
        // oAction.keyDown(Keys.CONTROL).click(
        // this.getBrowser().getElementWithWait(
        // optionBranchingProfileFilter));
        // .sendKeys(selectedValue[i]);
        /*
         * this.getBrowser().getElementWithWait(drpBranchingProfileFilter)
         * .sendKeys(selectedValue[i]);
         */

        // optionBranchingProfileFilter = getBrowser()
        // .getBrowserElementWithFormat("OR_Reports",
        // "isrBranchingProfile", interimOption);
        // Actions oAction = new Actions(getBrowser().getWebDriver());
        // System.out.println(optionBranchingProfileFilter.getLocator());
        // oAction.moveToElement(this.getBrowser().getElementWithWait(
        // optionBranchingProfileFilter));
        // oAction.click(this.getBrowser().getElementWithWait(
        // optionBranchingProfileFilter));
        // if (!new SendTextToElementAction(this.getBrowser(),
        // drpBranchingProfileFilter, selectedValue[i])
        // .performWithStates()) {
        // LOGGER.error("Unable select Branching Profile under the filter");
        // result = false;
        // }
        // }

        return result;
    }

    /**
     * @return true if the saved query is successfully Submitted
     */
    private boolean submitSavedQuery() {
        boolean result = true;
        if (isOffline) {
            LOGGER.info("Submitting Offline Report");
            if (!new ClearTextElementAction(this.getBrowser(), txtdescription)
                    .performWithStates()) {
                LOGGER.error("Unable type The report description");
                result = false;
            }
            if (!new SendTextToElementAction(this.getBrowser(), txtdescription,
                    submittedDescription).performWithStates()) {
                LOGGER.error("Unable type The report description");
                result = false;
            }

        }

        if (!new LeftClickElementAction(this.getBrowser(), btnSubmit)
                .performWithStates()) {
            LOGGER.error("Unable Click on Submit");
            result = false;
        }
        return result;

    }

    /**
     * Select the Automation Query
     * 
     * @return true if the saved query is successfully selected
     */
    private boolean selectSavedQuery() {
        AccuplacerBrowserUtils.WaitInMilliSeconds(TIMEOUT);
        boolean result = true;
        if (!new ElementExistsValidation(this.getBrowser(), optionText)
                .performWithStates()) {
            LOGGER.error("Unable find the option " + automationQuery);
            result = false;
        }
        AccuplacerBrowserUtils.WaitInMilliSeconds(TIMEOUT);

        if (!new DropdownSelectionAction(this.getBrowser(), drpSavedQuery,
                SelectorType.VISIBLE_TEXT, automationQuery).performWithStates()) {
            LOGGER.error("Unable select The Saved QUery");
            return false;
        }
        return result;
    }

    /**
     * @return The Name of the Report
     */
    public String getReportName() {
        return reportName;
    }

    /**
     * Set The Name of the Report
     */
    public void setReportName(String reportName) {
        String[] report = reportName.split(",");
        if (report.length > 1) {
            this.reportName = report[1];
        } else {
            this.reportName = reportName;

        }
    }

    public File getDownloadFile() {
        return downloadFileName;
    }

    public void setDownloadFile(File downloadFileName) {
        this.downloadFileName = downloadFileName;
    }

    public String getAutomationQuery() {
        return automationQuery;
    }
}
