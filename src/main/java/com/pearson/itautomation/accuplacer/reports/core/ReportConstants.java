package com.pearson.itautomation.accuplacer.reports.core;

public interface ReportConstants {
    public static final String PLACEMENT_TEST = "Placement";
    public static final String DIAGNOSTIC_TEST = "Diagnostic";
    public static final String STUDENT_INFORMATION = "StudentInformation";
    public static final String PLACEMENT_DIAGNOSTIC_TEST = "PlacementDiagnostic";
    public static final String TEST_SESSSION = "TestSession";
}
