package com.pearson.itautomation.accuplacer.reports.reportqueue;

import java.util.Set;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.pearson.itautomation.accuplacer.core.AccuplacerBrowserUtils;
import com.pearson.itautomation.accuplacer.core.action.LeftClickOnModalPopupButton;
import com.pearson.itautomation.bca.Browser;
import com.pearson.itautomation.bca.BrowserAction;
import com.pearson.itautomation.bca.BrowserElement;

public class ExportExcel extends BrowserAction {
    private static final Logger LOGGER = LoggerFactory
            .getLogger(ExportExcel.class);

    private BrowserElement btnclickOnExport;
    private BrowserElement btnexcel;

    public ExportExcel(Browser browser) {
        super(browser);

        btnclickOnExport = browser.getBrowserElement("OR_Reports",
                "buttonIndex").formatWithParms(1);
        btnexcel = browser.getBrowserElement("OR_Reports", "buttonExcel")
                .formatWithParms(1);
        btnclickOnExport.setTimeout(5000);

    }

    protected boolean perform() {
        boolean result = true;

        if (!new LeftClickOnModalPopupButton(this.getBrowser(),
                btnclickOnExport).performWithStates()) {
            LOGGER.error("Unable Click on Export button");
            return false;
        }
        if (!new LeftClickOnModalPopupButton(this.getBrowser(), btnexcel)
                .performWithStates()) {
            LOGGER.error("Unable Click on Donload Excel Button confirmation");
            result = false;
        }
        Set<String> handles = getBrowser().getWebDriver().getWindowHandles();
        for (String winHandle : handles) {
            getBrowser().switchTo().window(winHandle);
            break;
        }
        AccuplacerBrowserUtils.WaitInMilliSeconds(20000);

        return result;
    }
}
