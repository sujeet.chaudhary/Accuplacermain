package com.pearson.itautomation.accuplacer.reports.core;

import java.util.HashMap;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class DiagnosticReportValidation extends ReportValidateFacory {
    private static final Logger LOGGER = LoggerFactory
            .getLogger(DiagnosticReportValidation.class);
    HashMap<String, TestScores> testScores;
    private final String testName;
    private String expected = null;
    private String actual = null;

    public DiagnosticReportValidation(String currentExnum,
            String typeOfCurrentColumn, String valueOfCurrentColumn,
            ReadExcelReport reportToBeVerified, int columnIndex,
            HashMap<String, TestScores> testScores) {
        super(currentExnum, typeOfCurrentColumn, valueOfCurrentColumn,
                columnIndex, reportToBeVerified);
        this.testScores = testScores;
        this.testName = getValueOfCurrentColumn();

    }

    @Override
    public boolean evaluateReportFields() {
        boolean validatedScore = true;
        if (testScores.get(getCurrentExnum()).isDiagnosticScoresPresent()) {
            LOGGER.debug("Verify Diagnostic Score");
            String diagnosticScoreInExcel = getReportToBeVerified()
                    .getArrReportExcel()[getReportToBeVerified()
                    .getExnumPosition(getCurrentExnum())][getColumnIndex()];
            String diagnosticScoreInDB = testScores.get(getCurrentExnum())
                    .getDiagnosticScore(testName);
            if (testScores.get(getCurrentExnum()).getDiagnosticTestsMap()
                    .containsKey(testName)) {
                LOGGER.debug("TestName : " + testName + "Test Score"
                        + diagnosticScoreInDB);
                LOGGER.debug("--" + diagnosticScoreInExcel + " "
                        + Math.round(Double.parseDouble(diagnosticScoreInDB)));
                LOGGER.info("Exnum " + getCurrentExnum() + " TestName :  "
                        + testName + ACTUAL_SCORE_MESSAGE
                        + diagnosticScoreInExcel + EXPECTED_VALUE_MESSAGE
                        + Math.round(Double.parseDouble(diagnosticScoreInDB)));
                try {
                    validatedScore = Double.parseDouble(diagnosticScoreInExcel) == Math
                            .round(Double.parseDouble(diagnosticScoreInDB));
                } catch (Exception e) {
                    LOGGER.error("Exception occured while comparing "
                            + diagnosticScoreInExcel + " and"
                            + diagnosticScoreInDB, e);
                    expected = diagnosticScoreInDB;
                    actual = diagnosticScoreInExcel;
                    return false;
                }

            }
        }
        return validatedScore;
    }

    @Override
    public String returnValidationMessage() {
        return "Comparing Strand Scores for Test - " + testName + " , Exnum :"
                + getCurrentExnum();
    }

    @Override
    public String failedValidationMessage() {
        return String
                .format("Expected Diagnostic Score (for exnum =%s) is '%s' Actual Score = '%s'",
                        getCurrentExnum(), expected, actual);
    }
}
