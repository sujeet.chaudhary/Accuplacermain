package com.pearson.itautomation.accuplacer.reports.core;

import java.sql.Connection;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.pearson.itautomation.accuplacer.core.DBUtils;

/**
 * This class will create Student Object based on exnum as input
 * 
 * @author Deepak Radhakrishnan
 *
 */
public class Student extends TestSession {
    private static final Logger LOGGER = LoggerFactory.getLogger(Student.class);

    private String[] studentInfo;
    private String address1;
    private String address2;
    private String dob;
    private String city;
    private String studentID;
    private String zIP;
    private String firstName;
    private String LastName;
    private String query = "select %s from student where student_id_pk = (select student_id_pk from test_session where test_session_id = %s)";
    private static final String ADDRESS1 = "address_1";
    private static final String ADDRESS2 = "address_2";
    private static final String DOB = "dob";
    private static final String CITY = "city";
    private static final String STUDENT_ID = "student_id";
    private static final String ZIP = "zip";
    private static final String FIRST_NAME = "first_name";
    private static final String LAST_NAME = "last_name";
    private static final String STUDENT_COLUMNS = ADDRESS1 + "," + ADDRESS2
            + "," + DOB + "," + CITY + "," + STUDENT_ID + "," + ZIP + ","
            + FIRST_NAME + "," + LAST_NAME;
    private final String[] columns;

    public Student(Connection con, String exnum) {
        super(exnum);
        DBUtils dbUtils = new DBUtils(con);
        this.columns = STUDENT_COLUMNS.split(",");
        this.query = String.format(query, STUDENT_COLUMNS, getExnum());
        this.studentInfo = dbUtils.runAndReturnNTHRecord(query, 1);
        createStudentInfo();
    }

    /**
     * This method will assign Database array of student if to the fields of
     * Student Object
     *
     * @author Deepak Radhakrishnan
     */
    public void createStudentInfo() {
        if (studentInfo == null) {
            LOGGER.warn(String.format("No Student found for the exnum - %s",
                    getExnum()));
        } else {
            setAddress1(studentInfo[getPosition(columns, ADDRESS1)]);
            setAddress2(studentInfo[getPosition(columns, ADDRESS2)]);
            setDob(studentInfo[getPosition(columns, DOB)]);
            setCity((studentInfo[getPosition(columns, CITY)]));
            setStudentID((studentInfo[getPosition(columns, STUDENT_ID)]));
            setZIP((studentInfo[getPosition(columns, ZIP)]));
            setFirstName((studentInfo[getPosition(columns, FIRST_NAME)]));
            setLastName((studentInfo[getPosition(columns, LAST_NAME)]));
        }

    }

    public int getPosition(String[] arr, String value) {
        int position = -1;
        for (int i = 0; i < arr.length; i++) {
            if (arr[i].equals(value)) {
                position = i;
                break;
            }
        }
        return position;
    }

    // GETTERS and SETTERS

    public String[] getStudentInfo() {
        return studentInfo;
    }

    /**
     * Set the Student information into an array
     * 
     * @param studentInfo
     */
    public void setStudentInfo(String[] studentInfo) {
        this.studentInfo = studentInfo;
    }

    /**
     * @return Address1 column of the Student
     */
    public String getAddress1() {
        return address1;
    }

    /**
     * Set the address1 of the student
     * 
     * @param address1
     */
    public void setAddress1(String address1) {
        this.address1 = address1;
    }

    /**
     * @return Address2 column of the Student
     */
    public String getAddress2() {
        return address2;
    }

    /**
     * Set the address2 of the student
     * 
     * @param address2
     */
    public void setAddress2(String address2) {
        this.address2 = address2;
    }

    /**
     * @return Date of Birth column of the Student
     */
    public String getDob() {
        return dob;
    }

    /**
     * Set the Date of Birth of the student
     * 
     * @param dob
     */
    public void setDob(String dob) {
        this.dob = dob;
    }

    /**
     * @return City column of the Student
     */
    public String getCity() {
        return city;
    }

    /**
     * Set the city of the student
     * 
     * @param city
     */
    public void setCity(String city) {
        this.city = city;
    }

    /**
     * @return Student ID column of the Student
     */
    public String getStudentID() {
        return studentID;
    }

    /**
     * Set the Student ID of the student
     * 
     * @param studentID
     */
    public void setStudentID(String studentID) {
        this.studentID = studentID;
    }

    /**
     * @return ZIP CODE column of the Student
     */
    public String getZIP() {
        return zIP;
    }

    /**
     * Set the ZIP Code of the student
     * 
     * @param zIP
     */
    public void setZIP(String zIP) {
        this.zIP = zIP;
    }

    /**
     * @return First Name column of the Student
     */
    public String getFirstName() {
        return firstName;
    }

    /**
     * Set the First Name of the student
     * 
     * @param firstName
     */
    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    /**
     * @return Last Name column of the Student
     */
    public String getLastName() {
        return LastName;
    }

    /**
     * Set the Last Name of the student
     * 
     * @param lastName
     */
    public void setLastName(String lastName) {
        LastName = lastName;
    }

}
