package com.pearson.itautomation.accuplacer.reports.isr;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.pearson.itautomation.accuplacer.core.AccuplacerBrowserUtils;
import com.pearson.itautomation.accuplacer.core.Buttons;
import com.pearson.itautomation.accuplacer.core.action.LeftClickOnModalPopupButton;
import com.pearson.itautomation.bca.Browser;
import com.pearson.itautomation.bca.BrowserAction;
import com.pearson.itautomation.bca.BrowserElement;
import com.pearson.itautomation.bca.validation.ElementExistsValidation;

public class DownloadReport extends BrowserAction {
    private static final Logger LOGGER = LoggerFactory
            .getLogger(DownloadReport.class);

    /**
     * This constant is used for transition waits
     */
    private static final long TIMEOUT = 3000;
    private final String submittedDescription;
    private BrowserElement reportInQueue;
    private BrowserElement downloadReportExport;
    private BrowserElement btnRefresh;
    private String status = "";
    private BrowserElement reportStatusInQueue;

    public DownloadReport(Browser browser, String submittedDescription) {
        super(browser);
        this.submittedDescription = submittedDescription;
        this.reportInQueue = browser.getBrowserElementWithFormat("OR_Reports",
                "reportInQueue", submittedDescription);
        this.reportStatusInQueue = browser.getBrowserElementWithFormat(
                "OR_Reports", "reportStatusInQueue", submittedDescription);
        this.downloadReportExport = browser.getBrowserElementWithFormat(
                "OR_Reports", "downloadButton", submittedDescription);

        this.btnRefresh = Buttons.getButtonType5("Refresh");
    }

    public String getSubmittedDescription() {
        return submittedDescription;
    }

    protected boolean perform() {
        boolean result = true;
        AccuplacerBrowserUtils.WaitInMilliSeconds(TIMEOUT);

        if (new ElementExistsValidation(this.getBrowser(), reportInQueue)
                .performWithStates()) {
            LOGGER.info("Report is Present in the Queue ");
            int counter = 0;
            do {

                if (reportStatusInQueue != null) {
                    status = this.getBrowser()
                            .getElementWithWait(reportStatusInQueue).getText();
                    if (status != null) {
                        LOGGER.info("Checking Status");
                        if (status.equalsIgnoreCase("Completed")) {
                            result = true;
                            if (!new LeftClickOnModalPopupButton(
                                    this.getBrowser(), downloadReportExport)
                                    .performWithStates()) {
                                LOGGER.error("Unable Click on Download Button");
                                result = false;
                            } else {
                                AccuplacerBrowserUtils.WaitInMilliSeconds(5000);
                                LOGGER.info(" File Downloaded");
                            }
                            break;
                        } else {
                            counter++;
                            LOGGER.info("Status of '" + submittedDescription
                                    + "' report = " + status
                                    + "Refreshing Page");
                            AccuplacerBrowserUtils.WaitInMilliSeconds(5000);
                            if (!new LeftClickOnModalPopupButton(
                                    this.getBrowser(), btnRefresh)
                                    .performWithStates()) {
                                LOGGER.error("Unable Click on Refresh Button");
                                result = false;
                            }
                        }
                    } else {
                        LOGGER.error("Status  not Found");
                        return false;
                    }

                } else {
                    LOGGER.error("Status Element not Found");
                    return false;
                }
            } while (counter < 4);
        } else {
            LOGGER.error("Report not Found in the Queue");
            result = false;
        }

        return result;
    }
}
