package com.pearson.itautomation.accuplacer.reports.core;

import java.sql.Connection;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.pearson.itautomation.accuplacer.core.DBUtils;

/**
 * This class will create Test Scores Object based on exnum as input, generating <br>
 * 1. Placement Scores<br>
 * 2. Diagnostic Scores
 * 
 * 
 * @author Deepak Radhakrishnan
 *
 */
public class TestScores extends TestSession {
    private static final Logger LOGGER = LoggerFactory
            .getLogger(TestScores.class);

    private String[][] placementTests;
    private String[][] diagnosticTests;
    private final HashMap<String, String> diagnosticTestsMap;
    private final HashMap<String, String> placementTestsMap;
    private String diagnosticQuery = "select td.test_name, ssd.score from strand_Session_detail ssd, test_detail td where ssd.test_session_detail_id in (select tsd.test_session_detail_id from test_session ts, test_Session_detail tsd where tsd.test_session_id = ts.test_session_id and tsd.placement_type in (1,3) and ts.test_session_id = %s) and td.test_detail_id = ssd.test_detail_id";
    private String placementQuery = "select td.test_name, tsd.score from test_session ts, test_Session_detail tsd, test_detail td where td.test_detail_id = tsd.test_detail_id and tsd.test_session_id = ts.test_session_id and ts.test_session_id = %s";
    private boolean isPlacementScoresPresent = false;
    private boolean isDiagnosticScoresPresent = false;

    public TestScores(Connection con, String exnum) {
        super(exnum);
        DBUtils dbUtils = new DBUtils(con);
        this.placementQuery = String.format(placementQuery, getExnum());
        this.diagnosticQuery = String.format(diagnosticQuery, getExnum());
        placementTests = dbUtils
                .runAndReturnMultiDimentionalArray(placementQuery);
        diagnosticTests = dbUtils
                .runAndReturnMultiDimentionalArray(diagnosticQuery);
        if (placementTests == null) {
            LOGGER.warn(String.format("No Placement Tests Available for exnum"
                    + getExnum()));
            this.placementTestsMap = null;

        } else {
            LOGGER.info(String.format("Placement Tests ARE Available for exnum"
                    + getExnum()));
            setPlacementScoresPresent(true);
            this.placementTestsMap = convertArrayToMap(placementTests);
        }
        if (diagnosticTests == null) {
            LOGGER.warn(String.format("No Diagnostic Tests Available for exnum"
                    + getExnum()));
            this.diagnosticTestsMap = null;
        } else {
            setDiagnosticScoresPresent(true);
            LOGGER.info(String
                    .format("Diagnostic Tests ARE Available for exnum"
                            + getExnum()));
            this.diagnosticTestsMap = convertArrayToMap(diagnosticTests);
        }

        /*
         * displayHasmMap(diagnosticTestsMap);
         * displayHasmMap(placementTestsMap);
         */

    }

    public void displayHasmMap(HashMap<String, String> paramHMap) {

        HashMap<String, String> hmap = paramHMap;
        Iterator<Map.Entry<String, String>> iterator = hmap.entrySet()
                .iterator();

        while (iterator.hasNext()) {
            Map.Entry<String, String> entry = iterator.next();
            LOGGER.info(entry.getKey() + ":" + entry.getValue());
        }
    }

    public HashMap<String, String> convertArrayToMap(String[][] arr) {
        HashMap<String, String> hmap = new HashMap<String, String>();
        for (int i = 0; i < arr.length; i++) {
            if (!(arr[i][1] == null)) {
                hmap.put(arr[i][0].trim(), arr[i][1].trim());
            } else {
                LOGGER.warn("Null Scores found in DB");
            }
        }
        return hmap;
    }

    // GETTERS and SETTERS

    public String getPlacementScore(String testName) {
        if (getPlacementTestsMap() == null) {
            LOGGER.error("No Placement scores available for the exnum "
                    + getExnum());
            return null;
        }
        LOGGER.info("size of map" + getPlacementTestsMap().size());
        if (getPlacementTestsMap().containsKey(testName)) {
            LOGGER.info("Placement scores Found");
            return getPlacementTestsMap().get(testName);

        } else {
            LOGGER.warn(String
                    .format("No Placement score for the '%s' test available under the exnum %s",
                            testName, getExnum()));
            return null;
        }

    }

    public String getDiagnosticScore(String testName) {
        if (getDiagnosticTestsMap() == null) {
            LOGGER.error("No Diagnostic scores available for the exnum "
                    + getExnum());
            return null;
        }
        LOGGER.info("size of map" + getDiagnosticTestsMap().size());
        if (getDiagnosticTestsMap().containsKey(testName)) {
            LOGGER.info("Diagnostic scores Found");
            return getDiagnosticTestsMap().get(testName);

        } else {
            LOGGER.warn(String
                    .format("No Diagnostic score for the '%s' test available under the exnum %s",
                            testName, getExnum()));
            return null;
        }

    }

    public HashMap<String, String> getDiagnosticTestsMap() {
        return this.diagnosticTestsMap;
    }

    public HashMap<String, String> getPlacementTestsMap() {
        return this.placementTestsMap;
    }

    /**
     * @return the isPlacementScoresPresent implies if the placement scores are
     *         present for the given object
     */
    public boolean isPlacementScoresPresent() {
        return isPlacementScoresPresent;
    }

    /**
     * @param isPlacementScoresPresent
     *            the isPlacementScoresPresent to set placement score flag
     */
    public void setPlacementScoresPresent(boolean isPlacementScoresPresent) {
        this.isPlacementScoresPresent = isPlacementScoresPresent;
    }

    /**
     * @return the isDiagnosticScoresPresent implies if the Diagnostic scores
     *         are present for the given object
     */
    public boolean isDiagnosticScoresPresent() {
        return isDiagnosticScoresPresent;
    }

    /**
     * @param isDiagnosticScoresPresent
     *            the isDiagnosticScoresPresent to set Diagnostic score flag
     */
    public void setDiagnosticScoresPresent(boolean isDiagnosticScoresPresent) {
        this.isDiagnosticScoresPresent = isDiagnosticScoresPresent;
    }
}
