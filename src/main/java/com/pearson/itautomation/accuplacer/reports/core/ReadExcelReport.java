package com.pearson.itautomation.accuplacer.reports.core;

import java.io.FileInputStream;
import java.util.Iterator;

import org.apache.poi.hssf.usermodel.HSSFCell;
import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.poifs.filesystem.POIFSFileSystem;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * The Class acts as excel utility for reports verification
 * 
 * @author Deepak Radhakrishnan
 *
 */
public class ReadExcelReport {

    private String[][] arrReportExcel;
    private final String fileName;
    private String sheetName;
    private static final Logger LOGGER = LoggerFactory
            .getLogger(ReadExcelReport.class);

    /**
     * @param fileName
     *            Path of the excel
     */
    public ReadExcelReport(String fileName) {
        this.fileName = fileName;
    }

    /**
     * @param fileName
     *            Path of the excel
     * @param sheetName
     *            Name of the sheet
     */
    public ReadExcelReport(String fileName, String sheetName) {
        this(fileName);
        this.sheetName = sheetName;
        if (arrReportExcel == null) {
            LOGGER.warn("Array is Not Set, Calling the setter method");
            setArrReportExcel();
        }
    }

    /**
     * @return The excel file as a Multidimentional Array
     */
    public String[][] getArrReportExcel() {
        if (arrReportExcel == null) {
            LOGGER.warn("Array is Not Set, Calling the setter method");
            setArrReportExcel();
        }
        return arrReportExcel;
    }

    /**
     * @param columnNo
     *            nth column starting from 0
     * @return
     */
    public String[] getNthColumn(int columnNo) {
        String[] arrColumn;
        if (arrReportExcel == null) {
            LOGGER.warn("Array is Not Set, Calling the setter method");
            setArrReportExcel();
        }
        arrColumn = new String[arrReportExcel.length];
        for (int i = 0; i < arrReportExcel.length; i++) {
            arrColumn[i] = arrReportExcel[i][columnNo];
        }
        return arrColumn;
    }

    /**
     * This method will create a Multi dimentional array from the excel
     */
    public void setArrReportExcel() {

        try {
            /** Creating Input Stream **/

            FileInputStream myInput = new FileInputStream(fileName);

            /** Create a POIFSFileSystem object **/
            POIFSFileSystem myFileSystem = new POIFSFileSystem(myInput);

            /** Create a workbook using the File System **/
            HSSFWorkbook myWorkBook = new HSSFWorkbook(myFileSystem);

            /** Get the first sheet from workbook **/

            /** We now need something to iterate through the cells. **/
            HSSFSheet mySheet;
            if (sheetName == null) {

                /** read the First Sheet **/
                mySheet = myWorkBook.getSheetAt(0);

            } else {
                mySheet = myWorkBook.getSheet(sheetName);
            }
            Iterator<Row> rowIter = mySheet.rowIterator();
            int rowSheet = 0;
            int colSheet = 0;
            /**
             * --Define a MultiDimentional array --Holds Vectors Of Cells
             */
            String[][] temp = new String[getRowCount(mySheet)][getColumnCount(mySheet)];
            while (rowIter.hasNext()) {
                colSheet = 0;
                HSSFRow myRow = (HSSFRow) rowIter.next();
                if (myRow.getCell(0) != null) {
                    Iterator<Cell> cellIter = myRow.cellIterator();
                    while (cellIter.hasNext()) {
                        HSSFCell myCell = (HSSFCell) cellIter.next();

                        temp[rowSheet][colSheet] = myCell.toString().trim();
                        colSheet++;
                    }
                    rowSheet++;
                }
            }
            arrReportExcel = temp;

        } catch (Exception e) {
            LOGGER.error("Exception occured where reading the workbook", e);
        }

    }

    /**
     * 
     * @param sheet
     * @return Count of Number of rows in the excel
     */
    public static int getRowCount(final HSSFSheet sheet) {
        int count = 0;

        Iterator<Row> rowIterator = sheet.iterator();
        while (rowIterator.hasNext()) {
            count++;
            Row row = rowIterator.next();
            Iterator<Cell> cellIterator = row.cellIterator();
            while (cellIterator.hasNext()) {
                cellIterator.next();

            }
        }
        return count;
    }

    /**
     * @param sheet
     * @return Count of Number of columns in the excel
     */
    public static int getColumnCount(final HSSFSheet sheet) {
        int count = 0;
        Iterator<Row> rowIterator = sheet.iterator();
        Row row = rowIterator.next();
        Iterator<Cell> cellIterator = row.cellIterator();
        while (cellIterator.hasNext()) {
            count++;
            cellIterator.next();

        }
        return count;
    }

    /**
     * This Method will display 1-D array
     */
    public void displayArray(String[][] arr) {
        for (int i = 0; i < arr.length; i++) {
            for (int j = 0; j < arr[i].length; j++) {
                System.out.print(arr[i][j] + "|");
            }
            System.out.println();
        }
    }

    /**
     * This Method will display 2-D array
     */
    public void displayArray(String[] arr) {
        for (int j = 0; j < arr.length; j++) {
            System.out.print(arr[j] + "|");
        }
        System.out.println();
    }

    /**
     * This Method return the header i.e the first row of the excel
     */
    public String[] getHeader() {
        if (arrReportExcel == null) {
            LOGGER.warn("Array is Not Set, Calling the setter method");
            setArrReportExcel();
        }
        return arrReportExcel[0];
    }

    public int getExnumPosition(String exnum) {
        String[][] arr = getArrReportExcel();
        boolean exnumFound = false;
        for (int i = 0; i < arr.length; i++) {
            if (arr[i][0].equals(exnum)) {
                exnumFound = true;
                LOGGER.info("Exnum Found At " + i);
                return i;
            }
        }
        if (!exnumFound) {
            LOGGER.info("Exnum NOT Found ");
        }
        return -1;
    }

}
