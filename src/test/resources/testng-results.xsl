<?xml version="1.0" encoding="UTF-8"?>

<xsl:stylesheet version="2.0"
  xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
  xmlns:xs="http://www.w3.org/2001/XMLSchema">

<xsl:template match="/">
	<xsl:variable name="totalCount" select="//testng-results/@total"/>
	<xsl:variable name="passedCount" select="//testng-results/@passed"/>
	<xsl:variable name="skippedCount" select="//testng-results/@skipped"/>
	<xsl:variable name="failedCount" select="//testng-results/@failed"/>
	
	<xsl:variable name="failedColor">#F75959</xsl:variable>
	<xsl:variable name="skippedColor">#85F2F2</xsl:variable>
	<xsl:variable name="passedColor">#AFF576</xsl:variable>
	
	<xsl:variable name="failureReason">TNFAILUREREASON - </xsl:variable>
	<xsl:variable name="descriptionReason">TNDESCRIPTION - </xsl:variable>
	<xsl:variable name="noteReason">TNNOTE - </xsl:variable>
	<xsl:variable name="parmDesc">TNPARMDESC - </xsl:variable>
	<xsl:variable name="suiteInfo">TNSUITEINFO - </xsl:variable>	
	<xsl:variable name="errorText">ERROR</xsl:variable>
	<html>
		<head>
		</head>
		<body>
			<table width="90%" align="center"><tr><td>
					<table width="100%" style="border:2px solid black;border-collapse:collapse;">
					<tbody>
						<tr><th colspan="4" style="padding:7px;border:2px solid black;text-align:center;vertical-algin:middle"><H1>Summary for <xsl:value-of select="testng-results/suite/@name" /></H1></th></tr>
						<tr>
							<th bgcolor="#C6C9C1" style="padding:7px;border:2px solid black;text-align:center">Total</th>
							<th bgcolor="{$passedColor}" style="padding:7px;border:2px solid black;text-align:center">Passed</th>
							<th bgcolor="{$skippedColor}" style="padding:7px;border:2px solid black;text-align:center">Skipped</th>
							<th bgcolor="{$failedColor}" style="padding:7px;border:2px solid black;text-align:center">Failed</th>
						</tr>
						<tr>
							<td style="padding:7px;border:2px solid black;text-align:center"><xsl:value-of select="$totalCount"/></td>
							<td style="padding:7px;border:2px solid black;text-align:center"><a href="#passed"><xsl:value-of select="$passedCount"/></a></td>
							<td style="padding:7px;border:2px solid black;text-align:center"><a href="#skipped"><xsl:value-of select="$skippedCount"/></a></td>
							<td style="padding:7px;border:2px solid black;text-align:center"><a href="#failed"><xsl:value-of select="$failedCount"/></a></td>
						</tr>
					</tbody>
				</table>
				<br />
				<table width="100%" style="border:2px solid black;border-collapse:collapse;">
					<tbody>
						<tr>
							<td style="padding:7px;border:2px solid black;text-align:center;vertical-algin:middle;" colspan="2"><B>Suite Information</B></td>
						</tr>
						<tr>
							<td style="padding:7px;border:2px solid black;">Time started:</td>
							<td style="padding:7px;border:2px solid black;"><xsl:value-of select="format-dateTime(adjust-dateTime-to-timezone(//testng-results/suite/@started-at), '[MNn] [Do], [Y] - [H]:[m]:[s] [z]')" /></td>
						</tr>
						<tr>
							<td style="padding:7px;border:2px solid black;">Time completed:</td>
							<td style="padding:7px;border:2px solid black;"><xsl:value-of select="format-dateTime(adjust-dateTime-to-timezone(//testng-results/suite/@finished-at), '[MNn] [Do], [Y] - [H]:[m]:[s] [z]')" /></td>
						</tr>
						<tr>
							<td style="padding:7px;border:2px solid black;">Duration:</td>
							<td style="padding:7px;border:2px solid black;"><xsl:value-of select="format-number(//testng-results/suite/@duration-ms div 1000, '0.0')"/> seconds</td>
						</tr>
						<xsl:if test="count(testng-results/reporter-output/line[contains(text(), $suiteInfo)]) > 0">
							<tr>
								<td style="padding:7px;border:2px solid black;" colspan="2">
									<ul>
										<xsl:for-each select="testng-results/reporter-output/line[contains(text(), $suiteInfo)]">
											<li><xsl:value-of select="substring-after(text(), $suiteInfo)" disable-output-escaping="yes" /></li>
										</xsl:for-each>
									</ul>
								</td>
							</tr>
						</xsl:if>
					</tbody>
				</table>
				<br />
				<!--
					Start of failed test cases
				-->
				<table width="100%" style="border:2px solid {$failedColor};border-collapse:collapse;">
					<tbody>
						<tr>
							<td bgcolor="{$failedColor}" style="padding:7px;font-weight:bold;"><a name="failed">Failed Test Cases (<xsl:value-of select="$failedCount"/>)</a></td>
							<td bgcolor="{$failedColor}" style="padding:7px;text-align:right;"><a href="#" style="color:black;">Back To Top</a></td>
						</tr>
					</tbody>
				</table>
				<table width="100%" style="border:2px solid {$failedColor};border-collapse:collapse;">
					<tbody>
						<xsl:choose>
							<xsl:when test="$failedCount &gt; 0">
								<xsl:for-each select="//*/test-method[@status='FAIL']">
									<tr>
										<td style="border:2px solid {$failedColor};padding:7px;border-bottom-style:none;"><xsl:value-of select="position()" /><br/>&#160;<br/>&#160;</td><td style="border:2px solid {$failedColor};padding:7px;"><b><xsl:value-of select="tokenize(../@name, '\.')[last()]" /> - <xsl:value-of select
="@name" /></b><br/><b>Start time:&#160;</b> <xsl:value-of select="format-dateTime(@started-at, '[MNn] [Do], [Y] - [H]:[m]:[s]')" /><br/><b>Execution duration:&#160;</b> <xsl:value-of select="format-number(@duration-ms div 1000, '0.0')"/> seconds</td>
									</tr>
									<tr>
										<td>&#160;</td>
										<td style="border:2px solid {$failedColor};padding:7px;border-style:solid solid none;">
											<b>Description:&#160;</b>
												<xsl:for-each select="reporter-output/line[contains(text(), $descriptionReason)]">
													<xsl:value-of select="substring-after(text(), $descriptionReason)" disable-output-escaping="yes" />
												</xsl:for-each>
										</td>
									</tr>
									<tr>
										<td>&#160;</td>
										<td style="border:2px solid {$failedColor};padding:7px;"><b>Reason(s) for failure:</b>
											<ol>
												<xsl:for-each select="reporter-output/line[contains(text(), $failureReason)]">
													<li><xsl:value-of select="substring-after(text(), $failureReason)" disable-output-escaping="yes" /></li>
												</xsl:for-each>
												<li><xsl:value-of select="exception/message" /></li>
											</ol>
										</td>
									</tr>
									<xsl:if test="count(params/param) > 0">
										<tr>
											<td>&#160;</td>
											<td style="border:2px solid {$failedColor};padding:7px;border-style:solid solid none;"><b>Parameters</b><ol>
												<xsl:for-each select="params/param">
													<li>
														<xsl:variable name="parmpos" select="position()" />
														<xsl:value-of select="substring-after((../../reporter-output/line[contains(text(), $parmDesc)])[$parmpos], $parmDesc)" disable-output-escaping="yes"/>:&#160;
														<xsl:choose>
															<xsl:when test="value/@is-null = true()">NULL</xsl:when>
															<xsl:otherwise><xsl:value-of select="value" /></xsl:otherwise>
														</xsl:choose>
													</li>
												</xsl:for-each>
											</ol></td>
										</tr>
									</xsl:if>
									<xsl:if test="count(reporter-output/line[contains(text(), $noteReason)]) > 0">
										<tr>
											<td>&#160;</td>
											<td style="border:2px solid {$failedColor};padding:7px;border-style:solid solid none;"><b>Notes</b>
												<ol>
													<xsl:for-each select="reporter-output/line[contains(text(), $noteReason)]">
														<li><xsl:value-of select="substring-after(text(), $noteReason)" disable-output-escaping="yes" /></li>
													</xsl:for-each>
												</ol>
											</td>
										</tr>
									</xsl:if>
									<xsl:if test="count(reporter-output/line[contains(upper-case(text()), $errorText)]) > 0">
										<tr>
											<td>&#160;</td>
											<td style="border:2px solid {$failedColor};padding:7px;border-style:solid solid none;"><b>Errors</b>
												<ol>
													<xsl:for-each select="reporter-output/line[contains(upper-case(text()), $errorText)]">
														<li><xsl:value-of select="text()" disable-output-escaping="yes" /></li>
													</xsl:for-each>
												</ol>
											</td>
										</tr>
									</xsl:if>
									<tr>
										<td colspan="2" bgcolor="{$failedColor}">&#160;</td>
									</tr>
								</xsl:for-each>
							</xsl:when>
							<xsl:otherwise>
								<tr>
									<td style="border:2px solid {$failedColor};padding:7px;">No test cases failed.</td>
								</tr>
							</xsl:otherwise>
						</xsl:choose>
					</tbody>
				</table>
				<br />
				<!--
					Start of skipped test cases
				-->
				<table width="100%" style="border:2px solid {$skippedColor};border-collapse:collapse;">
					<tbody>
						<tr>
							<td bgcolor="{$skippedColor}" style="padding:7px;font-weight:bold;"><a name="skipped">Skipped Test Cases (<xsl:value-of select="$skippedCount"/>)</a></td>
							<td bgcolor="{$skippedColor}" style="padding:7px;text-align:right;"><a href="#" style="color:black;">Back To Top</a></td>
						</tr>
					</tbody>
				</table>
				<table width="100%" style="border:2px solid {$skippedColor};border-collapse:collapse;">
					<tbody>
						<xsl:choose>
							<xsl:when test="$skippedCount &gt; 0">
								<xsl:for-each select="//*/test-method[@status='SKIP']">
									<tr>
										<td style="border:2px solid {$skippedColor};padding:7px;border-bottom-style:none;"><xsl:value-of select="position()" /><br/>&#160;<br/>&#160;</td><td style="border:2px solid {$skippedColor};padding:7px;"><b><xsl:value-of select="tokenize(../@name, '\.')[last()]" /> - <xsl:value-of select
="@name" /></b><br/><b>Start time:&#160;</b> <xsl:value-of select="format-dateTime(@started-at, '[MNn] [Do], [Y] - [H]:[m]:[s]')" /><br/><b>Execution duration:&#160;</b> <xsl:value-of select="format-number(@duration-ms div 1000, '0.0')"/> seconds</td>
									</tr>
									<tr>
										<td colspan="2" bgcolor="{$skippedColor}">&#160;</td>
									</tr>
								</xsl:for-each>
							</xsl:when>
							<xsl:otherwise>
								<tr>
									<td style="border:2px solid {$skippedColor};padding:7px;">No test cases skipped</td>
								</tr>
							</xsl:otherwise>
						</xsl:choose>
					</tbody>
				</table>
				<br />
				<!--
					Start of passed test cases
				-->
				<table width="100%" style="border:2px solid {$passedColor};border-collapse:collapse;">
					<tbody>
						<tr>
							<td bgcolor="{$passedColor}" style="padding:7px;font-weight:bold;"><a name="passed">Passed Test Cases (<xsl:value-of select="$passedCount"/>)</a></td>
							<td bgcolor="{$passedColor}" style="padding:7px;text-align:right;"><a href="#" style="color:black;">Back To Top</a></td>
						</tr>
					</tbody>
				</table>
				<table width="100%" style="border:2px solid {$passedColor};border-collapse:collapse;">
					<tbody>
						<xsl:choose>
							<xsl:when test="$passedCount &gt; 0">
								<xsl:for-each select="//*/test-method[@status='PASS' and not(@is-config)]">
									<tr>
										<td style="border:2px solid {$passedColor};padding:7px;border-bottom-style:none;"><xsl:value-of select="position()" /><br/>&#160;<br/>&#160;</td><td style="border:2px solid {$passedColor};padding:7px;"><b><xsl:value-of select="tokenize(../@name, '\.')[last()]" /> - <xsl:value-of select
="@name" /></b><br/><b>Start time:&#160;</b> <xsl:value-of select="format-dateTime(@started-at, '[MNn] [Do], [Y] - [H]:[m]:[s]')" /><br/><b>Execution duration:&#160;</b> <xsl:value-of select="format-number(@duration-ms div 1000, '0.0')"/> seconds</td>
									</tr>
									<tr>
										<td>&#160;</td>
										<td style="border:2px solid {$passedColor};padding:7px;border-style:solid solid none;">
											<b>Description:&#160;</b>
												<xsl:for-each select="reporter-output/line[contains(text(), $descriptionReason)]">
													<xsl:value-of select="substring-after(text(), $descriptionReason)" disable-output-escaping="yes" />
												</xsl:for-each>
										</td>
									</tr>
									<xsl:if test="count(params/param) > 0">
										<tr>
											<td>&#160;</td>
											<td style="border:2px solid {$passedColor};padding:7px;border-style:solid solid none;"><b>Parameters</b><ol>
												<xsl:for-each select="params/param">
													<li>
														<xsl:variable name="parmpos" select="position()" />
														<xsl:value-of select="substring-after((../../reporter-output/line[contains(text(), $parmDesc)])[$parmpos], $parmDesc)" disable-output-escaping="yes"/>:&#160;
														<xsl:choose>
															<xsl:when test="value/@is-null = true()">NULL</xsl:when>
															<xsl:otherwise><xsl:value-of select="value" /></xsl:otherwise>
														</xsl:choose>
													</li>
												</xsl:for-each>
											</ol></td>
										</tr>
									</xsl:if>
									<xsl:if test="count(reporter-output/line[contains(text(), $noteReason)]) > 0">
										<tr>
											<td>&#160;</td>
											<td style="border:2px solid {$passedColor};padding:7px;border-style:solid solid none;"><b>Notes</b>
												<ol>
													<xsl:for-each select="reporter-output/line[contains(text(), $noteReason)]">
														<li><xsl:value-of select="substring-after(text(), $noteReason)" disable-output-escaping="yes" /></li>
													</xsl:for-each>
												</ol>
											</td>
										</tr>
									</xsl:if>
									<xsl:if test="count(reporter-output/line[contains(upper-case(text()), $errorText)]) > 0">
										<tr>
											<td>&#160;</td>
											<td style="border:2px solid {$passedColor};padding:7px;border-style:solid solid none;"><b>Errors</b>
												<ol>
													<xsl:for-each select="reporter-output/line[contains(upper-case(text()), $errorText)]">
														<li><xsl:value-of select="text()" disable-output-escaping="yes" /></li>
													</xsl:for-each>
												</ol>
											</td>
										</tr>
									</xsl:if>
									<tr>
										<td colspan="2" bgcolor="{$passedColor}">&#160;</td>
									</tr>
								</xsl:for-each>
							</xsl:when>
							<xsl:otherwise>
								<tr>
									<td style="border:2px solid {$passedColor};padding:7px;">No test cases passed.</td>
								</tr>
							</xsl:otherwise>
						</xsl:choose>
					</tbody>
				</table>
			</td></tr></table>
		</body>
	</html>
</xsl:template>

</xsl:stylesheet>