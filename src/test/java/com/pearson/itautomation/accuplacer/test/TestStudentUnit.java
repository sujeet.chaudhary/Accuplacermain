package com.pearson.itautomation.accuplacer.test;

import org.testng.annotations.Test;

import com.pearson.itautomation.accuplacer.reports.core.Student;
import com.pearson.itautomation.accuplacer.reports.core.TestScores;

public class TestStudentUnit extends TestBase {
    @Test
    public void testStudent() {
        // TestSession tsd = new TestSession("1111");
        Student s = new Student(getConnection(), "16866232");
        System.out.println(s.getAddress1());
        System.out.println(s.getAddress2());
        System.out.println(s.getCity());
        System.out.println(s.getDob());
        System.out.println(s.getExnum());
        System.out.println(s.getFirstName());
        TestScores ts = new TestScores(getConnection(), "16866232");

        System.out.println("access object"
                + ts.getPlacementScore("TSI Mathematics Placement"));

        System.out
                .println("access object"
                        + ts.getPlacementScore("ABE Diagnostics Math Patterns, Functions, and Algebra"));
        System.out
                .println("access object"
                        + ts.getDiagnosticScore("ABE Diagnostics Math Patterns, Functions, and Algebra"));

    }
}
