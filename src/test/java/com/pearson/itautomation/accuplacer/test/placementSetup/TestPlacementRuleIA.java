package com.pearson.itautomation.accuplacer.test.placementSetup;

import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import com.pearson.itautomation.accuplacer.core.Buttons;
import com.pearson.itautomation.accuplacer.core.CommonUtilities;
import com.pearson.itautomation.accuplacer.core.action.ClickOnHome;
import com.pearson.itautomation.accuplacer.core.action.LoginAction;
import com.pearson.itautomation.accuplacer.core.action.NavigateToMenu;
import com.pearson.itautomation.accuplacer.placementsetup.placementrules.AddConditionToPlacementRule;
import com.pearson.itautomation.accuplacer.placementsetup.placementrules.AddMultiWeightedMeasureCondition;
import com.pearson.itautomation.accuplacer.placementsetup.placementrules.ChoosePlacementRulesName;
import com.pearson.itautomation.accuplacer.placementsetup.placementrules.SelectCoursePlacement;
import com.pearson.itautomation.accuplacer.test.TestBase;
import com.pearson.itautomation.bca.BrowserElement;
import com.pearson.itautomation.testngutils.dataprovider.ExcelDataProvider;
import com.pearson.itautomation.testngutils.reporting.TNReporter;

public class TestPlacementRuleIA extends TestBase {

    private static final String PLACEMENT_RULE_TITLE = "PlacementRule";
    private static final String COURSE_NAME = "AutomationCourse1";

    public final BrowserElement btnAddPlacementRule = Buttons
            .getButtonType4("Add");

    public final BrowserElement btnSavePlacementRule = Buttons
            .getButtonType1("Save");

    public void addConditions(String strConditionAll, String strNoOfCondition,
            String strBracketAll, String strTestAll,
            String strLogicalOperatorAll, String strEqualNotEqualAll,
            String strAnswerOfBackgroundQuestionAll,
            String strSingleTestScorePrefixAll,
            String strSingleTestScoreFinalAll, String strSingleStrandScoreAll,
            String strSelectedMajorAll,
            String strSelectValueForMultipleTestScoreAll,
            String strSelectValueForAutoMultipleWeightedMajorAll,
            String strMultiWeightedValueAll,
            String strAnsForMultipleWeightedMeasuresAll) {

        addTestCaseDescription("Add Placement Rule with different conditions");

        TNReporter.setParmDesc("Conditions to add for Rule",
                "No of conditions to set in Rule",
                "Bracket for enclosing the conditions",
                "Value to be selected for conditions",
                "Logicall operator for separating the conditions",
                "Equals/not equals to be selected",
                "Answer to be selected for background question",
                "Prefix for the conditions", "Final score for the conditions",
                "Score for single strand score", "Major to be selected",
                "Value to be selected for Multiple Test Score",
                "Value to be selected for Auto Multiple Weighted Measure",
                "Value for Multi Weighted Measure",
                "Answer for Multi Weighted Measure");

        assertAndReport(browser,
                new NavigateToMenu(this.browser, "Placement Setup",
                        "Placement Rules", btnAddPlacementRule)
                        .performWithStates(), "Navigate to Placement Rule page");

        assertAndReport(browser, new ChoosePlacementRulesName(browser,
                PLACEMENT_RULE_TITLE + CommonUtilities.getCurrentDateTime(),
                "Description", "Comment").performWithStates(),
                "Creating Rule Name:");

        assertAndReport(browser, new SelectCoursePlacement(browser,
                COURSE_NAME, "1").performWithStates(),
                "Selecting Cource for Adding rule:");

        if (strConditionAll.equals("Multi Weighted Measures")) {

            assertAndReport(browser, new AddMultiWeightedMeasureCondition(
                    browser, strBracketAll, strConditionAll,
                    strMultiWeightedValueAll, strSingleTestScorePrefixAll,
                    strSingleTestScoreFinalAll, strEqualNotEqualAll,
                    strAnsForMultipleWeightedMeasuresAll).performWithStates(),
                    "dd Multi Weighted Measure Condition To Placement Rule:");

        } else {

            assertAndReport(browser,
                    new AddConditionToPlacementRule(browser, strConditionAll,
                            strNoOfCondition, strBracketAll, strTestAll,
                            strLogicalOperatorAll, strEqualNotEqualAll,
                            strAnswerOfBackgroundQuestionAll,
                            strSingleTestScorePrefixAll,
                            strSingleTestScoreFinalAll,
                            strSingleStrandScoreAll, strSelectedMajorAll,
                            strSelectValueForMultipleTestScoreAll,
                            strSelectValueForAutoMultipleWeightedMajorAll)
                            .performWithStates(),
                    "Add Conditions To PlacementRule:");
        }

        assertAndReport(browser, new ClickOnHome(browser).performWithStates(),
                "Click on Home");
    }

    @Test(groups = { "regression", "CATTECHTA-149", "Placement Setup" }, dataProvider = "standardExcelData", dataProviderClass = ExcelDataProvider.class)
    public void addConditionsToPlacementRuleIA(String strConditionAll,
            String strNoOfCondition, String strBracketAll, String strTestAll,
            String strLogicalOperatorAll, String strEqualNotEqualAll,
            String strAnswerOfBackgroundQuestionAll,
            String strSingleTestScorePrefixAll,
            String strSingleTestScoreFinalAll, String strSingleStrandScoreAll,
            String strSelectedMajorAll,
            String strSelectValueForMultipleTestScoreAll,
            String strSelectValueForAutoMultipleWeightedMajorAll,
            String strMultiWeightedValueAll,
            String strAnsForMultipleWeightedMeasuresAll) {

        addConditions(strConditionAll, strNoOfCondition, strBracketAll,
                strTestAll, strLogicalOperatorAll, strEqualNotEqualAll,
                strAnswerOfBackgroundQuestionAll, strSingleTestScorePrefixAll,
                strSingleTestScoreFinalAll, strSingleStrandScoreAll,
                strSelectedMajorAll, strSelectValueForMultipleTestScoreAll,
                strSelectValueForAutoMultipleWeightedMajorAll,
                strMultiWeightedValueAll, strAnsForMultipleWeightedMeasuresAll);

    }

    @Test(groups = { "sanity", "CATTECHTA-149", "Placement Setup" }, dataProvider = "standardExcelData", dataProviderClass = ExcelDataProvider.class)
    public void addConditionsToPlacementRuleIAS(String strConditionAll,
            String strNoOfCondition, String strBracketAll, String strTestAll,
            String strLogicalOperatorAll, String strEqualNotEqualAll,
            String strAnswerOfBackgroundQuestionAll,
            String strSingleTestScorePrefixAll,
            String strSingleTestScoreFinalAll, String strSingleStrandScoreAll,
            String strSelectedMajorAll,
            String strSelectValueForMultipleTestScoreAll,
            String strSelectValueForAutoMultipleWeightedMajorAll,
            String strMultiWeightedValueAll,
            String strAnsForMultipleWeightedMeasuresAll) {

        addConditions(strConditionAll, strNoOfCondition, strBracketAll,
                strTestAll, strLogicalOperatorAll, strEqualNotEqualAll,
                strAnswerOfBackgroundQuestionAll, strSingleTestScorePrefixAll,
                strSingleTestScoreFinalAll, strSingleStrandScoreAll,
                strSelectedMajorAll, strSelectValueForMultipleTestScoreAll,
                strSelectValueForAutoMultipleWeightedMajorAll,
                strMultiWeightedValueAll, strAnsForMultipleWeightedMeasuresAll);

    }

    @BeforeClass(alwaysRun = true)
    public void beforeClass() {
        browser = getBrowser();
        browser.get(getAppURL());

        User user = getUser(USER_ROLE.INSTITUTE_ADMIN);

        assertAndReport(
                browser,
                new LoginAction(this.browser, user.getUserName(), user
                        .getPassword()).performWithStates(), "Login Action");
    }

    @AfterClass(alwaysRun = true)
    public void afterClass() {
        quitBrowser();
    }
}
