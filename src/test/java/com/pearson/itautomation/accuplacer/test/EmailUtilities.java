package com.pearson.itautomation.accuplacer.test;

import com.pearson.itautomation.accuplacer.core.CommonUtilities;

public class EmailUtilities extends TestBase {

    public static void main(String args[]) {
        EmailMain
                .triggerEmail("Email For Automation Run",
                        "(" + CommonUtilities.getCurrentDateTime()
                                + ") Automation Run",
                        CommonUtilities.EMAIL_PROPERTIES.getProperty("TOLIST")
                                .trim(), CommonUtilities.EMAIL_PROPERTIES
                                .getProperty("fromMail").trim());
    }

}
