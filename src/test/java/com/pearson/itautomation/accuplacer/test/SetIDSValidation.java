package com.pearson.itautomation.accuplacer.test;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Properties;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import com.pearson.itautomation.accuplacer.core.CommonUtilities;
import com.pearson.itautomation.accuplacer.core.HTMLUtils;
import com.pearson.itautomation.testngutils.reporting.TNReporter;

public class SetIDSValidation extends TestBase {
    private static final String FAILURE_STRING = "9999";
    private static final Logger LOGGER = LoggerFactory
            .getLogger(SetIDSValidation.class);
    protected static Properties sqlQueries = CommonUtilities
            .readPropertyFile("//config//SQL.properties");
    protected static Properties utilities = CommonUtilities
            .readPropertyFile("//config//utilities.properties");
    private static int i = 0;
    String REGEX_TO_REPLACE_CHARECTER = "[4-9]";

    private static HashMap<String, TestSessionCount> hMap = new HashMap<>();
    private static HashMap<String, Integer> setIds = new HashMap<>();
    private static int numberOfSetItems = 0;

    @Test(dataProvider = "setIdData")
    public void setIDTests(String testSessionDetailID) {

        addTestCaseDescription("This Test Case will perform Validations for SET Item scenarios, For a given test session Transaction");
        TNReporter.setParmDesc("Test Session Detail ID");
        LOGGER.info("TC#" + (++i));
        int expectNumberOfSetItems = 0;
        int expectednumberofSet = 0;
        int actualNumberOfSetItems = 0;
        int actualNumberOfSet = 0;

        String catItemVectorDetail = runQuery(String.format(
                sqlQueries.getProperty("catItemVectorDetail"),
                testSessionDetailID), getConnection(), "new_item_vector_value");
        TNReporter.addNote("Dynamic Vector Set for test_Session_detail_id "
                + testSessionDetailID + " : " + catItemVectorDetail);
        String testName = runQuery(
                String.format(sqlQueries.getProperty("testNameForSet"),
                        testSessionDetailID), getConnection(), "test_alias")
                .toUpperCase();
        String vectorType = testName
                + ","
                + catItemVectorDetail
                        .replaceAll(REGEX_TO_REPLACE_CHARECTER, "");

        if (hMap.containsKey(vectorType)) {
            hMap.get(vectorType).setCount(hMap.get(vectorType).getCount() + 1);
            hMap.get(vectorType).setTestSessionIds(
                    hMap.get(vectorType).getTestSessionIds() + ","
                            + testSessionDetailID);

        } else {

            hMap.put(vectorType, new TestSessionCount(1, testSessionDetailID));
        }

        for (int i = 0; i < catItemVectorDetail.length(); i++) {
            if (!(catItemVectorDetail.charAt(i) == '1' || catItemVectorDetail
                    .charAt(i) == '2')) {
                expectNumberOfSetItems++;
                if (catItemVectorDetail.charAt(i) == '3') {
                    expectednumberofSet++;
                }
            }

        }
        if (expectNumberOfSetItems == 0) {
            TNReporter
                    .addNote("NO Set Item Available in the given Transaction");
            return;

        }
        actualNumberOfSetItems = Integer.parseInt(runQuery(String.format(
                sqlQueries.getProperty("itemsInFTPredefined"),
                testSessionDetailID), getConnection(), "count(1)"));
        TNReporter
                .addNote("Expected Number Of Set Items as per the Dynamic Vector : "
                        + expectNumberOfSetItems);
        TNReporter
                .addNote("Actual Number Of Set Items as per the Responses under the Database :"
                        + actualNumberOfSetItems);

        actualNumberOfSet = Integer.parseInt(runQuery(String.format(
                sqlQueries.getProperty("distinctActualNumberOfSetID"),
                testSessionDetailID), getConnection(),
                "count(distinct(set_id))"));
        if ((expectNumberOfSetItems - actualNumberOfSetItems) == 0) {
            assertAndReport(browser, true, "Number of Set Items");
        }
        TNReporter
                .addNote("Expected Number Of Set as per the Dynamic Vector Sequence "
                        + expectednumberofSet);
        TNReporter
                .addNote("Actual Number of Sets as per distinct SET IDs in the Database : "
                        + actualNumberOfSet);
        if ((expectednumberofSet - actualNumberOfSet) == 0) {
            assertAndReport(browser, true, "Number of Distinct SET ID");
        }

        Object[][] setIDs = runAndReturnMultiDimentionalArray(String.format(
                sqlQueries.getProperty("actualNumberOfSetID"),
                testSessionDetailID), "set_id");

        String positionsOfSetItems = "";
        Object[][] questionPositions = runAndReturnMultiDimentionalArray(
                String.format(sqlQueries.getProperty("questionPositions"),
                        testSessionDetailID), "question_position_id");
        Integer questionPositionsInt[] = new Integer[questionPositions.length];
        for (int i = 0; i < questionPositions.length; i++) {
            if (!(i == 0)) {
                positionsOfSetItems = positionsOfSetItems + ","
                        + questionPositions[i][0];
            } else {
                positionsOfSetItems = questionPositions[i][0] + "";
            }
            questionPositionsInt[i] = (Integer.parseInt(questionPositions[i][0]
                    + "") - 1);
        }
        TNReporter.addNote("Positions Of Set Items int the Vector Sequence "
                + positionsOfSetItems);

        String setIDList = "";
        for (int i = 0; i < setIDs.length; i++) {
            if (!setIDList.contains(setIDs[i][0] + "")) {
                if (!(i == 0)) {
                    setIDList = setIDList + "," + HTMLUtils.getBoldTagStart()
                            + "" + setIDs[i][0] + HTMLUtils.getBoldTagEnd();

                } else {
                    setIDList = HTMLUtils.getBoldTagStart() + setIDs[i][0]
                            + HTMLUtils.getBoldTagEnd();
                }
            }
            if (!setIds.containsKey(setIDs[i][0] + "")) {
                setIds.put(setIDs[i][0] + "", 0);
                ++numberOfSetItems;
            } else {
                setIds.put(setIDs[i][0] + "", setIds.get(setIDs[i][0] + "") + 1);
            }
        }
        TNReporter.addNote("List Of DISTINCT Set IDs " + setIDList);

        Object[][] itemIds = runAndReturnMultiDimentionalArray(String.format(
                sqlQueries.getProperty("ItemIdsBasedOnlistOfUserResponses"),
                testSessionDetailID), "item_id");
        TNReporter
                .addNote("Checking Preceeding Item is NOT of Type Operational and SET ie (0,S)");
        for (int i = 0; i < questionPositionsInt.length; i++) {

            String type = runQuery(String.format(
                    sqlQueries.getProperty("catItemParameter"),
                    itemIds[questionPositionsInt[i] - 1][0]), getConnection(),
                    "type");
            String pretest = runQuery(String.format(
                    sqlQueries.getProperty("catItemParameter"),
                    itemIds[questionPositionsInt[i] - 1][0]), getConnection(),
                    "pretest");
            if ("S".equalsIgnoreCase(type) && "0".equalsIgnoreCase(pretest)) {
                assertAndReport(
                        browser,
                        false,
                        "AS the Item "
                                + itemIds[questionPositionsInt[i] - 1][0]
                                + " is OPERATIONAL SET TYPE under test_Session_detail_id "
                                + testSessionDetailID);
            } else {
                TNReporter.addNote("Predecessor of Set Item   " + (i + 1)
                        + " is of Type :(" + pretest + "," + type + ")");
            }

        }
        TNReporter
                .addNote("(0 = Operational/ 1 = Pretest , D=Discrete Item/ S=SET Item)");

    }

    private String runQuery(String query, Connection con, String columnName) {
        String queryOutput = FAILURE_STRING;
        ResultSet rset = null;
        Statement stmt = null;

        try {
            stmt = con.createStatement();
            rset = stmt.executeQuery(query);
            if (!rset.next()) {
                LOGGER.error(String
                        .format("No records were found for the test session detail ID : %s",
                                query));
                return queryOutput;
            } else {
                do {
                    queryOutput = rset.getString(columnName);
                } while (rset.next());
            }
        } catch (SQLException e) {
            LOGGER.error(
                    "Exception occured while executing the query test session id "
                            + query, e);
            return queryOutput;
        } finally {
            closeStatements(stmt, con, rset);
        }
        return queryOutput;

    }

    public void closeStatements(Statement stmt, Connection con, ResultSet rset) {
        try {
            if (stmt != null && !con.isClosed()) {
                stmt.close();
            }
            if (rset != null && !con.isClosed()) {
                rset.close();
            }
        } catch (SQLException e) {
            LOGGER.error("Exception occured while closing the statement", e);
        }
    }

    @DataProvider(name = "setIdData")
    public Object[][] setIdData() throws Exception {
        String columnName = "test_Session_detail_id";
        String query = sqlQueries.getProperty("setTestSessionDetailID");

        Object[][] rValue = runAndReturnMultiDimentionalArray(query, columnName);
        return rValue;

    }

    public Object[][] runAndReturnMultiDimentionalArray(String query,
            String columnName) {
        ResultSet rset = null;
        Statement stmt = null;
        ArrayList<String> list = new ArrayList<String>();

        try {

            stmt = getConnection().createStatement();
            rset = stmt.executeQuery(query);
            if (!rset.next()) {
                LOGGER.error(String
                        .format("No records were found for the test session detail ID : %s",
                                query));
            } else {
                do {

                    list.add(rset.getString(columnName));
                } while (rset.next());
            }
        } catch (SQLException e) {
            LOGGER.error("Exception occured while executing the query" + query,
                    e);
        } finally {
            closeStatements(stmt, getConnection(), rset);
        }

        Object[][] rValue = new Object[list.size()][1];
        for (int i = 0; i < list.size(); i++) {
            rValue[i][0] = list.get(i);
        }
        return rValue;
    }

    @Test
    public void displayDynamicVectorDistribution() {

        addTestCaseDescription("This Test Case will Display the Occurance of Dynamic Vector Configuration");

        Iterator<Map.Entry<String, TestSessionCount>> iterator = hMap
                .entrySet().iterator();
        String htmlTable = HTMLUtils.getHtmlHeaderDynamicHeader();

        while (iterator.hasNext()) {

            Map.Entry<String, TestSessionCount> entry = iterator.next();
            LOGGER.info(entry.getKey());
            htmlTable = htmlTable
                    + (String.format(HTMLUtils.getHtmlDynamicHeaderRow(), entry
                            .getKey().split(",")[0],
                            entry.getKey().split(",")[1], entry.getValue()
                                    .getCount(), entry.getValue()
                                    .getTestSessionIds()));
            iterator.remove();
        }
        htmlTable = htmlTable + HTMLUtils.getHtmlTableEnd();
        TNReporter.addNote(htmlTable);
    }

    @Test
    public void displaySetIDsUnderTheSessions() {

        addTestCaseDescription("This Test Case will Display the Distinct SET IDs encountered during the Test Sessions");
        TNReporter.addNote("Count of Set Items " + numberOfSetItems);
        Iterator<Map.Entry<String, Integer>> iterator = setIds.entrySet()
                .iterator();
        String htmlTable = HTMLUtils.getHtmlSetIdHeader();

        while (iterator.hasNext()) {

            Map.Entry<String, Integer> entry = iterator.next();

            htmlTable = htmlTable
                    + String.format(HTMLUtils.getHtmlSetIdRow(),
                            entry.getKey(), (int) entry.getValue());
            iterator.remove();
        }
        htmlTable = htmlTable + HTMLUtils.getHtmlTableEnd();
        TNReporter.addNote(htmlTable);
    }
}
