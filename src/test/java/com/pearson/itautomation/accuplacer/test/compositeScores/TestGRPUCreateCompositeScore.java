package com.pearson.itautomation.accuplacer.test.compositeScores;

import org.openqa.selenium.WebElement;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import com.pearson.itautomation.accuplacer.core.Buttons;
import com.pearson.itautomation.accuplacer.core.CommonUtilities;
import com.pearson.itautomation.accuplacer.core.action.LoginAction;
import com.pearson.itautomation.accuplacer.core.action.NavigateToMenu;
import com.pearson.itautomation.accuplacer.placementsetup.compositescores.AddConditionsToCompositeScore;
import com.pearson.itautomation.accuplacer.placementsetup.compositescores.CreateNewCompositeScoreName;
import com.pearson.itautomation.accuplacer.test.TestBase;
import com.pearson.itautomation.bca.BrowserElement;
import com.pearson.itautomation.bca.interaction.LeftClickElementAction;
import com.pearson.itautomation.bca.validation.ElementExistsValidation;
import com.pearson.itautomation.testngutils.dataprovider.ExcelDataProvider;
import com.pearson.itautomation.testngutils.reporting.TNReporter;

/**
 * This Test Case will add conditions to Composite Score with 1 - 3 conditions,
 * and combination of conditions under same Composite Score, depending on the
 * Test Data Provided
 * 
 * @author Solomon Lingala
 * 
 */

public class TestGRPUCreateCompositeScore extends TestBase {

    public final String strTabName = "Placement Setup";
    public final String strSubMenuName = "Composite Scores";
    public final BrowserElement btnAdd = Buttons.getButtonType3("Add");
    private static final String OR_AREA = "OR_Placement_Setup";
    private static final String OR_ADD_COMPOSITESCORE = "AddCompositeScore";
    private static final String OR_COMPOSITESCORECONFMSG = "CompositeScoreConfirmMsg";
    private static final String OR_BACK_BUTTON = "BTNBack";

    @Test(dataProvider = "standardExcelData", dataProviderClass = ExcelDataProvider.class, groups = {
            "CATTECHTA-150", "regression", "PlacementSetup" })
    public void testCreateCSGURegression(final String testCaseDescription,
            final String compositeScoreName,
            final String compositeScoreDescritpion,
            final String compositeScoreComments, final String conditionVector,
            final String conditionValueVector) {
        newCompositeScoreName(testCaseDescription, compositeScoreName
                + CommonUtilities.getCurrentddhhmmss(),
                compositeScoreDescritpion, compositeScoreComments,
                conditionVector, conditionValueVector);

    }

    @Test(dataProvider = "standardExcelData", dataProviderClass = ExcelDataProvider.class, groups = {
            "CATTECHTA-150", "sanity", "PlacementSetup" })
    public void testCreateCSGUSanity(final String testCaseDescription,
            final String compositeScoreName,
            final String compositeScoreDescritpion,
            final String compositeScoreComments, final String conditionVector,
            final String conditionValueVector) {
        newCompositeScoreName(testCaseDescription, compositeScoreName
                + CommonUtilities.getCurrentddhhmmss(),
                compositeScoreDescritpion, compositeScoreComments,
                conditionVector, conditionValueVector);

    }

    public void newCompositeScoreName(final String testCaseDescription,
            final String compositeScoreName,
            final String compositeScoreDescritpion,
            final String compositeScoreComments, final String conditionVector,
            final String conditionValueVector) {
        addTestCaseDescription("Create Composite Score with atleast one of the 5 score types.");
        TNReporter
                .setParmDesc(
                        "Describes the type of score condition being added in the Composite Score.",
                        "The Composite Score Name",
                        "The Composite Score Description",
                        "The Composite Score Comments",
                        "The vector that defines different conditions to be added to score",
                        "The vector that has different values for conditions added to the score");

        assertAndReport(browser, new CreateNewCompositeScoreName(this.browser,
                compositeScoreName, compositeScoreDescritpion,
                compositeScoreComments).performWithStates(),
                "Create Composite Score Name with different condition type.");

        assertAndReport(browser,
                new AddConditionsToCompositeScore(this.browser,
                        conditionVector, conditionValueVector)
                        .performWithStates(), "Create Composite Score Name");

        assertAndReport(
                browser,
                new ElementExistsValidation(this.browser, browser
                        .getObjectRepository().getBrowserElement(OR_AREA,
                                OR_COMPOSITESCORECONFMSG)).performWithStates(),
                "Verify confirmation message after composite score is saved.");

    }

    @BeforeMethod(alwaysRun = true)
    public void beforeMethod() {
        // click Back
        BrowserElement btnBack = browser.getBrowserElement(OR_AREA,
                OR_BACK_BUTTON);
        WebElement element = browser.getElementWithWait(btnBack);
        if (element != null && element.isDisplayed()) {

            new LeftClickElementAction(getBrowser(), btnBack)
                    .performWithStates();
        }
        // click Add
        BrowserElement btnAddCompositeScore = browser.getBrowserElement(
                OR_AREA, OR_ADD_COMPOSITESCORE);
        element = browser.getElementWithWait(btnAddCompositeScore);
        if (element != null && element.isDisplayed()) {

            new LeftClickElementAction(getBrowser(), btnAddCompositeScore)
                    .performWithStates();
        }
    }

    @BeforeClass(alwaysRun = true)
    public void beforeClass() {
        browser = getBrowser();
        browser.get(getAppURL());
        User user = getUser(USER_ROLE.GROUP_USER);

        assertAndReport(
                browser,
                new LoginAction(this.browser, user.getUserName(), user
                        .getPassword()).performWithStates(), "Login Action");

        assertAndReport(browser, new NavigateToMenu(this.browser, strTabName,
                strSubMenuName, btnAdd).performWithStates(),
                "Navigation to Transfer Test Units Page");
    }

    @AfterClass(alwaysRun = true)
    public void afterClass() {
        quitBrowser();
    }
}
