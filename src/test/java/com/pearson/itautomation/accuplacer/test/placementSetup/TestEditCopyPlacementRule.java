package com.pearson.itautomation.accuplacer.test.placementSetup;

import org.testng.annotations.AfterClass;
import org.testng.annotations.Test;

import com.pearson.itautomation.accuplacer.core.Buttons;
import com.pearson.itautomation.accuplacer.core.CommonUtilities;
import com.pearson.itautomation.accuplacer.core.action.ClickOnHome;
import com.pearson.itautomation.accuplacer.core.action.LoginAction;
import com.pearson.itautomation.accuplacer.core.action.NavigateToMenu;
import com.pearson.itautomation.accuplacer.core.action.SaveAndAssert;
import com.pearson.itautomation.accuplacer.core.action.SearchRecordsAndClick;
import com.pearson.itautomation.accuplacer.placementsetup.placementrules.ClickOnEditPlacementRule;
import com.pearson.itautomation.accuplacer.placementsetup.placementrules.CopyPlacementRule;
import com.pearson.itautomation.accuplacer.test.TestBase;
import com.pearson.itautomation.bca.Browser;
import com.pearson.itautomation.bca.BrowserElement;
import com.pearson.itautomation.testngutils.dataprovider.ExcelDataProvider;

public class TestEditCopyPlacementRule extends TestBase {

    Browser browser = null;

    String PLACEMENT_RULE_TITLE;

    private static final String PLACEMENT_RULE_TITLE_FOR_COPY = "000COPY2405";

    String XPATH_OF_ELEMENT = "";

    private final String strTabName = "Placement Setup";
    private final String strSubMenuName = "Placement Rules";

    public final BrowserElement btnAddPlacementRule = Buttons
            .getButtonType4("Add");

    @Test(groups = { "sanity", "regression", "CATTECHTA-155", "Placement Setup" }, dataProvider = "standardExcelData", dataProviderClass = ExcelDataProvider.class, priority = 2)
    public void testEditPlacementRule(String UserName, String Password,
            String PlacementRule) {

        browser = getBrowser();
        browser.get(getAppURL());

        assertAndReport(browser, new LoginAction(this.browser, UserName,
                Password).performWithStates(), "Login Action");

        assertAndReport(browser, new NavigateToMenu(this.browser, strTabName,
                strSubMenuName, btnAddPlacementRule).performWithStates(),
                "Navigate to Placement Rule page");

        addTestCaseDescription("Search and Edit Placement Rule");

        XPATH_OF_ELEMENT = "//td[span[contains(text(),'" + PlacementRule
                + "')] or contains(text(),'" + PlacementRule + "')]";

        assertAndReport(browser, new SearchRecordsAndClick(browser,
                XPATH_OF_ELEMENT).performWithStates(), "Searching Records");

        assertAndReport(browser, new ClickOnEditPlacementRule(browser,
                PlacementRule).performWithStates(), "Click on Edit button");

        assertAndReport(browser, new SaveAndAssert(browser,
                "Placement Rule Updated Successfully").performWithStates(),
                "Save and Verify the confirmation Message");

        assertAndReport(browser, new ClickOnHome(browser).performWithStates(),
                "Click On Home");

        assertAndReport(browser, new NavigateToMenu(this.browser, strTabName,
                strSubMenuName, btnAddPlacementRule).performWithStates(),
                "Navigate to Placement Rule page");

        assertAndReport(browser, new SearchRecordsAndClick(browser,
                XPATH_OF_ELEMENT).performWithStates(), "Searching Records");

        assertAndReport(
                browser,
                new CopyPlacementRule(browser, PlacementRule,
                        PLACEMENT_RULE_TITLE_FOR_COPY
                                + CommonUtilities.getCurrentTime())
                        .performWithStates(), "copy Placement Rule");

    }

    @AfterClass(alwaysRun = true)
    public void afterClass() {
        quitBrowser();
    }
}
