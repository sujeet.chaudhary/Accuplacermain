package com.pearson.itautomation.accuplacer.test.placementSetup;

import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import com.pearson.itautomation.accuplacer.core.Buttons;
import com.pearson.itautomation.accuplacer.core.CommonUtilities;
import com.pearson.itautomation.accuplacer.core.action.ClickOnHome;
import com.pearson.itautomation.accuplacer.core.action.LoginAction;
import com.pearson.itautomation.accuplacer.core.action.NavigateToMenu;
import com.pearson.itautomation.accuplacer.core.action.SaveAndAssert;
import com.pearson.itautomation.accuplacer.core.action.SearchRecordsAndClick;
import com.pearson.itautomation.accuplacer.placementsetup.placementrules.ActivateInactivatePlacementRule;
import com.pearson.itautomation.accuplacer.placementsetup.placementrules.AddConditionToPlacementRule;
import com.pearson.itautomation.accuplacer.placementsetup.placementrules.ChoosePlacementRulesName;
import com.pearson.itautomation.accuplacer.placementsetup.placementrules.ClickOnEditPlacementRule;
import com.pearson.itautomation.accuplacer.placementsetup.placementrules.CopyPlacementRule;
import com.pearson.itautomation.accuplacer.placementsetup.placementrules.DeletePlacementRule;
import com.pearson.itautomation.accuplacer.placementsetup.placementrules.SelectCoursePlacement;
import com.pearson.itautomation.accuplacer.test.TestBase;
import com.pearson.itautomation.bca.BrowserElement;
import com.pearson.itautomation.testngutils.dataprovider.ExcelDataProvider;
import com.pearson.itautomation.testngutils.reporting.TNReporter;

public class TestPlacementRuleOperationsSM extends TestBase {

    private static final String COURSE_NAME = "AutomationCourse1";

    private static final String PLACEMENT_RULE_TITLE = "AAAPLACEMENT RULE"
            + CommonUtilities.getCurrentTime();

    private static final String PLACEMENT_RULE_TITLE_FOR_COPY = "000COPY"
            + CommonUtilities.getCurrentTime();
    private static final String XPATH_OF_ELEMENT = "//td[span[contains(text(),'"
            + PLACEMENT_RULE_TITLE
            + "')] or contains(text(),'"
            + PLACEMENT_RULE_TITLE + "')]";

    private static final String DESCRIPTION = "Description";
    private static final String COMMENT = "Comment";

    private final String strTabName = "Placement Setup";
    private final String strSubMenuName = "Placement Rules";

    public final BrowserElement btnAddPlacementRule = Buttons
            .getButtonType4("Add");

    @Test(groups = { "sanity", "regression", "CATTECHTA-155", "Placement Setup" })
    public void testAddPlacementRuleSM() {

        addTestCaseDescription("Add Placement Rule");

        assertAndReport(browser,
                new ChoosePlacementRulesName(browser, PLACEMENT_RULE_TITLE,
                        DESCRIPTION, COMMENT).performWithStates(),
                "Enter Placement Rule Name");

        assertAndReport(browser, new SelectCoursePlacement(browser,
                COURSE_NAME, "1").performWithStates(),
                "Selecting Course for Adding Condition:");

        assertAndReport(browser, new SaveAndAssert(browser,
                "Placement Rule was added Successfully").performWithStates(),
                "Save and Verify the confirmation Message");
    }

    @Test(groups = { "sanity", "regression", "CATTECHTA-155", "Placement Setup" }, dataProvider = "standardExcelData", dataProviderClass = ExcelDataProvider.class, dependsOnMethods = "testAddPlacementRuleSM", priority = 2)
    public void testEditPlacementRuleSM(String strConditionAll,
            String strNoOfCondition, String strBracketAll, String strTestAll,
            String strLogicalOperatorAll, String strEqualNotEqualAll,
            String strAnswerOfBackgroundQuestionAll,
            String strSingleTestScorePrefixAll,
            String strSingleTestScoreFinalAll, String strSingleStrandScoreAll,
            String strSelectedMajorAll,
            String strSelectValueForMultipleTestScoreAll,
            String strSelectValueForAutoMultipleWeightedMajorAll,
            String strMultiWeightedValueAll,
            String strAnsForMultipleWeightedMeasuresAll) {

        addTestCaseDescription("Search and Edit Placement Rule");

        TNReporter.setParmDesc("Conditions to add for Rule",
                "No of conditions to set in Rule",
                "Bracket for enclosing the conditions",
                "Value to be selected for conditions",
                "Logicall operator for separating the conditions",
                "Equals/not equals to be selected",
                "Answer to be selected for background question",
                "Prefix for the conditions", "Final score for the conditions",
                "Score for single strand score", "Major to be selected",
                "Value to be selected for Multiple Test Score",
                "Value to be selected for Auto Multiple Weighted Measure",
                "Value for Multi Weighted Measure",
                "Answer for Multi Weighted Measure");

        assertAndReport(browser, new SearchRecordsAndClick(browser,
                XPATH_OF_ELEMENT).performWithStates(), "Searching Records");

        assertAndReport(browser, new ClickOnEditPlacementRule(browser,
                PLACEMENT_RULE_TITLE).performWithStates(),
                "Click on Edit button");

        assertAndReport(browser,
                new AddConditionToPlacementRule(browser, strConditionAll,
                        strNoOfCondition, strBracketAll, strTestAll,
                        strLogicalOperatorAll, strEqualNotEqualAll,
                        strAnswerOfBackgroundQuestionAll,
                        strSingleTestScorePrefixAll,
                        strSingleTestScoreFinalAll, strSingleStrandScoreAll,
                        strSelectedMajorAll,
                        strSelectValueForMultipleTestScoreAll,
                        strSelectValueForAutoMultipleWeightedMajorAll)
                        .performWithStates(true, false),
                "Add Condition to created Placement Rule");

        assertAndReport(browser, new SaveAndAssert(browser,
                "Placement Rule Updated Successfully").performWithStates(),
                "Save and Verify the confirmation Message");
    }

    @Test(groups = { "sanity", "regression", "CATTECHTA-155", "Placement Setup" }, dependsOnMethods = "testAddPlacementRuleSM", priority = 3)
    public void testCopyPlacementRuleSM() {

        addTestCaseDescription("Search and Copy Placement Rule");

        assertAndReport(browser, new SearchRecordsAndClick(browser,
                XPATH_OF_ELEMENT).performWithStates(), "Searching Records");

        assertAndReport(browser,
                new CopyPlacementRule(browser, PLACEMENT_RULE_TITLE,
                        PLACEMENT_RULE_TITLE_FOR_COPY).performWithStates(),
                "copy Placement Rule");
    }

    @Test(groups = { "sanity", "regression", "CATTECHTA-155", "Placement Setup" }, dependsOnMethods = "testAddPlacementRuleSM", priority = 4)
    public void testActivatePlacementRuleSM() {

        addTestCaseDescription("Search and Activate Placement Rule");

        assertAndReport(browser, new SearchRecordsAndClick(browser,
                XPATH_OF_ELEMENT).performWithStates(), "Searching Records");

        assertAndReport(browser, new ActivateInactivatePlacementRule(browser,
                PLACEMENT_RULE_TITLE, 2).performWithStates(),
                "Activate Placement Rule");
    }

    @Test(groups = { "sanity", "regression", "CATTECHTA-155", "Placement Setup" }, dependsOnMethods = "testAddPlacementRuleSM", priority = 5)
    public void testInactivatePlacementRuleSM() {

        addTestCaseDescription("Search and Inactivate Placement Rule");

        assertAndReport(browser, new SearchRecordsAndClick(browser,
                XPATH_OF_ELEMENT).performWithStates(), "Searching Records");

        assertAndReport(browser, new ActivateInactivatePlacementRule(browser,
                PLACEMENT_RULE_TITLE, 1).performWithStates(),
                "Inactivate Placement Rule");
    }

    @Test(groups = { "sanity", "regression", "CATTECHTA-155", "Placement Setup" }, dependsOnMethods = "testAddPlacementRuleSM", priority = 6)
    public void testDeletePlacementRuleSM() {

        addTestCaseDescription("Search and Delete Placement rule");

        assertAndReport(browser, new SearchRecordsAndClick(browser,
                XPATH_OF_ELEMENT).performWithStates(), "Searching Records");

        assertAndReport(browser, new DeletePlacementRule(browser,
                PLACEMENT_RULE_TITLE).performWithStates(),
                "Delete Placement Rule");
    }

    @AfterMethod(alwaysRun = true)
    public void afterMethod() {
        assertAndReport(browser, new ClickOnHome(browser).performWithStates(),
                "Click On Home");
    }

    @BeforeMethod(alwaysRun = true)
    public void beforeMethod() {
        assertAndReport(browser, new NavigateToMenu(this.browser, strTabName,
                strSubMenuName, btnAddPlacementRule).performWithStates(),
                "Navigate to Placement Rule page");

    }

    @BeforeClass(alwaysRun = true)
    public void beforeClass() {
        browser = getBrowser();
        browser.get(getAppURL());
        User user = getUser(USER_ROLE.SITE_MANAGER);
        assertAndReport(
                browser,
                new LoginAction(this.browser, user.getUserName(), user
                        .getPassword()).performWithStates(), "Login Action");
    }

    @AfterClass(alwaysRun = true)
    public void afterClass() {
        quitBrowser();
    }
}
