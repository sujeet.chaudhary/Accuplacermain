package com.pearson.itautomation.accuplacer.test;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Map.Entry;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Optional;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import com.pearson.itautomation.accuplacer.core.DBUtils;
import com.pearson.itautomation.testngutils.reporting.TNReporter;

public class EnemyItems extends TestBase {
    private static final Logger LOGGER = LoggerFactory
            .getLogger(EnemyItems.class);
    private static final String DISCRETE = "D";
    private static final String OPERATIONAL = "0";
    private static final String FIELD_TEST = "1";
    private static final String SET = "S";
    private static final int START_INDEX = 0;
    private static final int INDEX_OF_OVERLAP_CONSTRAINT = 2;
    private static final String OVERLAP_TOKEN = "!O";
    private static final int INDEX_OF_PRETEST = 4;
    private static final int INDEX_OF_TYPE = 5;

    private static final String TEST_SESSION_IDS = "select test_Session_detail_id from test_Session_detail where trunc(created_on) > sysdate - %s and test_session_id in(select test_session_id from test_session where test_Session_status_id = 3 ) and test_detail_id in (select test_detail_id from test_detail where test_type_id in (1,8))";
    private static final String DELIMITER = ";";
    private static int testCase = 0;
    private int noOfCoumns;
    private int count;
    private String noOfDays;
    private boolean isNoOfDaysSet = false;

    @Test(dataProvider = "enemyItemsInput")
    public void enemyItemsValidate(String testSessionIDs) throws Exception {
        LOGGER.info("TC#" + ++testCase + " of " + count);
        TNReporter.setParmDesc("TEST_SESSION_DETAIL_ID");
        String itemUserResponse[][];
        addTestCaseDescription("This Test Case will perform Validations for Enemy Items, For a given test session Transaction");
        HashMap<String, String> setItemResponse = new HashMap<String, String>();
        HashMap<String, String> pretestItemResponse = new HashMap<String, String>();
        HashMap<String, String> operationalItemResponse = new HashMap<String, String>();
        HashMap<String, String> failedValidations = new HashMap<String, String>();
        String query = "select cuir.item_response_id,cuir.strand_session_detail_id,cip.overlap_constraints,cip.item_id,cip.pretest,cip.type from cat_item_user_response cuir,cat_item_parameter cip where cuir.strand_session_detail_id in (select strand_session_detail_id from strand_session_detail where test_session_detail_id = %s) and cuir.item_id = cip.item_id  order by cuir.item_response_id asc";
        DBUtils dbu = new DBUtils(getConnection());

        itemUserResponse = dbu.runAndReturnMultiDimentionalArray(String.format(
                query, testSessionIDs));
        String setFieldTestItems[][] = getArrayBasedOnPretestAndType(
                itemUserResponse, FIELD_TEST, SET, INDEX_OF_PRETEST,
                INDEX_OF_TYPE);
        if (setFieldTestItems == null) {
            noOfCoumns = 0;
        } else {
            noOfCoumns = setFieldTestItems[0].length;
        }
        if (setFieldTestItems != null) {
            String listOfSetItemOverlaps = "";

            for (int i = 0; i < setFieldTestItems.length; i++) {
                if (setFieldTestItems[i][INDEX_OF_OVERLAP_CONSTRAINT] != null) {
                    if (setFieldTestItems[i][INDEX_OF_OVERLAP_CONSTRAINT]
                            .contains(OVERLAP_TOKEN)) {
                        String[] tokenizedOverlap = setFieldTestItems[i][INDEX_OF_OVERLAP_CONSTRAINT]
                                .substring(
                                        START_INDEX,
                                        setFieldTestItems[i][INDEX_OF_OVERLAP_CONSTRAINT]
                                                .indexOf(" ")).split(",");

                        if (tokenizedOverlap != null) {

                            for (int m = 0; m < tokenizedOverlap.length; m++) {
                                if (!tokenizedOverlap[m]
                                        .contains(OVERLAP_TOKEN)) {
                                    tokenizedOverlap[m] = OVERLAP_TOKEN
                                            + tokenizedOverlap[m];
                                }
                                LOGGER.debug(tokenizedOverlap[m]);
                                setItemResponse
                                        .put(tokenizedOverlap[m], i + "");

                                listOfSetItemOverlaps = appendSemicolon(
                                        listOfSetItemOverlaps,
                                        tokenizedOverlap[m],
                                        listOfSetItemOverlaps.split(DELIMITER).length);

                            }
                        }

                    } else
                        continue;

                }
            }
            if (listOfSetItemOverlaps != null
                    && !listOfSetItemOverlaps.equalsIgnoreCase("")) {
                TNReporter
                        .addNote("List of Overlaps for the Set items Exposed "
                                + listOfSetItemOverlaps);
            }

        } else {
            setItemResponse = null;
        }
        String dicreteFieldTestItems[][] = getArrayBasedOnPretestAndType(
                itemUserResponse, FIELD_TEST, DISCRETE, INDEX_OF_PRETEST,
                INDEX_OF_TYPE);

        if (dicreteFieldTestItems != null) {
            String listOfDiscreteFTItemOverlaps = "";

            for (int i = 0; i < dicreteFieldTestItems.length; i++) {

                if (dicreteFieldTestItems[i][INDEX_OF_OVERLAP_CONSTRAINT] != null) {
                    if (dicreteFieldTestItems[i][INDEX_OF_OVERLAP_CONSTRAINT]
                            .contains(OVERLAP_TOKEN)) {
                        String tokenizedOverlap[] = dicreteFieldTestItems[i][INDEX_OF_OVERLAP_CONSTRAINT]
                                .substring(
                                        START_INDEX,
                                        dicreteFieldTestItems[i][INDEX_OF_OVERLAP_CONSTRAINT]
                                                .indexOf(" ")).split(",");

                        for (int m = 0; m < tokenizedOverlap.length; m++) {
                            if (!tokenizedOverlap[m].contains(OVERLAP_TOKEN)) {
                                tokenizedOverlap[m] = OVERLAP_TOKEN
                                        + tokenizedOverlap[m];
                            }
                            LOGGER.debug(tokenizedOverlap[m]);

                            if (setItemResponse != null
                                    && setItemResponse
                                            .containsKey(tokenizedOverlap[m])) {

                                checkAndAddFailure(failedValidations,
                                        tokenizedOverlap[m],
                                        "Overlap is present in Field Discrete and Field Set exposed items");

                            }
                            if (pretestItemResponse != null
                                    && pretestItemResponse
                                            .containsKey(tokenizedOverlap[m])) {

                                checkAndAddFailure(failedValidations,
                                        tokenizedOverlap[m],
                                        "Duplicate Overlap Found in Field Discrete exposed items");

                            } else {
                                pretestItemResponse.put(tokenizedOverlap[m], i
                                        + "");
                            }

                            listOfDiscreteFTItemOverlaps = appendSemicolon(
                                    listOfDiscreteFTItemOverlaps,
                                    tokenizedOverlap[m],
                                    listOfDiscreteFTItemOverlaps
                                            .split(DELIMITER).length);

                        }

                    } else
                        continue;
                }
            }
            if (listOfDiscreteFTItemOverlaps != null
                    && !listOfDiscreteFTItemOverlaps.equalsIgnoreCase("")) {
                TNReporter
                        .addNote("List of Overlaps for the Discrete FT items Exposed "
                                + listOfDiscreteFTItemOverlaps);
            }

        } else {
            pretestItemResponse = null;
        }
        String operationalItems[][] = getArrayBasedOnPretestAndType(
                itemUserResponse, OPERATIONAL, DISCRETE, INDEX_OF_PRETEST,
                INDEX_OF_TYPE);

        if (operationalItems != null) {
            String listOfOperationalItemOverlaps = "";

            for (int i = 0; i < operationalItems.length; i++) {

                if (operationalItems[i][INDEX_OF_OVERLAP_CONSTRAINT] != null) {
                    if (operationalItems[i][INDEX_OF_OVERLAP_CONSTRAINT]
                            .contains(OVERLAP_TOKEN)) {
                        String tokenizedOverlap[] = operationalItems[i][INDEX_OF_OVERLAP_CONSTRAINT]
                                .substring(
                                        START_INDEX,
                                        operationalItems[i][INDEX_OF_OVERLAP_CONSTRAINT]
                                                .indexOf(" ")).split(",");

                        for (int m = 0; m < tokenizedOverlap.length; m++) {
                            if (!tokenizedOverlap[m].contains(OVERLAP_TOKEN)) {
                                tokenizedOverlap[m] = OVERLAP_TOKEN
                                        + tokenizedOverlap[m];
                            }
                            LOGGER.debug(tokenizedOverlap[m]);
                            if (setItemResponse != null
                                    && setItemResponse
                                            .containsKey(tokenizedOverlap[m])) {

                                checkAndAddFailure(failedValidations,
                                        tokenizedOverlap[m],
                                        "Overlap is present in Field Set and Operational exposed items");

                            }

                            if (pretestItemResponse != null
                                    && pretestItemResponse
                                            .containsKey(tokenizedOverlap[m])) {
                                checkAndAddFailure(failedValidations,
                                        tokenizedOverlap[m],
                                        "Overlap is present in Field Discrete and Operational exposed items");

                            }
                            if (operationalItemResponse != null
                                    && operationalItemResponse
                                            .containsKey(tokenizedOverlap[m])) {
                                checkAndAddFailure(failedValidations,
                                        tokenizedOverlap[m],
                                        "Duplicate Overlaps found in Operational exposed items");
                            } else {
                                operationalItemResponse.put(
                                        tokenizedOverlap[m], i + "");
                            }

                            listOfOperationalItemOverlaps = appendSemicolon(
                                    listOfOperationalItemOverlaps,
                                    tokenizedOverlap[m],
                                    listOfOperationalItemOverlaps
                                            .split(DELIMITER).length);
                        }

                    } else
                        continue;
                }
            }
            if (listOfOperationalItemOverlaps != null
                    && !listOfOperationalItemOverlaps.equalsIgnoreCase("")) {
                TNReporter
                        .addNote("List of Overlaps for the Operational items Exposed "
                                + listOfOperationalItemOverlaps);
            }
        } else {
            operationalItemResponse = null;
        }

        if (failedValidations.size() > 0) {
            Iterator<Entry<String, String>> it = failedValidations.entrySet()
                    .iterator();
            while (it.hasNext()) {
                Map.Entry<String, String> pair = (Entry<String, String>) it
                        .next();
                LOGGER.error(pair.getValue() + " - " + pair.getKey());
                TNReporter.addFailureReason(pair.getValue() + " - "
                        + pair.getKey());
                it.remove();
            }

            assertAndReport(false,
                    "Check Failure reason for the enemy items found");

        } else {
            assertAndReport(
                    true,
                    testSessionIDs
                            + " Has no Enemy Items encoutered during the course of the session");
        }

    }

    public synchronized void checkAndAddFailure(
            HashMap<String, String> failedScenarios, String failedOverlap,
            String failureMsg) {
        if (failedScenarios.containsKey(failedOverlap)) {

            failedScenarios.put(failedOverlap + " Instance 2", failureMsg);
        } else {
            failedScenarios.put(failedOverlap, failureMsg);
        }
    }

    @Parameters(value = { "noOfDays" })
    @BeforeClass(alwaysRun = true)
    public void setNoOFDays(@Optional("10") String noOfDays) {
        if (!isNoOfDaysSet) {
            this.noOfDays = noOfDays;
            TNReporter.addSuiteInfo("Enemy Item Verification done for - <b>"
                    + noOfDays + " Days</b>");
        }
    }

    @DataProvider(name = "enemyItemsInput")
    public Object[][] setIdData() throws Exception {
        DBUtils dbu = new DBUtils(getConnection());
        Object[][] rValue = dbu.runAndReturnMultiDimentionalArray(String
                .format(TEST_SESSION_IDS, noOfDays));
        count = rValue.length;
        return rValue;

    }

    public String[][] getArrayBasedOnPretestAndType(
            String[][] itemUserResponse, String pretest, String type,
            int indexOfPretest, int indexOfType) {
        if (itemUserResponse == null) {
            LOGGER.info("No User Responses are present");
            return null;
        }
        int noOfRows = 0;
        for (int i = 0; i < itemUserResponse.length; i++) {
            if (itemUserResponse[i][indexOfPretest] != null
                    && itemUserResponse[i][indexOfPretest]
                            .equalsIgnoreCase(pretest)
                    && itemUserResponse[i][indexOfType] != null
                    && itemUserResponse[i][indexOfType].equalsIgnoreCase(type)) {
                noOfRows++;
            }

        }

        // if no items of (Pretest,Type) parameter is available
        if (noOfRows == 0) {
            LOGGER.info(String
                    .format("No Responses of Pretest =%s AND Type =%s is present in the current Test session",
                            pretest, type));

            return null;
        }
        String arrReturn[][] = new String[noOfRows][noOfCoumns];
        for (int j = 0, k = 0; j < itemUserResponse.length; j++) {
            if (itemUserResponse[j][indexOfPretest] != null
                    && itemUserResponse[j][indexOfPretest]
                            .equalsIgnoreCase(pretest)
                    && itemUserResponse[j][indexOfType] != null
                    && itemUserResponse[j][indexOfType].equalsIgnoreCase(type)) {
                arrReturn[k++] = itemUserResponse[j];
            }
        }
        return arrReturn;
    }

    public String appendSemicolon(String sequence, String value, int index) {
        sequence = sequence + value + DELIMITER;
        if (index != 0 && index % 20 == 0) {
            sequence = sequence + " ";
        }
        return sequence;

    }
}
