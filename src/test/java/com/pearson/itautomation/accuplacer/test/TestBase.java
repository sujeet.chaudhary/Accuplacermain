package com.pearson.itautomation.accuplacer.test;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.net.InetAddress;
import java.net.MalformedURLException;
import java.net.URL;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Properties;

import org.apache.commons.lang3.time.FastDateFormat;
import org.openqa.selenium.Rotatable;
import org.openqa.selenium.ScreenOrientation;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.firefox.FirefoxProfile;
import org.openqa.selenium.remote.Augmenter;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.testng.Assert;
import org.testng.annotations.AfterSuite;
import org.testng.annotations.BeforeSuite;
import org.testng.annotations.Optional;
import org.testng.annotations.Parameters;

import com.pearson.itautomation.accuplacer.core.AccuplacerBrowser;
import com.pearson.itautomation.accuplacer.core.CommonUtilities;
import com.pearson.itautomation.accuplacer.core.GridInfoExtractor;
import com.pearson.itautomation.accuplacer.core.HTMLUtils;
import com.pearson.itautomation.accuplacer.reports.isr.SubmitReport;
import com.pearson.itautomation.bca.Browser;
import com.pearson.itautomation.bca.ObjectRepository;
import com.pearson.itautomation.bca.ori.xsd.XmlORImporter;
import com.pearson.itautomation.testngutils.reporting.TNReporter;
import com.pearson.itautomation.testngutils.reporting.XSLName;

public class TestBase extends TestBaseAssert {
    private static final Logger LOGGER = LoggerFactory
            .getLogger(TestBase.class);

    private static final String CHROMEDRIVER_PATH = CommonUtilities.appPath()
            + "\\Exe\\chromedriver.exe";
    protected static final String DEFAULT_XSL = "src/test/resources/testng-results.xsl";
    protected Browser browser = null;
    protected static String environment;
    private static String objectRepoLocation;
    private static final Map<USER_ROLE, User> users = new HashMap<>();
    private static Connection conn;
    private static Properties envProperties;
    private static ObjectRepository objRepo;
    private static String outputDirectory;
    protected static String currentExecTimeStamp;
    private static List<SubmitReport> temporaryList;
    protected static String browserName;
    public String nodeIP = null;
    public String nodePort = null;

    private static String remoteIP;
    public static final String USERNAME = "drrk1";
    public static final String AUTOMATE_KEY = "fkDpqkozn4xaNDwEpssd";
    public static final String URL = "http://" + USERNAME + ":" + AUTOMATE_KEY
            + "@hub.browserstack.com/wd/hub";
    public static final String AUTOMATION_FILE_DOWNLOAD = CommonUtilities
            .appPath() + "\\AutomationDownloadWorkspace";
    public static final String AUTOMATION_FILE_DOWNLOAD1 = CommonUtilities
            .appPath() + "\\AutomationDownloadWorkspace1";
    private static final String DUMMY_IP = "null1";
    private static boolean dbConnection = true;

    @Parameters(value = { "browser", "environment", "objectRepository",
            "outputDirectory", "remoteIP" })
    @BeforeSuite(alwaysRun = true)
    public final void beforeSuite(
            @Optional("Firefox") final String browserName,
            @Optional("int-cat") final String env,
            @Optional("src\\main\\resources\\OR_Accuplacer.xml") final String objRepoLoc,
            @Optional("c:/accuplacer_output/") final String outputLoc,
            @Optional(DUMMY_IP) String remoteIP) {
        TestBase.remoteIP = remoteIP;
        TNReporter.writeXSL(XSLName.DEFAULT_TESTNG_VIEW, new File(DEFAULT_XSL));
        if (browserName.equalsIgnoreCase("FireFox1")) {
            dbConnection = false;
        }
        TestBase.browserName = browserName;
        environment = env;
        objectRepoLocation = objRepoLoc;
        FastDateFormat fdf = FastDateFormat
                .getInstance("MMM dd yyyy - kk.mm.ss");
        String currDate = fdf.format(new Date()) + File.separatorChar;
        outputDirectory = outputLoc + currDate;
        currentExecTimeStamp = Long.toString(System.currentTimeMillis());
        LOGGER.info("Current Execution Time Stamp : " + currentExecTimeStamp);

        Assert.assertTrue(new File(outputDirectory).mkdirs(),
                "Unable to create output folder for data. '" + outputDirectory
                        + "'");
        System.out.println("browserName " + browserName);
        if (TestBase.browserName == null) {
            LOGGER.error("Unsupported browser: " + browserName);
            throw new IllegalArgumentException("Unsupported browser: "
                    + browserName);
        }
        loadEnvProperties();
        if (!TestBase.remoteIP.equalsIgnoreCase(DUMMY_IP)) {
            TNReporter.addSuiteInfo("Platform = Docker , Browser = "
                    + browserName);

        } else {
            TNReporter.addSuiteInfo("Browser used: " + browserName);

        }
        TNReporter.addSuiteInfo("Target Environment: " + environment);
        TNReporter.addSuiteInfo("App URL: " + getAppURL());
    }

    @AfterSuite(alwaysRun = true)
    public final void afterSuite() {
        quitBrowser();

        if (conn != null) {
            try {
                conn.close();
                LOGGER.info("Connection is closed");
            } catch (SQLException e) {
                LOGGER.error(
                        "There was an issue closing the connection to the database.",
                        e);
            }
        }
    }

    /**
     * Get the application URL.
     * 
     * @return The application URL.
     */
    public final String getAppURL() {
        return envProperties.getProperty("APP_URL");
    }

    /**
     * Quit the browser.
     */
    public final synchronized void quitBrowser() {
        if (browser != null) {
            browser.manage().deleteAllCookies();
            browser.close();
            browser.quit();
        }

        browser = null;
    }

    /**
     * Get a user based on the role.
     * 
     * @param userRole
     *            The role the user is assigned to.
     * @return The user that matches the role.
     */
    public static final User getUser(final USER_ROLE userRole) {
        return users.get(userRole);
    }

    /**
     * Get the output directory where data files should be placed.
     * 
     * @return The output directory.
     */
    public static final String getOutputDirectory() {
        return outputDirectory;
    }

    /**
     * Get the connection to the database.
     * 
     * @return The database connection.
     */
    public static synchronized final Connection getConnection() {
        if (conn == null) {
            try {
                Class.forName("oracle.jdbc.driver.OracleDriver");
            } catch (ClassNotFoundException e) {
                LOGGER.error("Unable to load Oracle DB Driver.", e);
                return conn;
            }

            String dbHost = envProperties.getProperty("DB_HOST");
            String dbPort = envProperties.getProperty("DB_PORT");
            String dbServiceName = envProperties.getProperty("DB_SERVICE_NAME");
            String dbUser = envProperties.getProperty("DB_USER");
            String dbPass = envProperties.getProperty("DB_PASS");
            String dbSID = envProperties.getProperty("DB_SID");
            String connectionString = null;
            if (dbServiceName == null || dbServiceName.equalsIgnoreCase("NA")
                    || dbServiceName.equalsIgnoreCase("")) {
                connectionString = String.format("jdbc:oracle:thin:@%s:%s:%s",
                        dbHost, dbPort, dbSID);
            } else {
                connectionString = String.format(
                        "jdbc:oracle:thin:@//%s:%s/%s", dbHost, dbPort,
                        dbServiceName);
            }

            try {
                conn = DriverManager.getConnection(connectionString, dbUser,
                        dbPass);
            } catch (SQLException e) {
                LOGGER.error("Could not connect to database: '"
                        + connectionString + "'", e);
            }
        }

        return conn;
    }

    private static synchronized void loadEnvProperties() {
        if (envProperties == null) {
            envProperties = new Properties();
            File propertiesLocation = new File("env." + environment
                    + ".properties");
            try {
                envProperties.load(new FileInputStream(propertiesLocation));
                createUsers();
            } catch (IOException e) {
                LOGGER.error("Could not load environment properties from "
                        + propertiesLocation.getAbsolutePath(), e);
            }
        }
    }

    private static void createUsers() {
        for (USER_ROLE role : USER_ROLE.values()) {
            if (envProperties.containsKey(role.toString())) {
                String[] tokens = envProperties.getProperty(role.toString())
                        .split(":");
                User user = new User(tokens[0], tokens[1], role);
                users.put(role, user);
            }
        }
    }

    protected synchronized WebDriver setFireFoxProfile() {

        FirefoxProfile profile = new FirefoxProfile();
        profile.setPreference("browser.download.folderList", 2);
        profile.setPreference("browser.download.manager.showWhenStarting",
                false);
        profile.setPreference("browser.download.dir", AUTOMATION_FILE_DOWNLOAD);
        profile.setPreference(
                "browser.helperApps.neverAsk.openFile",
                "text/csv,application/x-msexcel,application/excel,application/x-excel,application/vnd.ms-excel,image/png,image/jpeg,text/html,text/plain,application/msword,application/xml");
        profile.setPreference(
                "browser.helperApps.neverAsk.saveToDisk",
                "text/csv,application/x-msexcel,application/excel,application/x-excel,application/vnd.ms-excel,image/png,image/jpeg,text/html,text/plain,application/msword,application/xml");
        profile.setPreference("browser.helperApps.alwaysAsk.force", false);
        profile.setPreference("browser.download.manager.alertOnEXEOpen", false);
        profile.setPreference("browser.download.manager.focusWhenStarting",
                false);
        profile.setPreference("browser.download.manager.useWindow", false);
        profile.setPreference("browser.download.manager.showAlertOnComplete",
                false);
        profile.setPreference("browser.download.manager.closeWhenDone", false);
        return new FirefoxDriver(profile);
    }

    public synchronized Browser getBrowser() {
        if (dbConnection && !(browserName.equalsIgnoreCase("remote"))) {
            try {
                ResultSet rset = null;
                Statement stmt = null;
                String sqlwriteplacerresponse = "select 1 from Dual";
                stmt = getConnection().createStatement();
                rset = stmt.executeQuery(sqlwriteplacerresponse);
                rset.getFetchSize();
            } catch (SQLException e) {
                LOGGER.error("Error Occured While executing the query", e);
                LOGGER.info("This is stack trace for SQL exception");
                e.printStackTrace();
            }
        }
        if (browser == null) {
            if (browserName.equalsIgnoreCase("Firefox")) {
                WebDriver driver = setFireFoxProfile();
                browser = new AccuplacerBrowser(driver);
            } else if (browserName.equalsIgnoreCase("Chrome")) {
                System.setProperty("webdriver.chrome.driver", CHROMEDRIVER_PATH);
                browser = new AccuplacerBrowser(new ChromeDriver());
            } else if (browserName.equalsIgnoreCase("BrowserStack")) {
                DesiredCapabilities caps = new DesiredCapabilities();
                caps.setCapability("browserName", "android");
                caps.setCapability("platform", "ANDROID");
                caps.setCapability("device", "Google Nexus 7");
                caps.setCapability("rotatable", true);
                caps.setCapability("browserstack.debug", "true");
                try {
                    URL url = new URL(URL);
                    WebDriver driver = new RemoteWebDriver(url, caps);
                    WebDriver augmentedDriver = new Augmenter().augment(driver);
                    ((Rotatable) augmentedDriver)
                            .rotate(ScreenOrientation.LANDSCAPE);
                    browser = new AccuplacerBrowser(driver);
                } catch (MalformedURLException e) {
                    LOGGER.error(
                            "URL is Malformed , could not open the browser", e);
                }
                browser = null;
            } else if (browserName.equalsIgnoreCase("remote")) {
                URL remoteAddress = null;
                try {
                    DesiredCapabilities capability = DesiredCapabilities
                            .firefox();
                    String hostIP;
                    if (remoteIP != null && remoteIP.equalsIgnoreCase(DUMMY_IP)) {
                        hostIP = (InetAddress.getLocalHost() + "").split("/")[1];
                    } else {
                        hostIP = remoteIP;
                    }
                    int port = 4444;
                    String URL = "http://" + hostIP + ":" + port + "/wd/hub";
                    LOGGER.info("Connecting to GRID " + URL);
                    remoteAddress = new URL(URL);
                    RemoteWebDriver remoteDriver = new RemoteWebDriver(
                            remoteAddress, capability);
                    browser = (new AccuplacerBrowser(remoteDriver));
                    if (hostIP != null && remoteAddress != null) {
                        String[] nodeInfo = new GridInfoExtractor()
                                .getHostNameAndPort(hostIP, port,
                                        remoteDriver.getSessionId());
                        nodeIP = nodeInfo[0];
                        nodePort = nodeInfo[1];
                    }

                } catch (Exception e) {
                    LOGGER.error(
                            "Exception occured while requesting the Remote Node IP",
                            e);
                }

            }
            if (objRepo == null) {
                objRepo = new ObjectRepository(new XmlORImporter(new File(
                        objectRepoLocation)));
            }
            browser.setObjectRepository(objRepo);
            browser.manage().window().maximize();

        }

        return browser;

    }

    public static class User {
        private final String userName;
        private final String password;
        private final USER_ROLE userRole;

        public User(final String userName, final String password,
                final USER_ROLE userRole) {
            super();
            this.userName = userName;
            this.password = password;
            this.userRole = userRole;
        }

        /**
         * @return the userName
         */
        public final String getUserName() {
            return userName;
        }

        /**
         * @return the password
         */
        public final String getPassword() {
            return password;
        }

        /**
         * @return the userRole
         */
        public final USER_ROLE getUserRole() {
            return userRole;
        }
    }

    public static enum USER_ROLE {
        INSTITUTE_ADMIN, COLLEGE_BOARD_ADMIN_APPROVE, SITE_MANAGER, COLLEGE_BOARD_ADMIN, GROUP_USER, CHANGE_PASSWORD, PROCTOR_REPORTER, PROCTOR, GENERIC_USER, RETEST_ATTEMPT_SM;
    }

    /**
     * @param condition
     *            - boolean value of the action
     * @param strActionName
     *            - One liner Description of what the action Does<br>
     *            <u>e.g.</u> Fill Create Institution Form
     */

    public static List<SubmitReport> getTemporaryList() {
        return temporaryList;
    }

    public static void setTemporary(List<SubmitReport> temporary) {
        TestBase.temporaryList = temporary;
    }

    public void addTestCaseDescription(String tcDescription) {
        if (nodeIP != null && nodePort != null) {
            TNReporter.addTestCaseDescription(tcDescription
                    + HTMLUtils.getLinebreak() + HTMLUtils.getBoldTagStart()
                    + "Node IP - " + nodeIP + ", Port - " + nodePort
                    + HTMLUtils.getBoldTagEnd());
        } else {
            TNReporter.addTestCaseDescription(tcDescription);
        }
    }
}
