package com.pearson.itautomation.accuplacer.test;

public class TestSessionCount {

    private int count;
    private String testSessionIds;

    public TestSessionCount(int count, String testSessionIds) {
        this.setCount(count);
        this.setTestSessionIds(testSessionIds);
    }

    public int getCount() {
        return count;
    }

    public void setCount(int count) {
        this.count = count;
    }

    public String getTestSessionIds() {
        return testSessionIds;
    }

    public void setTestSessionIds(String testSessionIds) {

        this.testSessionIds = testSessionIds;

    }
}
