package com.pearson.itautomation.accuplacer.test;

import com.pearson.itautomation.accuplacer.core.CommonUtilities;

public class JenkinsEmail extends TestBase {

    public static void main(String args[]) {
        EmailMain
                .triggerEmail(
                        "Email For Automation Run",
                        "(" + CommonUtilities.getCurrentDateTime()
                                + ")Jenkins_Functional Automation Run",
                        CommonUtilities.EMAIL_PROPERTIES.getProperty(
                                "JENKINSEMAIL").trim(),
                        CommonUtilities.EMAIL_PROPERTIES
                                .getProperty("fromMail").trim());
    }

}
