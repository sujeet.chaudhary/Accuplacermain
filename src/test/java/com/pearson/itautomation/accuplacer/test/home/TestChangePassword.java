package com.pearson.itautomation.accuplacer.test.home;

import org.openqa.selenium.By;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import com.pearson.itautomation.accuplacer.core.action.ChangePassword;
import com.pearson.itautomation.accuplacer.core.action.ChangeSecurityQuestions;
import com.pearson.itautomation.accuplacer.core.action.LoginAction;
import com.pearson.itautomation.accuplacer.core.action.NavigateToTab;
import com.pearson.itautomation.accuplacer.test.TestBase;
import com.pearson.itautomation.bca.BrowserElement;
import com.pearson.itautomation.bca.validation.ElementExistsValidation;

/**
 * This Class consists of the following test cases <br>
 * <li><b>changeOnlyPassword</b> - This Test case method will change password of
 * the logged in used and revert the same <br> <li>
 * <b>changePasswordWithSecurityQuestions</b> - This Test case method will
 * change password and security questions of the logged in used and revert the
 * same
 * 
 * @author Deepak Radhakrishnan
 * 
 */
public class TestChangePassword extends TestBase {

    User user;
    private static final String NEW_PASSWORD = "Pearson.13";
    private static String RESPONSE1 = "1";
    private static String RESPONSE2 = "2";
    private static String RESPONSE3 = "3";

    private static final String CHANGE_PASSWORD_VALIDATION_TEXT = "'Your password has been updated successfully. "
            + "Please use the new password on your next login.'";
    private static final String CHANGE_SECURITY_QUESTIONS_VALIDATION_TEXT = "'Your password and security questions have"
            + " been updated successfully.'";

    @BeforeMethod(alwaysRun = true)
    public void beforeClass() {
        browser = getBrowser();
        browser.get(getAppURL());
        user = getUser(USER_ROLE.CHANGE_PASSWORD);
        assertAndReport(
                browser,
                new LoginAction(this.browser, user.getUserName(), user
                        .getPassword()).performWithStates(), "Login Action");
    }

    @Test(groups = { "sanity", "regression", "Users", "CATTECHTA-17" })
    public void changeOnlyPassword() {
        addTestCaseDescription("Change and Revert a Users Password");
        assertAndReport(browser, new ChangePassword(browser,
                user.getPassword(), NEW_PASSWORD).performWithStates(),
                "Change User Password for " + user.getUserName());
        assertAndReport(
                browser,
                new ElementExistsValidation(this.browser, new BrowserElement(
                        "ChangePasswordValidate1", By
                                .xpath("//strong[contains(text(),"
                                        + CHANGE_PASSWORD_VALIDATION_TEXT
                                        + ")]"))).performWithStates(),
                "Verify Change Password");
        assertAndReport(browser,
                new NavigateToTab(browser, "Home").performWithStates(),
                "User Navigated to Home Tab");
        assertAndReport(browser,
                new ChangePassword(browser, NEW_PASSWORD, user.getPassword())
                        .performWithStates(), "Revert User Password for "
                        + user.getUserName());
        assertAndReport(
                browser,
                new ElementExistsValidation(this.browser, new BrowserElement(
                        "ChangePasswordValidate2", By
                                .xpath("//strong[contains(text(),"
                                        + CHANGE_PASSWORD_VALIDATION_TEXT
                                        + ")]"))).performWithStates(),
                "Verify Revert Password");

    }

    @Test(groups = { "sanity", "regression", "Users", "CATTECHTA-17" }, dependsOnMethods = { "changeOnlyPassword" })
    public void changePasswordWithSecurityQuestions() {
        addTestCaseDescription("Change and Revert a Users Password and Security Question");
        assertAndReport(browser,
                new NavigateToTab(browser, "Home").performWithStates(),
                "User Navigated to Home Tab");
        assertAndReport(browser, new ChangePassword(browser,
                user.getPassword(), NEW_PASSWORD).performWithStates(),
                "Change User Password for " + user.getUserName());
        assertAndReport(
                browser,
                new ElementExistsValidation(this.browser, new BrowserElement(
                        "ChangePasswordValidate1", By
                                .xpath("//strong[contains(text(),"
                                        + CHANGE_PASSWORD_VALIDATION_TEXT
                                        + ")]"))).performWithStates(),
                "Verify Change Password");
        assertAndReport(browser, new ChangeSecurityQuestions(browser,
                RESPONSE1, RESPONSE2, RESPONSE2).performWithStates(),
                "Change Security Question");

        assertAndReport(
                browser,
                new ElementExistsValidation(this.browser, new BrowserElement(
                        "ChangeSecurityValidate1",
                        By.xpath("//strong[contains(text(),"
                                + CHANGE_SECURITY_QUESTIONS_VALIDATION_TEXT
                                + ")]"))).performWithStates(),
                "Verify Security Respone Changes");
        assertAndReport(browser,
                new NavigateToTab(browser, "Home").performWithStates(),
                "User Navigated to Home Tab");
        assertAndReport(browser,
                new ChangePassword(browser, NEW_PASSWORD, user.getPassword())
                        .performWithStates(), "Revert User Password for "
                        + user.getUserName());
        assertAndReport(
                browser,
                new ElementExistsValidation(this.browser, new BrowserElement(
                        "ChangePasswordValidate2", By
                                .xpath("//strong[contains(text(),"
                                        + CHANGE_PASSWORD_VALIDATION_TEXT
                                        + ")]"))).performWithStates(),
                "Verify Revert Password");
        assertAndReport(browser, new ChangeSecurityQuestions(browser,
                RESPONSE1, RESPONSE2, RESPONSE3).performWithStates(),
                "Revert Security Question");
        assertAndReport(
                browser,
                new ElementExistsValidation(this.browser, new BrowserElement(
                        "ChangeSecurityValidate2",
                        By.xpath("//strong[contains(text(),"
                                + CHANGE_SECURITY_QUESTIONS_VALIDATION_TEXT
                                + ")]"))).performWithStates(),
                "Verify Revert Security Respone Changes");
    }

    @AfterMethod(alwaysRun = true)
    public void teardown() {
        quitBrowser();
    }
}
