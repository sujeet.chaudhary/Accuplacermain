package com.pearson.itautomation.accuplacer.test.administertest;

import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import com.pearson.itautomation.accuplacer.core.Buttons;
import com.pearson.itautomation.accuplacer.test.TestBase;
import com.pearson.itautomation.bca.BrowserElement;
import com.pearson.itautomation.testngutils.dataprovider.ExcelDataProvider;
import com.pearson.itautomation.testngutils.reporting.TNReporter;

public class TestAdminsterWithSaveResumeSiteManager extends TestBase {

    TestAdminsterCommon testAdminCommonProctorReporter = new TestAdminsterCommon();
    public final BrowserElement btnAddBranchingProfile = Buttons
            .getButtonType1("Add");

    @Test(dataProvider = "standardExcelData", dataProviderClass = ExcelDataProvider.class, groups = {
            "regression", "AdministerTest", "CATTECHTA-101", "CATTECHTA-115" })
    public void adminTestSiteManagerRegression(String studentId,
            String lastName, String DOB, String MOB, String YOB,
            String branchingProfile, String answerKeys, String saveAtQuestions) {
        administerTestSM(studentId, lastName, DOB, MOB, YOB, branchingProfile,
                answerKeys, saveAtQuestions);
    }

    @Test(dataProvider = "standardExcelData", dataProviderClass = ExcelDataProvider.class, groups = {
            "sanity", "AdministerTest", "CATTECHTA-101", "CATTECHTA-115" })
    public void adminTestSiteManagerSanity(String studentId, String lastName,
            String DOB, String MOB, String YOB, String branchingProfile,
            String answerKeys, String saveAtQuestions) {
        administerTestSM(studentId, lastName, DOB, MOB, YOB, branchingProfile,
                answerKeys, saveAtQuestions);
    }

    public void administerTestSM(String studentId, String lastName, String DOB,
            String MOB, String YOB, String branchingProfile, String answerKeys,
            String saveAtQuestions) {
        addTestCaseDescription("Administer Test as a Proctor Reporter");
        TNReporter.setParmDesc("Student ID", "Student's Last Name",
                "Date Of Birth", "Month Of Birth", "Year Of Birth",
                "Branching Profile", "Answer Key Parameter",
                "Save And Resume At");
        User user = getUser(USER_ROLE.SITE_MANAGER);
        testAdminCommonProctorReporter.administerTestCommon(browser, user,
                getConnection(), studentId, lastName, DOB, MOB, YOB,
                branchingProfile, answerKeys, saveAtQuestions);
    }

    @BeforeMethod(alwaysRun = true)
    public void setUp() {
        browser = getBrowser();
        browser.get(getAppURL());
    }

    @AfterMethod(alwaysRun = true)
    public void afterClass() {
        quitBrowser();
    }
}
