package com.pearson.itautomation.accuplacer.test.deletebatch;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.Properties;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.testng.annotations.Test;

import com.pearson.itautomation.accuplacer.core.CommonUtilities;
import com.pearson.itautomation.accuplacer.test.TestBase;
import com.pearson.itautomation.testngutils.reporting.TNReporter;

public class TestDeleteBatch extends TestBase {

    private static final Logger LOGGER = LoggerFactory
            .getLogger(TestDeleteBatch.class);
    private static final long FAILURE_CODE = 999999999;
    protected Properties sqlQueries = CommonUtilities
            .readPropertyFile("//config//SQL.properties");

    @Test(groups = { "DeleteBatch" })
    public void deleteBranchingProfiles() {
        if (environment.equalsIgnoreCase("prod")) {
            assertAndReport(false, "Failing test case as environment is prod");
        }
        long numberofRecordsDeleted;

        addTestCaseDescription("This Test Case will Perform a Soft Delete for the Branching Profiles created During the Automation Runs");
        String deleteSQLBP = sqlQueries
                .getProperty("deleteSQLBranchingProfile");
        numberofRecordsDeleted = deleteSQL(deleteSQLBP, "Branching Profile");
        TNReporter.addNote("Successfully Deleted " + numberofRecordsDeleted
                + " Branching Profiles");
    }

    @Test(groups = { "DeleteBatch" })
    public void deletePlacementRules() {
        if (environment.equalsIgnoreCase("prod")) {
            assertAndReport(false, "Failing test case as environment is prod");
        }
        long numberofRecordsDeleted;
        addTestCaseDescription("This Test Case will Perform a Soft Delete for the Placement Rules created During the Automation Runs");
        String deleteSQLBP = sqlQueries.getProperty("deleteSQLPlacementRules");
        numberofRecordsDeleted = deleteSQL(deleteSQLBP, "Placement Rule");
        TNReporter.addNote("Successfully Deleted " + numberofRecordsDeleted
                + " Placement Rules ");
    }

    @Test(groups = { "DeleteBatch" })
    public void deleteTestSettings() {
        if (environment.equalsIgnoreCase("prod")) {
            assertAndReport(false, "Failing test case as environment is prod");
        }
        long numberofRecordsDeleted;
        addTestCaseDescription("This Test Case will Perform a Soft Delete for the Test Settings created During the Automation Runs");
        String deleteSQLBP = sqlQueries.getProperty("deleteSQLTestSettings");
        numberofRecordsDeleted = deleteSQL(deleteSQLBP, "Test Setting");
        TNReporter.addNote("Successfully Deleted " + numberofRecordsDeleted
                + " Test Settings ");
    }

    @Test(groups = { "DeleteBatch" })
    public void deleteCompositeScores() {
        if (environment.equalsIgnoreCase("prod")) {
            assertAndReport(false, "Failing test case as environment is prod");
        }
        long numberofRecordsDeleted;
        addTestCaseDescription("This Test Case will Perform a Soft Delete for the Composite Scores created During the Automation Runs");
        String deleteSQLBP = sqlQueries.getProperty("deleteSQLCompositeScores");
        numberofRecordsDeleted = deleteSQL(deleteSQLBP, "Composite Score");
        TNReporter.addNote("Successfully Deleted " + numberofRecordsDeleted
                + " Composite Scores ");
    }

    public long deleteSQL(String deleteSQL, String entity) {
        PreparedStatement pStatement = null;
        long numberOfRecords = FAILURE_CODE;
        Connection conn = getConnection();
        long startTime = System.currentTimeMillis();
        try {
            conn.setAutoCommit(false);
            pStatement = conn.prepareStatement(deleteSQL);
            if (pStatement != null) {
                LOGGER.info("Query : " + deleteSQL);
                numberOfRecords = pStatement.executeUpdate();
                conn.commit();
                LOGGER.info("Number of " + entity + "(s) to be deleted = "
                        + numberOfRecords);
                TNReporter.addNote("Number of " + entity
                        + "(s) to be deleted = " + numberOfRecords);
            } else {
                LOGGER.error("NULL statement is passed");
                return FAILURE_CODE;
            }

        } catch (SQLException e) {
            LOGGER.error("Exception occured while executing the query", e);
        } finally {
            try {
                if (pStatement != null && !conn.isClosed()) {
                    pStatement.close();
                }

            } catch (SQLException e) {
                LOGGER.error(
                        "Exception occured while closing the statemnet, resultset",
                        e);
            }
            long endTime = System.currentTimeMillis();
            LOGGER.info("Total Time Taken = " + (endTime - startTime) / 1000
                    + " Seconds");
            TNReporter.addNote("Total Time Taken = " + (endTime - startTime)
                    / 1000 + " Seconds");
        }
        return numberOfRecords;
    }
}
