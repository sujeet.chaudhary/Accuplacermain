package com.pearson.itautomation.accuplacer.test.administertest;

import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import com.pearson.itautomation.accuplacer.core.CommonUtilities;
import com.pearson.itautomation.accuplacer.test.TestBase;
import com.pearson.itautomation.testngutils.dataprovider.ExcelDataProvider;
import com.pearson.itautomation.testngutils.reporting.TNReporter;

public class TestAdminsterRetestAttempt extends TestBase {

    TestAdminsterCommon testAdminCommonProctorReporter = new TestAdminsterCommon();

    @Test(dataProvider = "standardExcelData", dataProviderClass = ExcelDataProvider.class, groups = { "test" })
    public void adminTestGenericUserRT(String branchingProfile,
            String answerKeys, String saveAtQuestions) {
        String studentID = CommonUtilities.getCurrentddhhmmss();
        administerTestGenericUser(studentID, "dlast", "1", "Jan", "2000",
                branchingProfile, answerKeys, saveAtQuestions);
    }

    public void administerTestGenericUser(String studentID, String lastName,
            String DOB, String MOB, String YOB, String branchingProfile,
            String answerKeys, String saveAtQuestions) {
        addTestCaseDescription("Administer Test as a Generic User");
        TNReporter.setParmDesc("Branching Profile", "Answer Key Parameter",
                "Save And Resume At");
        User user = getUser(USER_ROLE.RETEST_ATTEMPT_SM);
        testAdminCommonProctorReporter.administerTestCommonRetestScenario(
                browser, studentID, user, getConnection(), lastName, DOB, MOB,
                YOB, branchingProfile, answerKeys, saveAtQuestions);
    }

    @BeforeMethod(alwaysRun = true)
    public void setUp() {
        browser = getBrowser();
        browser.get(getAppURL());
    }

    @AfterMethod(alwaysRun = true)
    public void afterClass() {
        quitBrowser();
    }
}
