package com.pearson.itautomation.accuplacer.test;

import java.io.File;
import java.io.IOException;

import org.apache.commons.io.FileUtils;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.testng.Assert;

import com.pearson.itautomation.bca.Browser;
import com.pearson.itautomation.testngutils.reporting.TNReporter;

public class TestBaseAssert {
    private static final Logger LOGGER = LoggerFactory
            .getLogger(TestBaseAssert.class);
    private static int screenshotNumber = 1;

    /**
     * @param condition
     *            - boolean value of the action
     * @param strActionName
     *            - One liner Description of what the action Does<br>
     *            <u>e.g.</u> Fill Create Institution Form
     */
    public void assertAndReport(Browser browser, boolean condition,
            String strActionName) {

        if (condition) {
            TNReporter.addNote(strActionName + " : Passed");
        } else {
            if (browser != null) {
                File scrFile = ((TakesScreenshot) browser.getWebDriver())
                        .getScreenshotAs(OutputType.FILE);
                try {
                    String screenshotName = TestBase.getOutputDirectory()
                            + "screenshot_" + screenshotNumber++ + ".png";
                    FileUtils.copyFile(scrFile, new File(screenshotName));
                    TNReporter.addFailureReason("<A href=\"" + screenshotName
                            + "\">Screenshot</A>");
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }

            Assert.fail(strActionName + " : Failed");
        }

    }

    public void assertAndReport(boolean condition, String strActionName) {

        if (condition) {
            TNReporter.addNote(strActionName + " : Passed");
        } else {

            Assert.fail(strActionName + " : Failed");
        }

    }

    /**
     * @param strActionName
     *            Action to be failed
     * @param strScreenshotName
     *            Name of the ScreenShot
     */
    public void failAndReportWithScreenShot(Browser browser,
            String strActionName, String strScreenshotName) {
        if (browser != null) {
            File scrFile = ((TakesScreenshot) browser.getWebDriver())
                    .getScreenshotAs(OutputType.FILE);
            try {
                String screenshotName = TestBase.getOutputDirectory()
                        + strScreenshotName + ".png";
                FileUtils.copyFile(scrFile, new File(screenshotName));
                TNReporter.addFailureReason("<A href=\"" + screenshotName
                        + "\">Screenshot</A>");
            } catch (IOException e) {
                LOGGER.error("Exception occured while failing the test", e);
            }
        }

        Assert.fail(strActionName + " : Failed");
    }

}
