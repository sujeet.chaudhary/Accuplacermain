package com.pearson.itautomation.accuplacer.test.reports;

import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import com.pearson.itautomation.accuplacer.core.action.LoginAction;
import com.pearson.itautomation.accuplacer.core.action.NavigateToMenu;
import com.pearson.itautomation.accuplacer.test.TestBase;

public class TestDownloadReportFromQueue extends TestBase {

    private static final String TAB_NAME = "Reports";
    private static final String SUBMENU = "Report Queue";

    @BeforeClass(alwaysRun = true)
    public void beforeClass() {
        browser = getBrowser();
        browser.get(getAppURL());
        User user = getUser(USER_ROLE.SITE_MANAGER);
        assertAndReport(
                browser,
                new LoginAction(this.browser, user.getUserName(), user
                        .getPassword()).performWithStates(), "Login Action");
    }

    @Test
    public void downloadReport() {
        addTestCaseDescription("Download Excel from reports under Report Queue");
        assertAndReport(browser, new NavigateToMenu(this.browser, TAB_NAME,
                SUBMENU).performWithStates(),
                "Navigate to Remote Report Queue Page");
        assertAndReport(browser, new NavigateToMenu(this.browser, TAB_NAME,
                SUBMENU).performWithStates(),
                "Navigate to Remote Report Queue Page");
        /*
         * assertAndReport(browser,new ExportExcel(browser).performWithStates(),
         * "Downloading report from Queue");
         */
    }

    @AfterClass(alwaysRun = true)
    public void teardown() {
        quitBrowser();
    }
}
