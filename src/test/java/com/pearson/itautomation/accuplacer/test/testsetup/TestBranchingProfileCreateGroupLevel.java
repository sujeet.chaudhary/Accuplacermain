package com.pearson.itautomation.accuplacer.test.testsetup;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import com.pearson.itautomation.accuplacer.core.Buttons;
import com.pearson.itautomation.accuplacer.core.CommonUtilities;
import com.pearson.itautomation.accuplacer.core.action.LoginAction;
import com.pearson.itautomation.accuplacer.core.action.NavigateToLink;
import com.pearson.itautomation.accuplacer.core.action.NavigateToMenu;
import com.pearson.itautomation.accuplacer.test.TestBase;
import com.pearson.itautomation.bca.BrowserElement;
import com.pearson.itautomation.testngutils.dataprovider.ExcelDataProvider;
import com.pearson.itautomation.testngutils.reporting.TNReporter;

/**
 * This Test Case will add a branching profile at Group level with 1 - 3 rules,
 * and combination of conditions under these rules,Depending on the Test Data
 * Provided
 * 
 * @author Deepak Radhakrishnan
 * 
 */
public class TestBranchingProfileCreateGroupLevel extends TestBase {
    private static final Logger LOGGER = LoggerFactory
            .getLogger(TestBranchingProfileCreateGroupLevel.class);

    private static final String TAB_NAME = "Test Setup";
    private static final String SUB_MENU = "Branching Profiles";
    private final BrowserElement btnAddBranchingProfile = Buttons
            .getButtonType3("Add");
    private User user;
    TestBranchingProfileUtils tBPU = new TestBranchingProfileUtils();

    @Test(dataProvider = "standardExcelData", dataProviderClass = ExcelDataProvider.class, groups = {
            "CATTECHTA-48", "regression", "TestSetup" })
    public void testCreateBPGURegression(String rule1, String rule2,
            String rule3, String condition1, String condition2,
            String condition3, String rule1Value, String rule2Value,
            String rule3Value, String condition1Value, String condition2Value,
            String condition3Value) {
        grpBPCreate(rule1, rule2, rule3, condition1, condition2, condition3,
                rule1Value, rule2Value, rule3Value, condition1Value,
                condition2Value, condition3Value);

    }

    @Test(dataProvider = "standardExcelData", dataProviderClass = ExcelDataProvider.class, groups = {
            "CATTECHTA-48", "sanity", "TestSetup" })
    public void testCreateBPGUSanity(String rule1, String rule2, String rule3,
            String condition1, String condition2, String condition3,
            String rule1Value, String rule2Value, String rule3Value,
            String condition1Value, String condition2Value,
            String condition3Value) {
        grpBPCreate(rule1, rule2, rule3, condition1, condition2, condition3,
                rule1Value, rule2Value, rule3Value, condition1Value,
                condition2Value, condition3Value);

    }

    public void grpBPCreate(String rule1, String rule2, String rule3,
            String condition1, String condition2, String condition3,
            String rule1Value, String rule2Value, String rule3Value,
            String condition1Value, String condition2Value,
            String condition3Value) {

        addTestCaseDescription("Create a Group level Branching Profile as per the Test Data");
        TNReporter.setParmDesc("Type of Rule 1: " + rule1, "Type of Rule 2: "
                + rule2, "Type of Rule 3: " + rule3, "Type of Condition 1: "
                + condition1, "Type of Condition 2: " + condition2,
                "Type of Condition 3: " + condition3, "Value for Rule 1: "
                        + rule1Value, "Value for Rule 2: " + rule2Value,
                "Value for Rule 3: " + rule3Value, "Value for Condition 1: "
                        + condition1Value, "Value for Condition 2: "
                        + condition2Value, "Value for Condition 3: "
                        + condition3Value);
        if (!browser.getPageSource().contains("Branching Profile Name")) {
            if (!browser.getPageSource().contains("Help & Information")) {
                user = getUser(USER_ROLE.GROUP_USER);
                LOGGER.info("User is NOT logged in, logging in as "
                        + user.getUserName());
                assertAndReport(
                        browser,
                        new LoginAction(this.browser, user.getUserName(), user
                                .getPassword()).performWithStates(),
                        "Login Action");
            }
            LOGGER.info("Navigating User to the Add Branching Profile Page");
            assertAndReport(browser, new NavigateToMenu(browser, TAB_NAME,
                    SUB_MENU, btnAddBranchingProfile).performWithStates(),
                    "Navigate to Branching Profile Page");

        }
        tBPU.createBranchingProfiles(
                "```BRANCHINGPROFILE" + CommonUtilities.getCurrentTime(),
                browser, rule1, rule2, rule3, condition1, condition2,
                condition3, rule1Value, rule2Value, rule3Value,
                condition1Value, condition2Value, condition3Value);
    }

    @AfterMethod(alwaysRun = true)
    public void afterMethod() {
        assertAndReport(browser, new NavigateToLink(browser, SUB_MENU,
                btnAddBranchingProfile).performWithStates(),
                "Navigate to Branching Profile Page");
    }

    @BeforeClass(alwaysRun = true)
    public void beforeClass() {
        btnAddBranchingProfile.setTimeout(20000);
        browser = getBrowser();
        browser.get(getAppURL());
    }

    @AfterClass(alwaysRun = true)
    public void afterClass() {
        quitBrowser();
    }

}
