package com.pearson.itautomation.accuplacer.test.testsetup;

import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import com.pearson.itautomation.accuplacer.core.Buttons;
import com.pearson.itautomation.accuplacer.core.CommonUtilities;
import com.pearson.itautomation.accuplacer.core.action.LoginAction;
import com.pearson.itautomation.accuplacer.core.action.NavigateToMenu;
import com.pearson.itautomation.accuplacer.core.action.SearchRecordsAndClick;
import com.pearson.itautomation.accuplacer.test.TestBase;
import com.pearson.itautomation.accuplacer.testsetup.branchingprofiles.CopyBranchingProfile;
import com.pearson.itautomation.accuplacer.testsetup.branchingprofiles.EditBranchingProfileEmpty;
import com.pearson.itautomation.bca.BrowserElement;
import com.pearson.itautomation.testngutils.dataprovider.ExcelDataProvider;

public class TestBranchingProfileOperationsGeneric extends TestBase {

    private static final String BRANCHING_PROFILE_TITLE_FOR_COPY = "000COPY2405";

    private final String strTabName = "Test Setup";
    private final String strSubMenuName = "Branching Profiles";
    public final BrowserElement btnAddBranchingProfile = Buttons
            .getButtonType4("Add");
    private User user;

    @Test(groups = { "sanity", "regression", "CATTECHTA-51", "Test Setup" }, dataProvider = "standardExcelData", dataProviderClass = ExcelDataProvider.class)
    public void testEditBranchingProfile(String userName, String password,
            String branchingProfile) {
        String xpathOfElement = "//td[span[contains(text(),'"
                + branchingProfile + "')] or contains(text(),'"
                + branchingProfile + "')]";
        addTestCaseDescription("Search and Edit Branching Profile");

        user = new User(userName, password, USER_ROLE.GENERIC_USER);
        assertAndReport(
                browser,
                new LoginAction(this.browser, user.getUserName(), user
                        .getPassword()).performWithStates(), "Login Action");
        assertAndReport(browser, new NavigateToMenu(browser, strTabName,
                strSubMenuName).performWithStates(), "Naviagate to Menu");

        assertAndReport(browser, new SearchRecordsAndClick(browser,
                xpathOfElement).performWithStates(), "Searching Records");

        assertAndReport(browser, new EditBranchingProfileEmpty(browser,
                branchingProfile).performWithStates(), "Edit BranchingProfile");
        assertAndReport(browser, new NavigateToMenu(browser, strTabName,
                strSubMenuName).performWithStates(), "Naviagate to Menu");
        assertAndReport(browser, new SearchRecordsAndClick(browser,
                xpathOfElement).performWithStates(), "Searching Records");

        assertAndReport(
                browser,
                new CopyBranchingProfile(browser, branchingProfile,
                        BRANCHING_PROFILE_TITLE_FOR_COPY
                                + CommonUtilities.getCurrentTime())
                        .performWithStates(), "copy BranchingProfile");

    }

    @BeforeMethod(alwaysRun = true)
    public void beforeClass() {
        browser = getBrowser();
        browser.get(getAppURL());
    }

    @AfterMethod(alwaysRun = true)
    public void afterMethod() {
        quitBrowser();
    }
}
