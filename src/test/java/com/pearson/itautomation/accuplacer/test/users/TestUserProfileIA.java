package com.pearson.itautomation.accuplacer.test.users;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import com.pearson.itautomation.accuplacer.core.CommonUtilities;
import com.pearson.itautomation.accuplacer.core.action.LoginAction;
import com.pearson.itautomation.accuplacer.core.action.SaveAndAssert;
import com.pearson.itautomation.accuplacer.test.TestBase;
import com.pearson.itautomation.accuplacer.users.userprofiles.AddUserProfile;
import com.pearson.itautomation.accuplacer.users.userprofiles.DeleteUser;
import com.pearson.itautomation.accuplacer.users.userprofiles.EditUser;
import com.pearson.itautomation.accuplacer.users.userprofiles.NavigateToAddUserPage;
import com.pearson.itautomation.accuplacer.users.userprofiles.SearchApprovedUser;
import com.pearson.itautomation.accuplacer.users.userprofiles.SelectUserType;
import com.pearson.itautomation.bca.BrowserElement;
import com.pearson.itautomation.bca.validation.ElementExistsValidation;
import com.pearson.itautomation.testngutils.dataprovider.ExcelDataProvider;
import com.pearson.itautomation.testngutils.reporting.TNReporter;

public class TestUserProfileIA extends TestBase {

    public String strUser;
    public String strGroupName;
    private static String USER_NAME = "";
    private static final int INDEX_OF_FIELD = 0;
    private BrowserElement eleValidateError;

    @Test(groups = { "sanity", "regression", "CATTECHTA-19", "Users" }, dataProvider = "standardExcelData", dataProviderClass = ExcelDataProvider.class)
    public void instituteUserProfile(final String strUser) {

        final Logger LOGGER = LoggerFactory.getLogger(TestUserProfileIA.class);

        addTestCaseDescription("Add, Approve, Edit, and Delete User");

        TNReporter.setParmDesc("Type of User to be created");

        this.strUser = strUser;

        USER_NAME = CommonUtilities.getCurrentDateTime();

        this.eleValidateError = browser.getBrowserElement("OR_User",
                "validationErrorUserFrom");

        assertAndReport(browser,
                new NavigateToAddUserPage(browser).performWithStates(),
                "NavigateToAddUserPage");

        assertAndReport(browser,
                new AddUserProfile(browser, USER_NAME).performWithStates(),
                "fill the form");

        assertAndReport(browser,
                new SelectUserType(browser, strUser).performWithStates(),
                "Select User Type:" + strUser);

        if (new ElementExistsValidation(getBrowser(), this.eleValidateError)
                .performWithStates()) {
            LOGGER.error("Validation Error Found for the \""
                    + (getBrowser().getElementWithWait(eleValidateError)
                            .getText()).split("is invalid")[INDEX_OF_FIELD]
                    + "\" Field");
            assertAndReport(browser, false, "Validation Error Check");
        }

        assertAndReport(browser,
                new SaveAndAssert(browser, "The user").performWithStates(),
                "Save and verify message for User Type:" + strUser);

        assertAndReport(browser,
                new SearchApprovedUser(browser, USER_NAME).performWithStates(),
                "Search Approved User for User Type:" + strUser);

        assertAndReport(browser, new EditUser(browser).performWithStates(),
                "Edit  User Type:" + strUser);

        assertAndReport(browser, new SaveAndAssert(browser,
                "User Information updated successfully").performWithStates(),
                "Save and Verify Msg");

        assertAndReport(browser,
                new SearchApprovedUser(browser, USER_NAME).performWithStates(),
                "Search User" + strUser);

        assertAndReport(browser, new DeleteUser(browser).performWithStates(),
                "Delete User" + strUser);
    }

    @BeforeClass(alwaysRun = true)
    public void beforeClass() {
        browser = getBrowser();
        browser.get(getAppURL());

        User user = getUser(USER_ROLE.INSTITUTE_ADMIN);

        assertAndReport(
                browser,
                new LoginAction(this.browser, user.getUserName(), user
                        .getPassword()).performWithStates(), "Login Action");
    }

    @AfterClass(alwaysRun = true)
    public void afterClass() {
        quitBrowser();
    }
}
