package com.pearson.itautomation.accuplacer.test.reports;

import java.util.ArrayList;
import java.util.List;

import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import com.pearson.itautomation.accuplacer.core.AccuplacerBrowser;
import com.pearson.itautomation.accuplacer.core.AccuplacerBrowserUtils;
import com.pearson.itautomation.accuplacer.core.CommonUtilities;
import com.pearson.itautomation.accuplacer.core.action.LoginAction;
import com.pearson.itautomation.accuplacer.core.action.NavigateToMenu;
import com.pearson.itautomation.accuplacer.reports.isr.DownloadReport;
import com.pearson.itautomation.accuplacer.reports.isr.SubmitReport;
import com.pearson.itautomation.accuplacer.reports.reportqueue.FileOperations;
import com.pearson.itautomation.accuplacer.test.TestBase;
import com.pearson.itautomation.testngutils.dataprovider.ExcelDataProvider;

public class TestSelectAndSubmit extends TestBase {

    private static final String TAB_NAME = "Reports";
    private List<SubmitReport> reportsToVerify = new ArrayList<SubmitReport>();

    @BeforeClass(alwaysRun = true)
    public void beforeClass() {
        setUp();
        boolean deleteFile1 = FileOperations
                .deleteFolder(AUTOMATION_FILE_DOWNLOAD);
        boolean deleteFile2 = FileOperations
                .deleteFolder(AUTOMATION_FILE_DOWNLOAD1);
        System.out.println("BOOLEANS" + deleteFile1 + deleteFile2);

    }

    public void setUp() {
        browser = getBrowser();
        browser.get(getAppURL());
        User user = getUser(USER_ROLE.SITE_MANAGER);
        assertAndReport(
                browser,
                new LoginAction(this.browser, user.getUserName(), user
                        .getPassword()).performWithStates(), "Login Action");

    }

    @Test(dataProvider = "standardExcelData", dataProviderClass = ExcelDataProvider.class, groups = {
            "sanity", "Reports", "CATTECHTA-225" })
    public void submitReport(String reportName, String queryName,
            boolean isOffline, int timeout) {
        ((AccuplacerBrowser) browser).setTimeoutsForPredicate(timeout);
        String[] menus = reportName.split(",");
        addTestCaseDescription("Submit reports ");
        if (menus.length == 1) {
            assertAndReport(browser, new NavigateToMenu(this.browser, TAB_NAME,
                    reportName).performWithStates(), "Navigate to Reports page");
        } else {
            assertAndReport(browser, new NavigateToMenu(menus[1], this.browser,
                    TAB_NAME, menus[0], null).performWithStates(),
                    "Navigate to Reports Page");
        }

        SubmitReport submitReport = new SubmitReport(browser, reportName,
                queryName, isOffline, CommonUtilities.getCurrentDateTime());
        boolean checkSubmit = submitReport.performWithStates();
        if (checkSubmit) {

            assertAndReport(browser, true, "Report Submission");
        } else {
            quitBrowser();
            setUp();
            failAndReportWithScreenShot(browser, "Report Submission",
                    "FailedReport");
        }
        if (submitReport.isOffline()) {
            reportsToVerify.add(submitReport);
        }
        assertAndReport(browser,
                new NavigateToMenu(this.browser, "Administer Test",
                        "Administer New Test Session").performWithStates(),
                "Navigation");
    }

    @Test(dataProvider = "exports", dependsOnMethods = "submitReport")
    public void downloadReport(SubmitReport submitReport) {

        AccuplacerBrowserUtils.WaitInMilliSeconds(5000);
        addTestCaseDescription("Download Excel from reports under Report Queue");
        assertAndReport(browser, new NavigateToMenu(this.browser, TAB_NAME,
                "Report Queue").performWithStates(),
                "Navigate to Remote Report Queue Page");
        assertAndReport(
                browser,
                new DownloadReport(browser, submitReport
                        .getSubmittedDescription()).performWithStates(),
                "Download Report");

        FileOperations.copyFile(
                FileOperations.lastFileModified(AUTOMATION_FILE_DOWNLOAD),
                AUTOMATION_FILE_DOWNLOAD1);
        submitReport.setDownloadFile(FileOperations
                .lastFileModified(AUTOMATION_FILE_DOWNLOAD1));

    }

    @DataProvider(name = "exports")
    public Object[][] exportFiles() {
        Object[][] exports = new Object[reportsToVerify.size()][1];
        for (int i = 0; i < reportsToVerify.size(); i++) {
            exports[i][0] = reportsToVerify.get(i);
        }
        return exports;
    }

    @AfterMethod(alwaysRun = true)
    public void afterdown() {

    }

    @AfterClass(alwaysRun = true)
    public void teardown() {
        TestBase.setTemporary(reportsToVerify);
        quitBrowser();
    }
}
