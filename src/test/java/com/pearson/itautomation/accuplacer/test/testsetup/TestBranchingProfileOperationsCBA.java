package com.pearson.itautomation.accuplacer.test.testsetup;

import org.openqa.selenium.WebDriver;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import com.pearson.itautomation.accuplacer.core.Buttons;
import com.pearson.itautomation.accuplacer.core.CommonUtilities;
import com.pearson.itautomation.accuplacer.core.action.ClickOnHome;
import com.pearson.itautomation.accuplacer.core.action.LoginAction;
import com.pearson.itautomation.accuplacer.core.action.NavigateToMenu;
import com.pearson.itautomation.accuplacer.core.action.SearchRecordsAndClick;
import com.pearson.itautomation.accuplacer.test.TestBase;
import com.pearson.itautomation.accuplacer.testsetup.branchingprofiles.ActivateInactivateBranchingProfile;
import com.pearson.itautomation.accuplacer.testsetup.branchingprofiles.AddBranchingProfile;
import com.pearson.itautomation.accuplacer.testsetup.branchingprofiles.CopyBranchingProfile;
import com.pearson.itautomation.accuplacer.testsetup.branchingprofiles.DeleteBranchingProfile;
import com.pearson.itautomation.accuplacer.testsetup.branchingprofiles.EditBranchingProfile;
import com.pearson.itautomation.accuplacer.testsetup.branchingprofiles.ViewBranchingProfile;
import com.pearson.itautomation.bca.BrowserElement;
import com.pearson.itautomation.testngutils.dataprovider.ExcelDataProvider;
import com.pearson.itautomation.testngutils.reporting.TNReporter;

public class TestBranchingProfileOperationsCBA extends TestBase {

    private static final String BRANCHING_PROFILE_TITLE = "(!@#)00000001"
            + CommonUtilities.getCurrentTime();
    private static final String BRANCHING_PROFILE_TITLE_FOR_COPY = "000COPY"
            + CommonUtilities.getCurrentTime();
    private static final String XPATH_OF_ELEMENT = "//td[span[contains(text(),'"
            + BRANCHING_PROFILE_TITLE
            + "')] or contains(text(),'"
            + BRANCHING_PROFILE_TITLE + "')]";

    private final String strTabName = "Test Setup";
    private final String strSubMenuName = "Branching Profiles";

    WebDriver driver;
    public final BrowserElement btnAddBranchingProfile = Buttons
            .getButtonType4("Add");

    @Test(groups = { "regression", "CATTECHTA-49", "Test Setup" }, dataProvider = "standardExcelData", dataProviderClass = ExcelDataProvider.class)
    public void testAddBranchingProfileCBA(final String newRuleType1,
            final String newRule2) {

        addTestCaseDescription("Add Branching Profile");

        TNReporter.setParmDesc("Rule 1 to add", "Rule 2 to add");

        assertAndReport(browser,
                new AddBranchingProfile(browser, BRANCHING_PROFILE_TITLE,
                        newRuleType1, newRule2).performWithStates(),
                "Adding BranchingProfile");
    }

    @Test(groups = { "regression", "CATTECHTA-49", "Test Setup" }, dependsOnMethods = "testAddBranchingProfileCBA", priority = 1)
    public void testViewBranchingProfileCBA() {

        addTestCaseDescription("Search and View Branching Profile");

        assertAndReport(browser, new SearchRecordsAndClick(browser,
                XPATH_OF_ELEMENT).performWithStates(), "Searching Records");

        assertAndReport(browser, new ViewBranchingProfile(browser,
                BRANCHING_PROFILE_TITLE).performWithStates(),
                "View BranchingProfile");
    }

    @Test(groups = { "regression", "CATTECHTA-49", "Test Setup" }, dependsOnMethods = "testAddBranchingProfileCBA", priority = 2)
    public void testEditBranchingProfileCBA() {

        addTestCaseDescription("Search and Edit Branching Profile");

        assertAndReport(browser, new SearchRecordsAndClick(browser,
                XPATH_OF_ELEMENT).performWithStates(), "Searching Records");

        assertAndReport(browser,
                new EditBranchingProfile(browser, BRANCHING_PROFILE_TITLE,
                        "Apply Test Settings").performWithStates(),
                "Edit BranchingProfile");
    }

    @Test(groups = { "regression", "CATTECHTA-49", "Test Setup" }, dependsOnMethods = "testAddBranchingProfileCBA", priority = 3)
    public void testCopyBranchingProfileCBA() {

        addTestCaseDescription("Search and Copy Branching Profile");

        assertAndReport(browser, new SearchRecordsAndClick(browser,
                XPATH_OF_ELEMENT).performWithStates(), "Searching Records");

        assertAndReport(browser,
                new CopyBranchingProfile(browser, BRANCHING_PROFILE_TITLE,
                        BRANCHING_PROFILE_TITLE_FOR_COPY).performWithStates(),
                "copy BranchingProfile");
    }

    @Test(groups = { "regression", "CATTECHTA-49", "Test Setup" }, dependsOnMethods = "testAddBranchingProfileCBA", priority = 4)
    public void testActivateBranchingProfileCBA() {

        addTestCaseDescription("Search and Activate Branching Profile");

        assertAndReport(browser, new SearchRecordsAndClick(browser,
                XPATH_OF_ELEMENT).performWithStates(), "Searching Records");

        assertAndReport(browser, new ActivateInactivateBranchingProfile(
                browser, BRANCHING_PROFILE_TITLE, 2).performWithStates(),
                "Activate BranchinfProfile");
    }

    @Test(groups = { "regression", "CATTECHTA-49", "Test Setup" }, dependsOnMethods = "testAddBranchingProfileCBA", priority = 5)
    public void testInactivateBranchingProfileCBA() {

        addTestCaseDescription("Search and Inactivate Branching Profile");

        assertAndReport(browser, new SearchRecordsAndClick(browser,
                XPATH_OF_ELEMENT).performWithStates(), "Searching Records");

        assertAndReport(browser, new ActivateInactivateBranchingProfile(
                browser, BRANCHING_PROFILE_TITLE, 1).performWithStates(),
                "Inactivate BranchinfProfile");
    }

    @Test(groups = { "regression", "CATTECHTA-49", "Test Setup" }, dependsOnMethods = "testAddBranchingProfileCBA", priority = 6)
    public void testDeleteBranchingProfileCBA() {

        addTestCaseDescription("Search and Delete Branching Profile");

        assertAndReport(browser, new SearchRecordsAndClick(browser,
                XPATH_OF_ELEMENT).performWithStates(), "Searching Records");

        assertAndReport(browser, new DeleteBranchingProfile(browser,
                BRANCHING_PROFILE_TITLE).performWithStates(),
                "Delete BranchingProfile");
    }

    @AfterMethod(alwaysRun = true)
    public void afterMethod() {
        assertAndReport(browser, new ClickOnHome(browser).performWithStates(),
                "Click On Home");
    }

    @BeforeMethod(alwaysRun = true)
    public void beforeMethod() {
        assertAndReport(browser, new NavigateToMenu(this.browser, strTabName,
                strSubMenuName, btnAddBranchingProfile).performWithStates(),
                "Navigate to BranchingProfile page");

    }

    @BeforeClass(alwaysRun = true)
    public void beforeClass() {
        browser = getBrowser();
        browser.get(getAppURL());
        User user = getUser(USER_ROLE.COLLEGE_BOARD_ADMIN);
        assertAndReport(
                browser,
                new LoginAction(this.browser, user.getUserName(), user
                        .getPassword()).performWithStates(), "Login Action");
    }

    @AfterClass(alwaysRun = true)
    public void afterClass() {
        quitBrowser();
    }
}
