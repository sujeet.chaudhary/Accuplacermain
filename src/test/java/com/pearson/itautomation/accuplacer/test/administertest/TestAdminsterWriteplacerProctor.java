package com.pearson.itautomation.accuplacer.test.administertest;

import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import com.pearson.itautomation.accuplacer.core.Buttons;
import com.pearson.itautomation.accuplacer.core.CommonUtilities;
import com.pearson.itautomation.accuplacer.test.TestBase;
import com.pearson.itautomation.bca.BrowserElement;
import com.pearson.itautomation.testngutils.dataprovider.ExcelDataProvider;
import com.pearson.itautomation.testngutils.reporting.TNReporter;

public class TestAdminsterWriteplacerProctor extends TestBase {

    TestAdminsterCommon testAdminCommonProctorWP = new TestAdminsterCommon();
    public final BrowserElement btnAddBranchingProfile = Buttons
            .getButtonType1("Add");

    @Test(dataProvider = "standardExcelData", dataProviderClass = ExcelDataProvider.class, groups = {
            "regression", "CATTECHTA-101", "CATTECHTA-123" })
    public void proctorWPTestRegression(String tcDescription, String studentId,
            String lastName, String DOB, String MOB, String YOB,
            String branchingProfile, String textFilePath) {
        adminWPTestAsPROC(tcDescription, studentId, lastName, DOB, MOB, YOB,
                branchingProfile, textFilePath);
    }

    @Test(dataProvider = "standardExcelData", dataProviderClass = ExcelDataProvider.class, groups = {
            "sanity", "CATTECHTA-101", "CATTECHTA-123" })
    public void proctorWPTestSanity(String tcDescription, String studentId,
            String lastName, String DOB, String MOB, String YOB,
            String branchingProfile, String textFilePath) {
        adminWPTestAsPROC(tcDescription, studentId, lastName, DOB, MOB, YOB,
                branchingProfile, textFilePath);
    }

    public void adminWPTestAsPROC(String tcDescription, String studentId,
            String lastName, String DOB, String MOB, String YOB,
            String branchingProfile, String textFilePath) {

        addTestCaseDescription(tcDescription);
        TNReporter.setParmDesc("Test Scenario ", "Student ID",
                "Student's Last Name", "Date Of Birth", "Month Of Birth",
                "Year Of Birth", "Branching Profile", "Text File Name");
        String essayResponse = "Response";
        if ("BLANK".equalsIgnoreCase(textFilePath)) {
            essayResponse = "";
        } else {
            essayResponse = CommonUtilities.readTxtAsString(CommonUtilities
                    .appPath()
                    + "\\src\\test\\resources\\WP Essays\\"
                    + textFilePath);
        }

        User user = getUser(USER_ROLE.PROCTOR);
        testAdminCommonProctorWP.administerWritePlacerTestCommon(browser, user,
                getConnection(), studentId, lastName, DOB, MOB, YOB,
                branchingProfile, user.getPassword(), essayResponse);
    }

    @BeforeMethod(alwaysRun = true)
    public void setUp() {
        browser = getBrowser();
        browser.get(getAppURL());
    }

    @AfterMethod(alwaysRun = true)
    public void afterClass() {
        quitBrowser();
    }
}
