package com.pearson.itautomation.accuplacer.test;

import java.util.Properties;

import javax.activation.DataHandler;
import javax.activation.DataSource;
import javax.activation.FileDataSource;
import javax.mail.BodyPart;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.Multipart;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMultipart;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.pearson.itautomation.accuplacer.core.CommonUtilities;

public class EmailMain {
    private static final Logger LOGGER = LoggerFactory
            .getLogger(EmailMain.class);

    public static void triggerEmail(String strBody, String strSubject,
            String strTOList, String strFrom) {

        String txtbyLine = strBody;

        String emailtolist = strTOList;
        // Recipient's email ID needs to be mentioned.
        String to = emailtolist;

        // Sender's email ID needs to be mentioned
        String from = strFrom;

        // Assuming you are sending email from localhost
        String host = CommonUtilities.EMAIL_PROPERTIES.getProperty("hostName")
                .trim();

        // Get system properties
        Properties properties = System.getProperties();

        // Setup mail server
        properties.setProperty("mail.smtp.host", host);

        // Get the default Session object.
        Session session = Session.getDefaultInstance(properties, null);
        // session.setDebug(true);
        try {
            // Create a default MimeMessage object.
            MimeMessage message = new MimeMessage(session);

            // Set From: header field of the header.
            message.setFrom(new InternetAddress(from));

            // Set To: header field of the header.
            String[] emailList = to.split(",");
            InternetAddress[] toAddress = new InternetAddress[emailList.length];
            for (int i = 0; i < emailList.length; i++) {
                toAddress[i] = new InternetAddress(emailList[i]);
            }

            for (int i = 0; i < toAddress.length; i++) {
                message.addRecipient(Message.RecipientType.TO, toAddress[i]);
            }

            // Set Subject: header field

            message.setSubject(strSubject);

            // Create the message part
            BodyPart messageBodyPart = new MimeBodyPart();

            // Fill the message
            messageBodyPart.setContent(txtbyLine, "text/html");

            // Create a multipart message
            Multipart multipart = new MimeMultipart();

            // Set text message part

            // Send the complete message parts
            message.setContent(multipart);
            // addAttachment(multipart, CommonUtilities.appPath()
            // + "\\target\\surefire-reports\\emailable-report.html",
            // "emailable-report.html");
            addAttachment(
                    multipart,
                    CommonUtilities.appPath()
                            + "\\target\\generated-resources\\xml\\xslt\\testng-results.html",
                    "TestReport.html");

            // Send message
            Transport.send(message);
            LOGGER.info("Sent message successfully.... to " + emailtolist);

        }

        catch (MessagingException mex) {
            LOGGER.error("Exception Occured while Sending Email", mex);

        }

        catch (Exception ex) {
            LOGGER.error("Exception Occured while Sending Email", ex);

        }
    }

    private static void addAttachment(Multipart multipart, String FilePath,
            String filename) {
        try {
            DataSource source = new FileDataSource(FilePath);
            BodyPart messageBodyPart = new MimeBodyPart();

            messageBodyPart.setDataHandler(new DataHandler(source));

            messageBodyPart.setFileName(filename);
            multipart.addBodyPart(messageBodyPart);
        } catch (MessagingException e) {
            LOGGER.error("Exception Occured while Attaching File", e);
        }
    }
}
