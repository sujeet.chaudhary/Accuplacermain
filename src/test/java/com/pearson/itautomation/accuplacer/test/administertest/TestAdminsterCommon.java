package com.pearson.itautomation.accuplacer.test.administertest;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Properties;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.pearson.itautomation.accuplacer.administertestold.AcceptPrivacyPolicy;
import com.pearson.itautomation.accuplacer.administertestold.AdministerBackgroundQuestionGroup;
import com.pearson.itautomation.accuplacer.administertestold.AdministerSaveAndResumeCATTest;
import com.pearson.itautomation.accuplacer.administertestold.AdministerWritePlacerTest;
import com.pearson.itautomation.accuplacer.administertestold.FillAndSaveStudentInformation;
import com.pearson.itautomation.accuplacer.administertestold.NavigateThroughWelcomeMessage;
import com.pearson.itautomation.accuplacer.administertestold.SelectBranchingProfile;
import com.pearson.itautomation.accuplacer.core.AccuplacerBrowserUtils;
import com.pearson.itautomation.accuplacer.core.CommonUtilities;
import com.pearson.itautomation.accuplacer.core.action.LoginAction;
import com.pearson.itautomation.accuplacer.core.action.NavigateToMenu;
import com.pearson.itautomation.accuplacer.test.TestBase;
import com.pearson.itautomation.bca.Browser;
import com.pearson.itautomation.bca.BrowserElement;
import com.pearson.itautomation.testngutils.reporting.TNReporter;

public class TestAdminsterCommon extends TestBase {
    private static final Logger LOGGER = LoggerFactory
            .getLogger(TestAdminsterCommon.class);
    private static final String TAB_NAME = "Administer Test";
    private static final String SUB_MENU = "Administer New Test Session";
    private static final String REGEX_TO_REPLACE_CHARECTER = "[^a-zA-Z0-9]";
    private static final String ACCUPLACER_WRITEPLACER_STATUS_ID = "A";
    private static final String SENT_TO_PKT_WRITEPLACER_STATUS_ID = "S";
    private static final String PKT_WRITEPLACER_STATUS_ID = "P";
    private static final String NOT_ABLE_TO_SCORE = "U";
    private static final String FAILURE_STRING = "NULL";
    private static final String BLANK_WRITEPLACER_STATUS_ID = "B";
    private static final int FAILURE_EXNUM = 000;

    private boolean testCompleteResult;

    private int exnumOfStudent = 0;

    protected Properties sqlQueries = CommonUtilities
            .readPropertyFile("//config//SQL.properties");
    private boolean takeScreenshot = false;

    /**
     * This method consists of common test administration steps which can Be
     * used by SM/PROR/PROC
     * 
     * @param browser
     *            Browser instantiated at Test level
     * @param user
     *            Type of UserRole
     * @param connection
     *            DB connection initiated at TestBase
     * @param studentId
     *            Unique Student ID
     * @param lastName
     *            Last Name of the Student
     * @param DOB
     *            Date
     * @param MOB
     *            Month
     * @param YOB
     *            Year
     * @param branchingProfile
     *            Branching Profile
     * @param answerKeys
     *            Answer Key Parameter
     * @param saveAtQuestions
     *            Questions separated by commas where save and resume action
     *            needs to be performed
     */
    public void administerTestCommon(Browser browser, User user,
            Connection connection, String studentId, String lastName,
            String DOB, String MOB, String YOB, String branchingProfile,
            String answerKeys, String saveAtQuestions) {

        startTest(browser, user, branchingProfile, studentId, DOB, MOB, YOB,
                lastName);
        AdministerSaveAndResumeCATTest administerTestObject;
        if (takeScreenshot) {

            administerTestObject = new AdministerSaveAndResumeCATTest(browser,
                    user.getPassword(), studentId, answerKeys, saveAtQuestions,
                    connection, true);
            testCompleteResult = administerTestObject.performWithStates();
        } else {

            administerTestObject = new AdministerSaveAndResumeCATTest(browser,
                    user.getPassword(), studentId, answerKeys, saveAtQuestions,
                    connection);
            testCompleteResult = administerTestObject.performWithStates();
        }
        // to check if the test has failed
        if (!testCompleteResult) {
            failAndReportWithScreenShot(
                    browser,
                    "Administer CAT test Action",
                    branchingProfile.replaceAll(REGEX_TO_REPLACE_CHARECTER, "")
                            + "_"
                            + studentId.replaceAll(REGEX_TO_REPLACE_CHARECTER,
                                    ""));
            TNReporter.addNote(administerTestObject.getItemListString());
        } else {
            assertAndReport(browser, true, "Administer CAT test Action");
            exnumOfStudent = getExnumForcompletedSession(studentId,
                    branchingProfile, connection);
            if (exnumOfStudent != 0 && exnumOfStudent != FAILURE_EXNUM) {
                displayExnum(exnumOfStudent, studentId, branchingProfile);
            } else {
                LOGGER.error("Could Not Find the Exnum");
            }
        }
        String itemValuesForTestSession = administerTestObject
                .getItemListString();
        TNReporter.addNote("ItemID List for Current Test Session [ "
                + itemValuesForTestSession + " ]");

    }

    public void administerTestCommonRetestScenario(Browser browser,
            String studentID, User user, Connection connection,
            String lastName, String DOB, String MOB, String YOB,
            String branchingProfile, String answerKeys, String saveAtQuestions) {
        BrowserElement testUnits = browser.getBrowserElement(
                "OR_Administer_Test", "TestingUnitsAvailable");
        float UnitsOnAttempt1, UnitsOnAttempt2, UnitsOnAttempt3;

        startTest(browser, user, branchingProfile, studentID, DOB, MOB, YOB,
                lastName);

        testCompleteResult = new AdministerSaveAndResumeCATTest(browser,
                user.getPassword(), studentID, answerKeys, saveAtQuestions,
                connection, false, user.getUserName()).performWithStates();

        UnitsOnAttempt1 = Float.parseFloat(browser
                .getElementWithWait(testUnits).getText());
        TNReporter.addNote("Units Remain after 1st attempt : "
                + UnitsOnAttempt1);
        if (testCompleteResult) {
            startTestWithoutLogin(browser, user, branchingProfile, studentID,
                    DOB, MOB, YOB, lastName);
            testCompleteResult = new AdministerSaveAndResumeCATTest(browser,
                    user.getPassword(), studentID, answerKeys, saveAtQuestions,
                    connection, true, user.getUserName()).performWithStates();

        }
        UnitsOnAttempt2 = Float.parseFloat(browser
                .getElementWithWait(testUnits).getText());
        TNReporter.addNote("Units Remain after 2nd attempt : "
                + UnitsOnAttempt2);
        if (testCompleteResult) {
            startTestWithoutLogin(browser, user, branchingProfile, studentID,
                    DOB, MOB, YOB, lastName);
            testCompleteResult = new AdministerSaveAndResumeCATTest(browser,
                    user.getPassword(), studentID, answerKeys, saveAtQuestions,
                    connection, false, user.getUserName()).performWithStates();

        }
        UnitsOnAttempt3 = Float.parseFloat(browser
                .getElementWithWait(testUnits).getText());
        TNReporter.addNote("Units Remain after 3rd attempt : "
                + UnitsOnAttempt3);
        if (!((UnitsOnAttempt1 - UnitsOnAttempt2 == 0) && (UnitsOnAttempt2
                - UnitsOnAttempt3 == 0))) {
            assertAndReport(browser, false,
                    "Number of Units Doesnt Match after Retest Attempts");
        }
        // to check if the test has failed
        if (!testCompleteResult) {
            failAndReportWithScreenShot(
                    browser,
                    "Administer CAT test Action",
                    branchingProfile.replaceAll(REGEX_TO_REPLACE_CHARECTER, "")
                            + "_"
                            + studentID.replaceAll(REGEX_TO_REPLACE_CHARECTER,
                                    ""));
        } else {
            assertAndReport(browser, true, "Administer CAT test Action");
            exnumOfStudent = getExnumForcompletedSession(studentID,
                    branchingProfile, connection);
            if (exnumOfStudent != 0 && exnumOfStudent != FAILURE_EXNUM) {
                displayExnum(exnumOfStudent, studentID, branchingProfile);
            } else {
                LOGGER.error("Could Not Find the Exnum");
            }
        }

    }

    /**
     * This method consists of common WritePlacer test administration steps
     * which can Be used by SM/PROR/PROC
     * 
     * @param browser
     * @param user
     * @param connection
     * @param studentId
     * @param lastName
     * @param DOB
     * @param MOB
     * @param YOB
     * @param branchingProfile
     * @param essayResponse
     */
    public void administerWritePlacerTestCommon(Browser browser, User user,
            Connection connection, String studentId, String lastName,
            String DOB, String MOB, String YOB, String branchingProfile,
            String password, String essayResponse) {
        int testSessionDetailID = FAILURE_EXNUM;
        String writePlacerDescription = FAILURE_STRING;
        String testSessionDetailScore = FAILURE_STRING;

        startTest(browser, user, branchingProfile, studentId, DOB, MOB, YOB,
                lastName);
        testCompleteResult = new AdministerWritePlacerTest(browser, password,
                essayResponse).performWithStates();
        // to check if the test has failed
        if (!testCompleteResult) {
            failAndReportWithScreenShot(
                    browser,
                    "Administer Writeplacer Test Action",
                    branchingProfile.replaceAll(REGEX_TO_REPLACE_CHARECTER, "")
                            + "_"
                            + studentId.replaceAll(REGEX_TO_REPLACE_CHARECTER,
                                    ""));
        } else {
            assertAndReport(browser, true, "Administer Writeplacer Test Action");
            exnumOfStudent = getExnumForcompletedSession(studentId,
                    branchingProfile, connection);
            if (exnumOfStudent != 0 && exnumOfStudent != FAILURE_EXNUM) {
                displayExnum(exnumOfStudent, studentId, branchingProfile);
            } else {
                LOGGER.error("Could Not Find the Exnum");
                assertAndReport(browser, true, "WritePlacer Scoring Scenario");
                return;
            }
            testSessionDetailID = getDataFromTestSessionDetailID(exnumOfStudent
                    + "", connection);
            if (testSessionDetailID != 0
                    && testSessionDetailID != FAILURE_EXNUM) {
                writePlacerDescription = getDataFromWriteplacerResponse(
                        testSessionDetailID + "", "writeplacer_status_id",
                        connection);
            } else {
                LOGGER.error("Could Not Find the testSessionDetailID , script terminated");
                assertAndReport(browser, true, "WritePlacer Scoring Scenario");
                return;
            }
            if (ACCUPLACER_WRITEPLACER_STATUS_ID
                    .equalsIgnoreCase(writePlacerDescription)) {
                TNReporter.addNote("Essay Response Scoring in progress");
                return;
            } else if (BLANK_WRITEPLACER_STATUS_ID
                    .equalsIgnoreCase(writePlacerDescription)) {
                TNReporter.addNote("Essay Response was Blank");
                testSessionDetailScore = getScoreFromTestSessionDetail(
                        testSessionDetailID + "", connection);
                if (!FAILURE_STRING.equalsIgnoreCase(testSessionDetailScore)
                        && Integer.parseInt(testSessionDetailScore) == 0) {
                    TNReporter.addNote("Score for the blank essay = 0");
                    assertAndReport(browser, true,
                            "WritePlacer Scoring Scenario");
                    return;
                } else {
                    TNReporter
                            .addFailureReason("Score for Blank essay is not set as zero");
                    assertAndReport(browser, false,
                            "WritePlacer Scoring Scenario");
                    return;
                }
            } else if (SENT_TO_PKT_WRITEPLACER_STATUS_ID
                    .equalsIgnoreCase(writePlacerDescription)) {
                TNReporter.addNote("Essay is Sent to PKT for Scoring");
                return;
            } else if (PKT_WRITEPLACER_STATUS_ID
                    .equalsIgnoreCase(writePlacerDescription)) {
                TNReporter
                        .addNote("Essay Response Scoring Status is \"Scored By PKT\"");
                testSessionDetailScore = getScoreFromTestSessionDetail(
                        testSessionDetailID + "", connection);
                if (!FAILURE_STRING.equalsIgnoreCase(testSessionDetailScore)) {
                    TNReporter.addNote("Score returned by PKT = "
                            + testSessionDetailScore);
                    assertAndReport(browser, true,
                            "WritePlacer Scoring Scenario");
                    return;
                } else {
                    TNReporter
                            .addFailureReason("Score NOT found in DB for the PKT scored Session");
                    assertAndReport(browser, false,
                            "WritePlacer Scoring Scenario");
                    return;
                }

            } else if (NOT_ABLE_TO_SCORE
                    .equalsIgnoreCase(writePlacerDescription)) {
                TNReporter
                        .addNote("DB status for scoring is : NOT ABLE TO SCORE");
                return;
            } else {
                TNReporter.addNote("TestSessionDetail_id : "
                        + testSessionDetailID
                        + " Writeplacer Scoring Status Description : "
                        + writePlacerDescription);
            }
        }
    }

    /**
     * This Method will Initiate a Test Session CAT or WP
     * 
     * @param browser
     * @param user
     * @param branchingProfile
     * @param studentId
     * @param DOB
     * @param MOB
     * @param YOB
     * @param lastName
     */
    public void startTest(Browser browser, User user, String branchingProfile,
            String studentId, String DOB, String MOB, String YOB,
            String lastName) {
        float UnitsOnAttempt1;
        BrowserElement testUnits = browser.getBrowserElement(
                "OR_Administer_Test", "TestingUnitsAvailable");
        AccuplacerBrowserUtils.WaitInMilliSeconds(5000);
        if (!browser.getPageSource().contains("Branching Profile Title")) {
            if (!browser.getPageSource().contains("Help & Information")) {
                LOGGER.info("User Is NOT Logged in");
                assertAndReport(
                        browser,
                        new LoginAction(browser, user.getUserName(), user
                                .getPassword()).performWithStates(),
                        "Login Action to the application");
            }
        }

        assertAndReport(browser,
                new NavigateToMenu(browser, TAB_NAME, SUB_MENU)
                        .performWithStates(),
                "Click on Administer New Test Link");

        if (browser.getElementWithWait(testUnits).getText() != null) {
            UnitsOnAttempt1 = Float.parseFloat(browser.getElementWithWait(
                    testUnits).getText());
            TNReporter.addNote("Units available prior to 1st attempt : "
                    + UnitsOnAttempt1);
        }

        assertAndReport(browser, new SelectBranchingProfile(browser,
                branchingProfile).performWithStates(),
                "Selecting Branching profile : " + branchingProfile
                        + " to Administer Test");
        assertAndReport(browser,
                new AcceptPrivacyPolicy(browser).performWithStates(),
                "Accept Privacy Policy");
        assertAndReport(browser, new FillAndSaveStudentInformation(browser,
                studentId, lastName, DOB, MOB, YOB).performWithStates(),
                "Fill Student Information Form 1");
        assertAndReport(browser,
                new NavigateThroughWelcomeMessage(browser).performWithStates(),
                "Navigation Through Welcome messages");
        assertAndReport(browser,
                new AdministerBackgroundQuestionGroup(browser)
                        .performWithStates(),
                "Administering Background Question Group");

    }

    public void startTestWithoutLogin(Browser browser, User user,
            String branchingProfile, String studentId, String DOB, String MOB,
            String YOB, String lastName) {

        assertAndReport(browser,
                new NavigateToMenu(browser, TAB_NAME, SUB_MENU)
                        .performWithStates(),
                "Click on Administer New Test Link");
        assertAndReport(browser, new SelectBranchingProfile(browser,
                branchingProfile).performWithStates(),
                "Selecting Branching profile : " + branchingProfile
                        + " to Administer Test");
        assertAndReport(browser,
                new AcceptPrivacyPolicy(browser).performWithStates(),
                "Accept Privacy Policy");
        assertAndReport(browser, new FillAndSaveStudentInformation(browser,
                studentId, lastName, DOB, MOB, YOB).performWithStates(),
                "Fill Student Information Form 1");
        assertAndReport(browser,
                new NavigateThroughWelcomeMessage(browser).performWithStates(),
                "Navigation Through Welcome messages");
        assertAndReport(browser,
                new AdministerBackgroundQuestionGroup(browser)
                        .performWithStates(),
                "Administering Background Question Group");

    }

    /**
     * This method will display the <b>exnum</b>
     * 
     * @param studentId
     * @param branchingProfile
     * @param connection
     */
    private void displayExnum(int exnum, String studentID,
            String branchingProfile) {

        // Print the exnum on the ISR
        if (exnum != 0 && exnum != FAILURE_EXNUM) {
            TNReporter
                    .addNote(String
                            .format("Test session Completed for Student \n Student ID : %s \nBranching Profile : %s \nExnum : %s",
                                    studentID, branchingProfile, exnumOfStudent
                                            + ""));
        }
    }

    /**
     * This Method will fetch the exnum from the database
     * 
     * @param studentID
     * @param branchingProfile
     * @param con
     * @return The Test_Session_id(<i>exnum</i>) , in case of failure 000
     */
    public int getExnumForcompletedSession(String studentID,
            String branchingProfile, Connection con) {
        String rstStringValuye = null;
        int exnum = FAILURE_EXNUM;
        ResultSet rset = null;
        Statement stmt = null;
        String sqlTestCompletedExnum = String.format(
                sqlQueries.getProperty("sqlCompleteTestSessionEXNUM"),
                studentID, branchingProfile);
        try {
            stmt = con.createStatement();
            rset = stmt.executeQuery(sqlTestCompletedExnum);
            if (!rset.next()) {
                LOGGER.error(String
                        .format("No records were found for the Student ID : %s , Branching_Profile :%s in the database",
                                studentID, branchingProfile));
                return FAILURE_EXNUM;
            } else {
                do {
                    rstStringValuye = rset.getString("max(ts.test_Session_id)");
                    exnum = Integer.parseInt(rstStringValuye);

                } while (rset.next());
            }
        } catch (NumberFormatException ex) {
            LOGGER.error("Exnum : " + rstStringValuye);
            LOGGER.error(
                    "Exception occured while executing the query for student exnum",
                    ex);
            return exnum;
        } catch (SQLException e) {
            LOGGER.error(
                    "Exception occured while executing the query for student exnum",
                    e);
            return exnum;
        } finally {
            closeStatements(stmt, con, rset);
        }
        return exnum;

    }

    public int getDataFromTestSessionDetailID(String exnum, Connection con) {
        int testSessionDetailIDs = FAILURE_EXNUM;
        ResultSet rset = null;
        Statement stmt = null;
        String sqlTestSessionDetailIDs = String.format(
                sqlQueries.getProperty("sqlGetTestSessionDetailIDs"), exnum);
        try {
            stmt = con.createStatement();
            rset = stmt.executeQuery(sqlTestSessionDetailIDs);
            if (!rset.next()) {
                LOGGER.error(String.format(
                        "No records were found for the exnum ID : %s", exnum));
                return testSessionDetailIDs;
            } else {
                do {

                    testSessionDetailIDs = Integer.parseInt(rset
                            .getString("max(test_session_detail_id)"));
                } while (rset.next());
            }
        } catch (SQLException e) {
            LOGGER.error(
                    "Exception occured while executing the query test session id "
                            + exnum, e);
            return testSessionDetailIDs;
        } finally {
            closeStatements(stmt, con, rset);
        }
        return testSessionDetailIDs;

    }

    public String getScoreFromTestSessionDetail(String testSessionDetailID,
            Connection con) {
        String testSessionDetailIDs = FAILURE_STRING;
        ResultSet rset = null;
        Statement stmt = null;
        String sqlGetScoreForTestSessionDetail = String.format(
                sqlQueries.getProperty("sqlGetScoreForTestSessionDetail"),
                testSessionDetailID);
        try {
            stmt = con.createStatement();
            rset = stmt.executeQuery(sqlGetScoreForTestSessionDetail);
            if (!rset.next()) {
                LOGGER.error(String
                        .format("No records were found for the test session detail ID : %s",
                                testSessionDetailID));
                return testSessionDetailIDs;
            } else {
                do {
                    testSessionDetailIDs = rset.getString("score");
                } while (rset.next());
            }
        } catch (SQLException e) {
            LOGGER.error(
                    "Exception occured while executing the query test session id "
                            + testSessionDetailID, e);
            return testSessionDetailIDs;
        } finally {
            closeStatements(stmt, con, rset);
        }
        return testSessionDetailIDs;

    }

    public String getDataFromWriteplacerResponse(String testSessionDetailID,
            String columnName, Connection con) {
        String wpColumnData = FAILURE_STRING;
        ResultSet rset = null;
        Statement stmt = null;
        String sqlWPResponseQuery = String.format(
                sqlQueries.getProperty("sqlGetWPStatusId"), columnName,
                testSessionDetailID);
        try {
            stmt = con.createStatement();
            rset = stmt.executeQuery(sqlWPResponseQuery);
            if (!rset.next()) {
                LOGGER.error(String
                        .format("No %s value records were found for the testsessiondetailid ID : %s",
                                columnName, testSessionDetailID));
                return wpColumnData;
            } else {
                do {
                    wpColumnData = rset.getString(columnName);
                } while (rset.next());
            }
        } catch (SQLException e) {
            LOGGER.error(
                    "Exception occured while executing the query test session detail id "
                            + testSessionDetailID, e);
            return wpColumnData;
        } finally {
            closeStatements(stmt, con, rset);
        }
        return wpColumnData;

    }

    public void closeStatements(Statement stmt, Connection con, ResultSet rset) {
        try {
            if (stmt != null && !con.isClosed()) {
                stmt.close();
            }
            if (rset != null && !con.isClosed()) {
                rset.close();
            }
        } catch (SQLException e) {
            LOGGER.error("Exception occured while closing the statement", e);
        }
    }

    public void administerTestCommon(Browser browser, User user,
            Connection connection, String studentId, String lastName,
            String dOB, String mOB, String yOB, String branchingProfile,
            String answerKeys, String saveAtQuestions, boolean b) {
        this.takeScreenshot = b;
        administerTestCommon(browser, user, connection, studentId, lastName,
                dOB, mOB, yOB, branchingProfile, answerKeys, saveAtQuestions);
    }

}
