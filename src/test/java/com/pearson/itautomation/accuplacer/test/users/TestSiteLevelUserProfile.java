package com.pearson.itautomation.accuplacer.test.users;

import org.openqa.selenium.By;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import com.pearson.itautomation.accuplacer.core.AccuplacerBrowserUtils;
import com.pearson.itautomation.accuplacer.core.CommonUtilities;
import com.pearson.itautomation.accuplacer.core.action.GenericSearchAction;
import com.pearson.itautomation.accuplacer.core.action.LoginAction;
import com.pearson.itautomation.accuplacer.core.action.SaveAndAssert;
import com.pearson.itautomation.accuplacer.core.action.SearchElementOnPageAndClick;
import com.pearson.itautomation.accuplacer.test.TestBase;
import com.pearson.itautomation.accuplacer.users.userprofiles.AddUserProfile;
import com.pearson.itautomation.accuplacer.users.userprofiles.EditSiteLevelUser;
import com.pearson.itautomation.accuplacer.users.userprofiles.NavigateToAddUserPage;
import com.pearson.itautomation.accuplacer.users.userprofiles.ReFillUserProfile;
import com.pearson.itautomation.bca.BrowserElement;
import com.pearson.itautomation.bca.interaction.DropdownSelectionAction;
import com.pearson.itautomation.bca.validation.ElementExistsValidation;

public class TestSiteLevelUserProfile extends TestBase {

    private static final Logger LOGGER = LoggerFactory
            .getLogger(TestSiteLevelUserProfile.class);
    private static final int INDEX_OF_FIELD = 0;
    private String userType;
    private String createUserName;
    private static String strAddUserConfirmation = "was created successfully and a confirmation email will "
            + "be sent to the new users email address";
    private static String strDeleteUserConfirmation = "User deleted successfully";
    private BrowserElement eleValidateError;

    @BeforeClass(alwaysRun = true)
    public void beforeClass() {
        browser = getBrowser();
        browser.get(getAppURL());
        User user = getUser(USER_ROLE.SITE_MANAGER);
        assertAndReport(
                browser,
                new LoginAction(this.browser, user.getUserName(), user
                        .getPassword()).performWithStates(), "Login Action");
    }

    @Test(groups = { "sanity", "regression", "AdministerTest", "CATTECHTA-20" })
    public void siteUserProfile() {

        addTestCaseDescription("Create ALL type os users at Site level");
        this.eleValidateError = browser.getBrowserElement("OR_User",
                "validationErrorUserFrom");
        for (int i = 1; i <= 6; i++) {

            assertAndReport(
                    browser,
                    new NavigateToAddUserPage(this.browser).performWithStates(),
                    "Navigation to Add Users Page");
            userType = browser
                    .getElementWithWait(
                            new BrowserElement(
                                    i + "NewUser",
                                    By.xpath("// select[@name='usertype']/option[@value='"
                                            + (i - 1) + "']"))).getText();
            createUserName = userType + CommonUtilities.getCurrentddmmss();
            if (createUserName.length() > 30) {
                createUserName = createUserName.substring(
                        createUserName.length() - 30, createUserName.length());
            }
            assertAndReport(browser,
                    new AddUserProfile(browser, createUserName)
                            .performWithStates(), "Add User Details");
            if (!new DropdownSelectionAction(browser, new BrowserElement(
                    "UserType", By.name("usertype")), i).performWithStates()) {
            }
            if (new ElementExistsValidation(getBrowser(), this.eleValidateError)
                    .performWithStates()) {
                LOGGER.error("Validation Error Found for the \""
                        + (getBrowser().getElementWithWait(eleValidateError)
                                .getText()).split("is invalid")[INDEX_OF_FIELD]
                        + "\" Field");
                assertAndReport(browser, false, "Validation Error Check");
            }
            assertAndReport(browser, new ReFillUserProfile(browser,
                    createUserName).performWithStates(),
                    "Check  \"Save\" Button Status");
            assertAndReport(browser, new SaveAndAssert(browser,
                    strAddUserConfirmation).performWithStates(), "Add User : "
                    + createUserName + " , User Type : " + userType);
            assertAndReport(browser, new GenericSearchAction(browser,
                    createUserName, "Username").performWithStates(),
                    "Seach User " + createUserName);
            assertAndReport(browser,
                    new SearchElementOnPageAndClick(browser, createUserName,
                            "Edit", "user in users", 1).performWithStates(),
                    "Click On Edit " + createUserName);
            try {
                Thread.sleep(500);
            } catch (InterruptedException e) {
                LOGGER.error("Waiting for Modal Transition failed");
            }
            assertAndReport(browser,
                    new EditSiteLevelUser(browser).performWithStates(),
                    "Edit User " + createUserName);
            assertAndReport(
                    browser,
                    (new SearchElementOnPageAndClick(browser, createUserName,
                            "Delete", "user in users", 1).performWithStates())
                            && (AccuplacerBrowserUtils
                                    .checkPageSourceForString(browser,
                                            strDeleteUserConfirmation)),
                    "Deleting User " + createUserName);

        }
    }

    @AfterClass(alwaysRun = true)
    public void teardown() {
        quitBrowser();
    }
}
