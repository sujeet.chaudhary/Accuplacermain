package com.pearson.itautomation.accuplacer.test.users;

import java.io.File;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;

import com.pearson.itautomation.accuplacer.core.Buttons;
import com.pearson.itautomation.accuplacer.core.CommonUtilities;
import com.pearson.itautomation.accuplacer.core.action.LoginAction;
import com.pearson.itautomation.accuplacer.core.action.NavigateToMenu;
import com.pearson.itautomation.accuplacer.test.TestBase;
import com.pearson.itautomation.bca.BrowserElement;
import com.pearson.itautomation.bca.interaction.FileUploadAction;
import com.pearson.itautomation.bca.interaction.LeftClickElementAction;
import com.pearson.itautomation.bca.validation.ElementExistsValidation;

public class TestStudentPreregistration extends TestBase {

    private BrowserElement validateImport1;
    private BrowserElement validateImportRecords;
    private BrowserElement fileUploadElement;
    private static final Logger LOGGER = LoggerFactory
            .getLogger(TestStudentPreregistration.class);
    private final String FILE_PATH_STUDENT_UPLOAD = CommonUtilities.appPath()
            + "/src/test/resources/test data/Student Preregistration/";

    @BeforeClass(alwaysRun = true)
    public void beforeClass() {
        browser = getBrowser();
        browser.get(getAppURL());
        User user = getUser(USER_ROLE.SITE_MANAGER);
        assertAndReport(
                browser,
                new LoginAction(this.browser, user.getUserName(), user
                        .getPassword()).performWithStates(), "Login Action");
    }

    /*
     * @Test(dataProvider = "standardExcelData", dataProviderClass =
     * ExcelDataProvider.class, groups = { "sanity", "Vouchers", "CATTECHTA-101"
     * })
     */
    public void StudentPreRegistrationPositive(
            final String scenarioDescription, final String templateFileName,
            final String headerFlag, final String noOfStudents) {
        createValidationElements();

        addTestCaseDescription("Uploading  Student PreRegistration for scenario "
                + scenarioDescription);
        assertAndReport(browser, new NavigateToMenu(this.browser, "Vouchers",
                "Student Preregistration").performWithStates(), "Login Action");
        File file = new File(FILE_PATH_STUDENT_UPLOAD + "Positive/"
                + templateFileName);
        if ((Integer.parseInt(noOfStudents)) > 100) {
            LOGGER.info("Student records is more than 100 , setting timeouts of the validation variables to 150 sec");
            validateImport1.setTimeout(200000);
            validateImportRecords.setTimeout(200000);
        }
        // try {
        // Thread.sleep(1000);
        // } catch (InterruptedException e) {
        // e.printStackTrace();
        // }
        assertAndReport(
                browser,
                new ElementExistsValidation(browser, browser.getBrowserElement(
                        "OR_User", "paginationCheck")).performWithStates(),
                "Element Check");
        assertAndReport(browser, new FileUploadAction(browser,
                fileUploadElement, file).performWithStates(false, false),
                "Upload File");
        assertAndReport(
                browser,
                new ElementExistsValidation(browser, Buttons
                        .getButtonType4("Remove")).performWithStates(),
                "File Upload Check");
        if (headerFlag.contains("N")) {
            LOGGER.info("Clicking on No option for Header");
            if (new LeftClickElementAction(browser, browser.getBrowserElement(
                    "OR_User", "PreRegHeaderON")).performWithStates())
                try {
                    Thread.sleep(1000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
        }
        assertAndReport(
                browser,
                new LeftClickElementAction(browser, Buttons
                        .getButtonType4("Import")).performWithStates(),
                "Click on Import Button");

        assertAndReport(browser, new ElementExistsValidation(browser,
                validateImport1).performWithStates(), "Import Validation");
        assertAndReport(
                browser,
                new ElementExistsValidation(browser, validateImportRecords
                        .formatWithParms(noOfStudents)).performWithStates(),
                "Loading " + noOfStudents + " Records");
        assertAndReport(
                browser,
                new LeftClickElementAction(browser, validateImportRecords
                        .formatWithParms(noOfStudents)).performWithStates(),
                "Loading " + noOfStudents + " Records");

        assertAndReport(
                browser,
                new LeftClickElementAction(browser, Buttons
                        .getButtonType4("Import")).performWithStates(),
                "Click on Import Button");

        BrowserElement checkStatus = browser.getBrowserElement("OR_User",
                "checkStudentImportSuccess");
        if (checkStatus == null) {
            assertAndReport(browser, false,
                    "Could Not Find the Status of File Upload");
        }
        if (browser.getElementWithWait(checkStatus).getText()
                .equals("Imported")) {
            assertAndReport(browser, true, "File Uploaded Successfully");
        } else if (browser.getElementWithWait(checkStatus).getText()
                .equals("Not Imported")) {
            assertAndReport(browser, false,
                    "File Uploaded Status is : \"Not Imported\" ");
        } else {
            assertAndReport(browser, false, "File Uploaded Status is  : "
                    + browser.getElementWithWait(checkStatus).getText());
        }

        try {
            Thread.sleep(20000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    public String[] CreateStudent(int numOfStud, int studid) {
        String studARR[] = new String[numOfStud];
        for (int row = 0; row < numOfStud; row++) {
            studARR[row] = "dd10"
                    + (studid++)
                    + ",dtest,dmid,dlast,1,1,2000,add1,add2,phoeniz,Arizona,,41001,United States,deepakr.test@gmail.com,Male,Black or African American,12350012112,12350012112,";

        }

        return studARR;
    }

    public void createValidationElements() {
        validateImport1 = browser.getBrowserElement("OR_User",
                "validateImport1");
        validateImportRecords = browser.getBrowserElement("OR_User",
                "validateImportRecords");
        fileUploadElement = browser.getBrowserElement("OR_User",
                "fileStudentUpload");
    }

    @AfterClass(alwaysRun = true)
    public void teardown() {
        quitBrowser();
    }
}
