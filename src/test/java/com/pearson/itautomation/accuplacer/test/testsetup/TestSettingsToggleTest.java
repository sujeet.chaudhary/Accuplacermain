package com.pearson.itautomation.accuplacer.test.testsetup;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.ss.usermodel.WorkbookFactory;
import org.openqa.selenium.WebElement;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.testng.Assert;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import com.pearson.itautomation.accuplacer.core.action.LoginAction;
import com.pearson.itautomation.accuplacer.core.action.LogoutAction;
import com.pearson.itautomation.accuplacer.core.action.NavigateToMenu;
import com.pearson.itautomation.accuplacer.interaction.ScrollToPageTop;
import com.pearson.itautomation.accuplacer.test.TestBase;
import com.pearson.itautomation.bca.Browser;
import com.pearson.itautomation.bca.BrowserElement;
import com.pearson.itautomation.bca.SelectorType;
import com.pearson.itautomation.bca.interaction.ClearTextElementAction;
import com.pearson.itautomation.bca.interaction.DropdownSelectionAction;
import com.pearson.itautomation.bca.interaction.LeftClickElementAction;
import com.pearson.itautomation.bca.interaction.SendTextToElementAction;
import com.pearson.itautomation.bca.validation.ElementExistsValidation;
import com.pearson.itautomation.testngutils.reporting.TNReporter;

/**
 * Validates that test settings are available for a user depending on the users
 * permissions and validates if the setting can be toggled to different
 * settings.<br>
 * 
 * <b>createDifferentTestSetting</b>- Capable to create multiple combination of
 * TestSetting <br>
 * 
 * <b>validateTestSettings</b>-Responsible to create TestSetting for Regression
 * Suit<br>
 * 
 * <b>validateTestSettingsForSanity</b>-Responsible to create TestSetting for
 * Sanity Suit<br>
 * 
 * <b>validateTestSettingsForE2E</b>-Responsible to create TestSetting for End
 * to End Scenerio <br>
 * 
 * @author Chris Zinkula
 * @since 0.0.1
 */
public class TestSettingsToggleTest extends TestBase {

    private static final Logger LOGGER = LoggerFactory
            .getLogger(TestSettingsToggleTest.class);

    private static final String MASTER_SHEET_NAME = "Test Settings Master";
    private static final String FIELD_MAP_SHEET_NAME = "FieldMap";

    private static final String OR_AREA = "OR_TestSettings";

    private final static Map<String, String> testSettingNames = new HashMap<>();

    private static int ISR_FEADER;

    private Browser browser;

    private final static int SETTING_NAME_COLUMN = 51;

    @BeforeClass(alwaysRun = true)
    public void beforeClass() {
        browser = getBrowser();
    }

    public void createDifferentTestSetting(TestSettingsData testSettingsData,
            String timeStamp) {

        if (testSettingsData.shouldLogin()) {
            browser.get(getAppURL());
            assertAndReport(
                    browser,
                    new LoginAction(browser, testSettingsData.getUserName(),
                            testSettingsData.getPassword()).performWithStates(),
                    "Could not login.");
        }

        // scroll to the top of the page
        assertAndReport(browser,
                new ScrollToPageTop(browser).performWithStates(),
                "Could not scroll to top of page.");

        // navigate to the correct menu
        assertAndReport(browser, new NavigateToMenu(browser, "Test Setup",
                "Test Settings").performWithStates(),
                "Unable to navigate to Test Settings menu.");

        assertAndReport(
                browser,
                new ElementExistsValidation(browser, browser.getBrowserElement(
                        OR_AREA, "NextLink")).performWithStates(),
                "Unable to Validate Next Button.");

        // add a new test setting

        assertAndReport(
                browser,
                new LeftClickElementAction(browser, browser.getBrowserElement(
                        OR_AREA, "AddButton")).performWithStates(),
                "Unable to click add button for new test setting.");

        assertAndReport(
                browser,
                new ElementExistsValidation(browser, browser.getBrowserElement(
                        OR_AREA, "LearningPathForValidation"))
                        .performWithStates(),
                "Unable to Validate Learning Path Section.");

        boolean submittable = true;

        String testSettingName = testSettingsData.getTestSettingName();
        String modifiedTestSettingName = testSettingName + timeStamp;

        // populate the test setting name
        submittable = new SendTextToElementAction(browser,
                browser.getBrowserElement(OR_AREA, "NewTestSettingName"),
                modifiedTestSettingName).performWithStates();
        if (!submittable) {
            TNReporter
                    .addFailureReason("Unable to assign name to test setting.");
        }

        List<TestSetting> testSettings = testSettingsData.getTestSettings();

        Iterator<TestSetting> iterator = testSettings.iterator();
        int priorHeader = -1;

        while (iterator.hasNext()) {
            TestSetting next = iterator.next();
            if (next.getValue().equalsIgnoreCase("Nil")) {
                continue;
            }

            int headerIndexOnPage = next.getHeaderIndexOnPage();

            if (priorHeader != headerIndexOnPage) {
                priorHeader = headerIndexOnPage;
                assertAndReport(
                        browser,
                        new LeftClickElementAction(browser, browser
                                .getBrowserElementWithFormat(OR_AREA,
                                        "Heading", headerIndexOnPage))
                                .performWithStates(),
                        "Unable to open accordion menu.");
                assertAndReport(
                        browser,
                        new ElementExistsValidation(browser, browser
                                .getBrowserElementWithFormat(OR_AREA,
                                        "HeadingOpen", headerIndexOnPage))
                                .performWithStates(),
                        "Unable to validate accordion menu is opened.");
            }

            TNReporter.addNote("Interacting with: " + next.getElementType()
                    + "\t" + next.getHeaderIndexOnPage() + "\t"
                    + next.getPromptIndex() + "\t" + next.getValue());
            if (next.getElementType().equals("RadioButton")) {
                if (next.getHeaderIndexOnPage() == 5
                        && browser.getElementWithWait(OR_AREA, "UserType")
                                .getText().equals("INSTITUTION ADMINISTRATOR")) {
                    boolean success = new LeftClickElementAction(browser,
                            browser.getBrowserElementWithFormat(OR_AREA,
                                    "RadioButtonItem1", next.getValue()))
                            .performWithStates();
                    if (!success) {
                        TNReporter
                                .addFailureReason("Could not click radio button as expected.  Header: "
                                        + headerIndexOnPage
                                        + " item: "
                                        + next.getPromptIndex()
                                        + " value: "
                                        + next.getValue());
                        submittable = false;
                    }
                } else {
                    boolean success = new LeftClickElementAction(browser,
                            browser.getBrowserElementWithFormat(OR_AREA,
                                    "RadioButtonItem", headerIndexOnPage,
                                    next.getPromptIndex(), next.getValue()))
                            .performWithStates();
                    if (!success) {
                        TNReporter
                                .addFailureReason("Could not click radio button as expected.  Header: "
                                        + headerIndexOnPage
                                        + " item: "
                                        + next.getPromptIndex()
                                        + " value: "
                                        + next.getValue());
                        submittable = false;
                    }
                }
            } else if (next.getElementType().equals("Dropdown")) {
                boolean success = new DropdownSelectionAction(browser,
                        browser.getBrowserElementWithFormat(OR_AREA,
                                "Dropdown", headerIndexOnPage,
                                next.getPromptIndex()),
                        SelectorType.VISIBLE_TEXT, next.getValue())
                        .performWithStates(false, true);
                if (!success) {
                    TNReporter
                            .addFailureReason("Could not select from dropdown as expected.  Header: "
                                    + headerIndexOnPage
                                    + " item: "
                                    + next.getPromptIndex()
                                    + " value: "
                                    + next.getValue());
                    submittable = false;
                }
            } else if (next.getElementType().equals("Textfield")) {
                boolean success = new ClearTextElementAction(browser,
                        browser.getBrowserElementWithFormat(OR_AREA,
                                "Textfield", headerIndexOnPage,
                                next.getPromptIndex())).performWithStates();
                if (success) {
                    success = new SendTextToElementAction(browser,
                            browser.getBrowserElementWithFormat(OR_AREA,
                                    "Textfield", headerIndexOnPage,
                                    next.getPromptIndex()), next.getValue())
                            .performWithStates();
                }
                if (!success) {
                    TNReporter
                            .addFailureReason("Could not populate text field as expected.  Header: "
                                    + headerIndexOnPage
                                    + " item: "
                                    + next.getPromptIndex()
                                    + " value: "
                                    + next.getValue());
                    submittable = false;
                }
            } else if (next.getElementType().equals("CheckBox")) {

                BrowserElement allCheckboxes = browser
                        .getBrowserElementWithFormat(OR_AREA, "AllCheckboxes",
                                headerIndexOnPage, next.getPromptIndex());
                List<WebElement> checkboxes = browser
                        .getElementsWithWait(allCheckboxes);
                for (WebElement checkbox : checkboxes) {
                    if (checkbox.getText().equals("Branching Profile")
                            || checkbox.getText().equals("Exnum")) {
                        checkbox.click();
                    }
                }

                String[] values = next.getValue().split("\\+");
                ISR_FEADER = values.length;

                for (String value : values) {
                    boolean success = new LeftClickElementAction(getBrowser(),
                            browser.getBrowserElementWithFormat(OR_AREA,
                                    "Checkbox", headerIndexOnPage,
                                    next.getPromptIndex(), value.trim()))
                            .performWithStates();

                    if (!success) {
                        TNReporter
                                .addFailureReason("Could not click on checkbox as expected.  Header: "
                                        + headerIndexOnPage
                                        + " item: "
                                        + next.getPromptIndex()
                                        + " value: "
                                        + value.trim());
                        submittable = false;
                    }
                }
            }
        }

        assertAndReport(browser, submittable,
                "Test setting could not be submitted.");

        // scroll to the top of the page
        assertAndReport(browser,
                new ScrollToPageTop(browser).performWithStates(),
                "Could not scroll to top of page.");

        assertAndReport(
                browser,
                new LeftClickElementAction(browser, browser.getBrowserElement(
                        OR_AREA, "SaveButton")).performWithStates(),
                "Could not click on save button.");

        if (ISR_FEADER <= 2) {
            Assert.assertTrue(
                    new ElementExistsValidation(browser, browser
                            .getBrowserElement(OR_AREA,
                                    "ConformationMsgPositiveScenerio"))
                            .performWithStates(),
                    "Unable to Validate Conformation Msg For positive scenerio.");
        } else {
            assertAndReport(
                    browser,
                    new ElementExistsValidation(browser, browser
                            .getBrowserElement(OR_AREA,
                                    "ErrorMsgNegativeScenerio"))
                            .performWithStates(),
                    "Unable to Validate Error Msg for Negative Scenerio");
        }

        testSettingNames.put(testSettingName, modifiedTestSettingName);

        if (testSettingsData.shouldLogout()) {
            assertAndReport(browser,
                    new LogoutAction(browser).performWithStates(),
                    "Could not log out of application.");
        }
    }

    @Test(dataProvider = "TestSettingsToggle", groups = { "regression",
            "CATTECHTA-21", "CATTECHTA-22", "CATTECHTA-23", "CATTECHTA-24",
            "Test Setup" })
    public void validateTestSettingsRegression(TestSettingsData testSettingsData) {

        addTestCaseDescription("Create a test setting based on the data gathered from the test settings spreadsheet.");
        TNReporter
                .setParmDesc("The username and the name of the test setting to be created");
        createDifferentTestSetting(testSettingsData,
                Long.toString(System.currentTimeMillis()));

    }

    @Test(dataProvider = "TestSettingsToggleForSanity", groups = { "sanity",
            "CATTECHTA-22", "CATTECHTA-23", "CATTECHTA-24", "Test Setup" })
    public void validateTestSettingsForSanity(TestSettingsData testSettingsData) {

        addTestCaseDescription("Create a test setting based on the data gathered from the test settings spreadsheet.");
        TNReporter
                .setParmDesc("The username and the name of the test setting to be created");
        createDifferentTestSetting(testSettingsData,
                Long.toString(System.currentTimeMillis()));
    }

    @Test(dataProvider = "TestSettingsToggleForE2E", groups = { "e2e",
            "CATTECHTA-22", "CATTECHTA-23", "CATTECHTA-24", "Test Setup" })
    public void validateTestSettingsForE2E(TestSettingsData testSettingsData) {

        addTestCaseDescription("Create a test setting for the end to end scenario. <b>TestSettings Name : "
                + testSettingsData.getTestSettingName()
                + currentExecTimeStamp
                + "</b>");
        TNReporter
                .setParmDesc("The username and the name of the test setting to be created");

        createDifferentTestSetting(testSettingsData, currentExecTimeStamp);
    }

    @AfterClass(alwaysRun = true)
    public void afterClass() {
        quitBrowser();
    }

    /**
     * Get the modified test setting name based on the name of the test setting
     * that was to be used.
     * 
     * @param testSettingName
     *            The name of the test setting that was to be used.
     * @return The modified test setting name.
     */
    public static final String getModifiedTestSettingName(
            final String testSettingName) {
        return testSettingNames.get(testSettingName);
    }

    @DataProvider(name = "TestSettingsToggle")
    public Object[][] testSettingsToggleData() {
        InputStream spreadsheet = TestSettingsToggleTest.class
                .getResourceAsStream("/test data/Test Settings Test Data.xlsx");
        List<TestSettingsData> testData = new ArrayList<>();

        try {
            Workbook workbook = WorkbookFactory.create(spreadsheet);
            Sheet masterSheet = workbook.getSheet(MASTER_SHEET_NAME);
            Sheet fieldMapSheet = workbook.getSheet(FIELD_MAP_SHEET_NAME);

            Map<Integer, TestSetting> fieldMaps = getFieldMappings(fieldMapSheet);

            Iterator<Row> rowIterator = masterSheet.rowIterator();
            // skip header row
            if (rowIterator.hasNext()) {
                rowIterator.next();
            }

            while (rowIterator.hasNext()) {
                Row row = rowIterator.next();
                List<TestSettingsData> testDataForUser = getDataForUser(row,
                        workbook, fieldMaps);
                testDataForUser.get(0).setShouldLogin(true);
                testDataForUser.get(testDataForUser.size() - 1)
                        .setShouldLogout(true);
                testData.addAll(testDataForUser);
            }

        } catch (InvalidFormatException | IOException e) {
            LOGGER.error(
                    "Unable to load data for test case which validates Test Settings.",
                    e);
        }

        Object[][] rValue = new Object[testData.size()][1];
        for (int i = 0; i < testData.size(); i++) {
            rValue[i][0] = testData.get(i);
        }
        return rValue;
    }

    @DataProvider(name = "TestSettingsToggleForE2E")
    public Object[][] testSettingsToggleDataForE2E() {
        InputStream spreadsheet = TestSettingsToggleTest.class
                .getResourceAsStream("/test data/Test Settings TestData For E2E.xlsx");
        List<TestSettingsData> testData = new ArrayList<>();

        try {
            Workbook workbook = WorkbookFactory.create(spreadsheet);
            Sheet masterSheet = workbook.getSheet(MASTER_SHEET_NAME);
            Sheet fieldMapSheet = workbook.getSheet(FIELD_MAP_SHEET_NAME);

            Map<Integer, TestSetting> fieldMaps = getFieldMappings(fieldMapSheet);

            Iterator<Row> rowIterator = masterSheet.rowIterator();
            // skip header row
            if (rowIterator.hasNext()) {
                rowIterator.next();
            }

            while (rowIterator.hasNext()) {
                Row row = rowIterator.next();
                List<TestSettingsData> testDataForUser = getDataForUser(row,
                        workbook, fieldMaps);
                testDataForUser.get(0).setShouldLogin(true);
                testDataForUser.get(testDataForUser.size() - 1)
                        .setShouldLogout(true);
                testData.addAll(testDataForUser);
            }

        } catch (InvalidFormatException | IOException e) {
            LOGGER.error(
                    "Unable to load data for test case which validates Test Settings.",
                    e);
        }

        Object[][] rValue = new Object[testData.size()][1];
        for (int i = 0; i < testData.size(); i++) {
            rValue[i][0] = testData.get(i);
        }
        return rValue;
    }

    @DataProvider(name = "TestSettingsToggleForSanity")
    public Object[][] testSettingsToggleDataForSanity() {
        InputStream spreadsheet = TestSettingsToggleTest.class
                .getResourceAsStream("/test data/Test Settings Test DataForSanity.xlsx");
        List<TestSettingsData> testData = new ArrayList<>();

        try {
            Workbook workbook = WorkbookFactory.create(spreadsheet);
            Sheet masterSheet = workbook.getSheet(MASTER_SHEET_NAME);
            Sheet fieldMapSheet = workbook.getSheet(FIELD_MAP_SHEET_NAME);

            Map<Integer, TestSetting> fieldMaps = getFieldMappings(fieldMapSheet);

            Iterator<Row> rowIterator = masterSheet.rowIterator();
            // skip header row
            if (rowIterator.hasNext()) {
                rowIterator.next();
            }

            while (rowIterator.hasNext()) {
                Row row = rowIterator.next();
                List<TestSettingsData> testDataForUser = getDataForUser(row,
                        workbook, fieldMaps);
                testDataForUser.get(0).setShouldLogin(true);
                testDataForUser.get(testDataForUser.size() - 1)
                        .setShouldLogout(true);
                testData.addAll(testDataForUser);
            }

        } catch (InvalidFormatException | IOException e) {
            LOGGER.error(
                    "Unable to load data for test case which validates Test Settings.",
                    e);
        }

        Object[][] rValue = new Object[testData.size()][1];
        for (int i = 0; i < testData.size(); i++) {
            rValue[i][0] = testData.get(i);
        }
        return rValue;
    }

    private Map<Integer, TestSetting> getFieldMappings(Sheet fieldMapSheet) {
        Map<Integer, TestSetting> fieldMaps = new LinkedHashMap<>();

        Iterator<Row> rows = fieldMapSheet.iterator();

        // skip header row
        if (rows.hasNext()) {
            rows.next();
        }

        while (rows.hasNext()) {
            Row row = rows.next();

            row.getCell(0, Row.CREATE_NULL_AS_BLANK).setCellType(
                    Cell.CELL_TYPE_STRING);
            row.getCell(1, Row.CREATE_NULL_AS_BLANK).setCellType(
                    Cell.CELL_TYPE_STRING);
            row.getCell(2, Row.CREATE_NULL_AS_BLANK).setCellType(
                    Cell.CELL_TYPE_STRING);
            row.getCell(3, Row.CREATE_NULL_AS_BLANK).setCellType(
                    Cell.CELL_TYPE_STRING);
            int colNum = Integer.parseInt(row.getCell(0,
                    Row.CREATE_NULL_AS_BLANK).getStringCellValue());
            int headerIndex = Integer.parseInt(row.getCell(1,
                    Row.CREATE_NULL_AS_BLANK).getStringCellValue());
            int promptIndex = Integer.parseInt(row.getCell(2,
                    Row.CREATE_NULL_AS_BLANK).getStringCellValue());
            String elementType = row.getCell(3, Row.CREATE_NULL_AS_BLANK)
                    .getStringCellValue();

            TestSetting ts = new TestSetting(null, headerIndex, promptIndex,
                    elementType);

            fieldMaps.put(colNum, ts);
        }

        return fieldMaps;
    }

    private List<TestSettingsData> getDataForUser(Row userRow,
            Workbook workbook, Map<Integer, TestSetting> fieldMaps) {
        final String userName = userRow.getCell(1, Row.CREATE_NULL_AS_BLANK)
                .getStringCellValue();
        final String password = userRow.getCell(2, Row.CREATE_NULL_AS_BLANK)
                .getStringCellValue();
        final String dataSheetName = userRow.getCell(3,
                Row.CREATE_NULL_AS_BLANK).getStringCellValue();

        List<TestSettingsData> testData = new ArrayList<>();
        Sheet dataSheet = workbook.getSheet(dataSheetName);

        Iterator<Row> rowIterator = dataSheet.rowIterator();
        int headerCount = 0;
        while (rowIterator.hasNext() && ++headerCount < 4) {
            rowIterator.next();
        }

        while (rowIterator.hasNext()) {
            Row settingsRow = rowIterator.next();
            TestSettingsData testSettingsData = new TestSettingsData(userName,
                    password);
            addTestSettingsData(testSettingsData, settingsRow, fieldMaps);
            testData.add(testSettingsData);
        }

        return testData;
    }

    private void addTestSettingsData(TestSettingsData testSettingsData,
            Row settingsRow, Map<Integer, TestSetting> fieldMaps) {
        testSettingsData.setTestSettingName(settingsRow.getCell(
                SETTING_NAME_COLUMN, Row.CREATE_NULL_AS_BLANK)
                .getStringCellValue());

        Iterator<Entry<Integer, TestSetting>> iterator = fieldMaps.entrySet()
                .iterator();

        while (iterator.hasNext()) {
            Entry<Integer, TestSetting> setting = iterator.next();
            String value = settingsRow.getCell(setting.getKey() - 1,
                    Row.CREATE_NULL_AS_BLANK).getStringCellValue();
            TestSetting ts = new TestSetting(value, setting.getValue()
                    .getHeaderIndexOnPage(), setting.getValue()
                    .getPromptIndex(), setting.getValue().getElementType());
            testSettingsData.addTestSetting(ts);
        }
    }

    private static final class TestSettingsData {
        private final String userName;
        private final String password;
        private boolean shouldLogin = false;
        private boolean shouldLogout = false;
        private final List<TestSetting> testSettings = new ArrayList<>();

        private String testSettingName;

        public TestSettingsData(String userName, String password) {
            this.userName = userName;
            this.password = password;
        }

        public void addTestSetting(TestSetting ts) {
            testSettings.add(ts);
        }

        public List<TestSetting> getTestSettings() {
            return testSettings;
        }

        /**
         * @return the shouldLogin
         */
        public final boolean shouldLogin() {
            return shouldLogin;
        }

        /**
         * @param shouldLogin
         *            the shouldLogin to set
         */
        public final void setShouldLogin(boolean shouldLogin) {
            this.shouldLogin = shouldLogin;
        }

        /**
         * @return the shouldLogout
         */
        public final boolean shouldLogout() {
            return shouldLogout;
        }

        /**
         * @param shouldLogout
         *            the shouldLogout to set
         */
        public final void setShouldLogout(boolean shouldLogout) {
            this.shouldLogout = shouldLogout;
        }

        /**
         * @return the userName
         */
        public final String getUserName() {
            return userName;
        }

        /**
         * @return the password
         */
        public final String getPassword() {
            return password;
        }

        /**
         * @return the testSettingName
         */
        public final String getTestSettingName() {
            return testSettingName;
        }

        /**
         * @param testSettingName
         *            the testSettingName to set
         */
        public final void setTestSettingName(String testSettingName) {
            this.testSettingName = testSettingName;
        }

        /*
         * (non-Javadoc)
         * 
         * @see java.lang.Object#toString()
         */
        @Override
        public String toString() {
            return "TestSettingsData [userName=" + userName
                    + ", testSettingName=" + testSettingName + "]";
        }
    }

    private static final class TestSetting {
        private final String value;
        private final int headerIndexOnPage;
        private final int promptIndex;
        private final String elementType;

        public TestSetting(String value, int headerIndexOnPage,
                int promptIndex, String elementType) {
            this.value = value;
            this.headerIndexOnPage = headerIndexOnPage;
            this.promptIndex = promptIndex;
            this.elementType = elementType;
        }

        /**
         * @return the value
         */
        public final String getValue() {
            return value;
        }

        /**
         * @return the headerIndexOnPage
         */
        public final int getHeaderIndexOnPage() {
            return headerIndexOnPage;
        }

        /**
         * @return the promptIndex
         */
        public final int getPromptIndex() {
            return promptIndex;
        }

        /**
         * @return the elementType
         */
        public final String getElementType() {
            return elementType;
        }
    }
}
