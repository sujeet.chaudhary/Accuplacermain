package com.pearson.itautomation.accuplacer.test.testsetup;

import org.openqa.selenium.By;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.testng.Assert;

import com.pearson.itautomation.accuplacer.core.Buttons;
import com.pearson.itautomation.accuplacer.core.action.NavigateToLink;
import com.pearson.itautomation.accuplacer.core.action.SaveAndAssert;
import com.pearson.itautomation.accuplacer.test.TestBaseAssert;
import com.pearson.itautomation.accuplacer.testsetup.branchingprofiles.AddAdministerBackgroundQuestionGroupRule;
import com.pearson.itautomation.accuplacer.testsetup.branchingprofiles.AddAdministerTestRule;
import com.pearson.itautomation.accuplacer.testsetup.branchingprofiles.AddBPConditionBackGroundQuestion;
import com.pearson.itautomation.accuplacer.testsetup.branchingprofiles.AddBPConditionMajors;
import com.pearson.itautomation.accuplacer.testsetup.branchingprofiles.AddBPConditionMultipleTestScore;
import com.pearson.itautomation.accuplacer.testsetup.branchingprofiles.AddBPConditionSingleTestScore;
import com.pearson.itautomation.accuplacer.testsetup.branchingprofiles.AddBPConditionTestStatus;
import com.pearson.itautomation.accuplacer.testsetup.branchingprofiles.AddTestSettingRule;
import com.pearson.itautomation.accuplacer.testsetup.branchingprofiles.CreateBranchingProfile;
import com.pearson.itautomation.accuplacer.testsetup.branchingprofiles.DeleteBPRule;
import com.pearson.itautomation.bca.Browser;
import com.pearson.itautomation.bca.BrowserElement;

/**
 * 
 * @author Deepak Radhakrishnan
 * 
 */
public class TestBranchingProfileUtils extends TestBaseAssert {
    private static final Logger LOGGER = LoggerFactory
            .getLogger(TestBranchingProfileUtils.class);

    private static final String DEMARCATION_PREFIX = "*** ";
    private final String SUB_MENU = "Branching Profiles";

    private boolean demarcationRequired = true;
    private String prefix = "";

    private boolean e2eTestSettingflag = false;
    private static final BrowserElement btnAddBranchingProfile = Buttons
            .getButtonType1("Add");

    public void createBranchingProfiles(String bPName, Browser browser,
            String rule1, String rule2, String rule3, String condition1,
            String condition2, String condition3, String rule1Value,
            String rule2Value, String rule3Value, String condition1Value,
            String condition2Value, String condition3Value,
            boolean demarcationRequired) {
        this.setDemarcationRequired(demarcationRequired);
        createBranchingProfiles(bPName, browser, rule1, rule2, rule3,
                condition1, condition2, condition3, rule1Value, rule2Value,
                rule3Value, condition1Value, condition2Value, condition3Value);

    }

    public void e2eCreateBranchingProfiles(String bPName, Browser browser,
            String rule1, String rule2, String rule3, String condition1,
            String condition2, String condition3, String rule1Value,
            String rule2Value, String rule3Value, String condition1Value,
            String condition2Value, String condition3Value,
            boolean e2eTestSettingflag) {
        this.setE2eTestSettingflag(e2eTestSettingflag);
        createBranchingProfiles(bPName, browser, rule1, rule2, rule3,
                condition1, condition2, condition3, rule1Value, rule2Value,
                rule3Value, condition1Value, condition2Value, condition3Value);
    }

    public void createBranchingProfiles(String bPName, Browser browser,
            String rule1, String rule2, String rule3, String condition1,
            String condition2, String condition3, String rule1Value,
            String rule2Value, String rule3Value, String condition1Value,
            String condition2Value, String condition3Value) {
        final String conditionValueList[];
        final String[] ruleValueList;
        if (!browser
                .getWebDriver()
                .findElement(
                        By.xpath("(//span[@class='inst-style ng-binding'])[2]"))
                .getText().contains("CB")) {
            LOGGER.info("Concatenating demarcation prefix as the logged in User is not a CB level User");
            if (this.isDemarcationRequired()) {
                prefix = DEMARCATION_PREFIX;
            }
            ruleValueList = new String[] { prefix + rule1Value,
                    prefix + rule2Value, prefix + rule3Value };
            conditionValueList = new String[] { prefix + condition1Value,
                    prefix + condition2Value, prefix + condition3Value };
        }

        else {
            ruleValueList = new String[] { rule1Value, rule2Value, rule3Value };
            conditionValueList = new String[] { condition1Value,
                    condition2Value, condition3Value };
        }

        String ruleList[] = { rule1, rule2, rule3 };
        String conditionList[] = { condition1, condition2, condition3 };

        assertAndReport(
                browser,
                new CreateBranchingProfile(browser, bPName).performWithStates(),
                "Create Branching profile");
        assertAndReport(browser,
                new DeleteBPRule(browser, 2).performWithStates(),
                "Delete BP rule 2");
        for (int i = 1; i <= ruleList.length; i++) {
            if (ruleList[i - 1].equalsIgnoreCase("Administer Test")) {
                if (ruleValueList[i - 1].equalsIgnoreCase("NA")
                        || rule1Value.equalsIgnoreCase("")) {
                    LOGGER.error("Please enter a valid value for Rule "
                            + (i + 1) + " dropdown content");
                    Assert.fail();
                } else {
                    LOGGER.info("Adding Administer Test Rule");
                    assertAndReport(browser, new AddAdministerTestRule(browser,
                            i + 1, ruleValueList[i - 1]).performWithStates(),
                            "Add Adminster Test Rule as Rule # " + (i + 1));
                }

            } else if (ruleList[i - 1]
                    .equalsIgnoreCase("Administer Background Question Group")) {
                if (ruleValueList[i - 1].equalsIgnoreCase("NA")
                        || rule1Value.equalsIgnoreCase("")) {
                    LOGGER.error("Please enter a valid value for Rule "
                            + (i + 1) + " dropdown content");
                    Assert.fail();
                } else {
                    LOGGER.info("Adding Administer Background Question Group Rule");
                    assertAndReport(browser,
                            new AddAdministerBackgroundQuestionGroupRule(
                                    browser, (i + 1), ruleValueList[i - 1])
                                    .performWithStates(),
                            "Add Adminster Background Question Group as Rule # "
                                    + (i + 1));
                }
            }

            else if (ruleList[i - 1].equalsIgnoreCase("Apply Test Settings")) {
                if (ruleValueList[i - 1].equalsIgnoreCase("NA")
                        || rule1Value.equalsIgnoreCase("")) {
                    LOGGER.error("Please enter a valid value for Rule "
                            + (i + 1) + " dropdown content");
                    Assert.fail();
                } else {
                    LOGGER.info("Adding Apply Test Setting Rule");
                    if (isE2eTestSettingflag()) {
                        LOGGER.info("Replacing demarcation prefix");
                        ruleValueList[i - 1] = ruleValueList[i - 1].replace(
                                DEMARCATION_PREFIX, "");
                        LOGGER.info("Value after replacing - "
                                + ruleValueList[i - 1]);

                    }
                    assertAndReport(browser, new AddTestSettingRule(browser,
                            (i + 1), ruleValueList[i - 1]).performWithStates(),
                            "Add Test Setting as Rule # " + (i + 1));
                }
            } else if (ruleList[i - 1].equalsIgnoreCase("NA")) {
                LOGGER.info("No Rule Added");
                break;
            } else {
                LOGGER.warn("Please enter a valid Rule Name");
            }

        }

        for (int i = 1; i <= conditionList.length; i++) {
            if (conditionList[i - 1].equalsIgnoreCase("Test Was Completed")
                    || conditionList[i - 1].equalsIgnoreCase("Test Not Taken")) {
                if (conditionValueList[i - 1].equalsIgnoreCase("NA")
                        || rule1Value.equalsIgnoreCase("")) {
                    LOGGER.error("Please enter a valid value for Condition "
                            + (i + 1) + " dropdown content");
                    Assert.fail();
                } else {
                    LOGGER.info("Adding " + conditionList[i - 1] + " condition");
                    assertAndReport(browser, new AddBPConditionTestStatus(
                            browser, i + 1, conditionValueList[i - 1],
                            conditionList[i - 1]).performWithStates(), "Add "
                            + conditionList[i - 1] + " to Rule # " + (i + 1));
                }

            } else if (conditionList[i - 1]
                    .equalsIgnoreCase("Background Question")) {
                if (conditionValueList[i - 1].equalsIgnoreCase("NA")
                        || rule1Value.equalsIgnoreCase("")) {
                    LOGGER.error("Please enter a valid value for Condition "
                            + (i + 1) + " dropdown content");
                    Assert.fail();
                } else {
                    LOGGER.info("Adding " + conditionList[i - 1] + " condition");
                    assertAndReport(browser,
                            new AddBPConditionBackGroundQuestion(browser,
                                    i + 1, conditionValueList[i - 1])
                                    .performWithStates(), "Add "
                                    + conditionList[i - 1] + " to Rule # "
                                    + (i + 1));
                }

            } else if (conditionList[i - 1]
                    .equalsIgnoreCase("Single Test Score")) {
                if (conditionValueList[i - 1].equalsIgnoreCase("NA")
                        || rule1Value.equalsIgnoreCase("")) {
                    LOGGER.error("Please enter a valid value for Condition "
                            + (i + 1) + " dropdown content");
                    Assert.fail();
                } else {
                    LOGGER.info("Adding " + conditionList[i - 1] + " condition");
                    assertAndReport(browser,
                            new AddBPConditionSingleTestScore(browser, i + 1,
                                    conditionValueList[i - 1])
                                    .performWithStates(), "Add "
                                    + conditionList[i - 1] + " to Rule # "
                                    + (i + 1));
                }

            } else if (conditionList[i - 1]
                    .equalsIgnoreCase("Multiple Test Score")) {
                if (conditionValueList[i - 1].equalsIgnoreCase("NA")
                        || rule1Value.equalsIgnoreCase("")) {
                    LOGGER.error("Please enter a valid value for Condition "
                            + (i + 1) + " dropdown content");
                    Assert.fail();
                } else {
                    LOGGER.info("Adding " + conditionList[i - 1] + " condition");
                    assertAndReport(browser,
                            new AddBPConditionMultipleTestScore(browser, i + 1,
                                    conditionValueList[i - 1])
                                    .performWithStates(), "Add "
                                    + conditionList[i - 1] + " to Rule # "
                                    + (i + 1));
                }

            } else if (conditionList[i - 1].equalsIgnoreCase("Major")) {
                if (conditionValueList[i - 1].equalsIgnoreCase("NA")
                        || rule1Value.equalsIgnoreCase("")) {
                    LOGGER.error("Please enter a valid value for Condition "
                            + (i + 1) + " dropdown content");
                    Assert.fail();
                } else {
                    LOGGER.info("Adding " + conditionList[i - 1] + " condition");
                    assertAndReport(browser,
                            new AddBPConditionMajors(browser, i + 1,
                                    conditionValueList[i - 1])
                                    .performWithStates(), "Add "
                                    + conditionList[i - 1] + " to Rule # "
                                    + (i + 1));
                }

            }
        }
        assertAndReport(browser,
                new DeleteBPRule(browser, 1).performWithStates(),
                "Delete BP rule 1");
        if (!new SaveAndAssert(browser, "Branching Profile added successfully")
                .performWithStates()) {
            LOGGER.error("Failed To Save The Branching Profile");
            assertAndReport(browser, new NavigateToLink(browser, SUB_MENU,
                    btnAddBranchingProfile).performWithStates(),
                    "Navigate to Branching Profile Page");
            assertAndReport(browser, false, "Save Branching Profile");
        } else {
            assertAndReport(browser, true, "Save Branching Profile");
        }

    }

    public boolean isDemarcationRequired() {
        return demarcationRequired;
    }

    public void setDemarcationRequired(boolean demarcationRequired) {
        this.demarcationRequired = demarcationRequired;
    }

    public boolean isE2eTestSettingflag() {
        return e2eTestSettingflag;
    }

    public void setE2eTestSettingflag(boolean e2eTestSettingflag) {
        this.e2eTestSettingflag = e2eTestSettingflag;
    }
}
