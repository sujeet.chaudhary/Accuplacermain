package com.pearson.itautomation.accuplacer.test.users;

import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import com.pearson.itautomation.accuplacer.core.Buttons;
import com.pearson.itautomation.accuplacer.core.action.LoginAction;
import com.pearson.itautomation.accuplacer.core.action.NavigateToMenu;
import com.pearson.itautomation.accuplacer.test.TestBase;
import com.pearson.itautomation.accuplacer.users.testingunits.TransferTestUnits;
import com.pearson.itautomation.bca.BrowserElement;
import com.pearson.itautomation.testngutils.dataprovider.ExcelDataProvider;
import com.pearson.itautomation.testngutils.reporting.TNReporter;

public class TransferTestUnitsTest extends TestBase {

    // private static final Logger LOGGER = LoggerFactory
    // .getLogger(TestAdminsterCommon.class);

    public final String strTabName = "Users";
    public final String strSubMenuName = "Transfer Test Units";
    public final BrowserElement btnClose = Buttons.getButtonType1("Close");

    public String strTransferUnitsFrom;
    public String strTransferUnitsTo;
    public String strNoOfUnits;

    public final BrowserElement btnTransferUnits = Buttons
            .getButtonType1("Transfer");

    @Test(dataProvider = "standardExcelData", dataProviderClass = ExcelDataProvider.class)
    public void transferTestUnits(final String failMessage,
            String strTransferUnitsFrom, String strTransferUnitsTo,
            final String strNoOfUnits) {

        addTestCaseDescription("Transfers test units from one organization to another organization.");
        TNReporter
                .setParmDesc(
                        "Describes the type of organization from which units are being transfered to",
                        "The organization to transfer units from",
                        "The organization to transfer units to",
                        "The number of units to transfer");
        this.strTransferUnitsFrom = strTransferUnitsFrom;
        this.strTransferUnitsTo = strTransferUnitsTo;
        this.strNoOfUnits = strNoOfUnits;
        if (strTransferUnitsFrom.startsWith("*")) {
            strTransferUnitsFrom = strTransferUnitsFrom.replace("*", "\\s*.*");
        }
        if (strTransferUnitsTo.startsWith("*")) {
            strTransferUnitsTo = strTransferUnitsTo.replace("*", "\\s*.*");
        }
        assertAndReport(browser,
                new TransferTestUnits(this.browser, strTransferUnitsFrom,
                        strTransferUnitsTo, strNoOfUnits).performWithStates(),
                "Transfer test units");

    }

    @BeforeClass
    public void beforeClass() {
        browser = getBrowser();
        browser.get(getAppURL());
        User user = getUser(USER_ROLE.INSTITUTE_ADMIN);

        assertAndReport(
                browser,
                new LoginAction(this.browser, user.getUserName(), user
                        .getPassword()).performWithStates(), "Login Action");

        assertAndReport(browser, new NavigateToMenu(this.browser, strTabName,
                strSubMenuName, btnTransferUnits).performWithStates(),
                "Navigation to Transfer Test Units Page");
    }

    @AfterClass
    public void afterClass() {
        quitBrowser();
    }
}
