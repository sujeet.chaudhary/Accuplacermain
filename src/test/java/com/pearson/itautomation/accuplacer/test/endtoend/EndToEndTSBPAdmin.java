package com.pearson.itautomation.accuplacer.test.endtoend;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import com.pearson.itautomation.accuplacer.core.Buttons;
import com.pearson.itautomation.accuplacer.core.action.LoginAction;
import com.pearson.itautomation.accuplacer.core.action.NavigateToMenu;
import com.pearson.itautomation.accuplacer.test.TestBase;
import com.pearson.itautomation.accuplacer.test.administertest.TestAdminsterCommon;
import com.pearson.itautomation.accuplacer.test.testsetup.TestBranchingProfileUtils;
import com.pearson.itautomation.bca.BrowserElement;
import com.pearson.itautomation.testngutils.dataprovider.ExcelDataProvider;
import com.pearson.itautomation.testngutils.reporting.TNReporter;

/**
 * This Test Case will add a branching profile at site level with 1 - 3 rules,
 * and combination of conditions under these rules,Depending on the Test Data
 * Provided
 * 
 * @author Deepak Radhakrishnan
 * 
 */
public class EndToEndTSBPAdmin extends TestBase {
    private static final Logger LOGGER = LoggerFactory
            .getLogger(EndToEndTSBPAdmin.class);

    private static final String TAB_NAME = "Test Setup";
    private static final String SUB_MENU = "Branching Profiles";
    private static final String STUDENT_ID = "d30000000";
    private static final String LAST_NAME = "dlast";
    private static final String MOB = "Jan";
    private static final String DOB = "1";
    private static final String YOB = "2000";

    private final BrowserElement btnAddBranchingProfile = Buttons
            .getButtonType3("Add");
    private User user;
    TestBranchingProfileUtils tBPU = new TestBranchingProfileUtils();
    private TestAdminsterCommon testAdminCommonProctorReporter = new TestAdminsterCommon();

    @Test(dataProvider = "standardExcelData", dataProviderClass = ExcelDataProvider.class, groups = {
            "CATTECHTA - 48", "e2e", "TestSetup" })
    public void e2eTSBPAdmin(String tcID, String rule1, String rule2,
            String rule3, String condition1, String condition2,
            String condition3, String rule1Value, String rule2Value,
            String rule3Value, String condition1Value, String condition2Value,
            String condition3Value, String bPName, String answerKeys) {
        addTestCaseDescription("<b><u>"
                + tcID
                + " : </b></u>End 2 End Scenario <b>Create Test Settings -> Create Branching Profile -> Administer Test</b> <br> <b>E2E Test Settings Used : "
                + rule1Value + currentExecTimeStamp + "</b>");
        TNReporter.setParmDesc("Type of Rule 1 ", "Type of Rule 2 ",
                "Type of Rule 3 ", "Type of Condition 1 ",
                "Type of Condition 2 ", "Type of Condition 3 ",
                "Value for Rule 1 ", "Value for Rule 2 ", "Value for Rule 3 ",
                "Value for Condition 1 ", "Value for Condition 2 ",
                "Value for Condition 3 ", "Name of BranchineProfile ",
                "Answekey Pattern For Test Administration");
        e2eUserBPCreate(rule1, rule2, rule3, condition1, condition2,
                condition3, rule1Value.replace("*** ", "")
                        + currentExecTimeStamp, rule2Value, rule3Value,
                condition1Value, condition2Value, condition3Value, bPName
                        + currentExecTimeStamp);

        testAdminCommonProctorReporter.administerTestCommon(browser, user,
                getConnection(), STUDENT_ID, LAST_NAME, DOB, MOB, YOB, bPName
                        + currentExecTimeStamp, answerKeys, "");

    }

    public void e2eUserBPCreate(String rule1, String rule2, String rule3,
            String condition1, String condition2, String condition3,
            String rule1Value, String rule2Value, String rule3Value,
            String condition1Value, String condition2Value,
            String condition3Value, String bPName) {

        if (!(browser.getPageSource().contains("Branching Profile") || browser
                .getPageSource().contains("Select Branching Profile"))) {
            if (!browser.getPageSource().contains("Help & Information")) {
                user = getUser(USER_ROLE.SITE_MANAGER);
                LOGGER.info("User is NOT logged in, logging in as "
                        + user.getUserName());
                assertAndReport(
                        browser,
                        new LoginAction(this.browser, user.getUserName(), user
                                .getPassword()).performWithStates(),
                        "Login Action");
            }
            LOGGER.info("Navigating User to the Add Branching Profile Page");
            assertAndReport(browser, new NavigateToMenu(browser, TAB_NAME,
                    SUB_MENU, btnAddBranchingProfile).performWithStates(),
                    "Navigate to Branching Profile Page");

        }
        tBPU.e2eCreateBranchingProfiles(bPName, browser, rule1, rule2, rule3,
                condition1, condition2, condition3, rule1Value, rule2Value,
                rule3Value, condition1Value, condition2Value, condition3Value,
                true);

    }

    @AfterMethod(alwaysRun = true)
    public void afterMehtod() {
        assertAndReport(browser, new NavigateToMenu(browser, TAB_NAME,
                SUB_MENU, btnAddBranchingProfile).performWithStates(),
                "Navigate to Branching Profile Page");
    }

    @BeforeClass(alwaysRun = true)
    public void beforeClass() {
        btnAddBranchingProfile.setTimeout(20000);
        browser = getBrowser();
        browser.get(getAppURL());
    }

    @AfterClass(alwaysRun = true)
    public void afterClass() {
        quitBrowser();
    }

}
