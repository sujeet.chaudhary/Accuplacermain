package com.pearson.itautomation.accuplacer.test.vouchers;

import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import com.pearson.itautomation.accuplacer.administertest.generatevoucher.GenerateRemoteVoucher;
import com.pearson.itautomation.accuplacer.core.AccuplacerBrowserUtils;
import com.pearson.itautomation.accuplacer.core.action.LoginAction;
import com.pearson.itautomation.accuplacer.core.action.NavigateToMenu;
import com.pearson.itautomation.accuplacer.test.TestBase;
import com.pearson.itautomation.testngutils.dataprovider.ExcelDataProvider;

public class TestRemoteTestVoucher extends TestBase {

    private static final String TAB_NAME = "Vouchers";
    private static final String SUB_MENU = "Generate Remote Test Voucher";

    @BeforeClass(alwaysRun = true)
    public void beforeClass() {
        browser = getBrowser();
        browser.get(getAppURL());
        User user = getUser(USER_ROLE.SITE_MANAGER);
        assertAndReport(
                browser,
                new LoginAction(this.browser, user.getUserName(), user
                        .getPassword()).performWithStates(), "Login Action");
    }

    @Test(dataProvider = "standardExcelData", dataProviderClass = ExcelDataProvider.class, groups = {
            "sanity", "Vouchers", "CATTECHTA-225" })
    public void generateRemoteVoucher(String studentID, String lastName,
            String dOB, String mOB, String yOB, String branchingProfile,
            String answerKeys, String country, String city, String siteName) {
        addTestCaseDescription("Generate a Remote Voucher ");
        assertAndReport(browser, new NavigateToMenu(this.browser, TAB_NAME,
                SUB_MENU).performWithStates(),
                "Navigate to Remote Voucher Generation Page");
        assertAndReport(browser, new GenerateRemoteVoucher(this.browser,
                studentID, lastName, dOB, mOB, yOB, branchingProfile, country,
                city, siteName).performWithStates(), "Generate Remote Voucher");
        AccuplacerBrowserUtils.WaitInMilliSeconds(10000);

    }

    @AfterClass(alwaysRun = true)
    public void teardown() {
        quitBrowser();
    }
}
