package com.pearson.itautomation.accuplacer.test.compositeScores;

import org.testng.annotations.AfterClass;
import org.testng.annotations.Test;

import com.pearson.itautomation.accuplacer.core.Buttons;
import com.pearson.itautomation.accuplacer.core.CommonUtilities;
import com.pearson.itautomation.accuplacer.core.action.ClickOnHome;
import com.pearson.itautomation.accuplacer.core.action.LoginAction;
import com.pearson.itautomation.accuplacer.core.action.NavigateToMenu;
import com.pearson.itautomation.accuplacer.core.action.SaveAndAssert;
import com.pearson.itautomation.accuplacer.core.action.SearchRecordsAndClick;
import com.pearson.itautomation.accuplacer.placementsetup.placementrules.ClickOnEditPlacementRule;
import com.pearson.itautomation.accuplacer.placementsetup.placementrules.CopyPlacementRule;
import com.pearson.itautomation.accuplacer.test.TestBase;
import com.pearson.itautomation.bca.Browser;
import com.pearson.itautomation.bca.BrowserElement;
import com.pearson.itautomation.testngutils.dataprovider.ExcelDataProvider;

public class TestEditCopyCompositeScore extends TestBase {

    Browser browser = null;

    String PLACEMENT_RULE_TITLE;

    private static final String PLACEMENT_RULE_TITLE_FOR_COPY = "000COPY2405";

    String XPATH_OF_ELEMENT = "";

    private final String strTabName = "Placement Setup";
    private final String strSubMenuName = "Composite Scores";

    public final BrowserElement btnAddPlacementRule = Buttons
            .getButtonType4("Add");

    @Test(groups = { "sanity", "regression", "CATTECHTA-155", "Placement Setup" }, dataProvider = "standardExcelData", dataProviderClass = ExcelDataProvider.class, priority = 2)
    public void testEditCompositeScore(String UserName, String Password,
            String CompositeScore) {

        browser = getBrowser();
        browser.get(getAppURL());

        assertAndReport(browser, new LoginAction(this.browser, UserName,
                Password).performWithStates(), "Login Action");

        assertAndReport(browser, new NavigateToMenu(this.browser, strTabName,
                strSubMenuName, btnAddPlacementRule).performWithStates(),
                "Navigate to Composite Score page");

        addTestCaseDescription("Search and Edit Composite Score");

        XPATH_OF_ELEMENT = "//td[span[contains(text(),'" + CompositeScore
                + "')] or contains(text(),'" + CompositeScore + "')]";

        assertAndReport(browser, new SearchRecordsAndClick(browser,
                XPATH_OF_ELEMENT).performWithStates(), "Searching Records");

        assertAndReport(browser, new ClickOnEditPlacementRule(browser,
                CompositeScore).performWithStates(), "Click on Edit button");

        assertAndReport(browser, new SaveAndAssert(browser,
                "Composite Score updated successfully").performWithStates(),
                "Save and Verify the confirmation Message");

        assertAndReport(browser, new ClickOnHome(browser).performWithStates(),
                "Click On Home");

        assertAndReport(browser, new NavigateToMenu(this.browser, strTabName,
                strSubMenuName, btnAddPlacementRule).performWithStates(),
                "Navigate to Composite Score page");

        assertAndReport(browser, new SearchRecordsAndClick(browser,
                XPATH_OF_ELEMENT).performWithStates(), "Searching Records");

        assertAndReport(
                browser,
                new CopyPlacementRule(browser, CompositeScore,
                        PLACEMENT_RULE_TITLE_FOR_COPY
                                + CommonUtilities.getCurrentTime())
                        .performWithStates(), "copy Composite");

    }

    @AfterClass(alwaysRun = true)
    public void afterClass() {
        quitBrowser();
    }
}
