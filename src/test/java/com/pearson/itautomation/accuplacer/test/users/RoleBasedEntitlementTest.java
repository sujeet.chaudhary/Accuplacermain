package com.pearson.itautomation.accuplacer.test.users;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.testng.annotations.Test;

import com.pearson.itautomation.accuplacer.core.action.RoleBasedEntitlementValidation;
import com.pearson.itautomation.accuplacer.test.TestBase;
import com.pearson.itautomation.testngutils.reporting.TNReporter;

public class RoleBasedEntitlementTest extends TestBase {
    private static final Logger LOGGER = LoggerFactory
            .getLogger(RoleBasedEntitlementTest.class);

    @Test(groups = { "CATTECHTA-25", "regression" })
    public void roleBasedEntitlementCheck() {

        addTestCaseDescription("Check for all the users of the system, that they get to see only the correct links/buttons by comparing data between DB and Entitlements I/P file.");
        int i;
        int totalRolesInAcc = 16;
        String inputFile = "/test data/Role Based Entitlement Test Data.xlsx";

        String fileName = getOutputDirectory()
                + "Role_Based_Entitlement_Results.xlsx";
        FileOutputStream out = null;

        try {
            out = new FileOutputStream(new File(fileName));
        } catch (IOException e) {
            LOGGER.error("Exception occured while creating output file", e);
        }

        if (out != null) {
            RoleBasedEntitlementValidation validation = new RoleBasedEntitlementValidation();
            validation.loadXlFile(inputFile);
            for (i = 1; i < totalRolesInAcc; i++) {
                validation.readDataFromDB(getConnection(), String.valueOf(i));
                validation.readDataFromXlFile(String.valueOf(i));
                Map<Integer, String> resultMap = validation.comparison(
                        getConnection(), String.valueOf(i));
                if (!resultMap.isEmpty()) {
                    validation.writeResultstoXL(out, i - 1);
                }
            }
            Map<String, Integer> mismatchMap = validation.getTrackingMap();

            for (String key : mismatchMap.keySet()) {
                int mismatchCounter = mismatchMap.get(key);
                TNReporter.addFailureReason("The user with role name " + key
                        + " has " + mismatchCounter + " mismatches");
            }
            try {
                if (!mismatchMap.isEmpty()) {
                    validation.getWorkbook().write(out);
                    TNReporter
                            .addNote("The comparision report can be found in the file at the location <a href='"
                                    + fileName + "'>" + fileName + "</a>");
                }
                if (!mismatchMap.isEmpty()) {
                    assertAndReport(browser, mismatchMap.isEmpty(),
                            "Role Based Entitlement Check for one or more user failed");
                } else {
                    assertAndReport(browser, mismatchMap.isEmpty(),
                            "Role Based Entitlement Check for one or more user passed");
                }
                out.flush();
                out.close();

            } catch (IOException e) {
                LOGGER.error("Exception occured while writing to output file",
                        e);
            }
        } else {
            assertAndReport(browser, false,
                    "The output file could not be created.");
        }

    }
}
