package com.pearson.itautomation.accuplacer.test.placementSetup;

import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import com.pearson.itautomation.accuplacer.core.Buttons;
import com.pearson.itautomation.accuplacer.core.CommonUtilities;
import com.pearson.itautomation.accuplacer.core.action.ClickOnHome;
import com.pearson.itautomation.accuplacer.core.action.LoginAction;
import com.pearson.itautomation.accuplacer.core.action.NavigateToMenu;
import com.pearson.itautomation.accuplacer.core.action.SaveAndAssert;
import com.pearson.itautomation.accuplacer.core.action.SearchRecordsAndClick;
import com.pearson.itautomation.accuplacer.placementsetup.majors.ActivateInactivateMajor;
import com.pearson.itautomation.accuplacer.placementsetup.majors.AddMajors;
import com.pearson.itautomation.accuplacer.placementsetup.majors.CopyMajor;
import com.pearson.itautomation.accuplacer.placementsetup.majors.DeleteMajor;
import com.pearson.itautomation.accuplacer.placementsetup.majors.EditMajor;
import com.pearson.itautomation.accuplacer.test.TestBase;
import com.pearson.itautomation.bca.BrowserElement;

public class TestOperationsMajorSM extends TestBase {

    private static final String MAJOR_NAME = "MSM"
            + CommonUtilities.getCurrentTime();
    private static final String MAJOR_NAME_FOR_COPY = "CSM";
    private static final String MAJOR_CODE = "CODE";
    private static final String XPATH_OF_ELEMENT = "//td[span[contains(text(),'"
            + MAJOR_NAME + "')] or contains(text(),'" + MAJOR_NAME + "')]";

    private static final String TAB_NAME = "Placement Setup";
    private static final String SUBMENU_NAME = "Major";

    public final BrowserElement btnAddMajor = Buttons.getButtonType4("Add");

    @Test(groups = { "regression", "sanity", "CATTECHTA-133" }, priority = 1)
    public void testAddMajorSM() {

        addTestCaseDescription("Add Major with SM user");

        assertAndReport(browser,
                new AddMajors(browser, MAJOR_NAME, MAJOR_CODE)
                        .performWithStates(), "Adding Major");

        assertAndReport(browser, new SaveAndAssert(browser,
                "Major added successfully").performWithStates(),
                "Major added successfully");
    }

    @Test(groups = { "regression", "sanity", "CATTECHTA-133" }, dependsOnMethods = "testAddMajorSM", priority = 2)
    public void testEditMajorSM() {

        addTestCaseDescription("Search and Edit Major with SM user");

        assertAndReport(browser, new SearchRecordsAndClick(browser,
                XPATH_OF_ELEMENT).performWithStates(), "Searching Records");

        assertAndReport(browser,
                new EditMajor(browser, MAJOR_NAME).performWithStates(),
                "Edit Major");
    }

    @Test(groups = { "regression", "sanity", "CATTECHTA-133" }, dependsOnMethods = "testAddMajorSM", priority = 3)
    public void testCopyMajorSM() {

        addTestCaseDescription("Search and Copy Major with SM user");

        assertAndReport(browser, new SearchRecordsAndClick(browser,
                XPATH_OF_ELEMENT).performWithStates(), "Searching Records");

        assertAndReport(
                browser,
                new CopyMajor(browser, MAJOR_NAME, MAJOR_NAME_FOR_COPY
                        + CommonUtilities.getCurrentTime()).performWithStates(),
                "copy Major");
    }

    @Test(groups = { "regression", "sanity", "CATTECHTA-133" }, dependsOnMethods = "testAddMajorSM", priority = 4)
    public void testInactivateMajorSM() {

        addTestCaseDescription("Search and Inactivate Major with SM user");

        assertAndReport(browser, new SearchRecordsAndClick(browser,
                XPATH_OF_ELEMENT).performWithStates(), "Searching Records");

        assertAndReport(browser, new ActivateInactivateMajor(browser,
                MAJOR_NAME, 1).performWithStates(), "Inactivate Major");
    }

    @Test(groups = { "regression", "sanity", "CATTECHTA-133" }, dependsOnMethods = "testAddMajorSM", priority = 5)
    public void testActivateMajorSM() {

        addTestCaseDescription("Search and Activate Major with SM user");

        assertAndReport(browser, new SearchRecordsAndClick(browser,
                XPATH_OF_ELEMENT).performWithStates(), "Searching Records");

        assertAndReport(browser, new ActivateInactivateMajor(browser,
                MAJOR_NAME, 2).performWithStates(), "Activate Major");
    }

    @Test(groups = { "regression", "sanity", "CATTECHTA-133" }, dependsOnMethods = "testAddMajorSM", priority = 6)
    public void testDeleteMajorSM() {

        addTestCaseDescription("Search and Delete Major with SM user");

        assertAndReport(browser, new SearchRecordsAndClick(browser,
                XPATH_OF_ELEMENT).performWithStates(), "Searching Records");

        assertAndReport(browser,
                new DeleteMajor(browser, MAJOR_NAME).performWithStates(),
                "Delete Major");
    }

    @Test(dataProvider = "ReserveCode", groups = { "regression", "sanity",
            "CATTECHTA-133" }, priority = 7)
    public void testValidateReserveMajorCodeSM(String majorCode) {

        addTestCaseDescription("Validate Major Code values -1,-2,-3 with SM user");

        assertAndReport(browser, new AddMajors(browser, MAJOR_NAME + "R",
                majorCode).performWithStates(), "Adding Major");

        assertAndReport(
                browser,
                new SaveAndAssert(
                        browser,
                        "Major codes \"-1\", \"-2\" and \"-3\" are reserved by the system and cannot be used.")
                        .performWithStates(),
                "Major codes negative scenerio checked");
    }

    @AfterMethod(alwaysRun = true)
    public void afterMethod() {
        assertAndReport(browser, new ClickOnHome(browser).performWithStates(),
                "Click On Home");
    }

    @BeforeMethod(alwaysRun = true)
    public void beforeMethod() {
        assertAndReport(browser, new NavigateToMenu(this.browser, TAB_NAME,
                SUBMENU_NAME, btnAddMajor).performWithStates(),
                "Navigate to Major page");

    }

    @BeforeClass(alwaysRun = true)
    public void beforeClass() {
        browser = getBrowser();
        browser.get(getAppURL());
        User user = getUser(USER_ROLE.SITE_MANAGER);
        assertAndReport(
                browser,
                new LoginAction(this.browser, user.getUserName(), user
                        .getPassword()).performWithStates(), "Login Action");
    }

    @AfterClass(alwaysRun = true)
    public void afterClass() {
        quitBrowser();
    }

    @DataProvider(name = "ReserveCode")
    public Object[][] getData() {
        return new Object[][] { { "-1" }, { "-2" }, { "-3" } };

    }
}
