package com.pearson.itautomation.accuplacer.test.reports;

import java.io.File;
import java.util.HashMap;
import java.util.List;
import java.util.Properties;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import com.pearson.itautomation.accuplacer.core.CommonUtilities;
import com.pearson.itautomation.accuplacer.core.DBUtils;
import com.pearson.itautomation.accuplacer.reports.core.ColumnSequence;
import com.pearson.itautomation.accuplacer.reports.core.DiagnosticReportValidation;
import com.pearson.itautomation.accuplacer.reports.core.PlacementReportValidation;
import com.pearson.itautomation.accuplacer.reports.core.ReadExcelReport;
import com.pearson.itautomation.accuplacer.reports.core.ReportConstants;
import com.pearson.itautomation.accuplacer.reports.core.ReportValidateFacory;
import com.pearson.itautomation.accuplacer.reports.core.Student;
import com.pearson.itautomation.accuplacer.reports.core.TestScores;
import com.pearson.itautomation.accuplacer.reports.isr.SubmitReport;
import com.pearson.itautomation.accuplacer.test.TestBase;
import com.pearson.itautomation.testngutils.reporting.TNReporter;

public class TestReportHeader extends TestBase {
    private static final Logger LOGGER = LoggerFactory
            .getLogger(TestReportHeader.class);
    private static final String PATH_OF_COLUMN_INFORMATION = CommonUtilities
            .appPath()
            + "\\src\\test\\resources\\test data\\Reports\\Columns.xls";
    protected Properties sqlQueries = CommonUtilities
            .readPropertyFile("//config//SQL.properties");
    HashMap<String, TestScores> testScores = new HashMap<String, TestScores>();
    HashMap<String, Student> studentInformation = new HashMap<String, Student>();
    protected String assertText = "";
    private String exnums[];
    DBUtils dbu;

    @DataProvider(name = "exports")
    public Object[][] exportFiles() {
        List<SubmitReport> reportsToVerify = TestBase.getTemporaryList();
        Object[][] exports = new Object[reportsToVerify.size()][1];
        for (int i = 0; i < reportsToVerify.size(); i++) {
            exports[i][0] = reportsToVerify.get(i);
        }
        return exports;
    }

    @BeforeClass(alwaysRun = true)
    public void setExunums() {
        DBUtils dbu = new DBUtils(getConnection());
        exnums = dbu.runAndReturnNthColumn(
                sqlQueries.getProperty("last30DaysTestsAutomationSite"), 0);
    }

    @Test(dataProvider = "exports")
    public void verifyReport(SubmitReport submitReport) {

        ReadExcelReport readExcelFields = new ReadExcelReport(
                PATH_OF_COLUMN_INFORMATION, submitReport.getAutomationQuery());
        String[] headerArrayInput = readExcelFields.getNthColumn(0);
        ColumnSequence cs = new ColumnSequence(headerArrayInput,
                getConnection());

        File fileName = submitReport.getDownloadFile();
        ReadExcelReport reportToBeVerified = new ReadExcelReport(
                fileName.getPath());

        String columnSequence = cs.getColumnSequence();
        String columnTypeSequence = cs.getColumnTypeSequence();
        String[] headerExcel = reportToBeVerified.getHeader();
        String actualHeaderSequence = "";
        for (int i = 0; i < headerExcel.length; i++) {
            if ("".equalsIgnoreCase(actualHeaderSequence)) {
                actualHeaderSequence = headerExcel[i];
            } else {
                actualHeaderSequence = actualHeaderSequence + ";"
                        + headerExcel[i];
            }
        }

        if (actualHeaderSequence.equalsIgnoreCase(columnSequence)) {
            assertAndReport(browser, true,
                    "Header Matches with the expected format");
            assertText = assertText + "Header Validation for '"
                    + submitReport.getDownloadFile() + "' Report, expected = '"
                    + columnSequence + "' Actual = '" + actualHeaderSequence
                    + "'" + "\n";
        } else {
            assertAndReport(
                    browser,
                    false,
                    "Header does NOT Matches with the expected format"
                            + String.format(" Expected %s , Actual - %s ",
                                    columnSequence, actualHeaderSequence));
            assertText = assertText + "Header Validation for '"
                    + submitReport.getDownloadFile() + "' Report, expected = '"
                    + columnSequence + "' Actual = '" + actualHeaderSequence
                    + "'" + "\n";
        }
        LOGGER.debug(cs.getColumnSequence());
        LOGGER.debug(cs.getColumnTypeSequence());

        for (int i = 0; i < exnums.length; i++) {
            if (reportToBeVerified.getExnumPosition(exnums[i]) == -1) {
                LOGGER.info("Exnum " + exnums[i]
                        + " is not present in the report");
                continue;
            } else
                generateTestObjects(exnums[i]);
            for (int j = 1; j < columnTypeSequence.split(";").length; j++) {
                ReportValidateFacory reportEval = null;
                String currentExnum = exnums[i];
                String typeOfCurrentColumn = columnTypeSequence.split(";")[j];
                String valueOfCurrentColumn = columnSequence.split(";")[j];
                switch (typeOfCurrentColumn) {
                case ReportConstants.DIAGNOSTIC_TEST: {
                    reportEval = new DiagnosticReportValidation(currentExnum,
                            typeOfCurrentColumn, valueOfCurrentColumn,
                            reportToBeVerified, j, testScores);
                    break;
                }
                case ReportConstants.PLACEMENT_TEST: {
                    reportEval = new PlacementReportValidation(currentExnum,
                            typeOfCurrentColumn, valueOfCurrentColumn,
                            reportToBeVerified, j, testScores);
                    break;
                }

                }
                if (reportEval != null) {
                    if (!reportEval.evaluateReportFields()) {
                        assertAndReport(reportEval.evaluateReportFields(),
                                reportEval.failedValidationMessage());
                    }
                    assertAndReport(true, reportEval.returnValidationMessage());
                    assertText = assertText
                            + reportEval.returnValidationMessage() + "\n";
                }

            }

        }
    }

    /**
     * This method will generate Student and Testscores Object based on exnum.
     * These objects will be treated as test basis in order to verify the export
     * output
     * 
     * @param exnum
     */
    public void generateTestObjects(String exnum) {
        if (!(studentInformation.containsKey(exnum) && testScores
                .containsKey(exnum))) {
            Student s = new Student(getConnection(), exnum);
            TestScores ts = new TestScores(getConnection(), exnum);
            testScores.put(ts.getExnum(), ts);
            studentInformation.put(s.getExnum(), s);
        }
    }

    @AfterClass(alwaysRun = true)
    public void displayAssertions() {
        TNReporter.addNote("Assert Text " + assertText);
        CommonUtilities.writeToTextFile(assertText, AUTOMATION_FILE_DOWNLOAD
                + "\\reportslogs.txt");
    }
}
