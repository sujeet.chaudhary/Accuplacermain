package com.pearson.itautomation.accuplacer.test.placementSetup;

import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import com.pearson.itautomation.accuplacer.core.Buttons;
import com.pearson.itautomation.accuplacer.core.CommonUtilities;
import com.pearson.itautomation.accuplacer.core.action.ClickOnHome;
import com.pearson.itautomation.accuplacer.core.action.LoginAction;
import com.pearson.itautomation.accuplacer.core.action.NavigateToMenu;
import com.pearson.itautomation.accuplacer.core.action.SaveAndAssert;
import com.pearson.itautomation.accuplacer.core.action.SearchRecordsAndClick;
import com.pearson.itautomation.accuplacer.placementsetup.majors.ActivateInactivateMajor;
import com.pearson.itautomation.accuplacer.placementsetup.majors.AddMajors;
import com.pearson.itautomation.accuplacer.placementsetup.majors.CopyMajor;
import com.pearson.itautomation.accuplacer.placementsetup.majors.DeleteMajor;
import com.pearson.itautomation.accuplacer.placementsetup.majors.EditMajor;
import com.pearson.itautomation.accuplacer.test.TestBase;
import com.pearson.itautomation.bca.BrowserElement;

public class TestOperationsMajorGRPU extends TestBase {

    private static final String MAJOR_NAME = "MGRPU"
            + CommonUtilities.getCurrentTime();
    private static final String MAJOR_NAME_FOR_COPY = "CGRPU";
    private static final String MAJOR_CODE = "CODE";
    private static final String XPATH_OF_ELEMENT = "//td[span[contains(text(),'"
            + MAJOR_NAME + "')] or contains(text(),'" + MAJOR_NAME + "')]";

    private static final String TAB_NAME = "Placement Setup";
    private static final String SUBMENU_NAME = "Major";

    public final BrowserElement btnAddMajor = Buttons.getButtonType4("Add");

    @Test(groups = { "regression", "sanity", "CATTECHTA-133" }, priority = 1)
    public void testAddMajorGRPU() {

        addTestCaseDescription("Add Major with GRPU user");

        assertAndReport(browser,
                new AddMajors(browser, MAJOR_NAME, MAJOR_CODE)
                        .performWithStates(), "Adding Major");

        assertAndReport(browser, new SaveAndAssert(browser,
                "Major added successfully").performWithStates(),
                "Major added successfully");
    }

    @Test(groups = { "regression", "sanity", "CATTECHTA-133" }, dependsOnMethods = "testAddMajorGRPU", priority = 2)
    public void testEditMajorGRPU() {

        addTestCaseDescription("Search and Edit Major with GRPU user");

        assertAndReport(browser, new SearchRecordsAndClick(browser,
                XPATH_OF_ELEMENT).performWithStates(), "Searching Records");

        assertAndReport(browser,
                new EditMajor(browser, MAJOR_NAME).performWithStates(),
                "Edit Major");
    }

    @Test(groups = { "regression", "sanity", "CATTECHTA-133" }, dependsOnMethods = "testAddMajorGRPU", priority = 3)
    public void testCopyMajorGRPU() {

        addTestCaseDescription("Search and Copy Major with GRPU user");

        assertAndReport(browser, new SearchRecordsAndClick(browser,
                XPATH_OF_ELEMENT).performWithStates(), "Searching Records");

        assertAndReport(
                browser,
                new CopyMajor(browser, MAJOR_NAME, MAJOR_NAME_FOR_COPY
                        + CommonUtilities.getCurrentTime()).performWithStates(),
                "copy Major");
    }

    @Test(groups = { "regression", "sanity", "CATTECHTA-133" }, dependsOnMethods = "testAddMajorGRPU", priority = 4)
    public void testInactivateMajorGRPU() {

        addTestCaseDescription("Search and Inactivate Major with GRPU user");

        assertAndReport(browser, new SearchRecordsAndClick(browser,
                XPATH_OF_ELEMENT).performWithStates(), "Searching Records");

        assertAndReport(browser, new ActivateInactivateMajor(browser,
                MAJOR_NAME, 1).performWithStates(), "Inactivate Major");
    }

    @Test(groups = { "regression", "sanity", "CATTECHTA-133" }, dependsOnMethods = "testAddMajorGRPU", priority = 5)
    public void testActivateMajorGRPU() {

        addTestCaseDescription("Search and Activate Major with GRPU user");

        assertAndReport(browser, new SearchRecordsAndClick(browser,
                XPATH_OF_ELEMENT).performWithStates(), "Searching Records");

        assertAndReport(browser, new ActivateInactivateMajor(browser,
                MAJOR_NAME, 2).performWithStates(), "Activate Major");
    }

    @Test(groups = { "regression", "sanity", "CATTECHTA-133" }, dependsOnMethods = "testAddMajorGRPU", priority = 6)
    public void testDeleteMajorGRPU() {

        addTestCaseDescription("Search and Delete Major with GRPU user");

        assertAndReport(browser, new SearchRecordsAndClick(browser,
                XPATH_OF_ELEMENT).performWithStates(), "Searching Records");

        assertAndReport(browser,
                new DeleteMajor(browser, MAJOR_NAME).performWithStates(),
                "Delete Major");
    }

    @Test(dataProvider = "ReserveCode", groups = { "regression", "sanity",
            "CATTECHTA-133" }, priority = 7)
    public void testValidateReserveMajorCodeGRPU(String majorCode) {

        addTestCaseDescription("Validate Major Code values -1,-2,-3 with GRPU user");

        assertAndReport(browser, new AddMajors(browser, MAJOR_NAME + "R",
                majorCode).performWithStates(), "Adding Major");

        assertAndReport(
                browser,
                new SaveAndAssert(
                        browser,
                        "Major codes \"-1\", \"-2\" and \"-3\" are reserved by the system and cannot be used.")
                        .performWithStates(),
                "Major codes negative scenerio checked");
    }

    @AfterMethod(alwaysRun = true)
    public void afterMethod() {
        assertAndReport(browser, new ClickOnHome(browser).performWithStates(),
                "Click On Home");
    }

    @BeforeMethod(alwaysRun = true)
    public void beforeMethod() {
        assertAndReport(browser, new NavigateToMenu(this.browser, TAB_NAME,
                SUBMENU_NAME, btnAddMajor).performWithStates(),
                "Navigate to Major page");

    }

    @BeforeClass(alwaysRun = true)
    public void beforeClass() {
        browser = getBrowser();
        browser.get(getAppURL());
        User user = getUser(USER_ROLE.GROUP_USER);
        assertAndReport(
                browser,
                new LoginAction(this.browser, user.getUserName(), user
                        .getPassword()).performWithStates(), "Login Action");
    }

    @AfterClass(alwaysRun = true)
    public void afterClass() {
        quitBrowser();
    }

    @DataProvider(name = "ReserveCode")
    public Object[][] getData() {
        return new Object[][] { { "-1" }, { "-2" }, { "-3" } };

    }
}
