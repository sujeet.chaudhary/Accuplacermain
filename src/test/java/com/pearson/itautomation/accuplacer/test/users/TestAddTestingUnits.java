package com.pearson.itautomation.accuplacer.test.users;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import com.pearson.itautomation.accuplacer.core.Buttons;
import com.pearson.itautomation.accuplacer.core.action.GenericSearchAction;
import com.pearson.itautomation.accuplacer.core.action.LoginAction;
import com.pearson.itautomation.accuplacer.core.action.NavigateToMenu;
import com.pearson.itautomation.accuplacer.test.TestBase;
import com.pearson.itautomation.accuplacer.test.administertest.TestAdminsterCommon;
import com.pearson.itautomation.accuplacer.users.testingunits.AddTestingUnits;
import com.pearson.itautomation.bca.BrowserElement;
import com.pearson.itautomation.bca.validation.ElementExistsValidation;
import com.pearson.itautomation.testngutils.dataprovider.ExcelDataProvider;
import com.pearson.itautomation.testngutils.reporting.TNReporter;

public class TestAddTestingUnits extends TestBase {

    private static final Logger LOGGER = LoggerFactory
            .getLogger(TestAdminsterCommon.class);

    public final String strTabName = "Users";
    public final String strSubMenuName = "Add Test Units";
    public final BrowserElement btnClose = Buttons.getButtonType1("Close");

    public String strOrgID;
    public String strTransferUnitsTo;
    public String strUnitsToAdd;

    public final BrowserElement btnSearch = Buttons.getButtonType1("Search");
    private static final String OR_AREA = "OR_User";
    private static final String OR_TRANSFERTESTUNITSCONFMSG = "AddTestUnitsConfirmMsg";

    @Test(dataProvider = "standardExcelData", dataProviderClass = ExcelDataProvider.class)
    public void AddTestingUnits(final String failMessage,
            final String strOrgID, final String strTypeOfUnits,
            final String strNoOfUnits, final String strBasePrice,
            final String strPO, final String strIOCOrder) {

        addTestCaseDescription("Add test units to an organization (site or institute).");
        TNReporter
                .setParmDesc(
                        "Describes the type of organization for which units are being added to",
                        "The organization to add units for",
                        "The index in the type of units to add drop down",
                        "The no of units to be added",
                        "The Base Price of the units being added",
                        "The PO for the units being added",
                        "The IOC Order for the units being added");
        boolean result;
        if (strOrgID.contains("-")) {
            result = new GenericSearchAction(getBrowser(), strOrgID, "Site ID")
                    .performWithStates();
        } else {
            result = new GenericSearchAction(getBrowser(), strOrgID,
                    "Institution ID").performWithStates();
        }
        if (result) {
            assertAndReport(browser, new AddTestingUnits(browser, strOrgID,
                    strTypeOfUnits, strNoOfUnits, strBasePrice, strPO,
                    strIOCOrder, getConnection()).performWithStates(),
                    "Verify if test units before and after adding units are equal.");
        } else {
            LOGGER.error("Search failed and hence could not verify before and after adding units");
        }

        assertAndReport(
                browser,
                new ElementExistsValidation(this.browser, browser
                        .getObjectRepository().getBrowserElement(OR_AREA,
                                OR_TRANSFERTESTUNITSCONFMSG))
                        .performWithStates(),
                "Verify confirmation message after units are added.");

    }

    @BeforeClass
    public void beforeClass() {
        browser = getBrowser();
        browser.get(getAppURL());

        User user = getUser(USER_ROLE.COLLEGE_BOARD_ADMIN_APPROVE);

        assertAndReport(
                browser,
                new LoginAction(this.browser, user.getUserName(), user
                        .getPassword()).performWithStates(), "Login Action");

        assertAndReport(browser, new NavigateToMenu(this.browser, strTabName,
                strSubMenuName, btnSearch).performWithStates(),
                "Navigation to Transfer Test Units Page");
    }

    @AfterClass
    public void afterClass() {
        quitBrowser();
    }
}
