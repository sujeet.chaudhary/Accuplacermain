package com.pearson.itautomation.accuplacer.test.users;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import com.pearson.itautomation.accuplacer.core.CommonUtilities;
import com.pearson.itautomation.accuplacer.core.action.LoginAction;
import com.pearson.itautomation.accuplacer.core.action.SaveAndAssert;
import com.pearson.itautomation.accuplacer.core.action.SearchRecordsAndClick;
import com.pearson.itautomation.accuplacer.test.TestBase;
import com.pearson.itautomation.accuplacer.users.userprofiles.AddUserProfile;
import com.pearson.itautomation.accuplacer.users.userprofiles.ApproveOrRejectUser;
import com.pearson.itautomation.accuplacer.users.userprofiles.DeleteUser;
import com.pearson.itautomation.accuplacer.users.userprofiles.EditUser;
import com.pearson.itautomation.accuplacer.users.userprofiles.NavigateToAddUserPage;
import com.pearson.itautomation.accuplacer.users.userprofiles.ReFillUserProfile;
import com.pearson.itautomation.accuplacer.users.userprofiles.SearchApprovedUser;
import com.pearson.itautomation.accuplacer.users.userprofiles.SearchUserForApproveReject;
import com.pearson.itautomation.accuplacer.users.userprofiles.SelectGroupName;
import com.pearson.itautomation.accuplacer.users.userprofiles.SelectUserType;
import com.pearson.itautomation.bca.BrowserElement;
import com.pearson.itautomation.bca.validation.ElementExistsValidation;
import com.pearson.itautomation.testngutils.dataprovider.ExcelDataProvider;
import com.pearson.itautomation.testngutils.reporting.TNReporter;

public class TestUserProfile extends TestBase {

    public String strUser;
    public String strGroupName;
    private static String USER_NAME = "";
    private String createUserName;
    private static String Xpath = "";
    private static final int INDEX_OF_FIELD = 0;
    private BrowserElement eleValidateError;

    @Test(groups = { "regression", "CATTECHTA-18", "Users" }, dataProvider = "standardExcelData", dataProviderClass = ExcelDataProvider.class)
    public void collegeBoardUserProfile(final String strUser,
            final String strGroupName) {

        final Logger LOGGER = LoggerFactory.getLogger(TestUserProfile.class);

        addTestCaseDescription("Add, Approve, Edit, and Delete User");

        TNReporter.setParmDesc("Type of User to be created", "Group Name");

        this.eleValidateError = browser.getBrowserElement("OR_User",
                "validationErrorUserFrom");

        USER_NAME = CommonUtilities.getCurrentDateTime();

        Xpath = "//td[span[contains(text(),'"
                + USER_NAME
                + "')]]/following-sibling::td[5]/descendant::a[contains(text(),'Approve')]";

        this.strUser = strUser;
        this.strGroupName = strGroupName;

        assertAndReport(browser,
                new NavigateToAddUserPage(browser).performWithStates(),
                "Navigate to Add User Page ");

        if (strUser.equals("Group User")) {
            assertAndReport(browser,
                    new SelectGroupName(browser, strGroupName)
                            .performWithStates(), "Select Group Name:"
                            + strUser);

        }

        assertAndReport(browser,
                new AddUserProfile(browser, USER_NAME).performWithStates(),
                "AddUserProfile for User Type:" + strUser);

        assertAndReport(browser,
                new SelectUserType(browser, strUser).performWithStates(),
                "Select Use Typer:" + strUser);

        if (new ElementExistsValidation(getBrowser(), this.eleValidateError)
                .performWithStates()) {
            LOGGER.error("Validation Error Found for the \""
                    + (getBrowser().getElementWithWait(eleValidateError)
                            .getText()).split("is invalid")[INDEX_OF_FIELD]
                    + "\" Field");
            assertAndReport(browser, false, "Validation Error Check");
        }

        assertAndReport(browser,
                new ReFillUserProfile(browser, createUserName)
                        .performWithStates(), "Check  \"Save\" Button Status");

        if (strUser.equals("Group User")) {
            assertAndReport(browser,
                    new SaveAndAssert(browser, "The user").performWithStates(),
                    "Save and Verify Msg");
        } else {
            assertAndReport(
                    browser,
                    new SaveAndAssert(browser,
                            "Request for new user has been submitted for approval")
                            .performWithStates(), "Save and Verify Msg");

        }
        if (!strUser.equals("Group User")) {
            assertAndReport(
                    browser,
                    new SearchUserForApproveReject(browser).performWithStates(),
                    "Search User For Approve or Reject for User type:"
                            + strUser);

            assertAndReport(browser,
                    new SearchRecordsAndClick(browser, Xpath)
                            .performWithStates(), "Search approved User:"
                            + strUser);

            assertAndReport(
                    browser,
                    new ApproveOrRejectUser(browser, Xpath).performWithStates(),
                    "Approve User for User Type:" + strUser);
        }
        assertAndReport(browser,
                new SearchApprovedUser(browser, USER_NAME).performWithStates(),
                "Search Approved User for User Type:" + strUser);

        assertAndReport(browser, new EditUser(browser).performWithStates(),
                "Edit  User Type:" + strUser);

        assertAndReport(browser, new SaveAndAssert(browser,
                "User Information updated successfully").performWithStates(),
                "Save and Verify Msg");

        assertAndReport(browser,
                new SearchApprovedUser(browser, USER_NAME).performWithStates(),
                "Search Approved User for User Type:" + strUser);

        assertAndReport(browser, new DeleteUser(browser).performWithStates(),
                "Delete  User Type:" + strUser);
    }

    @BeforeClass(alwaysRun = true)
    public void beforeClass() {
        browser = getBrowser();
        browser.get(getAppURL());

        User user = getUser(USER_ROLE.COLLEGE_BOARD_ADMIN_APPROVE);

        assertAndReport(
                browser,
                new LoginAction(this.browser, user.getUserName(), user
                        .getPassword()).performWithStates(), "Login Action");
    }

    @AfterClass(alwaysRun = true)
    public void afterClass() {
        quitBrowser();
    }
}
